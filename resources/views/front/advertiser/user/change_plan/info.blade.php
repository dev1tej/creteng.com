@extends('front.layouts.master')
@section('content')
<style type="text/css">
.fixed-top {
  position: relative;
}
#footer .footer-top { 
 padding: 50px 0 0 0;
 top: 0; 
}
#header.header-scrolled{
  position: fixed;
  top: 0;
  right: 0;
  left: 0;
  z-index: 1030;
}
</style>
<section class="inner-page">
<div class="container">
  <div class='alert-info alert text-center'>
    <h2><strong>Info</strong></h2>
    <p >Changing Plan would result in deactivation of current executive Plan</p><br/> 
    <p>The process is irreversible.</p><br/> 
    <p><a href="{{ route('changePlanPlan', Auth::user()->id) }}"> Confirm & Proceed </a></p>
  </div>
</div>
</section>
@endsection