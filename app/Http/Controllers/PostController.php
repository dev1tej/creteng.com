<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Post;
use App\PostLike;
use App\PostComment;
use App\SavePost;
use App\PostsTagged;
use App\PostCommentReply;
use App\PostVideoViewed;
use App\ReportPost;

class PostController extends Controller
{
    /**
     * Listing all posts 
     * 
     * @return \Illuminate\View\View
     */
    public function showAllPosts()
    {
        $data = Post::where([
            ['include_challenges', false],
        ])->orderBy('id','DESC')->get();
        foreach ($data as $post) {
            $is_reported = ReportPost::where('post_id', $post['id'])->first();
            if (is_null($is_reported)) {
                $post->reported = false;
            } else {
                $post->reported = true;
            }
        }
        return view('admin.posts.index',compact('data'));
    }

    /**
     * Remove the specified post from storage.
     *
     * @param  int  $id
     * @return view
     */

    public function deletePost($id)
    {
        Post::find($id)->delete();
        
        return redirect()->route('showAllPosts')->with('danger','Post deleted successfully');

    }

    /**
     * View the specified post Details.
     *
     * @param  int  $id
     * @return \Illuminate\View\View
     */

    public function viewPost($id)
    {
        $post = Post::find($id);

        $post_detail = array();
        $post_detail['id'] = $post['id'];
        $post_detail['user_id'] = $post['user_id'];
        $post_detail['post_type'] = $post['post_type'];
        $post_detail['post_text'] = $post['post_text'];
        $post_detail['post_binary'] = $post['post_binary'];
        $post_detail['post_binary_mime_type'] = $post['post_binary_mime_type'];
        $post_detail['created_at'] = $post['created_at'];
        $post_detail['updated_at'] = $post['updated_at'];

        //getting post owner
        $post_detail['posted_by'] = User::where([
            ['id', $post_detail['user_id']],
        ])->first();
        if ($post_detail['posted_by']['profile_pic'] == null) {
            $post_detail['posted_by']['profile_pic'] = asset('image/c.svg') ;
        }
        
        //getting tagged users
        $tagged_user_lists = PostsTagged::where([
            ['post_id', $post_detail['id']],
            ['tagged_by', $post_detail['user_id']],
        ])->get();
        
        $tagged_users = array();
        $tagged_users_count = 0;
        if ($tagged_user_lists) {
            foreach ($tagged_user_lists as $tagged_user) {
                $tagged_users[] = User::where([
                    ['id', trim($tagged_user->tagged_user)],
                ])->first();  
                $tagged_users_count++;
            }
            foreach ($tagged_users as $tagged_user) {
                if ($tagged_user['profile_pic'] == null) {
                    $tagged_user['profile_pic'] = asset('image/c.svg') ;
                }
            }
        }
        $post_detail['tagged_users']['tagged_users_count'] = $tagged_users_count;
        $post_detail['tagged_users']['users'] = $tagged_users;

        //getting likes details
        $likes = PostLike::where([
            ['post_id', $post_detail['id']],
        ])->get();
        $total_likes = 0;
        $liked_users = array();
        foreach ($likes as $like) {
            $liked_by_id = $like['user_id'];
            //getting user list who liked
            $liked_users[] = User::where([
                ['id', $liked_by_id],
            ])->first();
            $total_likes++;
        }

        $post_detail['post_likes']['total_likes'] = $total_likes;
        $post_detail['post_likes']['liked_by'] = $liked_users;

        //getting comments details
        $comments = PostComment::where([
            ['post_id', $post_detail['id']],
        ])->get();
        $comments_count = 0;
        $post_comments = array();
        $index = 0;
        foreach ($comments as $comment) {
            $post_comments[$index]['comment'] = $comment;
            $post_comments[$index]['comment_by'] = User::find($comment->user_id);
            $comment_replies = PostCommentReply::where([
                ['comment_id', $comment['id']],
            ])->get();
            $sub_index = 0;
            foreach ($comment_replies as $comment_reply) {
                $post_comments[$index]['comment_reply'][$sub_index]['comment_reply'] = $comment_reply;
                $post_comments[$index]['comment_reply'][$sub_index]['comment_reply_by'] = User::where([
                    ['id', $comment_reply['user_id']],
                ])->first(); 
                $sub_index++;
            }
            $comments_count++;
            $index++;
        }
        $post_detail['post_comments']['total_comments'] = $comments_count;
        $post_detail['post_comments']['comment_detail'] = $post_comments;

        //getting saved details
        $post_saved = SavePost::where([
            ['post_id', $post_detail['id']],
        ])->get();
        $total_saved = 0;
        $saved_users = array();
        foreach ($post_saved as $saved) {
            $total_saved++;
            $saved_by_id = $saved['user_id'];
            //getting user list who saved
            $saved_users[] = User::where([
                ['id', $saved_by_id],
            ])->first();
        }
        $post_detail['post_saved']['total_saved'] = $total_saved;
        $post_detail['post_saved']['saved_by'] = $saved_users;

        //getting video views
        if ($post_detail['post_binary_mime_type'] == 'video') {
            $post_video_view_count = 0;
            $post_video_views = array();
            $video_views = PostVideoViewed::where([
                ['post_id', $post_detail['id']],
            ])->get();
            foreach ($video_views as $video_view) {
                $post_video_views[] = User::where([
                    ['id', $video_view['user_id']],
                ])->first();
                $post_video_view_count++;
            }
            $post_detail['post_video_views']['total_video_views'] = $post_video_view_count;
            $post_detail['post_video_views']['viewed_by'] = $post_video_views;
        }

        $post_detail['reports'] = ReportPost::where('post_id', $id)->with('user')->get();
        
        return view('admin.posts.view', compact('post_detail'));
    }
    
    /**
     * Display the specified Customer.
     *
     * @param  int  $post_id, $user_id 
     * @return \Illuminate\View\View
     */
    public function showCustomer($post_id, $user_id)
    {
        $post = Post::find($post_id);
        $user = User::find($user_id);
        return view('admin.posts.show',compact('user', 'post'));
    }

    /**
     * Display the manage post report page.
     *
     * @return \Illuminate\View\View
     */
    public function managePostReport()
    {
        $data = Post::where([
            ['include_challenges', false],
        ])->orderBy('id','DESC')->get();
        return view('admin.reports.posts.index',compact('data'));
    }

    /**
     * To generate csv report for posts
     * 
     * @param \Illuminate\Http\Request  $request
     * @return csv file
     */
    public function postGenerateCsvReport(Request $request)
    {
        $from_date = $request->post('from_date');
        $to_date = $request->post('to_date');
        $posts = Post::where([
            ['created_at', '>',  $from_date],
            ['created_at', '<=',  $to_date],
        ])->with('postUser')->get();
        
        $posts_array = array();
        $i = 1;
        foreach ($posts as $post) {
            $post_array = array();
            $post_array['serial_no'] = $i;
            $post_array['user_id'] = $post->postUser->name;
            $post_array['post_type'] = $post->post_type;
            $post_array['post_text'] = $post->post_text;
            $post_array['post_binary'] = $post->post_binary;
            $post_array['post_binary_mime_type'] = $post->post_binary_mime_type;
            $post_array['created_at'] = date("m-d-Y", strtotime($post->created_at));

            $post_array['total_tags'] = PostsTagged::where([
                ['post_id', $post->id],
            ])->get()->count();
            $post_array['total_likes'] = PostLike::where([
                ['post_id', $post->id],
            ])->get()->count();
            $post_array['total_saves'] = SavePost::where([
                ['post_id', $post->id],
            ])->get()->count();

            if ($post->post_binary_mime_type == 'video') {
                $post_array['total_video_views'] = PostVideoViewed::where([
                    ['post_id', $post->id],
                ])->get()->count();
            } else {
                $post_array['total_video_views'] = 'NA';
            }
            $post_array['total_comments'] = PostComment::where([
                ['post_id', $post->id],
            ])->get()->count();

            $posts_array[] = $post_array;  
            $i++; 
        }
        $headers = [
            'Sr. No',
            'Post Owner',
            'Post Type',
            'Post',
            'File',
            'File Type',
            'Created At',
            'Total Tags',
            'Total Likes',
            'Total Saves',
            'Total Video Views',
            'Total Comments',
        ];
        header('Content-type: text/csv');
        header('Content-Disposition: attachment; filename="posts.csv"');
        header('Pragma: no-cache');
        header('Expires: 0');

        $file = fopen('php://output', 'w');
        fputcsv($file, $headers);
        foreach ($posts_array as $post_array) {
            fputcsv($file, $post_array);
        }
        fclose($file);
        exit();
    }

    public function deletePostComment(Request $request)
    {
        $comment_id = $request->comment_id;
        $comment = PostComment::find($comment_id);
        $comment->delete();
        return 'ok';
    }

    public function deletePostCommentReply(Request $request)
    {
        $comment_reply_id = $request->comment_reply_id;
        $comment_reply = PostCommentReply::find($comment_reply_id);
        $comment_reply->delete();
        return 'ok';
    }
}
