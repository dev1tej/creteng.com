<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\UsersFriend;
use App\PostsTagged;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\CustomPaginationController;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit($id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy($id)
    {
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id)
    {

    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function editUser($id)
    {
        $user = User::find($id);
        
        $roles = Role::pluck('name','name');
        $userRole = $user->roles->pluck('name','name')->all();

        return view('admin.users.edit',compact('user','roles','userRole'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function updateUser(Request $request, $id)
    {
		/* Check Validation */
        $this->validate($request, [
            'name' => 'required|regex:/^[\pL\s\-]+$/u',
            'email' => 'required|regex:/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix|unique:users,email,'.$id,
            'password' => 'required|same:confirm-password',
            
        ]);
		
		/* if user dont want to change password or skip password to update */
        $input = $request->all();
        if (!empty($input['password'])) { 
            $input['password'] = Hash::make($input['password']);
        } else {
            $input = Arr::except($input,array('password'));    
        }

		/*Update User */
        $user = User::find($id);
        $user->update($input);

        return redirect(route('dashboard'))->with('flash_message','User updated successfully.');
    }

    /**
     * To display edit admin password page.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */

    public function adminPasswordEdit($id)
    {
        return view('admin.password.edit',compact('id'));
    }
    
    /**
     * To update admin password.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function adminPasswordUpdate(Request $request, $id)
    {
        $this->validate($request, [
            'password' => 'same:confirm-password',
        ]);
        
        $input = $request->all();
        if(!empty($input['password'])){ 
            $input['password'] = Hash::make($input['password']);
        }
		/*Update User password */
        $user = User::find($id);
        $user->update($input);
        
        return back()->with('success', 'Password changed successfully.');
    }

    /**
     * To send email verification link to unverified users
     */

    public function sendVerificationEmail($id) 
    {
        $user = User::find($id);
        $user->sendEmailVerificationNotification();
        return back()->with('success', 'Email sent successfully.');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function showAllManagers()
    {
        $manager=env("APP_MANAGER");
        $data = User::role($manager)->orderBy('id','DESC')->get();
        return view('admin.managers.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function createManager()
    {
        $roles = Role::pluck('name','name')->only('Customers', 'Managers')->all();

        return view('admin.managers.create',compact('roles'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function showManager($id)
    {
        $user = User::find($id);
        return view('admin.managers.show',compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function editManager($id)
    {
        $user = User::find($id);
        $roles = Role::pluck('name','name')->only('Customers', 'Managers')->all();
        $userRole = $user->roles->pluck('name','name')->all();

        return view('admin.managers.edit',compact('user','roles','userRole'));
    }

    /**
     * To change manager's status.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Object $user
     * @return \Illuminate\Http\Response
     */
    public function changeStatusManager(Request $request, User $user)
    {
		$user_id = $request->user_id;
		$status = $request->status;

		User::where('id', $user_id)->update(['blocked'=>$status]);
		
		return response()->json(['success'=>'Status changed successfully.']);
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function addManager(Request $request)
    {
		/* Check Validation */
        $this->validate($request, [
            'name' => 'required|regex:/^[\pL\s\-]+$/u',
            'email' => 'required|regex:/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix|unique:users,email',
            'password' => 'required|same:confirm-password',
            'roles' => 'required'
        ]);
		
		/* Create USER */
        $input = $request->all();
        $input['password'] = Hash::make($input['password']);
        $user = User::create($input);
        
        /* Assign user a role*/
        $user->assignRole($request->input('roles'));

        return redirect()->route('showAllManagers')->with('success','User created successfully');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function updateManager(Request $request, $id)
    {
		/* Check Validation */
        $this->validate($request, [
            'name' => 'required|regex:/^[\pL\s\-]+$/u',
            'email' => 'required|regex:/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix|unique:users,email,'.$id,
            'password' => 'required|same:confirm-password',
            'roles' => 'required'
        ]);

		/* if user dont want to change password or skip password to update */
        $input = $request->all();
        if(!empty($input['password'])){ 
            $input['password'] = Hash::make($input['password']);
        }else{
            $input = Arr::except($input,array('password'));    
        }

		/*Update User and delete the previous role and assign new role */
        $user = User::find($id);
        $user->update($input);

        DB::table('model_has_roles')->where('model_id',$id)->delete();

        $user->assignRole($request->input('roles'));
        
        return redirect()->route('showAllManagers')->with('success','User updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function deleteManager($id)
    {
        User::find($id)->delete();
        
        return redirect()->route('showAllManagers')->with('danger','Manager deleted successfully');

    }

    /**
     * To display all customers from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function showAllCustomer()
    {
        $customers=env("APP_CUSTOMER");
        $data = User::role($customers)->orderBy('id','DESC')->get();
        return view('admin.customers.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function createCustomer()
    {
        $roles = Role::pluck('name','name')->only('Customers', 'Managers')->all();
        return view('admin.customers.create',compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function addCustomer(Request $request)
    {
		/* Check Validation */
        $this->validate($request, [
            'name' => 'required|regex:/^[\pL\s\-]+$/u',
            'email' => 'required|regex:/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix|unique:users,email',
            'password' => 'required|same:confirm-password',
            'roles' => 'required'
        ]);
		
		/* Create USER */
        $input = $request->all();
        $input['password'] = Hash::make($input['password']);
        $user = User::create($input);
        
        /* Assign user a role*/
        
        $user->assignRole($request->input('roles'));

        return redirect()->route('showAllCustomer')->with('success','Customer created successfully');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function editCustomer($id)
    {
        $user = User::find($id);
        $roles = Role::pluck('name','name')->only('Customers', 'Managers')->all();
        $userRole = $user->roles->pluck('name','name')->all();

        return view('admin.customers.edit',compact('user','roles','userRole'));
    }

    /**
     * Show the challenges details of the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function viewCustomerChallenges($id) 
    {
        //getting challenges details
        $user_challenges_sent = PostsTagged::where([
            ['tagged_by', $id],
        ])->get();
        $user_challenges_received = PostsTagged::where([
            ['tagged_user', $id],
        ])->get();
        $challenge_sent_count = 0;
        $challenge_sent_pending_count = 0;
        $challenge_sent_pending_user = array();
        $challenge_sent_accepted_count = 0;
        $challenge_sent_accepted_user = array();
        $challenge_received_count = 0;
        $challenge_received_accepted_count = 0;
        $challenge_received_accepted_user = array();
        $challenge_received_pending_count = 0;
        $challenge_received_pending_user = array();
        foreach ($user_challenges_sent as $user_challenge_sent) {
            $challenge_sent_count++;
            if ($user_challenge_sent['challenge_accepted'] == 1) {
                $challenge_sent_accepted_count++;
                $challenge_sent_accepted_user[] = User::where([
                    ['id', $user_challenge_sent['tagged_user']], 
                ])->first()->only(
                    'id', 'name', 'email', 'about', 'profile_pic', 'email_verified_at', 'blocked', 'created_at', 'updated_at'
                );
            }
            if ($user_challenge_sent['challenge_accepted'] == 0) {
                $challenge_sent_pending_count++;
                $challenge_sent_pending_user[] = User::where([
                    ['id', $user_challenge_sent['tagged_user']], 
                ])->first()->only(
                    'id', 'name', 'email', 'about', 'profile_pic', 'email_verified_at', 'blocked', 'created_at', 'updated_at'
                );
            }
        }
        foreach ($user_challenges_received as $user_challenge_received) {
            $challenge_received_count++;
            if ($user_challenge_received['challenge_accepted'] == 1) {
                $challenge_received_accepted_count++;
                $challenge_received_accepted_user[] = User::where([
                    ['id', $user_challenge_received['tagged_by']], 
                ])->first()->only(
                    'id', 'name', 'email', 'about', 'profile_pic', 'email_verified_at', 'blocked', 'created_at', 'updated_at'
                );
            }
            if ($user_challenge_received['challenge_accepted'] == 0) {
                $challenge_received_pending_count++;
                $challenge_received_pending_user[] = User::where([
                    ['id', $user_challenge_received['tagged_by']], 
                ])->first()->only(
                    'id', 'name', 'email', 'about', 'profile_pic', 'email_verified_at', 'blocked', 'created_at', 'updated_at'
                );
            }
        }
        $user = array();
        $user['id'] = $id;
        $user['challenge_sent_count'] = $challenge_sent_count;
        $user['challenge_sent_pending_count'] = $challenge_sent_pending_count;
        $user['challenge_sent_accepted_count'] = $challenge_sent_accepted_count;
        $user['challenge_received_count'] = $challenge_received_count;
        $user['challenge_received_pending_count'] = $challenge_received_pending_count;
        $user['challenge_received_accepted_count'] = $challenge_received_accepted_count;
        
        $user['challenge_sent_pending_user'] = $challenge_sent_pending_user;
        $user['challenge_sent_accepted_user'] = $challenge_sent_accepted_user;
        $user['challenge_received_pending_user'] = $challenge_received_pending_user;
        $user['challenge_received_accepted_user'] = $challenge_received_accepted_user;
        return view('admin.customers.challenges.index',compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function updateCustomer(Request $request, $id)
    {
		/* Check Validation */
        $this->validate($request, [
            'name' => 'required|regex:/^[\pL\s\-]+$/u',
            'email' => 'required|regex:/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix|unique:users,email,'.$id,
            'password' => 'same:confirm-password',
            'roles' => 'required'
        ]);

		/* if user dont want to change password or skip password to update */
        $input = $request->all();
        if (!empty($input['password'])) { 
            $input['password'] = Hash::make($input['password']);
        } else {
            $input = Arr::except($input,array('password'));    
        }

		/*Update User and delete the previous role and assign new role */
        $user = User::find($id);
        $user->update($input);

        DB::table('model_has_roles')->where('model_id',$id)->delete();

        $user->assignRole($request->input('roles'));
        
        return redirect()->route('showAllCustomer')->with('success','Customer updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function deleteCustomer($id)
    {
        User::find($id)->delete();
        
        return redirect()->route('showAllCustomer')->with('danger','Customer deleted successfully');

    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function showCustomer($id)
    {
        $user = User::find($id);
        return view('admin.customers.show',compact('user'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $user_id, $challenger_id 
     * @return \Illuminate\Http\Response
     */

    public function showChallenger($user_id, $challenger_id)
    {
        $user = User::find($challenger_id);
        return view('admin.customers.challenges.show',compact('user'));
    }

    /**
     * To change status of a customer.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Object  $user
     * @return \Illuminate\Http\Response
     */

    public function customerChangeStatus(Request $request, User $user)
    {
		$user_id = $request->user_id;
		$status = $request->status;

		User::where('id', $user_id)->update(['blocked'=>$status]);
		
		return response()->json(['success'=>'Status changed successfully.']);
    }
    
    /**
     * To view customer's friends.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function viewCustomerFriends($id)
    {
        $customers=env("APP_CUSTOMER");
        $auth_user = User::find($id);
        $friends = UsersFriend::where([
            ['user_id', $id], 
            ['friend_accepted', 1]
        ])->orWhere([
            ['user_friend_id', $id],
            ['friend_accepted', 1]
        ])->get();

        $friend_list = array(); 
        foreach ($friends as $friend) {
            $friend_list[] = User::where([
                ['id', $friend->user_friend_id], 
            ])->first(); 
        }

        $custom_pagination = new CustomPaginationController;
        $friend_list = collect($friend_list);
        $data = $custom_pagination->paginate($friend_list, 10);
        return view('admin.customers.friends.index', compact('data', 'auth_user'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function showCustomerFriend($customer_id, $friend_id)
    {
        $user = User::find($friend_id);
        return view('admin.customers.friends.show',compact('user'));
    }

    /**
     * To display customer's report page.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function manageCustomerReport()
    {
        $customers=env("APP_CUSTOMER");
        $data = User::role($customers)->orderBy('id','DESC')->get();
        return view('admin.reports.customers.index',compact('data'));
    }

    /**
     * To generate customer's report in csv format.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return csv file
     */
    public function customerGenerateCsvReport(Request $request)
    {
        $from_date = $request->post('from_date');
        $to_date = $request->post('to_date');
        $customer=env("APP_CUSTOMER");
        $users = User::Role($customer)->where([
            ['created_at', '>',  $from_date],
            ['created_at', '<=',  $to_date],
        ])->get(); 

        $customers = array();
        $i = 1;
        foreach ($users as $user) {
            $customer = array();
            $customer['serial_no'] = $i;
            $customer['name'] = $user->name;
            $customer['about'] = $user->about;
            $customer['profile_pic'] = $user->profile_pic;
            $customer['email'] = $user->email;
            $customer['blocked'] = $user->blocked;
            $customer['created_at'] = date("m-d-Y", strtotime($user->created_at));
            $customers[] = $customer; 
            $i++;  
        }
        $headers = [
            'Sr. No',
            'Name',
            'About',
            'Profile Pic',
            'Email',
            'Blocked',
            'Created_at',
        ];
        header('Content-type: text/csv');
        header('Content-Disposition: attachment; filename="customers.csv"');
        header('Pragma: no-cache');
        header('Expires: 0');

        $file = fopen('php://output', 'w');
        fputcsv($file, $headers);
        foreach ($customers as $customer) {
            fputcsv($file, $customer);
        }
        fclose($file);
        exit();
    }

    /**
     * Display a listing of the resource from last week.
     *
     * @return \Illuminate\Http\Response
     */
    public function managersLastWeek()
    {
        $manager=env("APP_MANAGER");
        $date = \Carbon\Carbon::today()->subDays(7);
        $data = User::role($manager)->where('created_at', '>=', $date)->orderBy('id','DESC')->get();
        return view('admin.managers.index', compact('data'));
    }

    /**
     * To display last week customers from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function customersLastWeek()
    {
        $customers=env("APP_CUSTOMER");
        $date = \Carbon\Carbon::today()->subDays(7);
        $data = User::role($customers)->where('created_at', '>=', $date)->orderBy('id','DESC')->get();
        return view('admin.customers.index', compact('data'));
    }
}
