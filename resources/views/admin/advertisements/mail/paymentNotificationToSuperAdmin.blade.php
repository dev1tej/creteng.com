<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ config('app.name', 'Creteng') }}</title>
  </head>
  <body>
    <div>
       <p><b>Advertisement Payment Transaction Notification from Creteng.</b></p>
    </div>
    <div>
      <p><b>Payment Detail:</b><br/>
        <b>Advertiser Name:</b> {{  $details['advertiser']['name'] }} <br/>
        <b>Advertiser Email:</b> {{  $details['advertiser']['email'] }} <br/>
        <b>Advertiser Contact:</b> {{  $details['advertiser']['contact'] }} <br/>
        <b>Ad Id:</b> {{  $details['advertisement']['id'] }} <br/>
        <b>Ad:</b> <a href="{{  $details['advertisement']['advertisement'] }}">{{  $details['advertisement']['advertisement'] }}</a> <br/>
        <b>Ad Publication Date:</b> {{  $details['advertisement']['publish_date'] }}<br/>
        <b>Ad Publication Expiry Date:</b> {{  $details['advertisement']['expiry_date'] }}<br/>
        <b>Transaction ID:</b> {{  $details['advertisement']['transaction_id'] }}<br/>
        <b>Payment Mode:</b> {{  $details['advertisement']['payment_mode'] }}<br/>
        <b>Amount:</b> {{  $details['advertisement']['amount'] }} USD<br/>
     </p>
    </div>
  </body>
</html>