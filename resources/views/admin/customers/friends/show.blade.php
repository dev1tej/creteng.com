@extends('admin.layouts.master')
@section('content')
@php
$url= $_SERVER['REQUEST_URI'];
$url_words = explode('/', $url);   
for ($i = 0; $i < count($url_words); $i++) {
	if ($url_words[$i] == 'customer') {
	    $customer_id = $url_words[$i+1];
	}
}
@endphp
<div class="container">
  <div class="row">
    <div class="col-lg-12 margin-tb">
      <div class="pull-left">
        <h2> Show Friend</h2>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="card" style="height: 100%; width: 100%;">
      <div class="card-header">
        <div class="col-lg-12 margin-tb">
          <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('viewCustomerFriends', $customer_id) }}"> Back</a>
          </div>
        </div>
      </div>
      <div class="card-body">
        <div class="card" style="height: 100%; width: 100%;">
          <img class="card-img-top" src="{{ $user->profile_pic }}" alt="{{ $user->profile_pic }}">
          <div class="card-body">
            <h5 class="card-title"><strong>{{ $user->name }}</strong></h5>
            <p class="card-text">{{ $user->about }}</p>
          </div>
          <ul class="list-group list-group-flush">
            <li class="list-group-item">{{ $user->email }}</li>
            <li class="list-group-item">
              @if(!empty($user->getRoleNames()))
              @foreach($user->getRoleNames() as $v)
              <label class="badge badge-success">{{ $v }}</label>
              @endforeach
              @endif
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection