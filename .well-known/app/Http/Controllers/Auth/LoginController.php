<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    
	protected function authenticated($request, $user){

		$superadmin=env("APP_SUPERADMIN");
		$admin=env("APP_ADMIN");
        $customer=env("APP_CUSTOMER");
        $manager=env("APP_MANAGER");
        if($user->hasRole($admin) || $user->hasRole($superadmin) || $user->hasRole($manager)){
            return redirect('/admin/dashboard');
        }elseif($user->hasRole($customer)) {
            $request->session()->flush();
            session()->flash('message', 'Sorry, You Don\'t have Authorization');
            return view('auth.login');
        }else {
            return route('/');
        }
    }
}
