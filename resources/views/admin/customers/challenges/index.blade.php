@extends('admin.layouts.master')
@section('content')
<div class="container">
  <div class="row">
    <div class="col-lg-12 margin-tb">
      <div class="pull-left">
        <h2>View Challenges</h2>
      </div>
      <div class="pull-right">
        <a class="btn btn-primary" href="{{ route('showAllCustomer') }}"> Back</a>
      </div>
    </div>
  </div>
  @if (count($errors) > 0)
  <div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
      @foreach ($errors->all() as $error)
      <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
  @endif
  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
      <div class="form-group">
        <strong>Challenges Sent Count:</strong>
        {{ $user['challenge_sent_count'] }}
        <br/>
      </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
      <div class="form-group">
        <strong>Challenges Sent Pending Count:</strong>
        {{ $user['challenge_sent_pending_count'] }}
        <br/>
      </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
      <div class="form-group">
        <strong>Challenge Sent Pending User's:</strong>
        @if($user['challenge_sent_pending_count'] != 0)
        @foreach($user['challenge_sent_pending_user'] as $pending_user)
        <a href="{{ route('showChallenger', [$user['id'], $pending_user['id']]) }}">{{ $pending_user['name'] }}</a>
        @if($user['challenge_sent_pending_count'] > 1)
        , 
        @endif
        @endforeach
        @endif
        <br/>
      </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
      <div class="form-group">
        <strong>Challenges Sent Accepted Count:</strong>
        {{ $user['challenge_sent_accepted_count'] }}
        <br/>
      </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
      <div class="form-group">
        <strong>Challenge Sent Accepted User's:</strong>
        @if($user['challenge_sent_accepted_count'] != 0)
        @foreach($user['challenge_sent_accepted_user'] as $accepted_user)
        <a href="{{ route('showChallenger', [$user['id'], $accepted_user['id']]) }}">{{ $accepted_user['name'] }}</a>
        @if($user['challenge_sent_accepted_count'] > 1)
        , 
        @endif
        @endforeach
        @endif
        <br/>
      </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
      <div class="form-group">
        <strong>Challenges Received Count:</strong>
        {{ $user['challenge_received_count'] }}
        <br/>
      </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
      <div class="form-group">
        <strong>Challenges Received Pending Count:</strong>
        {{ $user['challenge_received_pending_count'] }}
        <br/>
      </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
      <div class="form-group">
        <strong>Challenge Received Pending User's:</strong>
        @if($user['challenge_received_pending_count'] != 0)
        @foreach($user['challenge_received_pending_user'] as $pending_user)
        <a href="{{ route('showChallenger', [$user['id'], $pending_user['id']]) }}">{{ $pending_user['name'] }}</a>
        @if($user['challenge_received_pending_count'] > 1)
        , 
        @endif
        @endforeach
        @endif
        <br/>
      </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
      <div class="form-group">
        <strong>Challenges Received Accepted Count:</strong>
        {{ $user['challenge_received_accepted_count'] }}
        <br/>
      </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
      <div class="form-group">
        <strong>Challenge Received Accepted User's:</strong>
        @if($user['challenge_received_accepted_count'] != 0)
        @foreach($user['challenge_received_accepted_user'] as $accepted_user)
        <a href="{{ route('showChallenger', [$user['id'], $accepted_user['id']]) }}">{{ $accepted_user['name'] }}</a>
        @if($user['challenge_received_accepted_count'] > 1)
        , 
        @endif
        @endforeach
        @endif
        <br/>
      </div>
    </div>
  </div>
</div>
@endsection