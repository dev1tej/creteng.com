  <footer class="main-footer">
    <div class="row">
      <div class="col-md-9">
        <strong>Copyright &copy; @php echo date("Y") @endphp <a href="#">Creteng</a>.</strong> All rights
        reserved.
      </div>
      <div class="col-md-3">
        <strong>Developed by<a rel="nofollow" href="https://www.ost.agency/"><img src="{{url('/image/ostemblem.png')}}" alt="OpenSource Technologies" width="25%" height="80%"></a></strong> 
      </div>
    </div>
  </footer>
