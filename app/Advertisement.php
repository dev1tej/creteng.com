<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Advertisement extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 
        'advertiser_id', 
        'service_id',
        'advertisement',
        'description',
        'website_url',
        'is_app',
        'google_store_url',
        'apple_store_url',
        'ad_type',
        'status', 
        'order', 
        'paid', 
        'published', 
        'requested_for_publish',
        'archive',
        'publish_date', 
        'expiry_date'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * Get the user that owns the advertisement.
     */
    public function advertiser()
    {
        return $this->belongsTo('App\AdvUser', 'advertiser_id', 'id');
    }

    /**
     * Get the available service for the advertisement.
     */
    public function services()
    {
        return $this->belongsTo('App\AdvAvailableService', 'service_id', 'id');
    }
}
