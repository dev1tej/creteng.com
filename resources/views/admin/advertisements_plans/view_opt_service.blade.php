@extends('admin.layouts.master')
@section('content')
<div class="container">
  <div class="row">
    <div class="col-lg-12 margin-tb">
      <div class="pull-left">
        <h2>Advertisement Optional Services</h2>
      </div>
      <div class="pull-right">
        <a class="btn btn-primary" href="{{ route('showAllAdPlan') }}"> Back</a>
      </div>
    </div>
  </div>
  @if ($message = Session::get('success'))
  <div class="alert alert-success">
    <p>{{ $message }}</p>
  </div>
  @endif
  @if ($message = Session::get('danger'))
  <div class="alert alert-danger">
    <p>{{ $message }}</p>
  </div>
  @endif
  @if (isset($success))
  <div class="alert alert-success">
    <p>{{ $success }}</p>
  </div>
  @endif
  <section class="content">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-body">
            <table id="adPlanOptServiceData" class="table table-bordered table-striped table-hover">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Optional Service</th>
                  <th>Optional Service Price (in USD)</th>
                  <th>Optional Service Period</th>
                  <th width="280px">Action</th>
                </tr>
              </thead>
              <tbody>
                @php $srno = '1'; @endphp
                @forelse ($data as $key => $opt_service)
                <tr>
                  <td>@php echo $srno; @endphp</td>
                  <td>{{ $opt_service->optional_services }}</td>
                  <td>{{ $opt_service->aditional_price }}</td>
                  <td>{{ $opt_service->aditional_price_duration }}</td>
                  <td>
                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#exampleModal{{ $opt_service->id }}" data-id="{{ $opt_service->id }}" >Delete</button>	
                  </td>
                </tr>
                <!-- Delete Ad Plans Modal Starts -->
                <div class="modal fade" id="exampleModal{{ $opt_service->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                  <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Warning</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                        If you want to delete this Plan Optional Service, all information will be lost of this Service
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                        <a href="{{ route('deleteAdPlanOptService', $opt_service->id ) }}"><button type="button" class="btn btn-danger">Yes, go forward</button></a>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- Delete Ad Plans Modal Ends -->
                @php $srno++; @endphp
                @empty
                No Ad Plans Optional Services Found
                @endforelse
              </tbody>
              <tfoot>
                <tr>
                  <th>No</th>
                  <th>Name</th>
                  <th>Action</th>
                </tr>
              </tfoot>
            </table>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

<script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script>
  $(function () {
    $('#adPlanOptServiceData').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>

@endsection