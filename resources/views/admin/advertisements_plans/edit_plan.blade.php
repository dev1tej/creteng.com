@extends('admin.layouts.master')
@section('content')
<div class="container">
  <div class="row">
    <div class="col-lg-12 margin-tb">
      <div class="pull-left">
        <h2>Edit Advertisement Plan</h2>
      </div>
      <div class="pull-right">
        <a class="btn btn-primary" href="{{ route('showAllAdPlan') }}"> Back</a>
      </div>
    </div>
  </div>
  @if (count($errors) > 0)
  <div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
      @foreach ($errors->all() as $error)
      <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
  @endif
  @if (isset($success))
  <div class="alert alert-success">
    <p>{{ $success }}</p>
  </div>
  @endif
  @if (isset($danger))
  <div class="alert alert-danger">
    <p>{{ $danger }}</p>
  </div>
  @endif
  <form method="post" action="{{ route('updateAdPlan', $adv_plan->id) }}">
    {{ csrf_field() }}
    @method('PATCH')
    <div class="form-group row">
        <label for="plan_name" class="col-sm-3 col-form-label">Plan Name</label>
        <div class="col-sm-9">
            <input name="plan_name" type="text" class="form-control" id="plan_name" value="{{ $adv_plan->plan_name}}">
        </div>
    </div>
    @foreach($adv_plan->fixServices as $fixService)
    <div class="form-group row">
        <label for="plan_fix_services[]" class="col-sm-3 col-form-label">Plan Fix Service</label>
        <div class="col-sm-9">
            <input name="plan_fix_services[]" type="text" class="form-control" id="plan_fix_services[]"
                   value="{{ $fixService->fixed_services }}">
        </div>
    </div>
    @endforeach
    <div class="form-group row">
        <div class="offset-sm-3 col-sm-9">
            <button type="submit" class="btn btn-primary">Next</button>
        </div>
    </div>
  </form>
</div>
</div>
@endsection