@extends('admin.layouts.master')
@section('content')
<div class="container">
  <div class="row">
    <div class="col-lg-12 margin-tb">
      <div class="pull-left">
        <h2>Create Advertisement Plan Optional Service</h2>
      </div>
      <div class="pull-right">
        <a class="btn btn-primary" href="{{ route('showAllAdPlan') }}"> Back</a>
      </div>
    </div>
  </div>
  @if (count($errors) > 0)
  <div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
      @foreach ($errors->all() as $error)
      <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
  @endif
  @if (isset($success))
  <div class="alert alert-success">
    <p>{{ $success }}</p>
  </div>
  @endif
  @if (isset($danger))
  <div class="alert alert-danger">
    <p>{{ $danger }}</p>
  </div>
  @endif
  <form method="post" action="{{ route('addAdPlanOptService') }}">
    {{ csrf_field() }}
    <div class="form-group row">
        <label for="service" class="col-sm-3 col-form-label">Write Optional Service</label>
        <div class="col-sm-9">
            <input name="service" type="text" class="form-control" id="service" placeholder="likes, comments etc.">
        </div>
    </div>
    <div class="form-group row">
      <label for="service_price" class="col-sm-3 col-form-label">Optional Service Fix Price (in USD)</label>
      <div class="col-sm-9">
          <input name="service_price" type="number" step=".01"  class="form-control" id="service_price" placeholder="Enter price">
      </div>
    </div>
    <div class="form-group row">
        <label for="service_period" class="col-sm-3 col-form-label">Optional Service Price Period</label>
        <div class="col-sm-9">
            <input name="service_period" type="text" class="form-control" id="service_period"
                   value="Weekly" readonly>
        </div>
    </div>
    <div class="form-group row">
        <div class="offset-sm-3 col-sm-9">
            <button type="submit" class="btn btn-primary">Save</button>
        </div>
    </div>
  </form>
</div>
</div>
@endsection