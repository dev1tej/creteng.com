<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Hash;
use App\Advertisement;
use App\AdsPayment;
use App\AdvLike;
use App\AdvView;
use App\AdvComment;
use App\User;
use App\AdvUser;
use App\AdvAvailableService;
use App\AdvPlan;
use App\AdvPlDuration;
use App\AdvPlFixedService;
use App\AdvPlOptService;
use App\AdvPublicationLocation;
use Illuminate\Support\Facades\Session;
use Stripe\Stripe;
use Stripe\Customer;
use Stripe\Charge;
use App\Countries;

class AdvertisementController extends Controller
{
    public function usAndEuCountries()
    {
        $usAndEuCountries = [
            'UNITED STATES',
            'FRANCE',
            'SPAIN',
            'SWEDEN',
            'GERMANY',
            'FINLAND',
            'POLAND',
            'ITALY',
            //'ROMANIA',
            'GREECE',
            'BULGARIA',
            'HUNGARY',
            'PORTUGAL',
            'AUSTRIA',
            //'CZECH REPUBLIC',
            'IRELAND',
            'LITHUANIA',
            'LATVIA',
            'CROATIA',
            //'SLOVAKIA',
            'ESTONIA',
            'DENMARK',
            'NETHERLANDS',
            'BELGIUM',
            'SLOVENIA',
            'CYPRUS',
            'LUXEMBOURG',
            'MALTA',
        ];

        return $usAndEuCountries;
    }

    /**
     * Display a listing of the Advertiser.
     *
     * @return \Illuminate\View\View
     */
    public function showAllAdvertiser()
    {
        $data = AdvUser::orderBy('id','DESC')->get();
        $abc = array();
        foreach($data as $advertiser) {
            $expire_ad = Advertisement::where([
                ['advertiser_id', $advertiser->id],
                ['expiry_date', '0000-00-00'],
            ])->first();
            if(!is_null($expire_ad)) {
                $advertiser->ad_expire = true;
                $advertiser->advertisement = $expire_ad;
            } elseif (is_null($expire_ad)) {
                $advertiser->ad_expire = false;
            } 
        }
        //echo"<pre>";
        //print_r($abc);
        //exit();
        return view('admin.advertisers.index', compact('data'));
    }

    /**
     * Sending to create view.
     *
     * @return \Illuminate\View\View
     */

    public function createAdvertiser()
    {
        return view('admin.advertisers.create');
    }

    /**
     * Creating new Advertiser
     * 
     * @param \Illuminate\Http\Request  $request
     * @return View
     */
    public function addAdvertiser(Request $request) 
    {
        /* Check Validation */
        $this->validate($request, [
            'name' => 'required|regex:/^[\pL\s\-]+$/u',
            'email' => 'required|email|unique:adv_users,email',
            'contact' => 'required|digits_between: 10,12',
        ]);
		
		/* Create Advertiser */
        AdvUser::create($request->all());
        return redirect()->route('showAllAdvertiser')->with('success','Advertiser created successfully');
    }

    /**
     * To show an Advertiser
     * 
     * @param int  $id
     * @return \Illuminate\View\View
     */
    public function showAdvertiser($id)
    {
        $advertiser = AdvUser::find($id);

        $ad_serviceses = AdvAvailableService::where([
            ['advertiser', $id],
        ])->get();

        $all_plans = array();
        foreach ($ad_serviceses as $ad_services) {
            $my_plan = array();

            $my_plan['status'] = $ad_services->status;
            $my_plan['plan'] = AdvPlan::find($ad_services['plan']);
            $my_plan['plan_fixed_service'] = AdvPlFixedService::where('plan_id', $my_plan['plan']['id'])->first();
            $my_plan['plan_duration'] = AdvPlDuration::find($ad_services['duration']);
            if (!is_null($ad_services['opt_services'])) {
                $my_plan['plan_opt_service'] = AdvPlOptService::find($ad_services['opt_services']);
            } else {
                $my_plan['plan_opt_service'] = null;
            }

            $my_plan['payment'] = AdsPayment::where('service_id', $ad_services->id)->first();

            
            $total_opt_services_cost = 0;
            $multiplying_factor = '';
            if (!is_null($my_plan['plan_opt_service'])) {
                if ($my_plan['plan_duration']['plan_duration'] == 'Weekly') {
                    $multiplying_factor = '*1';
                    $total_opt_services_cost += $my_plan['plan_opt_service']['aditional_price'] * 1;
                } 
                if ($my_plan['plan_duration']['plan_duration'] == 'Monthly') {
                    $multiplying_factor = '*4';
                    $total_opt_services_cost += $my_plan['plan_opt_service']['aditional_price'] * 4;
                } 
                if ($my_plan['plan_duration']['plan_duration'] == 'Quarterly') {
                    $multiplying_factor = '*13';
                    $total_opt_services_cost += $my_plan['plan_opt_service']['aditional_price'] * 13;
                } 
                if ($my_plan['plan_duration']['plan_duration'] == 'Half Yearly') {
                    $multiplying_factor = '*26';
                    $total_opt_services_cost += $my_plan['plan_opt_service']['aditional_price'] * 26;
                } 
                if ($my_plan['plan_duration']['plan_duration'] == 'Yearly') {
                    $multiplying_factor = '*52';
                    $total_opt_services_cost += $my_plan['plan_opt_service']['aditional_price'] * 52;
                }
            }
            $my_plan['multiplying_factor'] = $multiplying_factor;
            $my_plan['total_opt_services_cost'] = $total_opt_services_cost;
            $my_plan['publishable_locations'] = AdvPublicationLocation::where('service_id', $ad_services->id)->get();
            $my_plan['advertisements'] = Advertisement::where([
                ['advertiser_id', $id],
                ['service_id', $ad_services->id],
            ])->get();

            $all_plans[] = $my_plan;
        }

        $my_plans = array();
        foreach ($all_plans as $my_plan) {
            if ($my_plan['status'] == true) {
                $my_plans[0] = $my_plan;
            }
        }

        $i = 1;
        foreach ($all_plans as $my_plan) {
            if ($my_plan['status'] != true) {
                $my_plans[$i] = $my_plan;
                $i++;
            }
        }

        //echo"<pre>";
        //print_r($my_plans);
        //exit();

        //$advertisements = Advertisement::where('advertiser_id', $id)->get();
        return view('admin.advertisers.show',compact('advertiser', 'my_plans'));
    }

    /**
     * To edit an Advertiser
     * 
     * @param int  $id
     * @return \Illuminate\View\View
     */
    public function editAdvertiser($id)
    {
        $user = AdvUser::find($id);
        $usAndEuCountries = $this->usAndEuCountries();
        $countries = Countries::all();
        $countries_with_iso = array();
        foreach($countries as $country) {
            if (in_array($country->name, $usAndEuCountries)) {
                $countries_with_iso[] = $country->name . '-' . $country->iso;
            }
        }
        return view('admin.advertisers.edit', compact('user', 'countries_with_iso'));
    }

    /**
     * To update an Advertiser
     * 
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return View
     */
    public function updateAdvertiser(Request $request, $id) 
    {
        /* Check Validation */
        $this->validate($request, [
            'name' => 'required|regex:/^[\pL\s\-]+$/u',
            'company_name' => 'string|max:255|nullable',
            'email' => 'required|regex:/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix|unique:adv_users,email,'.$id,
            'contact' => 'required|digits_between: 10,12',
            'password' => 'same:confirm-password',
            'address_line_1' => 'required|string|max:255',
            'address_line_2' => 'string|nullable|max:255',
            'city' => 'required|string|max:100',
            'state' => 'required|string|max:100',
            'postal_code' => 'required|string|max:20',
            'country' => 'required|string|max:100',
        ]);

        $input = $request->all();
        if (!empty($input['password'])) { 
            $input['password'] = Hash::make($input['password']);
        } else {
            $input = Arr::except($input,array('password'));    
        }
        $advertiser = AdvUser::find($id);
        $advertiser->update($input);
        
        return redirect()->route('showAllAdvertiser')->with('success','Advertiser updated successfully');
    }

    /**
     * To delete an Advertiser
     * 
     * @param int $id
     * @return View
     */
    public function deleteAdvertiser($id)
    {
        AdvUser::find($id)->delete();
        
        return redirect()->route('showAllAdvertiser')->with('danger','Advertiser deleted successfully');

    }

    /**
     * Display a listing of the Advertisements.
     *
     * @return \Illuminate\View\View
     */
    public function showAllAdvertisement()
    {
        $data = Advertisement::where([
            ['archive', false],
        ])->with('advertiser')->orderBy('id','DESC')->get();
        foreach ($data as $advertisement) {
            $ads_payment = AdsPayment::where([
                ['service_id', $advertisement->service_id],
            ])->first(); 

            if (is_null($ads_payment)) {
                $advertisement->ads_payment = null;
            } else {
                $advertisement->ads_payment = $ads_payment;
            }

            //finding multiple add for single advertiser for single service
            $advertiser_id = $advertisement->advertiser->id;
            $service_id = $advertisement->service_id;
            $advertiser_count = 0;
            foreach ($data as $adv) {
                if (($adv->advertiser->id == $advertiser_id) && ($adv->service_id == $service_id)) {
                    $advertiser_count++;
                }
            }
            if ($advertiser_count > 1) {
                $advertisement->multiple_add_for_single_advertiser = true;
            } else {
                $advertisement->multiple_add_for_single_advertiser = false;
            }
        }
        //echo"<pre>";
        //print_r($data);
        //exit();
        return view('admin.advertisements.index', compact('data'));
    }

    /**
     * Display a listing of the archived advertisements.
     *
     * @return \Illuminate\View\View
     */
    public function showAllArchivedAdvertisement()
    {
        $data = Advertisement::where([
            ['archive', true],
        ])->with('advertiser')->orderBy('id','DESC')->get();
        foreach ($data as $advertisement) {
            $ads_payment = AdsPayment::where([
                ['service_id', $advertisement->service_id],
            ])->first(); 

            if (isset($ads_payment['transaction_id']) && ($ads_payment['transaction_id'] != '')) {
                $advertisement->payment_transaction = true;
            } else {
                $advertisement->payment_transaction = false;
            }
        }
        return view('admin.advertisements.showArchivedAds', compact('data'));
    }

    /**
     * sending to create view with data.
     *
     * @return \Illuminate\View\View
     */

    public function createAdvertisement()
    {
        $advertisers = AdvUser::all();
        $advertiser_name_and_id = array();
        foreach($advertisers as $advertiser) {
            $advertiser_name_and_id[] = $advertiser->name . '(' . $advertiser->id . ')';
        }
        
        return view('admin.advertisements.create', compact('advertiser_name_and_id'));
    }

    /**
     * Creating new advertisement
     * 
     * @param \Illuminate\Http\Request  $request
     * @return \Illuminate\View\View
     */
    public function addAdvertisement(Request $request)
    {
        /* Check Validation */
        $this->validate($request, [
            'advertiser_name_and_id' => 'required',
            'advertisement' => 'required|image|max:10240',
            'order' => 'required|int|unique:advertisements,order',
            'publishable_location' => 'string|nullable|max:255',
        ]);
        
        $intermediate_explode_1 = explode('(', $request->advertiser_name_and_id);
        $intermediate_explode_2 = explode(')', $intermediate_explode_1[1]);
        $advertiser_id = $intermediate_explode_2[0];

        $path = $request->file('advertisement')->store('uservideos/advertisement', 's3');
        $aws_file_path = env('AWS_URL') . $path; 
        
        /* Create Advertisement */
        Advertisement::create([
            'advertiser_id'        => $advertiser_id,
            'advertisement'        => $aws_file_path,
            'order'                => $request->order,
            'publishable_location' => $request->publishable_location,
        ]);
        return redirect()->route('showAllAdvertisement')->with('success','Advertisement created successfully');
    }

    /**
     * sending to edit adv view with data
     * 
     * @param int $id
     * @return \Illuminate\View\View
     */
    public function editAdvertisement($id)
    {
        $advertisement = Advertisement::find($id);
        $advertiser = AdvUser::where([
            ['id', $advertisement->advertiser_id],
        ])->first();
        return view('admin.advertisements.edit',compact('advertisement', 'advertiser'));
    }

    /**
     * To update an adv
     * 
     * @param \Illuminate\Http\Request  $request
     * @param int $id
     * @return View
     */
    public function updateAdvertisement(Request $request, $id)
    {
        /* Check Validation */
        $this->validate($request, [
            'advertiser_name' => 'required|string|max:255',
            'advertiser_id' => 'required|int',
            'advertisement' => 'required|file|max:102400',
            'order' => 'required|int|unique:advertisements,order,'.$id,
            'publishable_location' => 'string|nullable|max:255',
            'publish_date' => 'date|nullable',
            'expiry_date' => 'date|nullable',
        ]);
        
        $advertisement = Advertisement::find($id);
        $path = $request->file('advertisement')->store('uservideos/advertisement', 's3');
        $ad = env('AWS_URL') . $path; 
    

        if (is_null($request->publishable_location)) {
            $publishable_location = $advertisement->publishable_location;
        } else {
            $publishable_location = $request->publishable_location;
        }

        if (is_null($request->publish_date)) {
            $publish_date = $advertisement->publish_date;
        } else {
            $publish_date = $request->publish_date;
        }

        if (is_null($request->expiry_date)) {
            $expiry_date = $advertisement->expiry_date;
        } else {
            $expiry_date = $request->expiry_date;
        }
        
        $advertisement->update([
            'advertiser_id'        => $request->advertiser_id,
            'advertisement'        => $ad,
            'order'                => $request->order,
            'publishable_location' => $publishable_location,
            'publish_date'         => $publish_date,
            'expiry_date'          => $expiry_date,
        ]);
        
        return redirect()->route('showAllAdvertisement')->with('success', 'Advertisement updated successfully');  
    }

    /**
     * To delete an adv
     * 
     * @param int $id
     * @return View
     */
    public function deleteAdvertisement($id)
    {
        $ad = Advertisement::find($id);
        //$advertiser = AdvUser::find($ad['advertiser_id']);
        $ad->delete();
        return redirect()->route('showAllAdvertisement')->with('danger', 'Advertisement deleted successfully');
    }

    /**
     * To archive an adv
     * 
     * @param int $id
     * @return View
     */
    public function archiveAdvertisement($id)
    {
        Advertisement::find($id)->update([
            'archive' => true,
        ]);
        return redirect()->route('showAllAdvertisement')->with('danger', 'Advertisement archived successfully');
    }

    /**
     * To unarchive an adv
     * 
     * @param int $id
     * @return View
     */
    public function unarchiveAdvertisement($id)
    {
        Advertisement::find($id)->update([
            'archive' => false,
        ]);
        return redirect()->route('showAllArchivedAdvertisement')->with('success', 'Advertisement archived successfully');
    }

    /**
     * To change the status of an adv
     * 
     * @param \Illuminate\Http\Request  $request
     * @return Json Response
     */
    public function advertisementChangeStatus(Request $request) 
    {
        $advertisement_id = $request->advertisement_id;
		$status = $request->status;

		$advertisement = Advertisement::where('id', $advertisement_id)->first();
        $advertisement->update([
            'published' => $status,
        ]);

        if ($advertisement->published == true) {
            $advertisement->update([
                'requested_for_publish' => false,
            ]);
        }

        /*
        if ($status == true) {
            $ad = Advertisement::find($advertisement_id);
            $individuals_ads = Advertisement::where('advertiser_id', $ad['advertiser_id'])->get();
            foreach($individuals_ads as $ads) {
                if ($ads->id != $advertisement_id) {
                    $ads->update(['published'=> false]);
                }
            }
        }
        */
		
		return response()->json(['success'=>'Status changed successfully.']);
    }

    /**
     * To show an adv
     * 
     * @param int $id
     * @return \Illuminate\View\View
     */
    public function showAdvertisement($id)
    {
        $data = Advertisement::where('id', $id)->with('advertiser')->first();

        $adv_available_service = AdvAvailableService::where('id', $data->service_id)->first();
        $data['plan'] = AdvPlan::find($adv_available_service['plan']);
        $data['sub_plan'] = AdvPlDuration::find($adv_available_service['duration']);
        $data['plan_fixed_service'] = AdvPlFixedService::where('plan_id', $data['plan']['id'])->get();
        $data['plan_opt_service'] = AdvPlOptService::where('id', $adv_available_service['opt_services'])->get();
        $data['publishable_locations'] = AdvPublicationLocation::where([
            ['advertiser', $data->advertiser_id],
            ['service_id', $data->service_id],
        ])->get();

        $likes = AdvLike::where([
            ['adv_id', $data->id],
        ])->with('user')->get();
        $data['likes'] = $likes;
        $data['likes']['likes_count'] = $likes->count();

        $views = AdvView::where([
            ['adv_id', $data->id],
        ])->with('user')->get();
        $data['views'] = $views;
        $data['views']['views_count'] = $views->count();

        $comments = AdvComment::where('adv_id', $data->id)
            ->whereNull('comment_id')
            ->with('user')
            ->get();

        $comment_details = array();
        $index = 0;
        foreach ($comments as $comment) {
            $comment_details[$index]['comment'] = $comment;
            $comment_details[$index]['comment']['comment_reply'] = AdvComment::where([
                ['adv_id', $data->id],
                ['comment_id', $comment->id],
            ])->with('user')->get();
            $index++;
        }
        $data['comments'] = $comment_details;
        //echo"<pre>";
        //print_r($data);
        //exit();
        
        return view('admin.advertisements.show',compact('data'));
    }

    public function updateAdDescription(Request $request) 
    {
        $advertisement = Advertisement::find($request->ad_id);
        $advertisement->update([
            'description' => $request->description,
        ]);
        return $advertisement['description'];
    }

    public function updateAdURL(Request $request) 
    {
        $advertisement = Advertisement::find($request->ad_id);
        $advertisement->update([
            'website_url' => $request->url,
        ]);
        return $advertisement['website_url'];
    }

    /**
     * To send payment link email for an adv
     * 
     * @param \Illuminate\Http\Request  $request
     * @return View
     */
    public function sendPaymentLink(Request $request) {
        $request->advertisement = Advertisement::where('id', $request->advertisement_id)->with('advertiser')->first();
        //$request->advertisement = json_decode($request->advertisement);

        $details = array();
        $details['amount'] = $request->amount;
        $details['payment_mode'] = $request->payment_mode;
        $details['advertisement']['id'] = $request->advertisement->id;
        $details['advertisement']['advertisement'] = $request->advertisement->advertisement;
        $details['advertiser']['id'] = $request->advertisement->advertiser->id;
        $details['advertiser']['name'] = $request->advertisement->advertiser->name;
        $details['advertiser']['email'] = $request->advertisement->advertiser->email;
        $details['advertiser']['contact'] = $request->advertisement->advertiser->contact;
        
        \Mail::to($details['advertiser']['email'])->send(new \App\Mail\AdsPaymentNotofication($details));

        return back()->with('success', 'Payment link sent successfully.');
    }

    /**
     * To direct to stripe payment page from the email
     * 
     * @param int $id
     * @param string $payment_mode
     * @param float $payment_amount
     * @return \Illuminate\View\View
     */
    public function stripe($ad_id, $payment_mode, $payment_amount)
    {
        return view('admin.advertisements.stripe',compact('ad_id', 'payment_mode', 'payment_amount'));
    }

    /**
     * stripe payment processing
     * 
     * @param \Illuminate\Http\Request  $request
     * @return View
     */
    public function stripePost(Request $request)
    {
        $advertisement = Advertisement::find($request->ad_id);
        $advertiser = AdvUser::find($advertisement->advertiser_id);
        $today_date = date("Y-m-d");

        if (!is_null($advertisement->expiry_date)) {
            if ($advertisement->expiry_date > $today_date) {
                Session::flash(
                    'failure', 'Duplicate Payment. Payment can not be done. Publication of this ad is still valid upto '.$advertisement->expiry_date
                );
                return back();
            }
        }

        $ads_payment = AdsPayment::where('service_id', $advertisement->service_id)->first();
        Stripe::setApiKey(env('STRIPE_SECRET')); 
        
        $customer = Customer::create(array(
            'name' => $advertiser->name,
            "source" => $request->stripeToken,      
            'email' => $advertiser->email,
            "address" => [
                'line1' => $advertiser->address_line_1, 
                'line2' => $advertiser->address_line_1, 
                'city' => $advertiser->city,  
                'state' => $advertiser->state,
                'postal_code' => $advertiser->postal_code, 
                'country' => $advertiser->country
            ], 
        ));
        $paid = Charge::create ([
            'customer' => $customer->id, 
            "amount" => 100 * $request->payment_amount,
            "currency" => "usd",
            "description" => "Payment for Ad Publication in Creteng", 
        ]);

        if ($paid->id) {
            $publish_date = $today_date;
            if ($request->payment_mode == 'Weekly') {
                $expiry_date = date("Y-m-d", strtotime('-0 day', strtotime('+1 week', strtotime($publish_date))));
            }
            if ($request->payment_mode == 'Monthly') {
                $expiry_date = date("Y-m-d", strtotime('-1 day', strtotime('+1 month', strtotime($publish_date))));
            }
            if ($request->payment_mode == 'Quarterly') {
                $expiry_date = date("Y-m-d", strtotime('-1 day', strtotime('+3 month', strtotime($publish_date))));
            }
            if ($request->payment_mode == 'Half Yearly') {
                $expiry_date = date("Y-m-d", strtotime('-1 day', strtotime('+6 month', strtotime($publish_date))));
            }
            if ($request->payment_mode == 'Yearly') {
                $expiry_date = date("Y-m-d", strtotime('-1 day', strtotime('+12 month', strtotime($publish_date))));
            }
            $advertisement->update([
                'paid' => true,
                'published' => true,
                'publish_date' => $publish_date,
                'expiry_date' => $expiry_date
            ]);
            $ads_payment = AdsPayment::create([
                'advertiser_id' => $advertisement->advertiser_id,
                'stripe_customer_id' => $customer->id,
                'transaction_id' => $paid->id,
                'status' => 'Charged',
                'payment_mode' => $request->payment_mode,
                'amount' => $request->payment_amount,
                'publish_date' => $publish_date,
                'expiry_date'  => $expiry_date
            ]);
            $details = array();
            $details['advertiser']['name'] = $advertiser->name;
            $details['advertiser']['email'] = $advertiser->email;
            $details['advertiser']['contact'] = $advertiser->contact;
            $details['advertisement']['id'] = $advertisement->id;
            $details['advertisement']['advertisement'] = $advertisement->advertisement;
            $details['advertisement']['publish_date'] = $advertisement->publish_date;
            $details['advertisement']['expiry_date'] = $advertisement->expiry_date;
            $details['advertisement']['transaction_id'] = $ads_payment->transaction_id;
            $details['advertisement']['payment_mode'] = $ads_payment->payment_mode;
            $details['advertisement']['amount'] = $ads_payment->amount;
            \Mail::to($advertiser->email)->send(new \App\Mail\ThanksNotificationToAdvertiser($details));
            \Mail::to(env('APP_SUPERADMIN_EMAIL_ID'))->send(new \App\Mail\PaymentNotificationToSuperAdmin($details));
            Session::flash('success', 'Payment successful!');
            return back();
        } else {
            Session::flash('failure', 'Payment unsuccessful!');
            return back();
        }
    }

    /**
     * To view adv payment history
     * 
     * @param \Illuminate\Http\Request  $request
     * @param int $ad_id
     * @return \Illuminate\View\View
     */
    public function adPaymentHistory($advertiser_id) 
    {
        $data = AdsPayment::where('advertiser_id', $advertiser_id)->with('advertiser')->get();
        /*
        foreach ($data as $payment) {
            $payment->advertisement = Advertisement::where('advertiser_id', $payment['advertiser_id'])->first();
        }*/
        $data = collect($data);
        $data = $data->sortByDesc('id'); //->paginate(10);
        return view('admin.advertisers.paymentHistory', compact('data'));
            //->with('i', ($request->input('page', 1) - 1) * 10);
    }

    /**
     * To approve new adv and capture payment
     * 
     * @param int $id
     * @return View
     */
    public function approveNewAdvertisement($id)
    {
        $advertisement = Advertisement::find($id);
        $advertiser = AdvUser::find($advertisement['advertiser_id']);
        $ads_payments = AdsPayment::where([
            ['service_id', $advertisement['service_id']],
        ])->first();    

        //capturing hold payment
        Stripe::setApiKey(env('STRIPE_SECRET'));
        $charge = Charge::retrieve($ads_payments['transaction_id']);
        $charge->capture();

        $publish_date = date("Y-m-d");
        if ($ads_payments['payment_mode'] == 'Weekly') {
            $expiry_date = date("Y-m-d", strtotime('-0 day', strtotime('+1 week', strtotime($publish_date))));
        }
        if ($ads_payments['payment_mode'] == 'Monthly') {
            $expiry_date = date("Y-m-d", strtotime('-1 day', strtotime('+1 month', strtotime($publish_date))));
        }
        if ($ads_payments['payment_mode'] == 'Quarterly') {
            $expiry_date = date("Y-m-d", strtotime('-1 day', strtotime('+3 month', strtotime($publish_date))));
        }
        if ($ads_payments['payment_mode'] == 'Half Yearly') {
            $expiry_date = date("Y-m-d", strtotime('-1 day', strtotime('+6 month', strtotime($publish_date))));
        }
        if ($ads_payments['payment_mode'] == 'Yearly') {
            $expiry_date = date("Y-m-d", strtotime('-1 day', strtotime('+12 month', strtotime($publish_date))));
        }
        $advertisement->update([
            'status'        => 'Active',
            'paid'          => true,
            'published'     => true,
            'publish_date'  => $publish_date,
            'expiry_date'   => $expiry_date,
        ]);
        $ads_payments->update([
            'status'        => 'Charged',
            'publish_date'  => $publish_date,
            'expiry_date'   => $expiry_date,
        ]);

        $details = array();
        $details['advertiser']['name'] = $advertiser['name'];
        $details['advertiser']['email'] = $advertiser['email'];
        $details['advertiser']['contact'] = $advertiser['contact'];
        $details['advertiser']['company_name'] = $advertiser['company_name'];
        $details['advertisement']['id'] = $advertisement['id'];
        $details['advertisement']['advertisement'] = $advertisement['advertisement'];
        $details['advertisement']['publish_date'] = $advertisement['publish_date'];
        $details['advertisement']['expiry_date'] = $advertisement['expiry_date'];
        $details['advertisement']['amount'] = $ads_payments['amount'];

        $ad_services = AdvAvailableService::where([
            ['id', $advertisement['service_id']],
        ])->first();
        $ad_services->update([
            'status' => true,
        ]);

        $details['advertisement']['plan'] = AdvPlan::find($ad_services['plan']);
        $details['advertisement']['plan_fixed_service'] = AdvPlFixedService::where('plan_id', $details['advertisement']['plan']['id'])->first();
        $details['advertisement']['plan_duration'] = AdvPlDuration::find($ad_services['duration']);
        if (!is_null($ad_services['opt_services'])) {
            $details['advertisement']['plan_opt_service'] = AdvPlOptService::find($ad_services['opt_services']);
        } else {
            $details['advertisement']['plan_opt_service'] = null;
        }
        
        $total_opt_services_cost = 0;
        $multiplying_factor = '';
        if (!is_null($details['advertisement']['plan_opt_service'])) {
            if ($details['advertisement']['plan_duration']['plan_duration'] == 'Weekly') {
                $multiplying_factor = '*1';
                $total_opt_services_cost += $details['advertisement']['plan_opt_service']['aditional_price'] * 1;
            } 
            if ($details['advertisement']['plan_duration']['plan_duration'] == 'Monthly') {
                $multiplying_factor = '*4';
                $total_opt_services_cost += $details['advertisement']['plan_opt_service']['aditional_price'] * 4;
            } 
            if ($details['advertisement']['plan_duration']['plan_duration'] == 'Quarterly') {
                $multiplying_factor = '*13';
                $total_opt_services_cost += $details['advertisement']['plan_opt_service']['aditional_price'] * 13;
            } 
            if ($details['advertisement']['plan_duration']['plan_duration'] == 'Half Yearly') {
                $multiplying_factor = '*26';
                $total_opt_services_cost += $details['advertisement']['plan_opt_service']['aditional_price'] * 26;
            } 
            if ($details['advertisement']['plan_duration']['plan_duration'] == 'Yearly') {
                $multiplying_factor = '*52';
                $total_opt_services_cost += $details['advertisement']['plan_opt_service']['aditional_price'] * 52;
            }
        }
        $details['advertisement']['multiplying_factor'] = $multiplying_factor;
        $details['advertisement']['total_opt_services_cost'] = $total_opt_services_cost;

        $details['advertisement']['transaction_id'] = $ads_payments['transaction_id'];
        $details['advertisement']['amount'] = $ads_payments['amount'];

        $details['advertisement']['publishable_locations'] = AdvPublicationLocation::where('service_id', $advertisement['service_id'])->get();

        \Mail::to($details['advertiser']['email'])->send(new \App\Mail\AdApprovedNotificationToAdvertiser($details));
        
        return back()->with('success', 'Advertisement approved successfully.');
    }

    /**
     * To disapprove new adv and refund payment
     * 
     * @param int $id
     * @return View
     */
    public function disapproveNewAdvertisement($id)
    {
        $advertisement = Advertisement::find($id);
        $advertiser = AdvUser::find($advertisement['advertiser_id']);
        $ads_payments = AdsPayment::where([
            ['service_id', $advertisement['service_id']],
        ])->first();

        $details = array();
        $details['advertiser']['name'] = $advertiser['name'];
        $details['advertiser']['email'] = $advertiser['email'];
        $details['advertiser']['contact'] = $advertiser['contact'];
        $details['advertiser']['company_name'] = $advertiser['company_name'];
        $details['advertisement']['id'] = $advertisement['id'];
        $details['advertisement']['advertisement'] = $advertisement['advertisement'];

        $ad_services = AdvAvailableService::where([
            ['id', $advertisement['service_id']],
        ])->first();

        $details['advertisement']['plan'] = AdvPlan::find($ad_services['plan']);
        $details['advertisement']['plan_fixed_service'] = AdvPlFixedService::where('plan_id', $details['advertisement']['plan']['id'])->first();
        $details['advertisement']['plan_duration'] = AdvPlDuration::find($ad_services['duration']);
        if (!is_null($ad_services['opt_services'])) {
            $details['advertisement']['plan_opt_service'] = AdvPlOptService::find($ad_services['opt_services']);
        } else {
            $details['advertisement']['plan_opt_service'] = null;
        }
        
        $total_opt_services_cost = 0;
        $multiplying_factor = '';
        if (!is_null($details['advertisement']['plan_opt_service'])) {
            if ($details['advertisement']['plan_duration']['plan_duration'] == 'Weekly') {
                $multiplying_factor = '*1';
                $total_opt_services_cost += $details['advertisement']['plan_opt_service']['aditional_price'] * 1;
            } 
            if ($details['advertisement']['plan_duration']['plan_duration'] == 'Monthly') {
                $multiplying_factor = '*4';
                $total_opt_services_cost += $details['advertisement']['plan_opt_service']['aditional_price'] * 4;
            } 
            if ($details['advertisement']['plan_duration']['plan_duration'] == 'Quarterly') {
                $multiplying_factor = '*13';
                $total_opt_services_cost += $details['advertisement']['plan_opt_service']['aditional_price'] * 13;
            } 
            if ($details['advertisement']['plan_duration']['plan_duration'] == 'Half Yearly') {
                $multiplying_factor = '*26';
                $total_opt_services_cost += $details['advertisement']['plan_opt_service']['aditional_price'] * 26;
            } 
            if ($details['advertisement']['plan_duration']['plan_duration'] == 'Yearly') {
                $multiplying_factor = '*52';
                $total_opt_services_cost += $details['advertisement']['plan_opt_service']['aditional_price'] * 52;
            }
        }
        $details['advertisement']['multiplying_factor'] = $multiplying_factor;
        $details['advertisement']['total_opt_services_cost'] = $total_opt_services_cost;

        $details['advertisement']['transaction_id'] = $ads_payments['transaction_id'];
        $details['advertisement']['amount'] = $ads_payments['amount'];

        $details['advertisement']['publishable_locations'] = AdvPublicationLocation::where('service_id', $advertisement['service_id'])->get();

        //refunding the holded payment
        $stripe = new \Stripe\StripeClient(env('STRIPE_SECRET'));
        $stripe->refunds->create([
            'charge' => $ads_payments['transaction_id'],
        ]);
        
        $advertiser->delete();
        \Mail::to($details['advertiser']['email'])->send(new \App\Mail\AdDisapprovedNotificationToAdvertiser($details));

        return back()->with('success', 'Advertisement disapproved successfully.');
    }

    public function deleteAdvertisementComment(Request $request)
    {
        $comment_id = $request->comment_id;
        //$comment_id = json_encode($comment_id);
        $comment = AdvComment::find($comment_id);
        $comment->delete();
        return 'ok';
    }
}
