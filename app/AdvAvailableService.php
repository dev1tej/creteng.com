<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdvAvailableService extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 
        'advertiser', 'plan', 'duration', 'opt_services', 'status'
    ];

    /**
     * Get the plan.
     */
    public function advertiser()
    {
        return $this->belongsTo('App\AdvUser', 'advertiser', 'id');
    }
    /**
     * Get the plan.
     */
    public function plan()
    {
        return $this->belongsTo('App\AdvPlan', 'plan', 'id');
    }
    /**
     * Get the plan.
     */
    public function duration()
    {
        return $this->belongsTo('App\AdvPlDuration', 'duration', 'id');
    }
    /**
     * Get the plan.
     */
    public function optServices()
    {
        return $this->belongsTo('App\AdvPlOptService', 'opt_services', 'id');
    }
}
