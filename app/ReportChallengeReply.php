<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReportChallengeReply extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 
        'user_id', 'challenge_reply_id', 'report_text'
    ];

    /**
     * Get the user that owns the comment.
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    /**
     * Get the post that owns the comment.
     */
    public function challengeReply()
    {
        return $this->belongsTo('App\PostsTaggedReply', 'challenge_reply_id', 'id');
    }
}
