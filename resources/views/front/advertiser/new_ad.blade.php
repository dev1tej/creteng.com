@extends('front.layouts.master')
@section('content')
<style type="text/css">
.fixed-top {
  position: relative;
}
#footer .footer-top {
 padding: 50px 0 0 0;
 top: 0; 
}
#header.header-scrolled{
  position: fixed;
  top: 0;
  right: 0;
  left: 0;
  z-index: 1030;
}

form div ul {
  box-sizing: border-box;
}

form div ul {
  list-style-type: none;
  padding: 0;
  margin: 0;
}

form div ul li {
  border: 1px solid #ddd;
  margin-top: -1px; /* Prevent double borders */
  background-color: #f6f6f6;
  padding: 12px;
  text-decoration: none;
  font-size: 18px;
  color: black;
  display: block;
  position: relative;
}

form div ul li:hover {
  background-color: #eee;
}

.close {
  cursor: pointer;
  position: absolute;
  top: 50%;
  right: 0%;
  padding: 12px 16px;
  transform: translate(0%, -50%);
}

.close:hover {background: #bbb;}
</style>
<section class="inner-page">
@if(Session::has('flash_message'))
  <div class="alert alert-danger"><span class="glyphicon"></span><em> {!! session('flash_message') !!}</em></div>
@endif
@if(Session::has('flash_success_message'))
  <div class="alert alert-success"><span class="glyphicon"></span><em> {!! session('flash_success_message') !!}</em></div>
@endif
@if ($message = Session::get('success'))
<div class="alert alert-success">
  <p>{{ $message }}</p>
</div>
@endif

@if ($message = Session::get('danger'))
<div class="alert alert-danger">
  <p>{{ $message }}</p>
</div>
@endif
<div class="container">
  <div class="row">
    <div class="col-lg-12 margin-tb">
      <div class="pull-left">
        <h2>Create New Ad</h2>
        <span class="text-danger font-weight-bold">All fields marked with * are required</span>
      </div>
    </div>
  </div>
  @if (count($errors) > 0)
  <div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
      @foreach ($errors->all() as $error)
      <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
  @endif
  <form action="{{ route('postCreateNewAd') }}" method="post" enctype="multipart/form-data" onsubmit = "return(validate());">
    @csrf
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12">
        
          <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6">
              <div class="form-group">
                <label class="formlable"><strong><span class="text-danger font-weight-bold">*</span>Select Advertisement Type:</strong></label>
                <input type="text" name="advertiser_id" class="form-control" value="{{ $advertiser_id }}" hidden/>
                <input type="text" name="ad_type" class="form-control" value="{{ ucfirst($ad_type) }}" readonly/>
              </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6">
              <div class="form-group">
                <label class="formlable"><strong><span class="text-danger font-weight-bold">*</span>Upload Advertisement:</strong></label>
                <input type="file" id="advertisement" name="advertisement"/>
                <button class="btn btn-info btn-sm" id="clear_file"> Clear Upload</button>
                <span id="advertisement_error" class="text-danger font-weight-bold"></span>
                <p>( {{ $plan_fixed_services->fixed_services }} )</p>
                <br>
              </div>
            </div>
          </div>
        
        
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
              <div class="form-group">
                <strong>Advertisement Description (Maximum 250 characters):</strong>
                <textarea maxlength="250" class="form-control" id="ad_description" name="ad_description" rows="3" placeholder="Enter advertisement description....."></textarea>
                <span id="ad_description_error" class="text-danger font-weight-bold"></span>
                <span class="text-info font-weight-bold"><span id="rchars">250</span> Character(s) Remaining</span>
                <br>
              </div>
            </div> 
          </div>
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
              <div class="form-group">
                <strong>Website URL (The user will be redirect to this URL once they click the advertisement in the mobile app):</strong>
                <input type="text" name="website_url" class="form-control" placeholder="Enter Website URL"/>
                <br>
              </div>
            </div> 
          </div>
        
          <div class="row">
            <label for="is_app" class="col-sm-6 col-form-label">Is publishable advertisement about an App ? </label>
            <div class="col-sm-6">
              <input type="checkbox" name="is_app"/>
              <span class="text-info font-weight-bold">*Select if it is true.</span>
            </div>
          </div>
          <hr/>
          <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6">
              <div class="form-group">
                <label for="google_store_url" class="col-sm-6 col-form-label">Google PlayStore URL:</label>
                <input type="text" name="google_store_url" class="form-control" placeholder="Enter PlayGoogle Store URL"/>
              </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6">
              <div class="form-group">
                <label for="apple_store_url" class="col-sm-6 col-form-label">Apple Store URL:</label>
                <input type="text" name="apple_store_url" class="form-control" placeholder="Enter Apple Store URL"/>
              </div>
            </div>
          </div>
        
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12 text-center">
        <button type="submit" id="submit"class="btn btn-primary">Submit</button>
      </div>
    </div>
  </form>
</div>
</div>

<script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script>
  $(document).ready(function(){
    $("#clear_file").click(function(event){
      event.preventDefault();
      $("#advertisement").val('');
    });
    var maxLength = 250;
    $('textarea').keyup(function() {
      var textlen = maxLength - $(this).val().length;
      $('#rchars').text(textlen);
    });
  });
</script>
<script type="text/javascript">
  // Form validation.
  function validate() {
    var ad_description = document.getElementById('ad_description').value;

    if ((ad_description.length > 250)) {
      document.getElementById('ad_description_error').innerHTML = " ** Description length can not be more than 250 characters";
      return false;
    } else {
      document.getElementById('ad_description_error').innerHTML = "";
    }
    return( true );
  }
</script>
@endsection

