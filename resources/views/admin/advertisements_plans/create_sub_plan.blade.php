@extends('admin.layouts.master')
@section('content')
<div class="container">
  <div class="row">
    <div class="col-lg-12 margin-tb pull-right">
      <a class="btn btn-primary" href="{{ route('showAllAdPlan') }}"> Back To Plan Page</a>
    </div>
    <div class="col-lg-12 margin-tb">
      <div class="pull-left">
        <h2>Create Advertisement Sub Plans</h2>
      </div>
    </div>
  </div>
  @if (count($errors) > 0)
  <div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
      @foreach ($errors->all() as $error)
      <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
  @endif
  @if (isset($success))
  <div class="alert alert-success">
    <p>{{ $success }}</p>
  </div>
  @endif
  @if (isset($danger))
  <div class="alert alert-danger">
    <p>{{ $danger }}</p>
  </div>
  @endif
  <div class="container">
    <h3 align="center">Plan Name:  {{ $ad_plan->plan_name }}</h3>
    <br />
    <h4 align="center">Add Sub Plans</h4>
    <br />
    <form method="post" action="{{ route('addAdSubPlan') }}" id="insert_form">
     {{ csrf_field() }}
     <input name="plan_id" type="hidden" class="form-control" id="plan_id" value="{{ $ad_plan->id }}">
     <div class="table-repsonsive">
      <span id="error"></span>
      <table class="table table-bordered" id="item_table">
       <tr>
        <th>Sub Plan</th>
        <th>Price (in USD)</th>
        <th><button type="button" name="add" class="btn btn-success btn-sm add"><span class="glyphicon glyphicon-plus"></span>+</button></th>
       </tr>
      </table>
      <div align="center">
       <input type="submit" name="submit" class="btn btn-primary" value="Insert" />
      </div>
     </div>
    </form>
   </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script>
  $(document).ready(function(){
   
    $(document).on('click', '.add', function(){
      var html = '';
      html += '<tr>';
      html += '<td>';
      html += '<select type="text" name="duration_name[]" class="form-control duration_name">';
      html += '<option value="Weekly">Weekly</option>';
      html += '<option value="Monthly">Monthly</option>';
      html += '<option value="Quarterly">Quarterly</option>';
      html += '<option value="Half Yearly">Half Yearly</option>';
      html += '<option value="Yearly">Yearly</option>';
      html += '</select>';
      html += '</td>';
      html += '<td><input type="number" step=".01" name="duration_price[]" class="form-control duration_price" /></td>';
      html += '<td><button type="button" name="remove" class="btn btn-danger btn-sm remove"><span class="glyphicon glyphicon-minus"></span>-</button></td></tr>';
      $('#item_table').append(html);
    });
   
    $(document).on('click', '.remove', function(){
      $(this).closest('tr').remove();
    });
   
    $('#insert_form').on('submit', function(event){
      event.preventDefault();
      var error = '';
      var count = 1;
      $('.duration_name').each(function(){
        if($(this).val() == '')
        {
          error += "<p>Enter Sub Plan Name at "+count+" Row</p>";
          //$('#error').html(error);
          //alert(error);
          return false;
        }
        count = count + 1;
      });

      var count = 1;
      $('.duration_price').each(function(){
        if($(this).val() == '')
        {
          error += "<p>Enter Sub Plan Price at "+count+" Row</p>";
          //$('#error').html(error);
          //alert(error);
          return false;
        }
        count = count + 1;
      });

      //$("#insert_form").unbind("submit");
      //$(this).unbind('submit').submit();
    
      
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });
      
      var form_data = $(this).serialize();
      //alert(form_data);
      if(error == '') {
        $.ajax({
          type: "POST",
          url: "{{ route('addAdSubPlan') }}",
          data: form_data,
          cache: false,
          success: function(response) {
            if(response == 'ok') {
              $('#item_table').find("tr:gt(0)").remove();
              $('#error').html('<div class="alert alert-success">Sub Plan Details Saved</div>');
            }
          }
          /*
          error: function(error) {
            $('#error').html('<div class="alert alert-danger">' +error+ '</div>');
          }*/
        });
      }
      else {
        $('#error').html('<div class="alert alert-danger">'+error+'</div>');
      }
      
    });
   
  });
  </script>
  @endsection