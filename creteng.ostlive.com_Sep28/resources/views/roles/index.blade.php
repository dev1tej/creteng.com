@extends('admin.layouts.master')


@section('content')
<div class="container">
	<div class="row">
		<div class="col-lg-12 margin-tb">
			<div class="pull-left">
				<h2>Role Management</h2>
			</div>
		</div>
	</div>


	@if ($message = Session::get('success'))
		<div class="alert alert-success">
			<p>{{ $message }}</p>
		</div>
	@endif


	<table class="table table-bordered">
	  <tr>
		 <th>No</th>
		 <th>Name</th>
		 <th width="280px">Action</th>
	  </tr>
		@foreach ($roles as $key => $role)
		<tr>
			<td>{{ ++$i }}</td>
			<td>{{ $role->name }}</td>
			<td>
				
				@can('role-edit')
					<a class="btn btn-primary" href="{{ route('roles.edit',$role->id) }}">Edit</a>
				@endcan
				
			</td>
		</tr>
		@endforeach
	</table>
	{!! $roles->render() !!}

</div>
@endsection
