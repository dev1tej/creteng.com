
(function($) {
    'use strict';
    
    /*-------------------------------------------
      01. jQuery MeanMenu
    --------------------------------------------- */
    
    $('.mobile-menu nav').meanmenu({
      meanMenuContainer: '.mobile-menu-area',
      meanScreenWidth: "767",
      meanRevealPosition: "right",
    });


	jQuery(document).ready(function($) {
	  jQuery(".search_icon, .close_button").click(function(event) {
		jQuery(".spicewpsearchform").slideToggle();
		event.preventDefault();
	  });

	  jQuery(document).keydown(function(e) {
		if (e.keyCode == 100) {
		  //$(modal_or_menu_element).closeElement();
		  jQuery(".spicewpsearchform").hide();
		}
	  });
	});


	$(document).ready(function() {	

		var id = '#dialog';
		
		if(jQuery.cookie('homepopup') != 'seen'){
			var date = new Date();
			date.setTime(date.getTime() + (24 * 60 * 60 * 1000));
			jQuery.cookie('homepopup', 'seen', { expires: date });
				
			//Get the screen height and width
			var maskHeight = $(document).height();
			var maskWidth = $(window).width();
				
			//Set heigth and width to mask to fill up the whole screen
			$('#mask').css({'width':maskWidth,'height':maskHeight});

			//transition effect
			$('#mask').fadeIn(500);	
			$('#mask').fadeTo("slow",0.9);	
				
			//Get the window height and width
			var winH = $(window).height();
			var winW = $(window).width();
						  
			//Set the popup window to center
			$(id).css('top',  winH/2-$(id).height()/2);
			$(id).css('left', winW/2-$(id).width()/2);
				
			//transition effect
			$(id).fadeIn(500); 	
				
			//if close_pop button is clicked
			$('.window .close_pop').click(function (e) {
				//Cancel the link behavior
				//e.preventDefault();

				$('#mask').hide();
				$('.window').hide();
			});

			//if mask is clicked
			$('#mask').click(function () {
				$(this).hide();
				$('.window').hide();
			});
			
			//alert($.cookie('homepopup'));
		}
		
	});

    /*-------------------------------------------
      02. wow js active
    --------------------------------------------- */
    new WOW().init();
    /*-------------------------------------------
      03. Portfolio  Masonry (width)
    --------------------------------------------- */ 
  $('.our-portfolio-page').imagesLoaded( function() {
        // filter items on button click
        $('#our-filters').on('click', 'li', function () {
            var filterValue = $(this).attr('data-filter');
            $containerpage.isotope({ filter: filterValue });
        });
        // change is-checked class on buttons
        $('#our-filters li').on('click', function () {
            $('#our-filters li').removeClass('is-checked');
            $(this).addClass('is-checked');
            var selector = $(this).attr('data-filter');
            $containerpage.isotope({ filter: selector });
            return false;
        });
        var $containerpage = $('.our-portfolio-page');
        $containerpage.isotope({
            itemSelector: '.pro-item',
            filter: '*',
            transitionDuration: '0.7s',
            masonry: {
                columnWidth: '.pro-item'
            }
          });
      });



/*-------------------------------------------
  04. Sticky Header
--------------------------------------------- */ 
  $(window).on('scroll',function() {    
     var scroll = $(window).scrollTop();
     if (scroll < 245) {
      $("#sticky-header-with-topbar").removeClass("scroll-header");
     }else{
      $("#sticky-header-with-topbar").addClass("scroll-header");
     }
    });
/*--------------------------
  05. ScrollUp
---------------------------- */
$.scrollUp({
    scrollText: '<i class="zmdi zmdi-chevron-up"></i>',
    easingType: 'linear',
    scrollSpeed: 900,
    animation: 'fade'
});

/*--------------------------------
  06. Slider Area
-----------------------------------*/
  $('.slider-activation').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      draggable: true,
      fade: false,
      dots: true,
  });
 
/*--------------------------------
  07. Testimonial Area
-----------------------------------*/
	$(document).ready(function() {
	  $(".testimonial-carousel").slick({
		infinite: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		autoplay: true,
		dots: true,
		arrows: false
	  });
	});

/*---------------------
 price slider
--------------------- */  
	
    $( "#slider-range" ).slider({
        range: true,
        min: 40,
        max: 600,
        values: [ 60, 570 ],
        slide: function( event, ui ) {
        $( "#amount" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
        }
    });
    $( "#amount" ).val( "$" + $( "#slider-range" ).slider( "values", 0 ) +
    " - $" + $( "#slider-range" ).slider( "values", 1 ) );    
    
    
    
    // tooltip
    $('.ratings a').tooltip();
    $('[data-toggle="tooltip"]').tooltip();
    
    
    /*--
    Simple Lens and Lightbox
    ------------------------*/
	$('.simpleLens-lens-image').simpleLens({
		loading_image: 'images/loading.gif'
	}); 
    
    
    /*--
    Pro Slider for Pro Details
    ------------------------*/
    $(".pro-img-tab-slider").owlCarousel({
        autoPlay: false,
        loop: true,
        slideSpeed: 2000,
        dots: false,
        nav: false,
        items: 3,
        responsive: {
            0: {
                items: 1
            },
            480: {
                items: 1
            },
            768: {
                items: 3
            },
            992: {
                items: 4
            },
            1200: {
                items: 4
            }
        }
    });
    
    
    
})(jQuery);







