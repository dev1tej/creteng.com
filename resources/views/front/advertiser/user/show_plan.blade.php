@extends('front.layouts.master')
@section('content')
<div class="container">
  <div class="row">
    <div class="card" style="height: 100%; width: 100%;">
      <div class="card-body">
        <h2><strong>My Plan</strong></h2>
        @if(($my_plan != '') && ($ad_services->status == true))
        <p class="card-text">Plan: <span>{{ $my_plan['plan']['plan_name'] }}</span></p>
        <p class="card-text">Plan Duration: <span>{{ $my_plan['plan_duration']['plan_duration'] }}</span></p>
        <p class="card-text">Plan Duration Cost: <span>${{ $my_plan['plan_duration']['price'] }}</span></p>
        <p class="card-text">Fixed Services: <span>{{ $my_plan['plan_fixed_service']['fixed_services'] }}</span></p>
        @if(!is_null($my_plan['plan_opt_service']))
        <p class="card-text">Optional Services: <span>{{ $my_plan['plan_opt_service']['optional_services'] }}</span></p>
        <p class="card-text">Optional Services Cost: <span>${{ $my_plan['plan_opt_service']['aditional_price'] }} {{ $my_plan['plan_opt_service']['aditional_price_duration'] }}</span></p>
        @endif
        <p class="card-text">Ad Publishable Locations:<br/>
          <span>
            @php $no_of_publication_location = 0; @endphp
            @foreach ($my_plan['publishable_locations'] as $publishable_location)
            @php $no_of_publication_location++; @endphp
            {{ $no_of_publication_location }}) Country: {{  $publishable_location['country'] }}, State: {{  $publishable_location['state'] }}<br/>
            @endforeach
          </span>
        </p>
        <p class="card-text">Number of Publication Location: <span>{{ $no_of_publication_location }}</span></p>
        <p class="card-text">Publish Date: <span>{{ $my_plan['payment']['publish_date'] }}</span></p>
        <p class="card-text">Expiry Date: <span>{{ $my_plan['payment']['expiry_date'] }}</span></p>
        <p class="card-text">Transaction Id: <span>{{ $my_plan['payment']['transaction_id'] }}</span></p>
        @if(!is_null($my_plan['plan_opt_service']))
        <p class="card-text">Total Optional Services Cost: <span>${{ $total_opt_services_cost }}</span></p>
        <p class="card-text">Total Paid: <span>({{ $my_plan['plan_duration']['price'] }} + {{ $my_plan['plan_opt_service']['aditional_price'] }} {{ $my_plan['plan_opt_service']['aditional_price_duration'] }} {{ $multiplying_factor }}) * {{ $no_of_publication_location }} = ${{ $my_plan['payment']['amount'] }}</span></p>
        @else
        <p class="card-text">Total Paid: <span>${{ $my_plan['payment']['amount'] }}</span></p>
        @endif    
        @elseif(($ad_services->status == false) && ($my_plan['payment']['status'] == 'Charged')) 
           Your Subscription has expired.
        @else
           Your Subscription is under evaluation. Please wait for approval. 
        @endif
      </div>
    </div>
  </div>
</div>
@endsection