@extends('layouts.app')
@section('content')
<style type="text/css">
.fixed-top {
  position: relative;
}
#footer .footer-top {
 padding: 50px 0 0 0;
 top: 0; 
}
#header.header-scrolled{
  position: fixed;
  top: 0;
  right: 0;
  left: 0;
  z-index: 1030;
}

form div ul {
  box-sizing: border-box;
}

form div ul {
  list-style-type: none;
  padding: 0;
  margin: 0;
}

form div ul li {
  border: 1px solid #ddd;
  margin-top: -1px; /* Prevent double borders */
  background-color: #f6f6f6;
  padding: 12px;
  text-decoration: none;
  font-size: 18px;
  color: black;
  display: block;
  position: relative;
}

form div ul li:hover {
  background-color: #eee;
}

.close {
  cursor: pointer;
  position: absolute;
  top: 50%;
  right: 0%;
  padding: 12px 16px;
  transform: translate(0%, -50%);
}

.close:hover {background: #bbb;}
</style>
<section class="inner-page">
@php
  use App\Http\Controllers\Auth\AdvertiserRegisterController;
  use App\Countries;  
  use App\States; 
  $usAndEuCountries = new AdvertiserRegisterController;
  $usAndEuCountries = $usAndEuCountries->usAndEuCountries();
  $countries_details = Countries::all();
  $countries = array();
  foreach($countries_details as $country) {
    if (in_array($country->name, $usAndEuCountries)) {
      $countries[] = $country->name;
    }
  }
  
  $united_states = Countries::where('name', 'UNITED STATES')->first();
  $states_details = States::where('country_id', $united_states->id)->get();
  $states = array();
  foreach ($states_details as $state) {
    $states[] = $state->name;
  }
  rsort($states);
@endphp
@if(Session::has('flash_message'))
  <div class="alert alert-danger"><span class="glyphicon"></span><em> {!! session('flash_message') !!}</em></div>
@endif
@if ($message = Session::get('success'))
<div class="alert alert-success">
  <p>{{ $message }}</p>
</div>
@endif
@if ($message = Session::get('danger'))
<div class="alert alert-danger">
  <p>{{ $message }}</p>
</div>
@endif
<div class="container">
  <div class="row">
    <div class="col-lg-12 margin-tb">
      <div class="pull-left">
        <h2>Step 3: Add Advertisement</h2>
        <span class="text-danger font-weight-bold">All fields marked with * are required</span>
      </div>
    </div>
  </div>
  @if (count($errors) > 0)
  <div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
      @foreach ($errors->all() as $error)
      <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
  @endif
  <form action="{{ route('postRegisterAd') }}" method="post" enctype="multipart/form-data" onsubmit = "return(validate());">
    @csrf
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12">
        <h4><span class="text-danger font-weight-bold">*</span>Publication Locations</h4>
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12">
            <table class="table table-bordered table-striped table-hover">
              <thead>
                <tr>
                  <th>Serial No.</th>
                  <th><span class="text-danger font-weight-bold">*</span>Select Ad Publication Country</th>
                  <th><span class="text-danger font-weight-bold">*</span>Select Ad Publication States/Countries</th>
                </tr>
              </thead>
              <tbody>
                {{--@php $no_of_publication_location = 2 @endphp--}}
                @for($i = 1; $i <= $no_of_publication_location; $i++)
                <tr>
                  <td>{{ $i }}</td>
                  <td id="country{{ $i }}">
                    <select name="countries[]" id="country_list" class="form-control country">
                      @foreach ($countries as $country)
                      @if($country == 'UNITED STATES')
                      <option selected value="{{ $country }}" {{ old('country') == $country ? 'selected' : '' }}>{{ $country }}</option>
                      @else
                      <option value="{{ $country }}" {{ old('country') == $country ? 'selected' : '' }}>{{ $country }}</option>
                      @endif
                      @endforeach
                    </select>
                  </td>
                  <td id="state{{ $i }}">
                    <select name="states[]" id="state_list" class="form-control state">
                      @foreach ($states as $state)
                      <option selected value="{{ $state }}" {{ old('state') == $state ? 'selected' : '' }}>{{ $state }}</option>
                      @endforeach
                    </select>
                  </td>
                </tr>
                @endfor
              </tbody>
              <tfoot></tfoot>
            </table>
            <span id="location_error" class="text-danger font-weight-bold"></span>
          </div>
        </div>
        <div class="card">
          <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6">
              <div class="form-group">
                <strong><span class="text-danger font-weight-bold">*</span>Select Advertisement Type:</strong>
                <input type="text" name="advertiser_id" class="form-control" value="{{ $advertiser_id }}" hidden/>
                <input type="text" name="plan_id" class="form-control" value="{{ $plan_id }}" hidden/>
                <input type="text" name="sub_plan_id" class="form-control" value="{{ $sub_plan_id }}" hidden/>
                @if(isset($opt_service_ids))
                @foreach($opt_service_ids as $opt_service_id) 
                <input type="text" name="opt_service_ids[]" class="form-control" value="{{ $opt_service_id }}" hidden/>
                @endforeach
                @endif 
                <input type="text" name="ad_type" class="form-control" value="{{ ucfirst($ad_type) }}" readonly/>
              </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6">
              <div class="form-group">
                <strong><span class="text-danger font-weight-bold">*</span>Upload Advertisement:</strong>
                <input type="file" id="advertisement" name="advertisement"/>
                <button class="btn btn-info btn-sm" id="clear_file"> Clear Upload</button>
                <span id="advertisement_error" class="text-danger font-weight-bold"></span>
                {{ $plan_fixed_services->fixed_services }}
                <br>
              </div>
            </div>  
          </div>
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
              <div class="form-group">
                <strong>Advertisement Description (Maximum 250 characters):</strong>
                <textarea maxlength="250" class="form-control" id="ad_description" name="ad_description" rows="3" placeholder="Enter advertisement description....."></textarea>
                <span id="ad_description_error" class="text-danger font-weight-bold"></span>
                <span class="text-info font-weight-bold"><span id="rchars">250</span> Character(s) Remaining</span>
                <br>
              </div>
            </div> 
          </div>
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
              <div class="form-group">
                <strong>Website URL (The user will be redirect to this URL once they click the advertisement in the mobile app):</strong>
                <input type="text" name="website_url" class="form-control" placeholder="Enter Website URL"/>
                <br>
              </div>
            </div> 
          </div>
        </div>
        <div class="card">
          <div class="row">
            <label for="is_app" class="col-sm-6 col-form-label">Is publishable advertisement about an App ? </label>
            <div class="col-sm-6">
              <input type="checkbox" name="is_app"/>
              <span class="text-info font-weight-bold">Select if it is true.</span>
            </div>
          </div>
          <hr/>
          <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6">
              <div class="form-group">
                <label for="google_store_url" class="col-sm-6 col-form-label">Google PlayStore URL:</label>
                <input type="text" name="google_store_url" class="form-control" placeholder="Enter Google PlayStore URL"/>
              </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6">
              <div class="form-group">
                <label for="apple_store_url" class="col-sm-6 col-form-label">Apple Store URL:</label>
                <input type="text" name="apple_store_url" class="form-control" placeholder="Enter Apple Store URL"/>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12 text-center">
        <button type="submit" class="btn btn-primary" id="submit">Next</button>
      </div>
      {{--<div class="col-xs-12 col-sm-12 col-md-12">
        <div class="col-xs-6 col-sm-6 col-md-6" style="text-align: left; margin-left: 0px;">
          <button type="submit" class="btn btn-primary previous"><span class="glyphicon glyphicon-chevron-left"></span>Prev</button>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6" style="text-align: right; margin-right: 0px;">
          <button type="submit" class="btn btn-primary submit">Next<span class="glyphicon glyphicon-chevron-right"></span></button>
        </div>
      </div>--}}
    </div>
  </form>
</div>
</div>
@endsection
<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script>
  $(document).ready(function() {
    $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
    });

    $('.country').on('change', function() {
      var country = this.value;
      var td_id = $(this).closest("td").attr("id");
      //console.log(td_id);
      
      $.ajax({
        type: "POST",
        url: "{{ route('countryStateList') }}",
        data: { country : country },
    
        success: function(data) {
          var td = $(document).find("#"+td_id).next("td");
          var select = $(document).find("#"+td_id).next("td").children("select");
          
          var html = '';
          html += '<select name="states[]" class="form-control state">';
          $.each(data, function(index, state) {
            html += '<option selected value="' +state+ '">' +state+ '</option>';
          });
          html += '</select>';

          select.remove();
          td.append(html);
        },
        error: function(error) {
            $('#location_error').text(error);
        },
        /*complete: function(){
            $('.ajax-loader').css("visibility", "hidden");
        }*/
      });
    });   
    
    
    $("#clear_file").click(function(event){
      event.preventDefault();
      $("#advertisement").val('');
    });

    var maxLength = 250;
    $('textarea').keyup(function() {
      var textlen = maxLength - $(this).val().length;
      $('#rchars').text(textlen);
    });

    /*$('.previous').click(function(event){
      event.preventDefault();
      return window.history.go(-1);
    });*/
  });
</script>
<script type="text/javascript">
  // Form validation.
  function validate() {
    var country_list = document.getElementById('country_list');
    var state_list = document.getElementById('state_list');
    var advertisement = document.getElementById('advertisement').value;
    var ad_description = document.getElementById('ad_description').value;

    //country
    if (country_list == "") {
      document.getElementById('location_error').innerHTML = " ** Country field is require";
      return false;
    } else {
      document.getElementById('location_error').innerHTML = "";
    }

    //state
    if (state_list == "") {
      document.getElementById('location_error').innerHTML = " ** State field is require";
      return false;
    } else {
      document.getElementById('location_error').innerHTML = "";
    }

    //advertisement
    if (advertisement == "") {
      document.getElementById('advertisement_error').innerHTML = " ** Please upload an advertisement";
      return false;
    } else {
      document.getElementById('advertisement_error').innerHTML = "";
    }

    //ad_description
    if ((ad_description.length > 250)) {
      document.getElementById('ad_description_error').innerHTML = " ** Description length can not be more than 250 characters";
      return false;
    } else {
      document.getElementById('ad_description_error').innerHTML = "";
    }
    
    return true;
  } 
</script>
