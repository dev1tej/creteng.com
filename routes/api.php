<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'json_response',], function () {
    
  // public routes
  //registration with email verification
  Route::post('/register', 'Api\Auth\AuthController@register')->name('register.api');
  Route::get('email/resend', 'Api\Auth\VerificationController@resend')->name('verification.resend');
  Route::get('email/verify/{id}/{hash}', 'Api\Auth\VerificationController@verify')->name('verification.verify');
  
  Route::post('/login', 'Api\Auth\AuthController@login')->name('login.api');
  Route::post('/forgetPassword', 'Api\Auth\AuthController@forgetPassword')->name('forgetPassword.api');
  //Route::post('/resetPassword', 'Api\Auth\AuthController@resetPassword')->name('resetPassword.api');

  //onboarding images routes
  Route::get('/showAllOnboardingImage', 'Api\GetResourceDetailController@showAllOnboardingImage')->name('showAllOnboardingImage.api');
  // protected routes
  Route::middleware('auth:api')->group(function() {
    Route::post('/logout', 'Api\Auth\AuthController@logout')->name('logout.api');
    Route::post('/changePassword', 'Api\Auth\AuthController@changePassword')->name('changePassword.api');
    Route::get('/viewMyProfile', 'Api\UserController@viewMyProfile')->name('viewMyProfile.api');
    Route::post('/updateMyProfile', 'Api\UserController@updateMyProfile')->name('updateMyProfile.api');
    Route::post('/searchUser', 'Api\UserController@searchUser')->name('searchUser.api');
    Route::get('/viewUserProfile/{id}', 'Api\UserController@viewUserProfile')->name('viewUserProfile.api');
    Route::get('/viewUserPost/{user_id}', 'Api\UserController@viewUserPost')->name('viewUserPost.api');

    Route::get('/sendFriendRequest/{id}', 'Api\UserController@sendFriendRequest')->name('sendFriendRequest.api');
    Route::get('/addFriendRequest/{id}', 'Api\UserController@addFriendRequest')->name('addFriendRequest.api');
    Route::get('/denyFriendRequest/{id}', 'Api\UserController@denyFriendRequest')->name('denyFriendRequest.api');
    Route::get('/removeFriend/{id}', 'Api\UserController@removeFriend')->name('removeFriend.api');
    Route::get('/viewFriendsList', 'Api\UserController@viewFriendsList')->name('viewFriendsList.api');
    Route::get('/viewReceivedFriendRequestList', 'Api\UserController@viewReceivedFriendRequestList')->name('viewReceivedFriendRequestList.api');
    Route::get('/viewSendFriendRequestList', 'Api\UserController@viewSendFriendRequestList')->name('viewSendFriendRequestList.api');

    //posts routes
    Route::post('/addPost', 'Api\PostController@addPost')->name('addPost.api');
    Route::post('/editPost', 'Api\PostController@editPost')->name('editPost.api');
    Route::delete('/deletePost/{id}', 'Api\PostController@deletePost')->name('deletePost.api');
    Route::post('/searchPost', 'Api\PostController@searchPost')->name('searchPost.api');
    Route::post('/likePost', 'Api\PostController@likePost')->name('likePost.api');
    
    Route::post('/commentPost', 'Api\PostController@commentPost')->name('commentPost.api');
    Route::delete('/deletePostComment/{id}', 'Api\PostController@deletePostComment')->name('deletePostComment.api');
    Route::post('/postCommentReply', 'Api\PostController@postCommentReply')->name('postCommentReply.api');
    Route::delete('/deletePostCommentReply/{id}', 'Api\PostController@deletePostCommentReply')->name('deletePostCommentReply.api');
    Route::get('/viewPostVideo/{post_id}', 'Api\PostController@viewPostVideo')->name('viewPostVideo.api');

    Route::get('/mySavedPostAndReply', 'Api\PostController@mySavedPostAndReply')->name('mySavedPostAndReply.api');
    Route::post('/savePostList', 'Api\PostController@savePostList')->name('savePostList.api');
    Route::post('/removeSavePostList', 'Api\PostController@removeSavePostList')->name('removeSavePostList.api');
    Route::get('/showAllPost', 'Api\PostController@showAllPost')->name('showAllPost.api');
    Route::get('/showMyPost', 'Api\PostController@showMyPost')->name('showMyPost.api');
    Route::get('/showMyReceivedChallenges', 'Api\PostController@showMyReceivedChallenges')->name('showMyReceivedChallenges.api');
    Route::get('/showUsersReceivedChallenges/{id}', 'Api\PostController@showUsersReceivedChallenges')->name('showUsersReceivedChallenges.api');

    Route::get('/sendChallengeList', 'Api\PostController@sendChallengeList')->name('sendChallengeList.api');
    Route::get('/pendingReceivedChallengeList', 'Api\PostController@pendingReceivedChallengeList')->name('pendingReceivedChallengeList.api');
    Route::post('/respondToChallenge', 'Api\PostController@respondToChallenge')->name('respondToChallenge.api');
    Route::get('/viewPostLikedBy/{post_id}', 'Api\PostController@viewPostLikedBy')->name('viewPostLikedBy.api');

    //challenge routes
    Route::post('/replyToChallenge', 'Api\ChallengeController@replyToChallenge')->name('replyToChallenge.api');
    Route::post('/commentToChallengeReply', 'Api\ChallengeController@commentToChallengeReply')->name('commentToChallengeReply.api');
    Route::get('/deleteChallengeReply/{reply_id}', 'Api\ChallengeController@deleteChallengeReply')->name('deleteChallengeReply.api');
    Route::post('/editChallengeReply', 'Api\ChallengeController@editChallengeReply')->name('editChallengeReply.api');
    Route::post('/likeReplyToChallenge', 'Api\ChallengeController@likeReplyToChallenge')->name('likeReplyToChallenge.api');
    Route::get('/saveReplyToChallenge/{reply_id}', 'Api\ChallengeController@saveReplyToChallenge')->name('saveReplyToChallenge.api');
    Route::get('/unsaveReplyToChallenge/{reply_id}', 'Api\ChallengeController@unsaveReplyToChallenge')->name('unsaveReplyToChallenge.api');
    Route::get('/userChallengeLists/{user_id}', 'Api\ChallengeController@userChallengeLists')->name('userChallengeLists.api');
    Route::post('/replyToChallengeReplyComment', 'Api\ChallengeController@replyToChallengeReplyComment')->name('replyToChallengeReplyComment.api');
    Route::get('/deleteCommentToChallengeReply/{comment_id}', 'Api\ChallengeController@deleteCommentToChallengeReply')->name('deleteCommentToChallengeReply.api');
    Route::get('/deleteReplyToCommentToChallengeReply/{reply_id}', 'Api\ChallengeController@deleteReplyToCommentToChallengeReply')->name('deleteReplyToCommentToChallengeReply.api');
    Route::get('/viewChallengeReplyVideo/{reply_id}', 'Api\ChallengeController@viewChallengeReplyVideo')->name('viewChallengeReplyVideo.api');

    //notification
    Route::get('/notification', 'Api\GetResourceDetailController@notification')->name('notification.api');
    Route::get('/deleteSingleNotification/{notification_id}', 'Api\GetResourceDetailController@deleteSingleNotification')->name('deleteSingleNotification.api');
    Route::get('/deleteAllNotification', 'Api\GetResourceDetailController@deleteAllNotification')->name('deleteAllNotification.api');
    Route::get('/updateNotificationToOld/{notification_id}', 'Api\GetResourceDetailController@updateNotificationToOld')->name('updateNotificationToOld.api');

    //report routes
    Route::post('/reportPost', 'Api\PostController@reportPost')->name('reportPost.api');
    Route::post('/reportChallenegReply', 'Api\ChallengeController@reportChallenegReply')->name('reportChallenegReply.api');

    //advertisement routes
    Route::post('/likeAdv', 'Api\AdvertisementController@likeAdv')->name('likeAdv.api');
    Route::get('/viewAdv/{adv_id}', 'Api\AdvertisementController@viewAdv')->name('viewAdv.api');
    Route::post('/commentAdv', 'Api\AdvertisementController@commentAdv')->name('commentAdv.api');
    Route::post('/deleteCommentAdv', 'Api\AdvertisementController@deleteCommentAdv')->name('deleteCommentAdv.api');
    Route::post('/showAllAdvertisement', 'Api\AdvertisementController@showAllAdvertisement')->name('showAllAdvertisement.api');

    //Delete Account
    Route::get('/deleteAccount', 'Api\UserController@deleteAccount')->name('deleteAccount.api');
  });
});


