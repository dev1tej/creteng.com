@component('mail::message')
{{-- Greeting --}}
@if (! empty($greeting))
# {{ $greeting }}
@else
@if ($level === 'error')
# @lang('Whoops!')
@endif

@php
$exploded_words = explode('/', $actionUrl);
for ($i = 0; $i < count($exploded_words); $i++) {
    if ($exploded_words[$i] == 'verify') {
        $user_id = $exploded_words[$i + 1];
    }
}
if (isset($user_id)) {
    $user = App\User::find($user_id);
    $user_name = $user->name;
    $user_email = $user->email;
}
@endphp

@if(isset($user_name) && isset($user_email))
# @lang('Dear '.$user_name)

{{ 'Welcome to Creteng! Your account has been created.'."\n" }}
 @lang('Username: ' . $user_email) 
@else
# @lang('Dear User')
@endif
@endif

{{-- Intro Lines --}}
@foreach ($introLines as $line)
{{ $line }}

@endforeach

{{-- Action Button --}}
@isset($actionText)
<?php
    switch ($level) {
        case 'success':
        case 'error':
            $color = $level;
            break;
        default:
            $color = 'primary';
    }
?>
@component('mail::button', ['url' => $actionUrl, 'color' => $color])
{{ $actionText }}
@endcomponent
@endisset

{{-- Outro Lines --}}
@foreach ($outroLines as $line)
{{ $line }}

@endforeach

{{-- Salutation --}}
@if (! empty($salutation))
{{ $salutation }}
@else
@lang('Regards'),<br>
{{ config('app.name') }}
@endif

{{-- Subcopy --}}
@isset($actionText)
@slot('subcopy')
@lang(
    "If you’re having trouble clicking the \":actionText\" button, copy and paste the URL below\n".
    'into your web browser:',
    [
        'actionText' => $actionText,
    ]
) <span class="break-all">[{{ $displayableActionUrl }}]({{ $actionUrl }})</span>
@endslot
@endisset

@endcomponent
