<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\Auth;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    public function redirectTo() {
        /*
        $user = Auth::user();
        $superadmin=env("APP_SUPERADMIN");
        $manager=env("APP_MANAGER");
        if($user->hasRole($superadmin) || $user->hasRole($manager)){
            return route('dashboard');
        } else {
            session()->flush();
            return route('comingSoon');
        }
        */
        session()->flush();
        //session()->flash('success', 'You have Updated your password successfully');
        //return route('login');
        return route('password.reset.temp', ['success' => 'You have Updated your password successfully']);
    }
}
