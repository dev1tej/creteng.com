<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Post;
use App\PostLike;
use App\PostComment;
use App\PostCommentReply;
use App\SavePost;
use App\User;
use App\UsersFriend;
use App\PostsTagged;
use App\PostsTaggedReply;
use App\PostVideoViewed;
use App\ReportPost;
use App\SaveReplyToChallenge;
use App\Notification;
use App\Http\Controllers\Api\CustomPaginationController;
use App\Http\Controllers\Api\GetResourceDetailController;

class PostController extends Controller
{

    /**
     * Add a new post.
     *
     * @param  object  $request
     * @return Restfull Api Response
     */
    public function addPost(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'post_text'        => 'nullable|string|max:255',
                'post_binary'      => 'nullable|string|max:255',
                'post_binary_mime_type' => 'required_with:post_binary|string|max:255',
                'post_type'        => 'required|string|max:255',
                'include_challenges' => 'required|boolean',
                'tagged_user_list' => 'max:255',
                'location'     => 'nullable|string|max:255',
            ]);   
            if ($validator->fails()) {
                return response([
                    'status' => 'failure',
                    'message'=>$validator->errors()->all()
                ], 422);
            }

            if (empty($request->post_text) && empty($request->post_binary)) {
                return response([
                    'status' => 'failure',
                    'message'=>'There is no post to Upload'
                ], 422);
            }
            
            if (!empty($request->tagged_user_list)) {
                $tagged_user_list_pattern = '/^\s+$|^[0-9]*(, {1,3}[0-9]+)*?$/u';
                if (!preg_match($tagged_user_list_pattern, $request->tagged_user_list)) {
                    return response([
                        'status' => 'failure',
                        'message'=>'The tagged user list format is invalid.'
                    ], 422);
                }
            }

            $post_type_pattern = '/^(public)|(private)$/u';
            if (!preg_match($post_type_pattern, $request->post_type)) {
                return response([
                    'status' => 'failure',
                    'message'=>'The post type must be \'public\' or \'private\'.'
                ], 422);
            }

            $input['user_id'] = Auth::user()->id;
            $input['location'] = $request->location;
            if ($input['user_id']) {
                if(!empty($request->post_text)) {
                    $input['post_text'] = $request->post_text;
                } else {
                    $input['post_text'] = null;
                }
                if(!empty($request->post_binary)) {
                    $input['post_binary'] = $request->post_binary;
                    $input['post_binary_mime_type'] = $request->post_binary_mime_type;
                    
                } else {
                    $input['post_binary'] = null;
                    $input['post_binary_mime_type'] = null;
                }
                $input['post_type'] = $request->post_type;
                $input['include_challenges'] = $request->include_challenges;
            }

            $tagged_users = array();
            if (!empty($request->tagged_user_list)) {
                //validating tagged/challenged users
                $tagged_user_list = explode(',', $request['tagged_user_list']);
                foreach ($tagged_user_list as $tagged_user) {
                    $tagged_user_id = trim($tagged_user);
                    $tagged_user = User::where([
                        ['id', $tagged_user_id],
                    ])->first();
                    if (is_null($tagged_user)) {
                        $response = [
                            'status' => 'failure',
                            'message' => 'Pls Tag a valid user',
                        ];
                        return response($response, 422);
                    }
                    $is_friend = UsersFriend::where([
                        ['user_id', $input['user_id']],
                        ['user_friend_id', $tagged_user_id],
                        ['friend_accepted', 1],
                    ])->orWhere([
                        ['user_id', $tagged_user_id],
                        ['user_friend_id', $input['user_id']],
                        ['friend_accepted', 1],
                    ])->first();
                    if (is_null($is_friend)) {
                        $response = [
                            'status' => 'failure',
                            'message' => 'Only a friend can be tagged',
                        ];
                        return response($response, 422);
                    }

                    $tagged_users[] = $tagged_user;
                } 
            }

            if ($request->include_challenges == false) {
                $tagged_type = 'Normal';
                $challenge_accepted = 'NA';
            } else {
                $tagged_type = 'Challenge';
                $challenge_accepted = 0;
            }

            // creating post
            $post = Post::create($input);

            //storing tagged/challenged users and notification
            $applicant = User::find($input['user_id']);
            foreach ($tagged_users as $tagged_user) {
                PostsTagged::create([
                    'post_id'     => $post['id'],
                    'tagged_by'   => $input['user_id'],
                    'tagged_user' => $tagged_user['id'],
                    'tagged_type' => $tagged_type,
                    'challenge_accepted' => $challenge_accepted,
                ]);

                //notification
                if ($tagged_type == 'Normal') {
                    if ($input['user_id'] != $tagged_user['id']) {
                        Notification::create([
                            'applicant_id'     => $input['user_id'],
                            'respondent_id'   => $tagged_user['id'],
                            'applicant_resource_id' => $post['id'],
                            'respondent_resource_id' => null,
                            'motive' => 'tagged_on_normal_post',
                            'notification_type' => 'New',
                            'message' => $applicant->name . ' has tagged you in a post',
                        ]);
                    }
                } elseif ($tagged_type == 'Challenge') {
                    if ($input['user_id'] != $tagged_user['id']) {
                        Notification::create([
                            'applicant_id'     => $input['user_id'],
                            'respondent_id'   => $tagged_user['id'],
                            'applicant_resource_id' => $post['id'],
                            'respondent_resource_id' => null,
                            'motive' => 'tagged_on_challenge_post',
                            'notification_type' => 'New',
                            'message' => $applicant->name . ' has tagged you in a challenge',
                        ]);
                    }
                }
            }

            //sending notification mail to challenged users if post type is challenged
            if ($request->include_challenges == true) {
                foreach ($tagged_users as $tagged_user) {
                    $details['auth_user'] = Auth::user();
                    $details['tagged_user'] = $tagged_user;
                    \Mail::to($tagged_user->email)->send(new \App\Mail\ChallengeNotification($details));
                } 
            }

            // adding tagged_users data in post object
            $post->tagged_users = $tagged_users;
            $response = [
                'status' => 'success',
                'message' => 'Post uploaded successfully',
                'post' => $post,
            ];
            return response($response, 200);
        } catch (\Exception $ex) {
            $response = [
                'status' => 'failure',
                'message' => 'Can not create a post. Unidentified error, please contact at ' . env("APP_SUPERADMIN_EMAIL_ID") ,
            ];
            return response($response, 500);
        }
    }

    /**
     * To show all post linked to logged user.
     *
     * @return Restfull Api Response
     */
    public function showAllPost()
    {
        try {
            $user_id = Auth::id();
            //array to have all posts
            $all_post = array();
            //getting all public posts
            $public_posts = Post::where([
                ['post_type', 'public'], 
            ])->get();
            foreach ($public_posts as $public_post) {
                $all_post[] = $public_post;
            }
            //getting private posts of authenticated user
            $user_private_posts = Post::where([
                ['user_id', $user_id],
                ['post_type', 'private'], 
            ])->get();
            foreach ($user_private_posts as $user_private_post) {
                $all_post[] = $user_private_post;
            }

            //getting post where auth user is tagged
            $user_tagged_posts = PostsTagged::where([
                ['tagged_user', $user_id]
            ])->get();
            
            if(!is_null($user_tagged_posts)) {
                foreach ($user_tagged_posts as $user_tagged_post) {
                    $tagged_post = Post::where([
                        ['id', $user_tagged_post->post_id],
                        ['post_type', 'private'],
                    ])->first();
                    if (!is_null($tagged_post)) {
                        $all_post[] = $tagged_post;
                    }
                }
            }
            /*
            //getting authenticated user's friend lists
            $user_friends = UsersFriend::where([
                ['user_id', $user_id], 
            ])->get();
            foreach ($user_friends as $friend) {
                $friend_id = $friend['user_friend_id'];
                //getting private posts of authenticated user's friend
                $friend_privat_posts = Post::where([
                    ['user_id', $friend_id],
                    ['post_type', 'private'], 
                ])->get();
                foreach ($friend_privat_posts as $friend_privat_post) {
                    $all_post[] = $friend_privat_post;
                }
            }
            */

            if (is_null($all_post)) {
                $response = [
                    'status' => 'success',
                    'message' => 'There is no post to show',
                    'All_public_private_post' => $all_post,
                ];
                return response($response, 200);
            } else {
                
                $posts = array();
                foreach ($all_post as $post) {
                    $posts[] = $post;
                }
                
                //getting other details about posts
                $get_resource_detail = new GetResourceDetailController;
                $posts = $get_resource_detail->getPostDetail($posts);
                
                //custom pagination
                $custom_pagination = new CustomPaginationController;
                $posts = collect($posts);
                $posts = $custom_pagination->paginate($posts);
                
                $response = [
                    'status' => 'success',
                    'message' => 'Posts found Successfully',
                    'All_public_private_post' => $posts,
                ];

                return response($response, 200);
            }
        } catch (\Exception $ex) {
            $response = [
                'status' => 'failure',
                'message' => 'Can not show posts. Unidentified error, please contact at ' . env("APP_SUPERADMIN_EMAIL_ID"),
            ];
            return response($response, 500);
        }
    }

    /**
     * Search posts by caption.
     *
     * @param  object  $request
     * @return Restfull Api Response
     */
    public function searchPost(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'post_caption' => 'required|string|max:255',
            ]);

            if ($validator->fails()) {
                return response([
                    'status' => 'failure',
                    'message'=>$validator->errors()->all()
                ], 422);
            }
            $user_id = Auth::id();
            $posts = array();
            //getting all public posts
            $public_posts = Post::where([
                ['post_type', 'public'], 
            ])->get();
            foreach ($public_posts as $public_post) {
                $posts[] = $public_post;
            }
            //getting private posts of authenticated user
            $user_private_posts = Post::where([
                ['user_id', $user_id],
                ['post_type', 'private'], 
            ])->get();
            foreach ($user_private_posts as $user_private_post) {
                $posts[] = $user_private_post;
            }

            //getting post where auth user is tagged
            $user_tagged_posts = PostsTagged::where([
                ['tagged_user', $user_id]
            ])->get();
            if(!is_null($user_tagged_posts)) {
                foreach ($user_tagged_posts as $user_tagged_post) {
                    $tagged_post = Post::where([
                        ['id', $user_tagged_post->post_id],
                        ['post_type', 'private'],
                    ])->first();
                    if (!is_null($tagged_post)) {
                        $posts[] = $tagged_post;
                    }
                }
            }

            if ($posts) {
                $search_post_list = array();
                $i = 0;
                foreach ($posts as $post) {
                    if (preg_match("/{$request->post_caption}/i", $post->post_text)) {
                        $search_post_list[$i] = $post;
                        $i++;
                    }
                }
                if ($search_post_list) {

                    //sorting in descending order of post id
                    uasort($search_post_list, function($id_a,$id_b){
                        return strcmp($id_b['id'], $id_a['id']);
                    });
                    $search_posts = array();
                    foreach ($search_post_list as $post) {
                        $search_posts[] = $post;
                    }
                    
                    //getting other details about posts
                    $get_resource_detail = new GetResourceDetailController;
                    $search_posts = $get_resource_detail->getPostDetail($search_posts);

                    //custom pagination
                    $custom_pagination = new CustomPaginationController;
                    $search_posts = collect($search_posts);
                    $search_posts = $custom_pagination->paginate($search_posts);
                    $response = [
                        'status' => 'success',
                        'message' => 'Posts fetched successfully',
                        'searched_post_list' => $search_posts,
                    ];
                    return response($response, 200);
                } else {
                    $response = [
                        'status' => 'success',
                        'message' => 'Search post with caption '.$request->post_caption.' does not exists',
                    ];
                    return response($response, 200);
                }
            }  else {
                $response = [
                    'status' => 'success',
                    'message' => 'Search post with caption '.$request->post_caption.' does not exists',
                ];
                return response($response, 200);
            }
        } catch (\Exception $ex) {
            $response = [
                'status' => 'failure',
                'message' => 'Can not search post. Unidentified error, please contact at ' . env("APP_SUPERADMIN_EMAIL_ID"),
            ];
            return response($response, 500);
        }
    }

    /**
     * Edit an existing post.
     *
     * @param  object  $request
     * @return Restfull Api Response
     */
    public function editPost(Request $request)
    {   
        try {
            $validator = Validator::make($request->all(), [
                'post_id'          => 'required|int',
                'post_text'        => 'string|nullable|max:255',
                'post_binary'      => 'string|nullable|max:255',
                'post_binary_mime_type' => 'required_with:post_binary|string|max:255',
                'include_challenges' => 'required|boolean',
                'tagged_user_list' => 'nullable|max:255',
                'post_type'        => 'required|string|max:255',
                'location'         => 'nullable|string|max:255',
            ]);   

            if ($validator->fails()) {
                return response([
                    'status' => 'failure',
                    'message'=>$validator->errors()->all()
                ], 422);
            }

            if (empty($request->post_text) && empty($request->post_binary)) {
                return response([
                    'status' => 'failure',
                    'message'=>'There is nothing to Update'
                ], 422);
            }
            
            if (!empty($request->tagged_user_list)) {
                $tagged_user_list_pattern = '/^\s+$|^[0-9]*(, {1,3}[0-9]+)*?$/u';
                if (!preg_match($tagged_user_list_pattern, $request->tagged_user_list)) {
                    return response([
                        'status' => 'failure',
                        'message'=>'The tagged user list format is invalid.'
                    ], 422);
                }
            }

            $post_type_pattern = '/^(public)|(private)$/u';
            if (!preg_match($post_type_pattern, $request->post_type)) {
                return response([
                    'status' => 'failure',
                    'message'=>'The post type must be \'public\' or \'private\'.'
                ], 422);
            }

            $user_id = Auth::user()->id;
            $post = Post::find($request->post_id);

            if ($post) {
                if ($post->user_id != $user_id) {
                    return response([
                        'status' => 'failure',
                        'message'=>'You are not authorize to edit this post'
                    ], 422);
                }

                if (!empty($request->tagged_user_list)) {
                    $tagged_user_list = explode(',', $request['tagged_user_list']);
                    //validating tagged/challenged user
                    $tagged_users = array();
                    foreach ($tagged_user_list as $tagged_user) {
                        $tagged_user_id = trim($tagged_user);
                        $tagged_user = User::where([
                            ['id', $tagged_user_id],
                        ])->first();
                        if (is_null($tagged_user)) {
                            $response = [
                                'status' => 'failure',
                                'message' => 'Pls Tagge a valid user',
                            ];
                            return response($response, 422);
                        }
                        $is_friend = UsersFriend::where([
                            ['user_id', $user_id],
                            ['user_friend_id', $tagged_user_id],
                            ['friend_accepted', 1],
                        ])->orWhere([
                            ['user_id', $tagged_user_id],
                            ['user_friend_id', $user_id],
                            ['friend_accepted', 1],
                        ])->first();
                        if (is_null($is_friend)) {
                            $response = [
                                'status' => 'failure',
                                'message' => 'Only a friend can be tagged',
                            ];
                            return response($response, 422);
                        }

                        $tagged_users[] = $tagged_user;
                    } 
                }

                if ($request->include_challenges == false) {
                    $tagged_type = 'Normal';
                    $challenge_accepted = 'NA';
                } else {
                    $tagged_type = 'Challenge';
                    $challenge_accepted = 0;
                }

                //updating post table
                $post->update([
                    'post_text' => $request->post_text,
                    'post_binary' => $request->post_binary,
                    'post_binary_mime_type' => $request->post_binary_mime_type,
                    'post_type' => $request->post_type,
                    'include_challenges' => $request->include_challenges,
                    'location' => $request->location,
                ]);

                //getting old tages
                $old_tagged_user_list = PostsTagged::where([
                    ['post_id', $post->id],
                    ['tagged_by', $user_id],
                ])->get();
                $old_tagged_users = array();
                foreach ($old_tagged_user_list as $old_tagged_user) {
                    $old_tagged_users[] = User::where([
                        ['id', $old_tagged_user['tagged_user']],
                    ])->first();
                }
                //updating tagged/challenged users and notification
                /*
                $applicant = User::find($user_id);
                //deleting old notifications
                Notification::where([
                    ['applicant_id', $applicant->id],
                    ['applicant_resource_id', $post->id],
                ])->delete();
                */
                if (!empty($request->tagged_user_list)) {
                    foreach ($tagged_user_list as $tagged_user) {
                        $tagged_user_id = trim($tagged_user);

                        //storing only new tagged users
                        if (!empty($old_tagged_user_list)) {
                            $match = false;
                            foreach ($old_tagged_users as $old_tagged_user) {
                                if ($old_tagged_user['id'] == $tagged_user_id) {
                                    $match = true;
                                }
                            }
                            if ($match == false) {
                                PostsTagged::create([
                                    'post_id'     => $post->id,
                                    'tagged_by'   => $user_id,
                                    'tagged_user' => $tagged_user_id,
                                    'tagged_type' => $tagged_type,
                                    'challenge_accepted' => $challenge_accepted,
                                ]);
                            }
                        } else {
                            PostsTagged::create([
                                'post_id'     => $post->id,
                                'tagged_by'   => $user_id,
                                'tagged_user' => $tagged_user_id,
                                'tagged_type' => $tagged_type,
                                'challenge_accepted' => $challenge_accepted,
                            ]);
                        }

                        //updating notification
                        /*
                        if ($tagged_type == 'Normal') {
                            if ($applicant->id != $tagged_user_id) {
                                Notification::create([
                                    'applicant_id'     => $applicant->id,
                                    'respondent_id'   => $tagged_user_id,
                                    'applicant_resource_id' => $post->id,
                                    'respondent_resource_id' => null,
                                    'motive' => 'tagged_on_normal_post',
                                    'notification_type' => 'New',
                                    'message' => $applicant->name . ' has tagged you in a post',
                                ]);
                            }
                        } elseif ($tagged_type == 'Challenge') {
                            if ($applicant->id != $tagged_user_id) {
                                Notification::create([
                                    'applicant_id'     => $applicant->id,
                                    'respondent_id'   => $tagged_user_id,
                                    'applicant_resource_id' => $post->id,
                                    'respondent_resource_id' => null,
                                    'motive' => 'tagged_on_challenge_post',
                                    'notification_type' => 'New',
                                    'message' => $applicant->name . ' has tagged you in a challenge',
                                ]);
                            }
                        } */
                    }
                }
                //sending notification mail to new challenged users if it includes challenges
                if ($request->include_challenges == true) {
                    if (!empty($request->tagged_user_list)) {
                        foreach ($tagged_users as $tagged_user) {
                            if (!empty($old_tagged_user_list)) {
                                $match = false;
                                foreach ($old_tagged_users as $old_tagged_user) {
                                    if ($old_tagged_user['id'] == $tagged_user['id']) {
                                        $match = true;
                                    }
                                }
                                if ($match == false) { 
                                    $details['auth_user'] = Auth::user();
                                    $details['tagged_user'] = $tagged_user;
                                    \Mail::to($tagged_user->email)->send(new \App\Mail\ChallengeNotification($details));
                                }
                            } else {
                                $details['auth_user'] = Auth::user();
                                $details['tagged_user'] = $tagged_user;
                                \Mail::to($tagged_user->email)->send(new \App\Mail\ChallengeNotification($details));
                            }
                        }
                    } 
                }
                //sending notification mail to old challenged users if it includes challenges
                if ($request->include_challenges == true) {
                    if (!empty($old_tagged_user_list)) {
                        foreach ($old_tagged_users as $old_tagged_user) {
                            $details['auth_user'] = Auth::user();
                            $details['tagged_user'] = $old_tagged_user;
                            \Mail::to($old_tagged_user->email)->send(new \App\Mail\ChallengeHasEditedNotification($details));
                        }
                    } 
                }

                $response = [
                    'status' => 'success',
                    'message' => 'Post edited successfully',
                    'post' => $post,
                ];
                return response($response, 200);  
            } else {
                $response = [
                    'status' => 'failure',
                    'message' => 'There is no such post to edit',
                ];
                return response($response, 422);
            }  
        } catch (\Exception $ex) {
            $response = [
                'status' => 'failure',
                'message' => 'Can not edit post. Please contact at ' . env("APP_SUPERADMIN_EMAIL_ID") ,
            ];
            return response($response, 500);
        }      
    }

    /**
     * Delete an existing post.
     *
     * @param int $id
     * @return Restfull Api Response
     */
    public function deletePost($id)
    {
        try {
            $user_id = Auth::user()->id;
            $post = Post::find($id);

            if ($post) {
                if ($post->user_id != $user_id) {
                    return response([
                        'status' => 'failure',
                        'message'=>'You are not authorize to delete this post'
                    ], 422);
                }  
                //deleting post table
                $post->delete();
                $response = [
                    'status' => 'success',
                    'message' => 'Post deleted successfully',
                ];
                return response($response, 200);
            } else {
                $response = [
                    'status' => 'failure',
                    'message' => 'There is no such post',
                ];
                return response($response, 422);
            } 
        } catch (\Exception $ex) {
            $response = [
                'status' => 'failure',
                'message' => 'Can not delete post. Unidentified error, please contact at ' . env("APP_SUPERADMIN_EMAIL_ID"),
            ];
            return response($response, 500);
        }
    }

    /**
     * Like or Unlike a post by logged user.
     *
     * @param  object  $request
     * @return Restfull Api Response
     */
    public function likePost(Request $request) 
    {
        try {
            $validator = Validator::make($request->all(), [
                'post_id' => 'required|regex:/^[0-9]{1,11}$/u',
                'status' => 'required|regex:/^[1,0]$/u',
            ]);   
            if ($validator->fails()) {
                return response([
                    'status' => 'failure',
                    'errors'=>$validator->errors()->all()
                ], 422);
            }

            $input['user_id'] = Auth::user()->id;
            $input['post_id'] = $request->post_id;
            $input['status'] = $request->status;
            $post = Post::where([
                ['id', $input['post_id']],
            ])->first();
            if (is_null($post)) {
                $response = [
                    'status' => 'failure',
                    'message' => 'Provid a valid post id',
                ];
                return response($response, 422);
            } elseif ($post->post_type == 'private') {
                $is_friend = UsersFriend::where([
                    ['user_id', $input['user_id']],
                    ['user_friend_id', $post->user_id],
                    ['friend_accepted', 1],
                ])->orWhere([
                    ['user_id', $post->user_id],
                    ['user_friend_id', $input['user_id']],
                    ['friend_accepted', 1],
                ])->first();
                if (is_null($is_friend)) {
                    $response = [
                        'status' => 'failure',
                        'message' => 'This is a private post. Only a friend can like it',
                    ];
                    return response($response, 422);
                }
            }

            if ($input['status'] == 1) {

                //checking if already liked
                $post_already_liked = PostLike::where([
                    ['user_id', $input['user_id']], 
                    ['post_id', $input['post_id']],
                    ['status', 1],
                ])->get();
                if (!($post_already_liked->isEmpty())) {
                    $response = [
                        'status' => 'failure',
                        'message' => 'You have already liked this post',
                    ];
                    return response($response, 422);
                } else {
                    PostLike::create($input);
                    $response = [
                        'status' => 'success',
                        'message' => 'You have liked the post successfully',
                    ];
                    return response($response, 200);
                }
            }

            if ($input['status'] == 0) {
                $post_like = PostLike::where([
                    ['user_id', $input['user_id']], 
                    ['post_id', $input['post_id']],
                ])->delete();

                if (!$post_like) {
                    $response = [
                        'status' => 'failure',
                        'message' => 'Post not found',
                    ];
                    return response($response, 422);
                } else { 
                    $response = [
                        'status' => 'success',
                        'message' => 'Post Unlike successfully',
                    ];
                    return response($response, 200);
                }
            }
        } catch (\Exception $ex) {
            $response = [
                'status' => 'failure',
                'message' => 'Can not like post. Unidentified error, please contact at ' . env("APP_SUPERADMIN_EMAIL_ID"),
            ];
            return response($response, 500);
        }
    }

    /**
     * Comment a existing post by logged user.
     *
     * @param  object  $request
     * @return Restfull Api Response
     */
    public function commentPost(Request $request) 
    {
        try {
            $validator = Validator::make($request->all(), [
                'post_id' => 'required|regex:/^[0-9]{1,11}$/u',
                'comment' => 'required|string|max:255',
            ]);   

            if ($validator->fails()) {
                return response([
                    'status' => 'failure',
                    'message'=>$validator->errors()->all()
                ], 422);
            }

            $input['user_id'] = Auth::user()->id;
            $input['post_id'] = $request->post_id;
            $input['comments'] = $request->comment;
            $post = Post::where([
                ['id', $input['post_id']],
            ])->first();
            if (is_null($post)) {
                $response = [
                    'status' => 'failure',
                    'message' => 'Provid a valid post id',
                ];
                return response($response, 422);
            } elseif ($post->post_type == 'private') {
                $is_friend = UsersFriend::where([
                    ['user_id', $input['user_id']],
                    ['user_friend_id', $post->user_id],
                    ['friend_accepted', 1],
                ])->orWhere([
                    ['user_id', $post->user_id],
                    ['user_friend_id', $input['user_id']],
                    ['friend_accepted', 1],
                ])->first();
                if ((is_null($is_friend)) && ($input['user_id'] != $post->user_id)) {
                    $response = [
                        'status' => 'failure',
                        'message' => 'This is a private post. Only a friend or post owner can comment on it',
                    ];
                    return response($response, 422);
                }
            }
            $post_comment = PostComment::create($input); 
            //notification
            if ($post->include_challenges == true) {
                $motive = 'comment_on_challenge_post';
            } elseif ($post->include_challenges == false) {
                $motive = 'comment_on_normal_post';
            }
            $applicant = User::find($input['user_id']);
            if ($applicant->id != $post->user_id) {
                Notification::create([
                    'applicant_id'     => $applicant->id,
                    'respondent_id'   => $post->user_id,
                    'applicant_resource_id' => $post_comment->id,
                    'respondent_resource_id' => $post->id,
                    'motive' => $motive,
                    'notification_type' => 'New',
                    'message' => $applicant->name . ' has commented at your challenge',
                ]);
            }
            $response = [
                'status' => 'success',
                'message' => 'The post commented successfully',
                'post' => $post_comment,
            ];
            return response($response, 200);
        } catch (\Exception $ex) {
            $response = [
                'status' => 'failure',
                'message' => 'Can not comment. Unidentified error, please contact at ' . env("APP_SUPERADMIN_EMAIL_ID"),
            ];
            return response($response, 500);
        }
    }

    /**
     * listing saved post list of logged user.
     *
     * @return Restfull Api Response
     */
    public function mySavedPostAndReply() 
    {
        try {
            $my_user_id = Auth::id();

            //getting all saved posts
            $saved_posts = SavePost::where([
                ['user_id', $my_user_id], 
            ])->get();
            $posts = array();
            $index = 0;
            $get_resource_detail = new GetResourceDetailController;
            foreach ($saved_posts as $saved_post) {
                $post = Post::where([
                    ['id', $saved_post['post_id']], 
                ])->get();
                $post_array = $get_resource_detail->getPostDetail($post);
                foreach ($post_array as $post) {
                    $post['saved_at'] = $saved_post->updated_at;
                    $posts[$index] = $post;
                }
                $index++;
            }
            
            //getting all saved replies
            $saved_replies = SaveReplyToChallenge::where([
                ['user_id', $my_user_id], 
            ])->get();

            $replies = array();
            $index = 0;
            foreach ($saved_replies as $saved_reply) {
                $reply = PostsTaggedReply::where([
                    ['id', $saved_reply['reply_to_challenge_id']], 
                ])->get();
                $reply_array = $get_resource_detail->getReplyDetail($reply);
                foreach ($reply_array as $reply) {
                    $reply['saved_at'] = $saved_reply->updated_at;
                    $replies[$index] = $reply;
                }
                $index++;
            }

            $saved_posts_and_replies = array();
            foreach ($posts as $post) {
                $saved_posts_and_replies[] = $post;
            }
            foreach ($replies as $reply) {
                $saved_posts_and_replies[] = $reply;
            }
            //custom pagination
            $saved_posts_and_replies = collect($saved_posts_and_replies);
            $saved_posts_and_replies = $saved_posts_and_replies->SortByDesc('saved_at');
            $saved_posts_and_replies_array = array();
            foreach ($saved_posts_and_replies as $saved_post_and_reply) {
                $saved_posts_and_replies_array[] = $saved_post_and_reply;
            }
            $custom_pagination = new CustomPaginationController;
            $saved_posts_and_replies_array = collect($saved_posts_and_replies_array);
            $saved_posts_and_replies_array = $custom_pagination->paginate($saved_posts_and_replies_array);
            $response = [
                'status' => 'success',
                'message' => 'Saved posts and replies found Successfully',
                'saved_post_and_replies' => $saved_posts_and_replies_array,
            ];
            return response($response, 200);
        } catch (\Exception $ex) {
            $response = [
                'status' => 'failure',
                'message' => 'Can not show saved posts and replies. Unidentified error, please contact at ' . env("APP_SUPERADMIN_EMAIL_ID") ,
            ];
            return response($response, 500);
        } 
    }

    /**
     * Saving post list by logged user.
     *
     * @param  object  $request
     * @return Restfull Api Response
     */
    public function savePostList(Request $request) 
    {
        try {
            $validator = Validator::make($request->all(), [
                'post_id_list' => 'required|max:255|regex:/^[0-9]*(, {0,3}[0-9]+)*?$/u',
            ]);   
            if ($validator->fails()) {
                return response([
                    'status' => 'failure',
                    'message'=>$validator->errors()->all()
                ], 422);
            }

            $post_id_list_array = explode(',', $request->post_id_list);
            $input['user_id'] = Auth::user()->id;
            foreach ($post_id_list_array as $post_id) {
                $input['post_id'] = trim($post_id);

                $post = Post::where([
                    ['id', $input['post_id']],
                ])->first();
                if (is_null($post)) {
                    $response = [
                        'status' => 'failure',
                        'message' => $input['post_id'].' is a invalid post id',
                    ];
                    return response($response, 422);
                } elseif ($post->post_type == 'private') {
                    $is_friend = UsersFriend::where([
                        ['user_id', $input['user_id']],
                        ['user_friend_id', $post->user_id],
                        ['friend_accepted', 1],
                    ])->orWhere([
                        ['user_id', $post->user_id],
                        ['user_friend_id', $input['user_id']],
                        ['friend_accepted', 1],
                    ])->first();
                    if ((is_null($is_friend)) && ($input['user_id'] != $post->user_id)) {
                        $response = [
                            'status' => 'failure',
                            'message' => $input['post_id'].' is a private post. Only a friend and post owner can save it',
                        ];
                        return response($response, 422);
                    }
                }

                $post_already_saved = SavePost::where([
                    ['post_id', $input['post_id']],
                    ['user_id', $input['user_id']],
                ])->get();

                if (!($post_already_saved->isEmpty())) {
                    $response = [
                        'status' => 'failure',
                        'message' => 'You have already saved post with id '.$input['post_id'],
                    ];
                    return response($response, 422);
                }
            }
            foreach ($post_id_list_array as $post_id) {
                $input['post_id'] = trim($post_id);
                SavePost::create($input);
            }
            $response = [
                'status' => 'success',
                'message' => 'Listed posts saved successfully',
            ];
            return response($response, 200);
        } catch (\Exception $ex) {
            $response = [
                'status' => 'failure',
                'message' => 'Can not save posts. Unidentified error, please contact at ' . env("APP_SUPERADMIN_EMAIL_ID"),
            ];
            return response($response, 500);
        }
    }

    /**
     * Removing save post list by logged user.
     *
     * @param  object  $request
     * @return Restfull Api Response
     */
    public function removeSavePostList(Request $request) 
    {
        try {
            $validator = Validator::make($request->all(), [
                'post_id_list' => 'required|max:255|regex:/^[0-9]*(, {0,3}[0-9]+)*?$/u',
            ]);   

            if ($validator->fails()) {
                return response([
                    'status' => 'failure',
                    'message'=>$validator->errors()->all()
                ], 422);
            }

            $post_id_list_array = explode(',', $request->post_id_list);

            $input['user_id'] = Auth::user()->id;

            foreach ($post_id_list_array as $post_id) {
                $input['post_id'] = trim($post_id);
                $removed = SavePost::where([
                    ['user_id', $input['user_id']], 
                    ['post_id', $input['post_id']]
                ])->delete();
                if (!$removed) {
                    return response([
                        'status' => 'failure',
                        'message'=> 'post id ' .$input['post_id']. ' does not exist in save list',
                    ], 422);
                }
            }
            $response = [
                'status' => 'success',
                'message' => 'Listed posts removed from save list successfully',
            ];
            return response($response, 200);
        } catch (\Exception $ex) {
            $response = [
                'status' => 'failure',
                'message' => 'Can not remove posts from saved list. Unidentified error, please contact at ' . env("APP_SUPERADMIN_EMAIL_ID"),
            ];
            return response($response, 500);
        }
    }

    /**
     * To show posts of logged user.
     *
     * @return Restfull Api Response
     */
    public function showMyPost() 
    {
        try {
            $my_user_id = Auth::id();

            //getting all public posts
            $my_posts = Post::where([
                ['user_id', $my_user_id], 
            ])->get();

            $my_posts = $my_posts->toArray();
            if (is_null($my_posts)) {
                $response = [
                    'status' => 'success',
                    'message' => 'There is no post to show',
                    'my_posts' => $my_posts,
                ];
                return response($response, 200);
            } else {
                //sorting in descending order of post id
                uasort($my_posts, function($id_a,$id_b){
                    return strcmp($id_b['id'], $id_a['id']);
                });
                $posts = array();
                foreach ($my_posts as $post) {
                    $posts[] = $post;
                }
                
                //getting other details about posts
                $get_resource_detail = new GetResourceDetailController;
                $posts = $get_resource_detail->getPostDetail($posts);
                //custom pagination
                $custom_pagination = new CustomPaginationController;
                $posts = collect($posts);
                $posts = $custom_pagination->paginate($posts);
                
                $response = [
                    'status' => 'success',
                    'message' => 'My Posts found Successfully',
                    'my_posts' => $posts,
                ];

                return response($response, 200);
            }
        } catch (\Exception $ex) {
            $response = [
                'status' => 'failure',
                'message' => 'Can not show your posts. Unidentified error, please contact at ' . env("APP_SUPERADMIN_EMAIL_ID") ,
            ];
            return response($response, 500);
        }  
    }

    /**
     * To show received challenges by logged user.
     *
     * @return Restfull Api Response
     */
    public function showMyReceivedChallenges()
    {
        try {
            $my_user_id = Auth::id();
            $challenges_to_me_list = PostsTagged::where([
                ['tagged_user', $my_user_id],
                ['tagged_type', 'Challenge'],
            ])->orderBy('updated_at', 'DESC')->get();

            if (is_null($challenges_to_me_list)) {
                $response = [
                    'status' => 'success',
                    'message' => 'So far you have not been challenge by any one',
                ];
                return response($response, 200);
            }
            $challenges_details = array();
            $index = 0;
            foreach ($challenges_to_me_list as $challenge_to_me) {

                //getting post details
                $post_id = $challenge_to_me->post_id;
                $post = Post::where([
                    ['id', $post_id], 
                ])->get();
                //getting other details about post
                $get_resource_detail = new GetResourceDetailController;
                $get_post_details = $get_resource_detail->getPostDetail($post);
                foreach ($get_post_details as $get_post_detail) {
                    $challenges_details[$index] = $get_post_detail;
                }

                if ($challenge_to_me->challenge_accepted == 1 ) {
                    $challenges_details[$index]['status'] = 'Accepted';
                } elseif ($challenge_to_me->challenge_accepted == 0 ) {
                    $challenges_details[$index]['status'] = 'Pending';
                }
                //getting challenge_by details
                $challenge_by_id = $challenge_to_me->tagged_by;
                $challenge_by = User::where([
                    ['id', $challenge_by_id], 
                ])->first();
                
                $challenges_details[$index]['challenge_by'] = $get_resource_detail->getUserDetail($challenge_by);

                $index++;
            }
            //custom pagination
            $custom_pagination = new CustomPaginationController;
            $challenges_details = collect($challenges_details);
            $challenges_details = $custom_pagination->paginate($challenges_details);
            $response = [
                'status' => 'success',
                'message' => 'Challenges to me found Successfully',
                'challenges_details' => $challenges_details,
            ];
            return response($response, 200);
        } catch (\Exception $ex) {
            $response = [
                'status' => 'failure',
                'message' => 'Can not show your Challenges. Unidentified error, please contact at ' . env("APP_SUPERADMIN_EMAIL_ID"),
            ];
            return response($response, 500);
        }
    }

    /**
     * To show received challenges by any user.
     *
     * @param  int  $id
     * @return Restfull Api Response
     */
    public function showUsersReceivedChallenges($id)
    {
        try {
            $my_id = Auth::id();
            $user_id = $id;
            //checking if friend exists
            $is_friend = UsersFriend::where([
                ['user_id', $my_id],
                ['user_friend_id', $user_id],
                ['friend_accepted', 1],
            ])->orWhere([
                ['user_id', $user_id],
                ['user_friend_id', $my_id],
                ['friend_accepted', 1],
            ])->first();
            if (is_null($is_friend)) {
                $response = [
                    'status' => 'success',
                    'message' => 'Respective user is not your friend. You are not authorized to view its challenges',
                ];
                return response($response, 200);
            }

            $challenges_to_user_list = PostsTagged::where([
                ['tagged_user', $user_id],
                ['tagged_type', 'Challenge'],
                ['challenge_accepted', 1],
            ])->orderBy('updated_at', 'DESC')->get();

            if ($challenges_to_user_list->isEmpty()) {
                $response = [
                    'status' => 'success',
                    'message' => 'So far user has not been challenge by any one',
                ];
                return response($response, 200);
            }
            $challenges_details = array();
            $index = 0;
            foreach ($challenges_to_user_list as $challenge_to_user) {

                if ($challenge_to_user->challenge_accepted == 1 ) {
                    $challenges_details[$index]['status'] = 'Accepted';
                } elseif ($challenge_to_user->challenge_accepted == 0 ) {
                    $challenges_details[$index]['status'] = 'Pending';
                }
                //getting challenge_by details
                $challenge_by_id = $challenge_to_user->tagged_by;
                $challenge_by = User::where([
                    ['id', $challenge_by_id], 
                ])->first();
                $get_resource_detail = new GetResourceDetailController;
                $challenges_details[$index]['challenge_by'] = $get_resource_detail->getUserDetail($challenge_by);
                //getting post details
                $post_id = $challenge_to_user->post_id;
                $post = Post::where([
                    ['id', $post_id], 
                ])->get();
                //getting other details about post
                $get_post_details = $get_resource_detail->getPostDetail($post);
                foreach ($get_post_details as $get_post_detail) {
                    $challenges_details[$index]['post_details'] = $get_post_detail;
                }
                $index++;
            }
            //custom pagination
            $custom_pagination = new CustomPaginationController;
            $challenges_details = collect($challenges_details);
            $challenges_details = $custom_pagination->paginate($challenges_details);
            $response = [
                'status' => 'success',
                'message' => 'Challenges to user found Successfully',
                'challenges_details' => $challenges_details,
            ];
            return response($response, 200);
        } catch (\Exception $ex) {
            $response = [
                'status' => 'failure',
                'message' => 'Can not show user\'s Challenges. Unidentified error, please contact at ' . env("APP_SUPERADMIN_EMAIL_ID"),
            ];
            return response($response, 500);
        }
    }
    
    /**
     * To Accept or Deny a challenge by logged user.
     *
     * @param  object  $request
     * @return Restfull Api Response
     */
    public function respondToChallenge(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'post_id'         => 'required|int',
                'challenge_by_id' => 'required|int',
                'response'        => 'required|string|max:255',
            ]);   
            if ($validator->fails()) {
                return response([
                    'status' => 'failure',
                    'message'=>$validator->errors()->all()
                ], 422);
            }
            $response_pattern = '/^(Accept)|(Deny)$/u';
            if (!preg_match($response_pattern, $request->response)) {
                return response([
                    'status' => 'failure',
                    'message'=>'The response must be \'Accept\' or \'Deny\'.'
                ], 422);
            }

            $challenge = PostsTagged::where([
                ['post_id', $request->post_id],
                ['tagged_by', $request->challenge_by_id],
                ['tagged_user', Auth::id()],
            ])->first();

            if (is_null($challenge)) {
                return response([
                    'status' => 'failure',
                    'message'=>'Invalid form data'
                ], 422);
            } else {
                if ($challenge->tagged_type != 'Challenge') {
                    return response([
                        'status' => 'failure',
                        'message'=>'This is not a challenge post'
                    ], 422);
                }
                if ($challenge->challenge_accepted == 1) {
                    return response([
                        'status' => 'failure',
                        'message'=>'This challenge has already accepted'
                    ], 422);
                }
                if ($request->response == 'Accept') {
                    //updating challenge record
                    $challenge->update(
                        ['challenge_accepted' => 1]
                    );
                    return response([
                        'status' => 'success',
                        'message'=>'Challenge accepted Successfully'
                    ], 200);
                }
                if ($request->response == 'Deny') {
                    //deleting challenge record
                    $challenge->delete();
                    return response([
                        'status' => 'success',
                        'message'=>'Challenge denied Successfully'
                    ], 200);
                }
            }
        } catch (\Exception $ex) {
            $response = [
                'status' => 'failure',
                'message' => 'Can not response to Challenge. Unidentified error, please contact at ' . env("APP_SUPERADMIN_EMAIL_ID"),
            ];
            return response($response, 500);
        }
    }

    /**
     * To fetch post liked by users by logged user.
     *
     * @param  int  $id
     * @return Restfull Api Response
     */
    public function viewPostLikedBy($id)
    {
        try {
            $post = Post::find($id);
            if (is_null($post)) {
                $response = [
                    'status' => 'failure',
                    'message' => 'Provide a valid post id',
                ];
                return response($response, 422);
            }
            $post_likes = PostLike::where([
                ['post_id', $id],
            ])->get();
            
            $users_details = array();
            foreach ($post_likes as $post_like) {
                $liked_by_user = User::where([
                    ['id', $post_like->user_id],
                ])->first();
                $get_resource_detail = new GetResourceDetailController;
                $users_details[] = $get_resource_detail->getUserDetail($liked_by_user);
            }
            //custom pagination
            $custom_pagination = new CustomPaginationController;
            $users_details = collect($users_details);
            $users_details = $custom_pagination->paginate($users_details);
            $response = [
                'status' => 'success',
                'message' => 'Liked by user list found Successfully', 
                'Liked_by' => $users_details,
            ];
            return response($response, 200);   
        } catch (\Exception $ex) {
            $response = [
                'status' => 'failure',
                'message' => 'Can not find the liked by user list. Unidentified error, please contact at ' . env("APP_SUPERADMIN_EMAIL_ID") ,
            ];
            return response($response, 500);
        }
    }

    /**
     * To view video of a post by logged user.
     *
     * @param  int  $post_id
     * @return Restfull Api Response
     */
    public function viewPostVideo($post_id)
    {
        try {
            $user_id = Auth::id();
            $post = Post::where([
                ['id', $post_id],
            ])->first();

            if (!is_null($post)) {
                if ($post->post_binary_mime_type == 'video') {
                    //storing post video is viewed
                    PostVideoViewed::create([
                        'post_id' => $post_id,
                        'user_id' => $user_id,
                    ]);
                    $response = [
                        'status' => 'Success',
                        'message' => 'Video Viewed Successfully', 
                    ];
                    return response($response, 200);
                } else {
                    $response = [
                        'status' => 'failure',
                        'message' => 'Can not view. This post does not contain a video',
                    ];
                    return response($response, 422);
                }
            } else {
                $response = [
                    'status' => 'failure',
                    'message' => 'Pls provide a valid post ID',
                ];
                return response($response, 422);
            }
        } catch (\Exception $ex) {
            $response = [
                'status' => 'failure',
                'message' => 'Can not view the video. Unidentified error, please contact at ' . env("APP_SUPERADMIN_EMAIL_ID") ,
            ];
            return response($response, 500);
        }
    }

    /**
     * To fetch send challenge list by logged user.
     *
     * @return Restfull Api Response
     */
    public function sendChallengeList() 
    {
        try {
            $my_user_id = Auth::id();
            $send_challenges_list = PostsTagged::where([
                ['tagged_by', $my_user_id],
                ['tagged_type', 'Challenge'],
            ])->orderBy('updated_at', 'DESC')->get();

            $challenges_details = array();
			$postIdList = array();
            $index = 0;
            foreach ($send_challenges_list as $send_challenges) {
                //getting post details
                $post_id = $send_challenges->post_id;
				if ($index != 0 && in_array($post_id, $postIdList)) {
					//
				} else {
					//store post id to check duplication
					array_push($postIdList, $post_id);
					$post = Post::where([
						['id', $post_id], 
					])->get();
					//getting other details about post
					$get_resource_detail = new GetResourceDetailController;
					$get_post_details = $get_resource_detail->getPostDetail($post);
					foreach ($get_post_details as $get_post_detail) {
						$challenges_details[$index] = $get_post_detail;
					}
					if ($send_challenges->challenge_accepted == 1 ) {
						$challenges_details[$index]['status'] = 'Accepted';
					} elseif ($send_challenges->challenge_accepted == 0 ) {
						$challenges_details[$index]['status'] = 'Pending';
					}
					//getting challenge_by details
					$challenge_to_id = $send_challenges->tagged_user;
					$challenge_to = User::where([
						['id', $challenge_to_id], 
					])->first();
					
					$challenges_details[$index]['challenge_to'] = $get_resource_detail->getUserDetail($challenge_to);

					$index++;
				}
            }
            //custom pagination
            $custom_pagination = new CustomPaginationController;
            $challenges_details = collect($challenges_details);
            $challenges_details = $custom_pagination->paginate($challenges_details);
            $response = [
                'status' => 'success',
                'message' => 'Challenges send by me found Successfully',
                'challenges_details' => $challenges_details,
            ];
            return response($response, 200);
        } catch (\Exception $ex) {
            $response = [
                'status' => 'failure',
                'message' => 'Can not show your send Challenges. Unidentified error, please contact at ' . env("APP_SUPERADMIN_EMAIL_ID"),
            ];
            return response($response, 500);
        }   
    }

    /**
     * To fetch pending received challenge list by logged user.
     *
     * @return Restfull Api Response
     */
    public function pendingReceivedChallengeList()
    {
        try {
            $my_user_id = Auth::id();
            $pending_challenges_to_me_list = PostsTagged::where([
                ['tagged_user', $my_user_id],
                ['tagged_type', 'Challenge'],
                ['challenge_accepted', 0],
            ])->orderBy('updated_at', 'DESC')->get();

            $challenges_details = array();
            $index = 0;
            foreach ($pending_challenges_to_me_list as $challenge_to_me) {
                //getting post details
                $post_id = $challenge_to_me->post_id;
                $post = Post::where([
                    ['id', $post_id], 
                ])->get();
                //getting other details about post
                $get_resource_detail = new GetResourceDetailController;
                $get_post_details = $get_resource_detail->getPostDetail($post);
                foreach ($get_post_details as $get_post_detail) {
                    $challenges_details[$index] = $get_post_detail;
                }
                $challenges_details[$index]['status'] = 'Pending';
                //getting challenge_by details
                $challenge_by_id = $challenge_to_me->tagged_by;
                $challenge_by = User::where([
                    ['id', $challenge_by_id], 
                ])->first();
                $challenges_details[$index]['challenge_by'] = $get_resource_detail->getUserDetail($challenge_by);

                $index++;
            }
            //custom pagination
            $custom_pagination = new CustomPaginationController;
            $challenges_details = collect($challenges_details);
            $challenges_details = $custom_pagination->paginate($challenges_details);
            $response = [
                'status' => 'success',
                'message' => 'Received Challenges pending by me found Successfully',
                'challenges_details' => $challenges_details,
            ];
            return response($response, 200);
        } catch (\Exception $ex) {
            $response = [
                'status' => 'failure',
                'message' => 'Can not show your received pending Challenges. Unidentified error, please contact at ' . env("APP_SUPERADMIN_EMAIL_ID"),
            ];
            return response($response, 500);
        }
    }

    /**
     * Reply on an existing comment under post by logged user.
     *
     * @param  object  $request
     * @return Restfull Api Response
     */
    public function postCommentReply(Request $request) 
    {
        try {
            $validator = Validator::make($request->all(), [
                'comment_id' => 'required|regex:/^[0-9]{1,11}$/u',
                'comment_reply' => 'required|string|max:255',
            ]);   

            if ($validator->fails()) {
                return response([
                    'status' => 'failure',
                    'message'=>$validator->errors()->all()
                ], 422);
            }

            $input['user_id'] = Auth::user()->id;
            $input['comment_id'] = $request->comment_id;
            $input['comment_reply'] = $request->comment_reply;
            $post_comment = PostComment::where([
                ['id', $input['comment_id']],
            ])->first();
        
            if (is_null($post_comment)) {
                $response = [
                    'status' => 'failure',
                    'message' => 'Provid a valid comment id',
                ];
                return response($response, 422);
            } 

            $post = Post::where([
                ['id', $post_comment->post_id],
            ])->first();
            if (($input['user_id'] != $post->user_id) && ($post->post_type == 'private')) {
                $is_friend = UsersFriend::where([
                    ['user_id', $input['user_id']],
                    ['user_friend_id', $post->user_id],
                    ['friend_accepted', 1],
                ])->orWhere([
                    ['user_id', $post->user_id],
                    ['user_friend_id', $input['user_id']],
                    ['friend_accepted', 1],
                ])->first();
                if (is_null($is_friend)) {
                    $response = [
                        'status' => 'failure',
                        'message' => 'This is a private post. Only a friend can reply on comment',
                    ];
                    return response($response, 422);
                }
            }
            $post_comment_reply = PostCommentReply::create($input); 

            //notification
            $applicant = User::find($input['user_id']);
            if ($applicant->id != $post_comment->user_id) {
                Notification::create([
                    'applicant_id'     => $applicant->id,
                    'respondent_id'   => $post_comment->user_id,
                    'applicant_resource_id' => $post_comment_reply->id,
                    'respondent_resource_id' => $post_comment->id,
                    'motive' => 'reply_on_comment',
                    'notification_type' => 'New',
                    'message' => $applicant->name . ' has replied to your comment',
                ]);
            }
            $response = [
                'status' => 'success',
                'message' => 'The comment replied successfully',
                'post' => $post_comment_reply,
            ];
            return response($response, 200);
        } catch (\Exception $ex) {
            $response = [
                'status' => 'failure',
                'message' => 'Can not reply on comment. Unidentified error, please contact at ' . env("APP_SUPERADMIN_EMAIL_ID"),
            ];
            return response($response, 500);
        }
    } 

    /**
     * Delete an existing Post Comment Reply.
     *
     * @param int $id
     * @return Restfull Api Response
     */
    public function deletePostCommentReply($id)
    {
        try {
            $auth_id = Auth::user()->id;
            $post_comment_reply = PostCommentReply::find($id);
            if (is_null($post_comment_reply)) {
                $response = [
                    'status' => 'failure',
                    'message' => 'There is no such comment reply to delete',
                ];
                return response($response, 422);
            }
            $post_comment = PostComment::find($post_comment_reply->comment_id);
            
            if (($auth_id == $post_comment_reply->user_id) || ($auth_id == $post_comment->user_id)) {
                //deleting reply on comment
                $post_comment_reply->delete();
                $response = [
                    'status' => 'success',
                    'message' => 'Reply on comment deleted successfully',
                ];
                return response($response, 200);
            } else {
                $response = [
                    'status' => 'failure',
                    'message' => 'Yor are not authorised to delete this comment reply',
                ];
                return response($response, 422);
            } 
        } catch (\Exception $ex) {
            $response = [
                'status' => 'failure',
                'message' => 'Can not delete reply on comment. Unidentified error, please contact at ' . env("APP_SUPERADMIN_EMAIL_ID"),
            ];
            return response($response, 500);
        }
    }

    /**
     * Delete an existing Post Comment.
     *
     * @param int $id
     * @return Restfull Api Response
     */
    public function deletePostComment($id)
    {
        try {
            $auth_id = Auth::user()->id;
            $post_comment = PostComment::find($id);
            if (is_null($post_comment)) {
                $response = [
                    'status' => 'failure',
                    'message' => 'There is no such comment on post to delete',
                ];
                return response($response, 422);
            }
            $post = Post::find($post_comment->post_id);
            
            if (($auth_id == $post_comment->user_id) || ($auth_id == $post->user_id)) {
                //deleting comment on post
                $post_comment->delete();
                $response = [
                    'status' => 'success',
                    'message' => 'Comment on post deleted successfully',
                ];
                return response($response, 200);
            } else {
                $response = [
                    'status' => 'failure',
                    'message' => 'Yor are not authorised to delete this comment ',
                ];
                return response($response, 422);
            } 
        } catch (\Exception $ex) {
            $response = [
                'status' => 'failure',
                'message' => 'Can not delete post comment. Unidentified error, please contact at ' . env("APP_SUPERADMIN_EMAIL_ID"),
            ];
            return response($response, 500);
        }
    }

    /**
     * To report a post by logged user.
     *
     * @param  object  $request
     * @return Restfull Api Response
     */
    public function reportPost(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'post_id'     => 'required|regex:/^[0-9]{1,11}$/u',
                'report_text' => 'required|string|max:255',
            ]);   

            if ($validator->fails()) {
                return response([
                    'status' => 'failure',
                    'message'=>$validator->errors()->all()
                ], 422);
            }
            $post = Post::find($request->post_id);
            if (is_null($post)) {
                $response = [
                    'status' => 'failure',
                    'message' => 'Please provide a valid post id',
                ];
                return response($response, 422);
            }
            $auth_user = Auth::user()->id;
            $already_reported = ReportPost::where([
                ['user_id', $auth_user],
                ['post_id', $post->id],
            ])->first();

            if ($already_reported) {
                $response = [
                    'status' => 'failure',
                    'message' => 'Post can not be reported more then once',
                ];
                return response($response, 422);
            }
            $report = ReportPost::create([
                'user_id'     => $auth_user,
                'post_id'   => $post->id,
                'report_text' => $request->report_text,
            ]);
            
            //getting other details about post 
            $get_resource_detail = new GetResourceDetailController;
            $post = Post::where([
                ['id', $request->post_id],
            ])->get();
            //getting other details about post
            $report->post = $get_resource_detail->getPostDetail($post);

            $details = array();
            $details['report'] = $report;
            $details['reported_by'] = User::find($auth_user);
            \Mail::to(env("APP_SUPERADMIN_EMAIL_ID"))->send(new \App\Mail\PostReportedNotificationToSuperAdmin($details));
            $response = [
                'status' => 'success',
                'message' => 'The post reported successfully',
                'report' => $report,
            ];
            return response($response, 200);
        } catch (\Exception $ex) {
            $response = [
                'status' => 'failure',
                'message' => 'Can not report post. Unidentified error, please contact at ' . env("APP_SUPERADMIN_EMAIL_ID"),
            ];
            return response($response, 500);
        }
    }
}
