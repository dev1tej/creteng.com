<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;

class AdvertiserResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    
    public function __construct()
    {
        $this->middleware('guest:adv_user');
    }
    
    /*
    protected function guard() {
        return Auth::guard('guest:adv_user');
    }
    */
    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;


    protected function broker() {
        return Password::broker('adv_users');
    }

    public function redirectTo() {
        /*
        $user = Auth::user();
        $superadmin=env("APP_SUPERADMIN");
        $manager=env("APP_MANAGER");
        if($user->hasRole($superadmin) || $user->hasRole($manager)){
            return route('dashboard');
        } else {
            session()->flush();
            return route('comingSoon');
        }
        */
        session()->flush();
        //session()->flash('success', 'You have Updated your password successfully');
        //return route('login');
        return route('advertiser.password.reset.temp', ['success' => 'You have Updated your password successfully']);
    }

    /**
     * Display the password reset view for the given token.
     *
     * If no token is present, display the link request form.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string|null  $token
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showResetForm(Request $request, $token = null)
    {
        return view('auth.passwords.reset')->with(
            ['token' => $token, 'email' => $request->email]
        );
    }
}
