@extends('admin.layouts.master')
@section('content')
<div class="container">
  <div class="row">
    <div class="col-lg-12 margin-tb">
      <div class="pull-left">
        <h2>{{ $auth_user->name }}'s Friends</h2>
      </div>
    </div>
  </div>
  @if ($message = Session::get('success'))
  <div class="alert alert-success">
    <p>{{ $message }}</p>
  </div>
  @endif
  @if ($message = Session::get('danger'))
  <div class="alert alert-danger">
    <p>{{ $message }}</p>
  </div>
  @endif
  @if (isset($success))
  <div class="alert alert-success">
    <p>{{ $success }}</p>
  </div>
  @endif
  <section class="content">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title"></h3>
            <div class="pull-right">
              <a class="btn btn-primary" href="{{ route('showAllCustomer') }}"> Back</a>
            </div>
          </div>
          <div class="card-body">
            <table id="friendData" class="table table-bordered table-striped table-hover">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Name</th>
                  <th>About</th>
                  <th>Email</th>
                  <th>Roles</th>
                </tr>
              </thead>
              <tbody>
                @php $srno = '1'; @endphp
                @forelse ($data as $key => $user)
                <tr>
                  <td>@php echo $srno; @endphp</td>
                  <td><a href="{{ route('showCustomerFriend', [$auth_user->id, $user->id]) }}">{{ $user->name }}</a></td>
                  <td>{{ $user->about }}</td>
                  <td>{{ $user->email }}</td>
                  <td>
                    @php
                    $customers=env("APP_CUSTOMER");
                    $managers=env("APP_MANAGER");
                    @endphp
                    @if(!empty($user->getRoleNames()))
                    @foreach($user->getRoleNames() as $v)
                    @if($v == $customers)
                    <label class="badge badge-warning">{{ $v }}</label>
                    @endif
                    @if($v == $managers)
                    <label class="badge badge-success">{{ $v }}</label>
                    @endif
                    @endforeach
                    @endif
                  </td>
                </tr>
                @php $srno++; @endphp
                @empty
                No Friends Found
                @endforelse
              </tbody>
              <tfoot>
                <tr>
                  <th>No</th>
                  <th>Name</th>
                  <th>About</th>
                  <th>Email</th>
                  <th>Roles</th>
                </tr>
              </tfoot>
            </table>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

<script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script>
  $(function () {
    $('#friendData').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>
@endsection