<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Stripe\Stripe;
use Stripe\Customer;
use Stripe\Charge;
use Owenoj\LaravelGetId3\GetId3;
use App\Countries;
use App\States;
use App\Advertisement;
use App\AdsPayment;
use App\AdvUser;
use App\AdvPlan;
use App\AdvPlDuration;
use App\AdvPlFixedService;
use App\AdvPlOptService;
use App\AdvAvailableService;
use App\AdvPublicationLocation;

class AdvertiserRegisterController extends Controller
{
    
    public function __construct()
    {
        //$this->middleware('guest:adv_user');
    }
    

    public function usAndEuCountries()
    {
        $usAndEuCountries = [
            'UNITED STATES',
            'FRANCE',
            'SPAIN',
            'SWEDEN',
            'GERMANY',
            'FINLAND',
            'POLAND',
            'ITALY',
            //'ROMANIA',
            'GREECE',
            'BULGARIA',
            'HUNGARY',
            'PORTUGAL',
            'AUSTRIA',
            //'CZECH REPUBLIC',
            'IRELAND',
            'LITHUANIA',
            'LATVIA',
            'CROATIA',
            //'SLOVAKIA',
            'ESTONIA',
            'DENMARK',
            'NETHERLANDS',
            'BELGIUM',
            'SLOVENIA',
            'CYPRUS',
            'LUXEMBOURG',
            'MALTA',
        ];

        return $usAndEuCountries;
    }

    public function showRegisterPersonalDetailForm()
    {
        /*Session::flush();
         Session::save();
        print_r(Session::get('advertiser_id'));
        exit();*/
        $usAndEuCountries = $this->usAndEuCountries();
        $countries = Countries::all();
        $countries_with_iso = array();
        foreach($countries as $country) {
            if (in_array($country->name, $usAndEuCountries)) {
                $countries_with_iso[] = $country->name . '-' . $country->iso;
            }
        }
        //echo"<pre>";
        //print_r($countries_with_iso);
        //exit();
        return view('front.registration.index', compact('countries_with_iso'));
    }

    /**
     * Creating new Advertisement
     * 
     * @param \Illuminate\Http\Request  $request
     * @return \Illuminate\View\View
     */

    public function postRegisterPersonalDetail(Request $request)
    {
        /*Session::put('countries', '$request->countries');
        Session::put('states', '$request->states');
        echo"<pre>";
        print_r(Session::get('countries'));
        print_r(Session::get('states'));
        exit();*/

        /*if (session()->exists('advertiser_id')) {
            Session::forget('advertiser_id');
            Session::forget('subscription'); 
            Session::save();
        }
        print_r(Session::get('advertiser_id'));
        exit();*/
        if (session()->exists('advertiser_id')) {
            $advertiser_id = Session::get('advertiser_id');
        } else {
            /* Check Validation */
            $this->validate($request, [
                'name' => 'required|string|max:255',
                'company_name' => 'string|max:255|nullable',
                'email' => 'required|email|string|unique:adv_users',
                'password' => 'required|string|min:8|confirmed',
                'contact' => 'required|digits_between: 8,12',
                'address_line_1' => 'required|string|max:255',
                'address_line_2' => 'string|nullable|max:255',
                'city' => 'required|string|max:100',
                'state' => 'required|string|max:100',
                'postal_code' => 'required|string|max:20',
                'country' => 'required|string|max:100',
            ]);

            $password = Hash::make($request->password);
            $advertiser = AdvUser::create([
                'name'           => $request->name,
                'company_name'   => $request->company_name,
                'email'          => $request->email,
                'password'       => $password,
                'contact'        => $request->contact,
                'address_line_1' => $request->address_line_1,
                'address_line_2' => $request->address_line_2,
                'city'           => $request->city,
                'state'          => $request->state,
                'postal_code'    => $request->postal_code,
                'country'        => $request->country,
            ]);
            
            $advertiser_id = $advertiser->id;
            Session::put('advertiser_id', $advertiser->id);
        }
        
        return redirect()->route('showRegisterSelectedPlan', [$advertiser_id]);
    }

    public function showRegisterSelectedPlan($advertiser_id) 
    {
        $ad_plans = AdvPlan::with('fixServices', 'planDuration')->get();
        $plan_opt_services = AdvPlOptService::all();
        return view('front.registration.plan', compact('ad_plans', 'plan_opt_services', 'advertiser_id'));
    }

    /**
     * Creating new Advertisement
     * 
     * @param \Illuminate\Http\Request  $request
     * @return \Illuminate\View\View
     */

    public function postRegisterSelectedPlan(Request $request)
    {
        /* Check Validation */
        $this->validate($request, [
            'advertiser_id' => 'required',
            'plan_id' => 'required',
            'sub_plan_id' => 'required',
            'opt_service_ids' => 'array',
            'no_of_publication_location' => 'required',
        ]);
        $advertiser_id = $request->advertiser_id;
        $plan_id = $request->plan_id;
        $sub_plan_id = $request->sub_plan_id;
        $opt_services = $request->opt_service_ids;
        $no_of_publication_location = $request->no_of_publication_location;

        $opt_service_ids = '';
        if(!is_null($opt_services)) {
            foreach ($opt_services as $opt_service) {
                $opt_service_ids .= 0;
                $opt_service_ids .= $opt_service;
                
            }
        } else {
            $opt_service_ids .= 0;
        }

        return redirect()->route('showRegisterAd', [
            $advertiser_id, 
            $plan_id, 
            $sub_plan_id,
            $opt_service_ids,
            $no_of_publication_location
        ]);
    }

    public function showRegisterAd($advertiser_id, $plan_id, $sub_plan_id, $opt_service_ids, $no_of_publication_location) 
    {
        $opt_service_ids = explode('0', $opt_service_ids);
        array_shift($opt_service_ids); 
        
        $plan_fixed_services = AdvPlFixedService::where([
            ['plan_id', $plan_id],
        ])->first();

        if (preg_match("/image/i", $plan_fixed_services->fixed_services)) {
            $ad_type = 'image';
        } elseif (preg_match("/video/i", $plan_fixed_services->fixed_services)) {
            $ad_type = 'video';
        }
        return view('front.registration.advertisement', compact(
            'advertiser_id', 
            'plan_id', 
            'sub_plan_id',
            'opt_service_ids',
            'plan_fixed_services',
            'ad_type',
            'no_of_publication_location'
        ));
    }

    public function getStates(Request $request) {
        $country = Countries::where('name', $request->country)->first();
        if ($country->name == 'UNITED STATES') {
            $states_details = States::where('country_id', $country->id)->get();
            $states = array();
            foreach ($states_details as $state) {
                $states[] = $state->name;
            }
            rsort($states);
        } else {
            $states = array();
            $states[] = $country->name;
        }
        return $states;
    }

    public function postRegisterAd(Request $request) {

        /* Check Validation */
        $this->validate($request, [
            'countries' => 'required|array',
            'states' => 'required|array',
            'advertiser_id' => 'required',
            'plan_id' => 'required',
            'sub_plan_id' => 'required',
            'opt_service_ids' => 'array',
            'ad_type'  => 'required',
            'advertisement'  => 'required|file',
            'ad_description' => 'string|nullable',
            'website_url' => 'string|nullable',
            'google_store_url' => 'string|nullable',
            'apple_store_url' => 'string|nullable',
        ]);

        //Validating ad geolocations
        $pc_geolocation = array();
        $index = 0;
        $no_of_publication_state = 0;
        foreach ($request->countries as $country_key=>$country) {
            foreach ($request->states as $state_key=>$state) {
                if ($country_key == $state_key) {
                    $no_of_publication_state++;
                    if ($country != $state) {
                        $url = "https://maps.googleapis.com/maps/api/geocode/json?address=".urlencode($state)."&key=".env("GOOGLE_API_KEY");
                        $result_string = file_get_contents($url);
                        $result = json_decode($result_string, true);
                        //echo"<pre>";
                        //print_r($result);
                        //exit();

                        if ($result['status'] == 'OK') {
                            $pc_geolocation[$index]['lat'] = $result['results'][0]['geometry']['location']['lat'];
                            $pc_geolocation[$index]['lng'] = $result['results'][0]['geometry']['location']['lng'];
                            $pc_geolocation[$index]['country'] = $country;
                            $pc_geolocation[$index]['state'] = $state;
                            $index++;
                        } elseif ($result['status'] == 'ZERO_RESULTS') {
                            return redirect()->back()->with('flash_message', 'Invalid State of a country');
                        }
                    } else {
                        $url = "https://maps.googleapis.com/maps/api/geocode/json?address=".urlencode($country)."&key=".env("GOOGLE_API_KEY");
                        $result_string = file_get_contents($url);
                        $result = json_decode($result_string, true);

                        if ($result['status'] == 'OK') {
                            $pc_geolocation[$index]['lat'] = $result['results'][0]['geometry']['location']['lat'];
                            $pc_geolocation[$index]['lng'] = $result['results'][0]['geometry']['location']['lng'];
                            $pc_geolocation[$index]['country'] = $country;
                            $pc_geolocation[$index]['state'] = $country;
                            $index++;
                        } elseif ($result['status'] == 'ZERO_RESULTS') {
                            return redirect()->back()->with('flash_message', 'Invalid country');
                        }
                        //echo"<pre>";
                        //print_r($result);
                        //exit();
                    }
                } 
            }
        }
        //echo "<pre>";
        //print_r($pc_geolocation);
        //exit();

        //validating file upload
        $track = GetId3::fromUploadedFile($request->file('advertisement'));
        $file_info = $track->extractInfo();
        if (($request->ad_type == 'Video') && (preg_match("/Video/i", $file_info['mime_type']))) {
            $play_time = $track->getPlaytime();
            $plan_fixed_services = AdvPlFixedService::where([
                ['plan_id', $request->plan_id],
            ])->first();

            if (preg_match("!\d+!", $plan_fixed_services->fixed_services, $allow_play_time)) {
                if ($allow_play_time[0] < $play_time) {
                    $msg = 'Video length cannot be greater than '.$allow_play_time[0].' minutes';
                    return back()->with('danger', $msg);
                }
            } 
        } elseif (($request->ad_type == 'Video') && (preg_match("/image/i", $file_info['mime_type'])) || ($request->ad_type == 'image') && (preg_match("/video/i", $file_info['mime_type']))) {
            return back()->with('danger', 'Uploaded file is not in accordance with the chosen plan');
        }

        //storing file to amazon
        $path = $request->file('advertisement')->store('uservideos/advertisement', 's3');
        $aws_file_path = env('AWS_URL') . $path; 

        //storing ad in db
        if (isset($request->is_app) && ($request->is_app == 'on')) {
            $is_app = true;
        } else {
            $is_app = false;
        }

        /* Collecting selected plan details*/
        $plan = AdvPlan::find($request->plan_id);
        $sub_plan = AdvPlDuration::find($request->sub_plan_id);

        $opt_services = array();
        $total_opt_services_cost = 0;
        $multiplying_factor = '';
        if (isset($request->opt_service_ids) && (!is_null($request['opt_service_ids'][0]))) {
            foreach($request->opt_service_ids as $opt_service_id) {
                if (session()->exists('subscription')) {
                    $adv_available_service = AdvAvailableService::where('advertiser', Session::get('advertiser_id'))->first();
                    $adv_available_service->update([
                        'advertiser'    => $request->advertiser_id,
                        'plan'          => $request->plan_id,
                        'duration'      => $request->sub_plan_id,
                        'opt_services'  => $opt_service_id,
                        'status'        => false,
                    ]);

                } else {
                    $adv_available_service = AdvAvailableService::create([
                        'advertiser'    => $request->advertiser_id,
                        'plan'          => $request->plan_id,
                        'duration'      => $request->sub_plan_id,
                        'opt_services'  => $opt_service_id,
                        'status'        => false,
                    ]);
                }
                $opt_service = AdvPlOptService::find($opt_service_id);

                if ($sub_plan->plan_duration == 'Weekly') {
                    $multiplying_factor = '*1';
                    $total_opt_services_cost += $opt_service->aditional_price * 1;
                } 
                if ($sub_plan->plan_duration == 'Monthly') {
                    $multiplying_factor = '*4';
                    $total_opt_services_cost += $opt_service->aditional_price * 4;
                } 
                if ($sub_plan->plan_duration == 'Quarterly') {
                    $multiplying_factor = '*13';
                    $total_opt_services_cost += $opt_service->aditional_price * 13;
                } 
                if ($sub_plan->plan_duration == 'Half Yearly') {
                    $multiplying_factor = '*26';
                    $total_opt_services_cost += $opt_service->aditional_price * 26;
                } 
                if ($sub_plan->plan_duration == 'Yearly') {
                    $multiplying_factor = '*52';
                    $total_opt_services_cost += $opt_service->aditional_price * 52;
                }
                $opt_services[] = $opt_service;
            }
        } else {
            if (session()->exists('subscription')) {
                $adv_available_service = AdvAvailableService::where('advertiser', Session::get('advertiser_id'))->first();
                $adv_available_service->update([
                    'advertiser'    => $request->advertiser_id,
                    'plan'          => $request->plan_id,
                    'duration'      => $request->sub_plan_id,
                    'opt_services'  => null,
                    'status'        => false,
                ]);

            } else {
                $adv_available_service = AdvAvailableService::create([
                    'advertiser'    => $request->advertiser_id,
                    'plan'          => $request->plan_id,
                    'duration'      => $request->sub_plan_id,
                    'opt_services'  => null,
                    'status'        => false,
                ]);
            }
        } 

        if (session()->exists('subscription')) {
            $advertisement = Advertisement::where('advertiser_id', Session::get('advertiser_id'))->first();
            /* Deleting existing ad */
            Storage::disk('s3')->delete($advertisement->advertisement);
            
            $advertisement->update([
                'advertiser_id'        => $request->advertiser_id,
                'service_id'           => $adv_available_service['id'],
                'advertisement'        => $aws_file_path,
                'description'          => $request->ad_description,
                'website_url'          => $request->website_url,
                'is_app'               => $is_app,
                'google_store_url'     => $request->google_store_url,
                'apple_store_url'      => $request->apple_store_url,
                'ad_type'              => $request->ad_type,
            ]);
        } else {
            $advertisement = Advertisement::create([
                'advertiser_id'        => $request->advertiser_id,
                'service_id'           => $adv_available_service['id'],
                'advertisement'        => $aws_file_path,
                'description'          => $request->ad_description,
                'website_url'          => $request->website_url,
                'is_app'               => $is_app,
                'google_store_url'     => $request->google_store_url,
                'apple_store_url'      => $request->apple_store_url,
                'ad_type'              => $request->ad_type,
            ]);
        }

        $advertiser_id = $request->advertiser_id;
        $advertisement_id = $advertisement->id;
        $total_price = ($sub_plan->price + $total_opt_services_cost) * $no_of_publication_state;

        if (session()->exists('subscription')) {
            AdvPublicationLocation::where('advertiser', Session::get('advertiser_id'))->delete();
        }
        foreach ($pc_geolocation as $geolocation) {
            AdvPublicationLocation::create([
                'advertiser'  => $advertisement->advertiser_id,
                'service_id'  => $adv_available_service['id'],
                'country'     => $geolocation['country'],
                'state'       => $geolocation['state'],
                'lat'         => $geolocation['lat'],
                'long'        => $geolocation['lng'],
            ]);
        }
        if (!session()->exists('subscription')) {
            Session::put('subscription', 'done'); 
        }      

        return view('front.registration.stripe', compact(
            'advertiser_id', 
            'advertisement_id', 
            'plan',
            'sub_plan',
            'opt_services',
            'multiplying_factor',
            'total_opt_services_cost',
            'total_price',
            'no_of_publication_state'
        ));
    }

    /**
     * Creating Adv payment at hold position
     * 
     * @param \Illuminate\Http\Request  $request
     * @return \Illuminate\View\View
     */
    public function postRegisterPayment(Request $request) 
    {
        if (session()->exists('advertiser_id')) {
            $advertisement = Advertisement::find($request->advertisement_id);
            $advertiser    = AdvUser::find($request->advertiser_id);
            $country_explode = explode('-', $advertiser->country);
            $iso_code = $country_explode[1];

            Stripe::setApiKey(env('STRIPE_SECRET'));
            $customer = Customer::create(array(
                'name' => $advertiser->name,
                "source" => $request->stripeToken,      
                'email' => $advertiser->email,
                "address" => [
                    'city'        => $advertiser->city, 
                    'country'     => $iso_code, 
                    'line1'       => $advertiser->address_line_1, 
                    'line2'       => $advertiser->address_line_2, 
                    'postal_code' => $advertiser->postal_code, 
                    'state'       => $advertiser->state
                ], 
            ));
            $charge = \Stripe\Charge::create([
                'customer' => $customer->id,
                'amount' => 100 * $request->amount,
                'currency' => 'usd',
                'description' => 'Payment request for Ad Publication in Creteng',
                'capture' => false,
            ]);

            $ad_services = AdvAvailableService::where([
                ['advertiser', $advertisement['advertiser_id']],
            ])->first();
            
            $ads_payment  = AdsPayment::create([
                'advertiser_id'      => $advertiser->id,
                'service_id'         => $ad_services['id'],
                'stripe_customer_id' => $customer->id,
                'transaction_id'     => $charge->id,
                'status'             => 'Hold',
                'payment_mode'       => $request->payment_mode,
                'amount'             => $request->amount,
            ]);
            
            

            $details = array();
            $details['advertiser']['id'] = $advertiser->id;
            $details['advertiser']['name'] = $advertiser->name;
            $details['advertiser']['email'] = $advertiser->email;
            $details['advertiser']['contact'] = $advertiser->contact;
            $details['advertiser']['company_name'] = $advertiser->company_name;
            $details['advertisement']['id'] = $advertisement->id;
            $details['advertisement']['advertisement'] = $advertisement->advertisement;

            $details['advertisement']['plan'] = AdvPlan::find($ad_services['plan']);
            $details['advertisement']['plan_fixed_service'] = AdvPlFixedService::where('plan_id', $details['advertisement']['plan']['id'])->first();
            $details['advertisement']['plan_duration'] = AdvPlDuration::find($ad_services['duration']);
            if (!is_null($ad_services['opt_services'])) {
                $details['advertisement']['plan_opt_service'] = AdvPlOptService::find($ad_services['opt_services']);
            } else {
                $details['advertisement']['plan_opt_service'] = null;
            }
            
            $total_opt_services_cost = 0;
            $multiplying_factor = '';
            if (!is_null($details['advertisement']['plan_opt_service'])) {
                if ($details['advertisement']['plan_duration']['plan_duration'] == 'Weekly') {
                    $multiplying_factor = '*1';
                    $total_opt_services_cost += $details['advertisement']['plan_opt_service']['aditional_price'] * 1;
                } 
                if ($details['advertisement']['plan_duration']['plan_duration'] == 'Monthly') {
                    $multiplying_factor = '*4';
                    $total_opt_services_cost += $details['advertisement']['plan_opt_service']['aditional_price'] * 4;
                } 
                if ($details['advertisement']['plan_duration']['plan_duration'] == 'Quarterly') {
                    $multiplying_factor = '*13';
                    $total_opt_services_cost += $details['advertisement']['plan_opt_service']['aditional_price'] * 13;
                } 
                if ($details['advertisement']['plan_duration']['plan_duration'] == 'Half Yearly') {
                    $multiplying_factor = '*26';
                    $total_opt_services_cost += $details['advertisement']['plan_opt_service']['aditional_price'] * 26;
                } 
                if ($details['advertisement']['plan_duration']['plan_duration'] == 'Yearly') {
                    $multiplying_factor = '*52';
                    $total_opt_services_cost += $details['advertisement']['plan_opt_service']['aditional_price'] * 52;
                }
            }
            $details['advertisement']['multiplying_factor'] = $multiplying_factor;
            $details['advertisement']['total_opt_services_cost'] = $total_opt_services_cost;

            $details['advertisement']['transaction_id'] = $ads_payment->transaction_id;
            $details['advertisement']['amount'] = $ads_payment->amount;

            $details['advertisement']['publishable_locations'] = AdvPublicationLocation::where('advertiser',$advertisement['advertiser_id'])->get();

            \Mail::to($advertiser->email)->send(new \App\Mail\front\PublishableAdNotificationToAdvertiser($details));
            \Mail::to(env('APP_SUPERADMIN_EMAIL_ID'))->send(new \App\Mail\front\PublishableAdNotificationToSuperAdmin($details));
            Session::flush();
        }

        return view('front.registration.thanks');
    }
}
