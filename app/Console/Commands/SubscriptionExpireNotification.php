<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use App\AdvAvailableService;
use App\AdsPayment;
use App\AdvUser;
use App\AdvPlan;
use App\AdvPlFixedService;
use App\AdvPlDuration;
use App\AdvPlOptService;
use App\SubscriptionExpireCronTest;

class SubscriptionExpireNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'SubscriptionExpire:Notification';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'To notify advertisers about subscription expiry in last seven days of expiry.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('SubscriptionExpire:Notification Command Run successfully!');
        $ads_payment = AdsPayment::all();
        $date_now = date('Y-m-d');
        $date_7days = \Carbon\Carbon::today()->addDays(7)->format('Y-m-d');
        foreach ($ads_payment as $ad_payment) {
            if (($ad_payment->expiry_date >= $date_now) && ($ad_payment->expiry_date <= $date_7days)) {
                
                $service = AdvAvailableService::find($ad_payment->service_id);
                if ($service['status'] == true) {
                    $details = array();
                    $details['advertiser'] = AdvUser::find($ad_payment->advertiser_id);
                    $details['ad_payment'] = $ad_payment;
                    $ad_services = AdvAvailableService::where([
                        ['advertiser', $ad_payment->advertiser_id],
                        ['status', true],
                    ])->first();
                    
                    $details['plan'] = AdvPlan::find($ad_services['plan']);
                    $details['plan_fixed_service'] = AdvPlFixedService::where('plan_id', $details['plan']['id'])->first();
                    $details['plan_duration'] = AdvPlDuration::find($ad_services['duration']);
                    if (!is_null($ad_services['opt_services'])) {
                        $details['plan_opt_service'] = AdvPlOptService::find($ad_services['opt_services']);
                    } else {
                        $details['plan_opt_service'] = null;
                    }
                    \Mail::to($details['advertiser']['email'])->send(new \App\Mail\Cron\SubscriptionExpireNotificationToAdvertiser($details));
                }
            }
        }
        SubscriptionExpireCronTest::create(['run_successfully' => true]);
        Log::info("SubscriptionExpire:Notification is working fine!");
    }
}
