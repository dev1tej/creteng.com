@extends('admin.layouts.master')
@section('content')
<div class="container">
  <div class="row">
    <div class="col-lg-12 margin-tb">
      <div class="pull-left">
        <h2>View Advertisement Plan</h2>
      </div>
      <div class="pull-right">
        <a class="btn btn-primary" href="{{ route('showAllAdPlan') }}"> Back</a>
      </div>
    </div>
  </div>
  @if (count($errors) > 0)
  <div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
      @foreach ($errors->all() as $error)
      <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
  @endif
  @if (isset($success))
  <div class="alert alert-success">
    <p>{{ $success }}</p>
  </div>
  @endif
  @if (isset($danger))
  <div class="alert alert-danger">
    <p>{{ $danger }}</p>
  </div>
  @endif
  <div class="container">
    <h3 align="center">Plan Name:  {{ $ad_plan->plan_name }}</h3>
    <br />
    <strong align="left">Plan Fix Service:</strong>
    @foreach($ad_plan->fixServices as $fixService)
    @php $srno = '1'; @endphp
    <p><span>@php echo $srno; @endphp:</span> {{ $fixService->fixed_services }}</p>
    @php $srno++; @endphp
    @endforeach

    <h3 align="center">Plan Durations</h3>
    <div class="table-repsonsive">
    <span id="error"></span>
    <table class="table table-bordered" id="item_table">
      <tr>
        <th>No</th>
        <th>Plan Duration</th>
        <th>Price (in USD)</th>
      </tr>
      @php $srno = '1'; @endphp
      @foreach($ad_plan->planDuration as $planDuration)
      <tr>
        <td>@php echo $srno; @endphp</td>
        <td>{{ $planDuration->plan_duration }}</td>
        <td>{{ $planDuration->price }}</td>
      </tr>
      @php $srno++; @endphp
      @endforeach
    </table>
    </div>
   </div>
</div>
@endsection
