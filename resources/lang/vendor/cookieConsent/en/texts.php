<?php

return [
    'message' => "We use cookies to enhance your experience. 
                  By continuing to visit this site you agree to our use of cookies.
                  <a href='https://en.wikipedia.org/wiki/HTTP_cookie'>More info</a>",
    'agree' => 'Got it!',
];
