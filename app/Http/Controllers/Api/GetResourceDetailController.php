<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Post;
use App\PostLike;
use App\PostComment;
use App\PostCommentReply;
use App\SavePost;
use App\User;
use App\UsersFriend;
use App\PostsTagged;
use App\PostVideoViewed;
use App\PostsTaggedReply;
use App\SaveReplyToChallenge;
use App\ChallengeReplyLike;
use App\ChallengeCommentReply;
use App\ChallengeCommentCommentReply;
use App\OnBoardingImage;
use App\ChallengeReplyVideoViewed;
use App\Notification;
use App\Http\Controllers\Api\CustomPaginationController;

class GetResourceDetailController extends Controller
{
    /**
     * Getting other relevent details about posts.
     *
     * @param array $posts
     * @return $posts_with_details
     */
    public function getPostDetail($posts) 
    {
        $logged_user_id = Auth::id();
        $post_details = array();
        foreach ($posts as $post) {
            //getting post details
            $post_detail = array();
            $post_detail['id'] = $post['id'];
            $post_detail['user_id'] = $post['user_id'];
            $post_detail['post_type'] = $post['post_type'];
            $post_detail['post_text'] = $post['post_text'];
            $post_detail['post_binary'] = $post['post_binary'];
            $post_detail['post_binary_mime_type'] = $post['post_binary_mime_type'];
            $post_detail['include_challenges'] = $post['include_challenges'];
            $post_detail['location'] = $post['location'];
            $post_detail['created_at'] = $post['created_at'];
            $post_detail['updated_at'] = $post['updated_at'];

            //getting post video viewed
            if ($post_detail['post_binary_mime_type'] == 'video') {
                $post_detail['video_view_by_logged_user'] = false;
                $post_video_views = PostVideoViewed::where([
                    ['post_id', $post_detail['id']],
                ])->get();
                foreach ($post_video_views as $post_video_view) {
                    if ($post_video_view->user_id == $logged_user_id) {
                        $post_detail['video_view_by_logged_user'] = true;
                    }
                }
                $post_detail['total_video_views'] = $post_video_views->count();
            }
            //getting post owner
            $post_detail['posted_by'] = User::where([
                ['id', $post_detail['user_id']],
            ])->first();
            if ($post_detail['posted_by']['profile_pic'] == null) {
                $post_detail['posted_by']['profile_pic'] = asset('image/c.svg') ;
            }
            
            //getting tagged users
            $tagged_user_lists = PostsTagged::where([
                ['post_id', $post_detail['id']],
                ['tagged_by', $post_detail['user_id']],
            ])->get();
            if ($tagged_user_lists) {
                $tagged_users = array();
                $index = 0;
                foreach ($tagged_user_lists as $tagged_user) {
                    $tagged_users[$index] = User::where([
                        ['id', trim($tagged_user['tagged_user'])],
                    ])->first();
                    if ($tagged_user['challenge_accepted'] == 1) {
                        $tagged_users[$index]['challenge_accepted'] = true;
                    }
                    if ($tagged_user['challenge_accepted'] == 0) {
                        $tagged_users[$index]['challenge_accepted'] = false;
                    }
                    $index++;
                }
                foreach ($tagged_users as $tagged_user) {
                    if ($tagged_user['profile_pic'] == null) {
                        $tagged_user['profile_pic'] = asset('image/c.svg') ;
                    }
                }
                if ($post_detail['include_challenges'] == 0) {
                    $post_detail['tagged_users']['post_type'] = 'Normal';
                    $post_detail['tagged_users']['tagged_count'] = $tagged_user_lists->count();
                    $post_detail['tagged_users']['challenges_count'] = 'NA';
                } elseif ($post_detail['include_challenges'] == 1) {
                    $post_detail['tagged_users']['post_type'] = 'Challenge';
                    $post_detail['tagged_users']['tagged_count'] = 'NA';
                    $post_detail['tagged_users']['challenges_count'] = $tagged_user_lists->count();
                }
                $post_detail['tagged_users']['users'] = $tagged_users;
            } else {
                $post_detail['tagged_users'] = null;
            }
            
            //getting post challenge reply
            if ($post_detail['include_challenges'] == 0) {
                $post_detail['post_challenge_reply'] = 'NA';
            } elseif ($post_detail['include_challenges'] == 1) {

                $post_challenge_replies = PostsTaggedReply::where([
                    ['post_id', $post_detail['id']],
                ])->get();

                $challenge_reply_detail = array();
                $index = 0;
                foreach ($post_challenge_replies as $post_challenge_reply) {

                    //Challenge reply can only be viewed by challenge owner or tagged users if the challenge reply is private.
                    if ($post_challenge_reply->reply_type == 'private') {

                        $is_tagged = PostsTagged::where([
                            ['post_id', $post['id']],
                            ['tagged_user', $logged_user_id],
                        ])->first();
                        if (($logged_user_id != $post['user_id']) && is_null($is_tagged)) {
                            continue;
                        }
                    }
                    
                    $challenge_reply_detail[$index] = $post_challenge_reply;

                    if ($post_challenge_reply->reply_by_id == $logged_user_id) {
                        $challenge_reply_detail[$index]['can_edit_by_logged_user'] = true;
                    } elseif ($post_challenge_reply->reply_by_id != $logged_user_id) {
                        $challenge_reply_detail[$index]['can_edit_by_logged_user'] = false;
                    }
                    
                    //getting post video viewed
                    if ($post_challenge_reply['reply_binary_mime_type'] == 'video') {
                        $challenge_reply_detail[$index]['video_view_by_logged_user'] = false;
                        $challenge_reply_video_views = ChallengeReplyVideoViewed::where([
                            ['reply_id', $post_challenge_reply['id']],
                        ])->get();
                        foreach ($challenge_reply_video_views as $challenge_reply_video_view) {
                            if ($challenge_reply_video_view->user_id == $logged_user_id) {
                                $challenge_reply_detail[$index]['video_view_by_logged_user'] = true;
                            }
                        }
                        $challenge_reply_detail[$index]['total_video_views'] = $challenge_reply_video_views->count();
                    }

                    $challenge_reply_detail[$index]['challenge_by'] = User::where([
                        ['id', $post_challenge_reply->challenge_by_id],
                    ])->first();
                    $challenge_reply_detail[$index]['challenge_reply_by'] = User::where([
                        ['id', $post_challenge_reply->reply_by_id],
                    ])->first();

                    //getting comment details
                    $comments = ChallengeCommentReply::where([
                        ['challenge_comment_id', $post_challenge_reply->id],
                    ])->get();
                    $i = 0;
                    $comment_details = array();
                    foreach ($comments as $comment) {
                        $comment_details[$i] = $comment;
                        $comment_by = User::find($comment->user_id);

                        //Post owner, Reply owner and comment owner can delete
                        if (($comment_by->id == $logged_user_id) || ($post_challenge_reply->reply_by_id == $logged_user_id) || ($post['user_id'] == $logged_user_id)) {
                            $comment_details[$i]['can_delete_by_logged_user'] = true;
                        } else {
                            $comment_details[$i]['can_delete_by_logged_user'] = false;
                        }

                        if ($comment_by->id == $logged_user_id) {
                            $comment_details[$i]['can_edit_by_logged_user'] = true;
                        } elseif ($comment_by->id != $logged_user_id) {
                            $comment_details[$i]['can_edit_by_logged_user'] = false;
                        }
						
                        $comment_details[$i]['comment_by'] = $comment_by;

                        $comment_replies = ChallengeCommentCommentReply::where([
                            ['r_comment_id', $comment->id],
                        ])->get();
                        $comment_reply_detail = array();
                        $j = 0;
                        $replies_count = 0;
                        foreach ($comment_replies as $comment_reply) {
                            $comment_reply_detail[$j] = $comment_reply;
                            $reply_by = User::find($comment_reply->user_id);

                            //Post owner, Reply owner, comment owner and comment reply owner can delete
                            if (($reply_by->id == $logged_user_id) || ($comment_by->id == $logged_user_id) || ($post_challenge_reply->reply_by_id == $logged_user_id) || ($post['user_id'] == $logged_user_id)) {
                                $comment_reply_detail[$j]['can_delete_by_logged_user'] = true;
                            } else {
                                $comment_reply_detail[$j]['can_delete_by_logged_user'] = false;
                            }

                            if ($reply_by->id == $logged_user_id) {
                                $comment_reply_detail[$j]['can_edit_by_logged_user'] = true;
                            } elseif ($reply_by->id != $logged_user_id) {
                                $comment_reply_detail[$j]['can_edit_by_logged_user'] = false;
                            }
						
                            $comment_reply_detail[$j]['reply_by'] = $reply_by;
                            $j++;
                            $replies_count++;
                        }
                        $comment_replies = array();
                        $comment_replies['replies_count'] = $replies_count;
                        $comment_replies['replies'] = $comment_reply_detail;
                        
                        $comment_details[$i]['comment_replies'] = $comment_replies;


                        $i++;
                    }
                    $c_details = array();
                    $c_details['total_comments'] = $comments->count();
                    $c_details['comment_detail'] = $comment_details;
                    $challenge_reply_detail[$index]['reply_comments'] = $c_details;

                    
                    //getting save details
                    $reply_saves = SaveReplyToChallenge::where([
                        ['reply_to_challenge_id', $post_challenge_reply->id],
                    ])->get();
                    $saved_by = array();
                    $saved_by_logged_user = false;
                    foreach ($reply_saves as $reply_save) {
                        if ($reply_save->user_id == $logged_user_id) {
                            $saved_by_logged_user = true;
                        }
                        $saved_by[] = User::find($reply_save->user_id);
                    }
                    $reply_saved = array();
                    $reply_saved['saved_by_logged_user'] = $saved_by_logged_user;
                    $reply_saved['total_saved'] = $reply_saves->count();
                    $reply_saved['saved_by'] = $saved_by;

                    $challenge_reply_detail[$index]['reply_saved'] = $reply_saved;
                    
                    //getting likes deatils
                    $reply_likes = ChallengeReplyLike::where([
                        ['challenge_reply_id', $post_challenge_reply->id],
                    ])->get();
                    $liked_by = array();
                    $liked_by_logged_user = false;
                    foreach ($reply_likes as $reply_like) {
                        if ($reply_like->user_id == $logged_user_id) {
                            $liked_by_logged_user = true;
                        }
                        $liked_by[] = User::find($reply_like->user_id);
                    }
                    $likes = array();
                    $likes['liked_by_logged_user'] = $liked_by_logged_user;
                    $likes['total_likes'] = $reply_likes->count();
                    $likes['liked_by'] = $liked_by;

                    $challenge_reply_detail[$index]['reply_likes'] = $likes; 
        
                    $index++;
                }
                $post_detail['post_challenge_reply']['challenge_reply_count'] = $post_challenge_replies->count();
                $post_detail['post_challenge_reply']['challenge_reply'] = $challenge_reply_detail;
            }
            
            //getting likes details
            $likes = PostLike::where([
                ['post_id', $post_detail['id']],
            ])->get();
            $liked_by_logged_user = false;
            $total_likes = 0;
            $liked_users = array();
            foreach ($likes as $like) {
                $total_likes++;
                $liked_by_id = $like['user_id'];
                if ($like['user_id'] == $logged_user_id) {
                    $liked_by_logged_user = true;
                }
                //getting user list who liked
                $liked_users[] = User::where([
                    ['id', $liked_by_id],
                ])->first();
            }

            if ($liked_by_logged_user == true) {
                $post_detail['post_likes']['liked_by_logged_user'] = true;
            } elseif($liked_by_logged_user == false) {
                $post_detail['post_likes']['liked_by_logged_user'] = false;
            }

            $post_detail['post_likes']['total_likes'] = $total_likes;

            foreach ($liked_users as $liked_user) {
                if ($liked_user['profile_pic'] == null) {
                    $liked_user['profile_pic'] = asset('image/c.svg') ;
                }
            }
            $post_detail['post_likes']['liked_by'] = $liked_users;

            //getting saved details
            $post_saved = SavePost::where([
                ['post_id', $post_detail['id']],
            ])->get();
            $saved_by_logged_user = false;
            $total_saved = 0;
            $saved_users = array();
            foreach ($post_saved as $saved) {
                $total_saved++;
                $saved_by_id = $saved['user_id'];
                if ($saved['user_id'] == $logged_user_id) {
                    $saved_by_logged_user = true;
                }
                //getting user list who saved
                $saved_users[] = User::where([
                    ['id', $saved_by_id],
                ])->first();
            }
            if ($saved_by_logged_user == true) {
                $post_detail['post_saved']['saved_by_logged_user'] = true;
            } elseif($saved_by_logged_user == false) {
                $post_detail['post_saved']['saved_by_logged_user'] = false;
            }
            
            $post_detail['post_saved']['total_saved'] = $total_saved;

            foreach ($saved_users as $saved_user) {
                if ($saved_user['profile_pic'] == null) {
                    $saved_user['profile_pic'] = asset('image/c.svg') ;
                }
            }
            $post_detail['post_saved']['saved_by'] = $saved_users;

            //getting comments details
            $total_comments = 0;
            $comment_detail = array();
            $index = 0;
            $comments_in_asc_order = PostComment::where([
                ['post_id', $post_detail['id']],
            ])->get();

            //sorting in descending order of comment id
            $comments_in_asc_order = $comments_in_asc_order->toArray();
            uasort($comments_in_asc_order, function($id_a,$id_b){
                return strcmp($id_b['id'], $id_a['id']);
            });
            $comments = array();
            foreach ($comments_in_asc_order as $comment) {
                $comments[] = $comment;
            }

            foreach ($comments as $comment) {
                $total_comments++;
                $comment_detail[$index] = $comment;
                $comment_by_id = $comment['user_id'];
                //getting user list who commented
                $comment_by = User::where([
                    ['id', $comment_by_id],
                ])->first();

                if ($comment_by->id == $logged_user_id) {
                    $comment_detail[$index]['can_edit_by_logged_user'] = true;
                } elseif ($comment_by->id != $logged_user_id) {
                    $comment_detail[$index]['can_edit_by_logged_user'] = false;
                }
				//Post owner can also delete
				$post_owner_id = Post::select('user_id')->where('id', $comment['post_id'])->first();
				if ($post_owner_id->user_id == $logged_user_id) {
                    $comment_detail[$index]['can_delete_by_logged_user'] = true;
                } elseif ($comment_by->id == $logged_user_id) {
                    $comment_detail[$index]['can_delete_by_logged_user'] = true;
                } else {
                    $comment_detail[$index]['can_delete_by_logged_user'] = false;
                }
				
                $comment_detail[$index]['comment_by'] = $comment_by;

                //getting replies on comment
                $comment_replies = array();
                $i = 0;
                $replies = PostCommentReply::where([
                    ['comment_id', $comment['id']],
                ])->get();
                foreach ($replies as $reply) {
                    $comment_replies[$i] = $reply;
                    $reply_by = User::where([
                        ['id', $reply->user_id],
                    ])->first();

                    if ($reply_by->id == $logged_user_id) {
                        $comment_replies[$i]['can_edit_by_logged_user'] = true;
                    } elseif ($reply_by->id != $logged_user_id) {
                        $comment_replies[$i]['can_edit_by_logged_user'] = false;
                    }
					//Post owner can also delete
					if ($post_owner_id->user_id == $logged_user_id) {
						$comment_replies[$i]['can_delete_by_logged_user'] = true;
					} elseif ($reply_by->id == $logged_user_id) {
						$comment_replies[$i]['can_delete_by_logged_user'] = true;
					} else {
						$comment_replies[$i]['can_delete_by_logged_user'] = false;
					}
					
                    $comment_replies[$i]['reply_by'] = $reply_by;
                    $i++;
                }
                $comment_detail[$index]['comment_replies']['replies_count'] = $replies->count();
                $comment_detail[$index]['comment_replies']['replies'] = $comment_replies;
                

                if ($comment_detail[$index]['comment_by']['profile_pic'] == null) {
                    $comment_detail[$index]['comment_by']['profile_pic'] = asset('image/c.svg') ;
                }
                $index++;
            }
            $post_detail['post_comments']['total_comments'] = $total_comments;
            $post_detail['post_comments']['comment_detail'] = $comment_detail;

            //adding each post in an array
            $post_details[] = $post_detail;
        }

        // re-ordering posts on recently updated priority.
        $post_details = collect($post_details);
        $post_details = $post_details->sortByDesc('updated_at');
        $posts_with_details = array();
        foreach ($post_details as $post_detail) {
            $posts_with_details[] = $post_detail;
        }
        return $posts_with_details;
    }

     /**
     * Getting other relevent details about a user.
     *
     * @param array $user
     * @return $user_details
     */
    public function getUserDetail($user) 
    {
        $user_details = array();
        //confirming auth and user's relation
        if (isset(Auth::user()->id)) {
            $auth_id = Auth::user()->id;
            if ($auth_id != $user['id']) {
                $user_details['is_friend'] = false;
                $user_details['friend_request_sent'] = false;
                $user_details['friend_request_received'] = false;
                $user_details['can_sent_friend_request'] = true;
                $friend_request_sent = UsersFriend::where([
                    ['user_id', $auth_id], 
                    ['user_friend_id', $user['id']],
                ])->first();
                if (!is_null($friend_request_sent)) {
                    if ($friend_request_sent->friend_accepted == 1) {
                        $user_details['is_friend'] = true;
                        $user_details['friend_request_sent'] = false;
                        $user_details['friend_request_received'] = false;
                        $user_details['can_sent_friend_request'] = false;
                    }
                    if ($friend_request_sent->friend_accepted == 0) {
                        $user_details['is_friend'] = false;
                        $user_details['friend_request_sent'] = true;
                        $user_details['friend_request_received'] = false;
                        $user_details['can_sent_friend_request'] = false;
                    }
                }
                $friend_request_received = UsersFriend::where([
                    ['user_friend_id', $auth_id], 
                    ['user_id', $user['id']],
                ])->first();
                if (!is_null($friend_request_received)) {
                    if ($friend_request_received->friend_accepted == 1) {
                        $user_details['is_friend'] = true;
                        $user_details['friend_request_sent'] = false;
                        $user_details['friend_request_received'] = false;
                        $user_details['can_sent_friend_request'] = false;
                    }
                    if ($friend_request_received->friend_accepted == 0) {
                        $user_details['is_friend'] = false;
                        $user_details['friend_request_sent'] = false;
                        $user_details['friend_request_received'] = true;
                        $user_details['can_sent_friend_request'] = false;
                    }
                }
            }
        }
        //getting user details
        $user_details['id'] = $user['id'];
        $user_details['name'] = $user['name'];
        $user_details['email'] = $user['email'];
        $user_details['about'] = $user['about'];
        $user_details['profile_pic'] = $user['profile_pic'];
        $user_details['email_verified_at'] = $user['email_verified_at'];
        $user_details['blocked'] = $user['blocked'];
        $user_details['location'] = ['lat'=>$user['lat'], 'long'=>$user['long']];
        $user_details['created_at'] = $user['created_at'];
        $user_details['updated_at'] = $user['updated_at'];

        //getting friends details
        $user_friends_by_sending = UsersFriend::where([
            ['user_id', $user['id']], 
        ])->get();
        $user_friends_by_receiving = UsersFriend::where([
            ['user_friend_id', $user['id']], 
        ])->get();
        $accepted_friends_count = 0;
        $sent_friend_request_count = 0;
        $received_friend_request_count = 0;
        $accepted_friends = array();
        $sent_friend_request_users = array();
        $received_friend_request_users = array();
        foreach ($user_friends_by_sending as $user_friend_by_sending) {
            if ($user_friend_by_sending['friend_accepted'] == 0) {
                $sent_friend_request_count++;
                $sent_friend_request_users[] = User::where([
                    ['id', $user_friend_by_sending['user_friend_id']], 
                ])->first()->only(
                    'id', 'name', 'email', 'about', 'profile_pic', 'email_verified_at', 'blocked', 'created_at', 'updated_at'
                );
            }
            if ($user_friend_by_sending['friend_accepted'] == 1) {
                $accepted_friends_count++;
                $accepted_friends[] = User::where([
                    ['id', $user_friend_by_sending['user_friend_id']], 
                ])->first()->only(
                    'id', 'name', 'email', 'about', 'profile_pic', 'email_verified_at', 'blocked', 'created_at', 'updated_at'
                );
            }
        }
        foreach ($user_friends_by_receiving as $user_friend_by_receiving) {
            if ($user_friend_by_receiving['friend_accepted'] == 0) {
                $received_friend_request_count++;
                $received_friend_request_users[] = User::where([
                    ['id', $user_friend_by_receiving['user_id']], 
                ])->first()->only(
                    'id', 'name', 'email', 'about', 'profile_pic', 'email_verified_at', 'blocked', 'created_at', 'updated_at'
                );
            }
            if ($user_friend_by_receiving['friend_accepted'] == 1) {
                $accepted_friends_count++;
                $accepted_friends[] = User::where([
                    ['id', $user_friend_by_receiving['user_id']], 
                ])->first()->only(
                    'id', 'name', 'email', 'about', 'profile_pic', 'email_verified_at', 'blocked', 'created_at', 'updated_at'
                );
            }
        }
        $user_details['friends_details']['accepted_friends_count'] = $accepted_friends_count;
        $user_details['friends_details']['sent_friend_request_count'] = $sent_friend_request_count;
        $user_details['friends_details']['received_friend_request_count'] =  $received_friend_request_count;
        $user_details['friends_details']['accepted_friends'] = $accepted_friends;
        $user_details['friends_details']['sent_friend_request_users'] = $sent_friend_request_users;
        $user_details['friends_details']['received_friend_request_users'] = $received_friend_request_users;

        //getting challenges details
        $user_challenges_sent = PostsTagged::where([
            ['tagged_by', $user['id']],
        ])->get();
        $user_challenges_received = PostsTagged::where([
            ['tagged_user', $user['id']],
        ])->get();
        $challenge_sent_count = 0;
        $challenge_sent_pending_count = 0;
        $challenge_sent_pending_user = array();
        $challenge_sent_accepted_count = 0;
        $challenge_sent_accepted_user = array();
        $challenge_received_count = 0;
        $challenge_received_accepted_count = 0;
        $challenge_received_accepted_user = array();
        $challenge_received_pending_count = 0;
        $challenge_received_pending_user = array();
        foreach ($user_challenges_sent as $user_challenge_sent) {
            $challenge_sent_count++;
            if ($user_challenge_sent['challenge_accepted'] == 1) {
                $challenge_sent_accepted_count++;
                $challenge_sent_accepted_user[] = User::where([
                    ['id', $user_challenge_sent['tagged_user']], 
                ])->first()->only(
                    'id', 'name', 'email', 'about', 'profile_pic', 'email_verified_at', 'blocked', 'created_at', 'updated_at'
                );
            }
            if ($user_challenge_sent['challenge_accepted'] == 0) {
                $challenge_sent_pending_count++;
                $challenge_sent_pending_user[] = User::where([
                    ['id', $user_challenge_sent['tagged_user']], 
                ])->first()->only(
                    'id', 'name', 'email', 'about', 'profile_pic', 'email_verified_at', 'blocked', 'created_at', 'updated_at'
                );
            }
        }
        foreach ($user_challenges_received as $user_challenge_received) {
            $challenge_received_count++;
            if ($user_challenge_received['challenge_accepted'] == 1) {
                $challenge_received_accepted_count++;
                $challenge_received_accepted_user[] = User::where([
                    ['id', $user_challenge_received['tagged_by']], 
                ])->first()->only(
                    'id', 'name', 'email', 'about', 'profile_pic', 'email_verified_at', 'blocked', 'created_at', 'updated_at'
                );
            }
            if ($user_challenge_received['challenge_accepted'] == 0) {
                $challenge_received_pending_count++;
                $challenge_received_pending_user[] = User::where([
                    ['id', $user_challenge_received['tagged_by']], 
                ])->first()->only(
                    'id', 'name', 'email', 'about', 'profile_pic', 'email_verified_at', 'blocked', 'created_at', 'updated_at'
                );
            }
        }
        $user_details['challenge_details']['challenge_sent_count'] = $challenge_sent_count;
        $user_details['challenge_details']['challenge_sent_pending_count'] = $challenge_sent_pending_count;
        $user_details['challenge_details']['challenge_sent_accepted_count'] = $challenge_sent_accepted_count;
        $user_details['challenge_details']['challenge_received_count'] = $challenge_received_count;
        $user_details['challenge_details']['challenge_received_pending_count'] = $challenge_received_pending_count;
        $user_details['challenge_details']['challenge_received_accepted_count'] = $challenge_received_accepted_count;
        
        $user_details['challenge_details']['challenge_sent_pending_user'] = $challenge_sent_pending_user;
        $user_details['challenge_details']['challenge_sent_accepted_user'] = $challenge_sent_accepted_user;
        $user_details['challenge_details']['challenge_received_pending_user'] = $challenge_received_pending_user;
        $user_details['challenge_details']['challenge_received_accepted_user'] = $challenge_received_accepted_user;
        
        return $user_details;
    }

    /**
     * Getting relevent details about notification.
     *
     * @return Restfull Api Response
     */
    public function notification()
    {
        try {
            $auth_id = Auth::user()->id;
            $notifications = Notification::where([
                ['respondent_id', $auth_id],
            ])->orderBy('created_at','DESC')->get();

            $notification_with_detail = array();
            $reply_on_comment_count = 0;
            $comment_on_normal_post_count = 0;
            $reply_to_challenge_post_count = 0;
            $tagged_on_normal_post_count = 0;
            $tagged_on_challenge_post_count = 0;
            $comment_on_challenge_post_count = 0;
            $friend_request_count = 0;
            $new_notification_count = 0;
            $comment_to_challenge_reply_count =0;
            $index = 0;
            foreach ($notifications as $notification) {
                $notification_with_detail[$index]['id'] = $notification->id;
                $notification_with_detail[$index]['motive'] = $notification->motive;
                $notification_with_detail[$index]['notification_type'] = $notification->notification_type;
                $notification_with_detail[$index]['message'] = $notification->message;
                $notification_with_detail[$index]['created_at'] = $notification->created_at;
                $notification_with_detail[$index]['updated_at'] = $notification->updated_at;
                
                if ($notification->notification_type == 'New') {
                    $new_notification_count++;
                }

                if ($notification->motive == 'reply_on_comment') {
                    $notification_with_detail[$index]['reply'] = PostCommentReply::find($notification->applicant_resource_id);
                    $notification_with_detail[$index]['reply_by'] = User::find($notification->applicant_id);
                    $post_comment = PostComment::find($notification->respondent_resource_id);
                    $notification_with_detail[$index]['comment'] = $post_comment;
                    $notification_with_detail[$index]['commented_by'] = User::find($notification->respondent_id);
                    $post = Post::where([
                        ['id', $post_comment['post_id']],
                    ])->get();
                    $notification_with_detail[$index]['post'] = $this->getPostDetail($post);
                    $reply_on_comment_count++;
                }
                if ($notification->motive == 'comment_on_normal_post') {
                    $notification_with_detail[$index]['comment'] = PostComment::find($notification->applicant_resource_id);
                    $notification_with_detail[$index]['commented_by'] = User::find($notification->applicant_id);
                    $post = Post::where([
                        ['id', $notification->respondent_resource_id],
                    ])->get();
                    $notification_with_detail[$index]['post'] = $this->getPostDetail($post);
                    $notification_with_detail[$index]['posted_by'] = User::find($notification->respondent_id);
                    $comment_on_normal_post_count++;
                }
                if ($notification->motive == 'comment_on_challenge_post') {
                    $notification_with_detail[$index]['comment'] = PostComment::find($notification->applicant_resource_id);
                    $notification_with_detail[$index]['commented_by'] = User::find($notification->applicant_id);
                    $post = Post::where([
                        ['id', $notification->respondent_resource_id],
                    ])->get();
                    $notification_with_detail[$index]['challenge'] = $this->getPostDetail($post);
                    $notification_with_detail[$index]['challenge_by'] = User::find($notification->respondent_id);
                    $comment_on_challenge_post_count++;
                }
                if ($notification->motive == 'reply_to_challenge_post') {
                    $notification_with_detail[$index]['reply'] = PostsTaggedReply::find($notification->applicant_resource_id);
                    $notification_with_detail[$index]['reply_by'] = User::find($notification->applicant_id);
                    $post = Post::where([
                        ['id', $notification->respondent_resource_id],
                    ])->get();
                    $notification_with_detail[$index]['challenge'] = $this->getPostDetail($post);
                    $notification_with_detail[$index]['challenge_by'] = User::find($notification->respondent_id);
                    $reply_to_challenge_post_count++;
                }
                if ($notification->motive == 'tagged_on_normal_post') {
                    $post = Post::where([
                        ['id', $notification->applicant_resource_id],
                    ])->get();
                    $notification_with_detail[$index]['tagging_post'] = $this->getPostDetail($post);
                    $notification_with_detail[$index]['tagged_by'] = User::find($notification->applicant_id);
                    $notification_with_detail[$index]['tagged_to'] = User::find($notification->respondent_id);
                    $tagged_on_normal_post_count++;
                }
                if ($notification->motive == 'tagged_on_challenge_post') {
                    $post = Post::where([
                        ['id', $notification->applicant_resource_id],
                    ])->get();
                    $notification_with_detail[$index]['tagging_challenge_post'] = $this->getPostDetail($post);
                    $notification_with_detail[$index]['tagged_by'] = User::find($notification->applicant_id);
                    $notification_with_detail[$index]['tagged_to'] = User::find($notification->respondent_id);
                    $tagged_on_challenge_post_count++;
                }
                if ($notification->motive == 'friend_request') {
                    $notification_with_detail[$index]['requested_by'] = User::find($notification->applicant_id);
                    $notification_with_detail[$index]['requested_to'] = User::find($notification->respondent_id);
                    $friend_request_count++;
                }
                if ($notification->motive == 'comment_to_challenge_reply') {
                    $post_tagged_reply = PostsTaggedReply::find($notification->respondent_resource_id);
                    $notification_with_detail[$index]['challenge_reply'] = $post_tagged_reply;
                    $notification_with_detail[$index]['challenge_reply_by'] = User::find($notification->applicant_id);
                    $notification_with_detail[$index]['comment'] = ChallengeCommentReply::find($notification->applicant_resource_id);
                    $notification_with_detail[$index]['commented_by'] = User::find($notification->respondent_id);
                    $post = Post::where([
                        ['id', $post_tagged_reply['post_id']], 
                    ])->get();
                    $notification_with_detail[$index]['post'] = $this->getPostDetail($post);
                    $comment_to_challenge_reply_count++;
                }
                $index++;
            }
            $notification_with_details = array();
            $notification_with_details['new_notification_count'] = $new_notification_count;
            $notification_with_details['friend_request_count'] = $friend_request_count;
            $notification_with_details['reply_on_comment_count'] = $reply_on_comment_count;
            $notification_with_details['comment_on_normal_post_count'] = $comment_on_normal_post_count;
            $notification_with_details['comment_on_challenge_post_count'] = $comment_on_challenge_post_count;
            $notification_with_details['reply_to_challenge_post_count'] = $reply_to_challenge_post_count;
            $notification_with_details['tagged_on_normal_post_count'] = $tagged_on_normal_post_count;
            $notification_with_details['tagged_on_challenge_post_count'] = $tagged_on_challenge_post_count;
            $notification_with_details['comment_to_challenge_reply_count'] = $comment_to_challenge_reply_count;
            $custom_pagination = new CustomPaginationController;
            $notification_with_detail = collect($notification_with_detail);
            $notification_with_detail = $custom_pagination->paginate($notification_with_detail, 10);
            $notification_with_details['notification'] = $notification_with_detail;

            $response = [
                'status' => 'success',
                'message' => 'notification found Successfully',
                'notifications' => $notification_with_details,
            ];
            return response($response, 200);
        } catch (\Exception $ex) {
            $response = [
                'status' => 'failure',
                'message' => 'Can not provide notification. Unidentified error, please contact at ' . env("APP_SUPERADMIN_EMAIL_ID"),
            ];
            return response($response, 500);
        }
    }

    /**
     * Deleting a single notification.
     *
     * @param int $notification_id
     * @return Restfull Api Response
     */
    public function deleteSingleNotification($notification_id) {
        try {
            $notifications = Notification::where([
                ['id', $notification_id],
            ])->delete();

            if ($notifications) {
                $response = [
                    'status' => 'success',
                    'message' => 'Notification deleted Successfully',
                ];
                return response($response, 200);
            } else {
                $response = [
                    'status' => 'failure',
                    'message' => 'Can not delete notification. Unidentified error, please contact at ' . env("APP_SUPERADMIN_EMAIL_ID"),
                ];
                return response($response, 500);
            }
            
        } catch (\Exception $ex) {
            $response = [
                'status' => 'failure',
                'message' => 'Can not delete notification. Unidentified error, please contact at ' . env("APP_SUPERADMIN_EMAIL_ID"),
            ];
            return response($response, 500);
        }
    }

    /**
     * Deleting all notifications.
     *
     * @return Restfull Api Response
     */
    public function deleteAllNotification() {
        try {
            $auth_id = Auth::user()->id;
            $notifications = Notification::where([
                ['respondent_id', $auth_id],
            ])->delete();

            if ($notifications) {
                $response = [
                    'status' => 'success',
                    'message' => 'Notifications deleted Successfully',
                ];
                return response($response, 200);
            } else {
                $response = [
                    'status' => 'failure',
                    'message' => 'Can not delete notifications. Unidentified error, please contact at ' . env("APP_SUPERADMIN_EMAIL_ID"),
                ];
                return response($response, 500);
            }
        } catch (\Exception $ex) {
            $response = [
                'status' => 'failure',
                'message' => 'Can not delete notifications. Unidentified error, please contact at ' . env("APP_SUPERADMIN_EMAIL_ID"),
            ];
            return response($response, 500);
        }
    }

    /**
     * Updating a notification from New to Old.
     *
     * @param int $notification_id
     * @return Restfull Api Response
     */
    public function updateNotificationToOld($notification_id)
    {
        try{
            $notification = Notification::find($notification_id);
            if (is_null($notification)) {
                $response = [
                    'status' => 'failure',
                    'message' => 'Provid a valid notification id',
                ];
                return response($response, 422);
            } 

            $notification->update([
                'notification_type' => 'Old',
            ]);

            if ($notification) {
                $response = [
                    'status' => 'success',
                    'message' => 'Notification type updated to Old Successfully',
                ];
                return response($response, 200);
            }
        } catch (\Exception $ex) {
            $response = [
                'status' => 'failure',
                'message' => 'Can not update notification to Old. Unidentified error, please contact at ' . env("APP_SUPERADMIN_EMAIL_ID"),
            ];
            return response($response, 500);
        }
    }

    /**
     * Getting other relevent details about replies.
     *
     * @param array $replies
     * @return $reply_details
     */
    public function getReplyDetail($replies)
    {
        $logged_user_id = Auth::id();
        $reply_details = array();
        foreach ($replies as $reply) {
            $reply_detail = array();
            $reply_detail['id'] = $reply->id;
            $reply_detail['post_id'] = $reply->post_id;
            $reply_detail['challenge_by_id'] = $reply->challenge_by_id;
            $reply_detail['reply_by_id'] = $reply->reply_by_id;
            $reply_detail['reply_type'] = $reply->reply_type;
            $reply_detail['reply_text'] = $reply->reply_text;
            $reply_detail['reply_binary'] = $reply->reply_binary;
            $reply_detail['reply_binary_mime_type'] = $reply->reply_binary_mime_type;
            $reply_detail['location'] = $reply->location;
            $reply_detail['created_at'] = $reply->created_at;
            $reply_detail['updated_at'] = $reply->updated_at;

            if ($reply->challenge_by_id == $logged_user_id) {
                $reply_detail['can_edit_by_logged_user'] = true;
            } elseif ($reply->challenge_by_id != $logged_user_id) {
                $reply_detail['can_edit_by_logged_user'] = false;
            }

            $reply_detail['challenge_by'] = User::where([
                ['id', $reply->challenge_by_id],
            ])->first();

            $reply_detail['challenge_reply_by'] = User::where([
                ['id', $reply->reply_by_id],
            ])->first();
            
            $reply_detail['post'] = Post::where([
                ['id', $reply->post_id],
            ])->first();

            //getting comment details
            $comments = ChallengeCommentReply::where([
                ['challenge_comment_id', $reply->id],
            ])->get();
            $comment_detail = array();
            $index = 0;
            foreach ($comments as $comment) {
                $comment_detail[$index] = $comment;

                if ($comment->user_id == $logged_user_id) {
                    $comment_detail[$index]['can_edit_by_logged_user'] = true;
                } elseif ($comment->user_id != $logged_user_id) {
                    $comment_detail[$index]['can_edit_by_logged_user'] = false;
                }
				
				//Post owner can also delete
				$postIdForPostOwner = PostsTaggedReply::select('post_id')->where('id', $comment->challenge_comment_id)->first();
				$post_owner_id = Post::select('user_id')->where('id', $postIdForPostOwner->post_id)->first();
				if ($post_owner_id->user_id == $logged_user_id) {
					$comment_detail[$index]['can_delete_by_logged_user'] = true;
				} elseif ($comment->user_id == $logged_user_id) {
					$comment_detail[$index]['can_delete_by_logged_user'] = true;
				} else {
					$comment_detail[$index]['can_delete_by_logged_user'] = false;
				}

                $comment_detail[$index]['comment_by'] = User::find($comment->user_id);
                $comment_replies = ChallengeCommentCommentReply::where([
                    ['r_comment_id', $comment->id],
                ])->get();
                $comment_reply_detail = array();
                $i = 0;
                foreach ($comment_replies as $comment_reply) {
                    $comment_reply_detail[$i] = $comment_reply;

                    if ($comment_reply->user_id == $logged_user_id) {
                        $comment_reply_detail[$i]['can_edit_by_logged_user'] = true;
                    } elseif ($comment_reply->user_id != $logged_user_id) {
                        $comment_reply_detail[$i]['can_edit_by_logged_user'] = false;
                    }
					
					//Post owner can also delete
					if ($post_owner_id->user_id == $logged_user_id) {
						$comment_reply_detail[$i]['can_delete_by_logged_user'] = true;
					} elseif ($comment_reply->user_id == $logged_user_id) {
						$comment_reply_detail[$i]['can_delete_by_logged_user'] = true;
					} else {
						$comment_reply_detail[$i]['can_delete_by_logged_user'] = false;
					}

                    $comment_reply_detail[$i]['reply_by'] = User::find($comment_reply->user_id);
                    $i++;
                }
                $temp_comment_detail = array();
                $temp_comment_detail['replies_count'] = $comment_replies->count();
                $temp_comment_detail['replies'] = $comment_reply_detail;
                
                $comment_detail[$index]['comment_replies'] = $temp_comment_detail;

                $index++;
            }
            $reply_detail['reply_comments']['total_comments'] = $comments->count();
            $reply_detail['reply_comments']['comment_detail'] = $comment_detail;

            //getting save details
            $reply_saves = SaveReplyToChallenge::where([
                ['reply_to_challenge_id', $reply->id],
            ])->get();
            $saved_by = array();
            $saved_by_logged_user = false;
            foreach ($reply_saves as $reply_save) {
                if ($reply_save->user_id == $logged_user_id) {
                    $saved_by_logged_user = true;
                }
                $saved_by[] = User::find($reply_save->user_id);
            }
            $reply_saved = array();
            $reply_saved['saved_by_logged_user'] = $saved_by_logged_user;
            $reply_saved['total_saved'] = $reply_saves->count();
            $reply_saved['saved_by'] = $saved_by;

            $reply_detail['reply_saved'] = $reply_saved;
            
            //getting likes deatils
            $reply_likes = ChallengeReplyLike::where([
                ['challenge_reply_id', $reply->id],
            ])->get();
            $liked_by = array();
            $liked_by_logged_user = false;
            foreach ($reply_likes as $reply_like) {
                if ($reply_like->user_id == $logged_user_id) {
                    $liked_by_logged_user = true;
                }
                $liked_by[] = User::find($reply_like->user_id);
            }
            $likes = array();
            $likes['liked_by_logged_user'] = $liked_by_logged_user;
            $likes['total_likes'] = $reply_likes->count();
            $likes['liked_by'] = $liked_by;

            $reply_detail['reply_likes'] = $likes; 

            $reply_details[] = $reply_detail;
        }
        return $reply_details;
    }

    /**
     * Getting all Onboarding Images.
     *
     * @return Restfull Api Response
     */
    public function showAllOnboardingImage()
    {
        try {
            $images = OnBoardingImage::all();

            $response = [
                'status' => 'success',
                'message' => 'Onboarding Images found Successfully',
                'advertisement' => $images,
            ];
            return response($response, 200);
        } catch (\Exception $ex) {
            $response = [
                'status' => 'failure',
                'message' => 'Can not show onboarding Images. Unidentified error, please contact at ' . env("APP_SUPERADMIN_EMAIL_ID"),
            ];
            return response($response, 500);
        }
    }
}
