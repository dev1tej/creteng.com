<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdvCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adv_comments', function (Blueprint $table) {
            $table->id();
            $table->foreignId('adv_id');
            $table->foreignId('user_id');
            $table->bigInteger('comment_id')->nullable();
            $table->string('comments', 255);
            $table->timestamps();

            $table->foreign('adv_id')
                ->references('id')
                ->on('advertisements')
                ->onDelete('cascade');
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('adv_comments');
    }
}
