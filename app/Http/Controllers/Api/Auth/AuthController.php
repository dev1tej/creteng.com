<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Controllers\Api\GetResourceDetailController;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Password;

class AuthController extends Controller
{
    /**
     * Register a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return Restfull Api Response
     */
    public function register(Request $request) 
    {
        try {
            $validator = Validator::make($request->all(), [
                'name' => 'required|max:255|regex:/^[\pL\s\-]+$/u',
                'email' => 'required|string|email|max:255|unique:users',
                'password' => 'required|string|min:8|confirmed',
                'latitude' => ['required','regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/'],
                'longitude' => ['required','regex:/^[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/'],
            ]);
            if ($validator->fails()) {
                return response([
                    'status' => 'failure',
                    'message'=>$validator->errors()->all()
                ], 422);
            }

            $request['blocked'] = 1;
            $request['password'] = Hash::make($request['password']);
            //$request['remember_token'] = Str::random(10);
            $request['profile_pic'] = asset('image/c.svg');
            $request['lat'] = $request['latitude'];
            $request['long'] = $request['longitude'];
            $user = User::create($request->toArray());
            
            $customers = env('APP_CUSTOMER');
            $user->assignRole($customers);
            $token = $user->createToken('Registration')->accessToken;
            $user->sendEmailVerificationNotification();

            $response = [
                'status' => 'success',
                'message' => 'A Verification email has been sent to your email address. Pls verify email to continue',
                'Verification_link_expiration_time' => '60 minute',
                'user' => $user->only(
                    'id', 'name', 'email', 'email_verified_at', 'profile_pic', 'updated_at', 'created_at' 
                ),
                'token' => $token
            ];
            return response($response, 200);
        } catch (\Exception $ex) {
            $response = [
                'status' => 'failure',
                'message' => 'Can not register. Unidentified error, please contact at ' . env("APP_SUPERADMIN_EMAIL_ID"),
            ];
            return response($response, 500);
        }
    }

    /**
     * login a resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return Restfull Api Response
     */
    public function login(Request $request) 
    {
        try {
            $validator = Validator::make($request->all(), [
                'email' => 'required|string|email|max:255',
                'password' => 'required|string|min:8',
            ]);
            if ($validator->fails()) {
                return response([
                    'status' => 'failure',
                    'message' => $validator->errors()->all()
                ], 422);
            }

            $user = User::where('email', $request->email)->first();
            if ($user) {
                if (Hash::check($request->password, $user->password)) {
                    if ($user->email_verified_at == null) {
                        $response = [
                            'status' => 'success',
                            'message' => 'Your email address is not verified yet. Please contact administration at '.env("APP_SUPERADMIN_EMAIL_ID").' !',
                        ];
                        return response($response, 422);
                    }
                    if ($user->blocked == 1) {
                        $response = [
                            'status' => 'failure',
                            'message' => 'Unauthorized access, Pls contact administration at '.env("APP_SUPERADMIN_EMAIL_ID").' !',
                        ];
                        return response($response, 422);
                    }
                    $token = $user->createToken('login')->accessToken;
                    if ($user->profile_pic == null) {
                        $user->profile_pic = asset('image/c.svg') ;
                    }
                    $get_resource_detail = new GetResourceDetailController;
                    $user = $get_resource_detail->getUserDetail($user);
                    $response = [
                        'status' => 'success',
                        'message' => 'You are logged successfully',
                        'user' => $user,
                        'token' => $token
                    ];
                    return response($response, 200);
                    
                } else {
                    $response = [
                        'status' => 'failure',
                        'message' => 'Password mismatch'];
                    return response($response, 422);
                }
                
            } else {
                $response = [
                    'status' => 'failure',
                    'message' => 'User does not exist'];
                return response($response, 422);
            }
        } catch (\Exception $ex) {
            $response = [
                'status' => 'failure',
                'message' => 'Can not loggin. Unidentified error, please contact at ' . env("APP_SUPERADMIN_EMAIL_ID"),
            ];
            return response($response, 500);
        }
    }

    /**
     * logout a resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return Restfull Api Response
     */
    public function logout(Request $request) 
    {
        try {
            $token = $request->user()->token();
            $token->revoke();
            $response = [
                'status' => 'success',
                'message' => 'You are successfully logged out!'];
            return response($response, 200);
        } catch (\Exception $ex) {
            $response = [
                'status' => 'failure',
                'message' => 'Can not logout. Unidentified error, please contact at ' . env("APP_SUPERADMIN_EMAIL_ID"),
            ];
            return response($response, 500);
        }
    }

    /**
     * To send a password reset email if user forget password.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return Restfull Api Response
     */
    public function forgetPassword(Request $request) 
    {
        try {
            $validator = Validator::make($request->all(), [
                'email' => 'required|string|email|max:255',
            ]);
            if ($validator->fails()) {
                return response([
                    'status' => 'failure',
                    'message' => $validator->errors()->all()
                ], 422);
            }
            $user = User::where('email', $request->email)->first();
            if ($user) {
                if ($user->email_verified_at == null) {
                    $response = [
                        'status' => 'failure',
                        'message' => 'Your email address is not verified yet. Please contact at '.env("APP_SUPERADMIN_EMAIL_ID").' for further action.',
                    ];
                    return response($response, 422);
                }
                $credencials = $request->all();
                Password::sendResetLink($credencials);
                $response = [
                    'status' => 'success',
                    'message' => 'Reset password link has sent to your email Id.'];
                return response($response, 200);   
            } else {
                $response = [
                    'status' => 'failure',
                    'message' => 'User does not exist',
                ];
                return response($response, 422);
            }                   
        } catch (\Exception $ex) {
            $response = [
                'status' => 'failure',
                'message' => 'Can not sent reset password link. Unidentified error, please contact at ' . env("APP_SUPERADMIN_EMAIL_ID"),
            ];
            return response($response, 500);
        }
    }

    /**
     * Change Password of the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return Restfull Api Response
     */
    public function changePassword(Request $request) 
    {
        try {
            $validator = Validator::make($request->all(), [
                'old_password' => 'required',
                'new_password' => 'required|min:8',
                'confirm_password' => 'required|same:new_password',
            ]);
            $user_id = Auth::user()->id;
            
            if ($validator->fails()) {
                return response([
                    'status' => 'failure',
                    'message' => $validator->errors()->all()
                ], 422);
            } else {
                if ((Hash::check(request('old_password'), Auth::user()->password)) == false) {
                    return response([
                        'status' => 'failure',
                        'message' => 'Check your old password'
                    ], 422);
                } else {
                    User::where('id', $user_id)->update(['password' => Hash::make($request['new_password'])]);
                    return response([
                        'status' => 'success',
                        'message' => 'Password updated successfully.'
                    ], 200);
                } 
            }
        } catch (\Exception $ex) {
            $response = [
                'status' => 'failure',
                'message' => 'Can not change the password. Unidentified error, please contact at ' . env("APP_SUPERADMIN_EMAIL_ID"),
            ];
            return response($response, 500);
        }
    }
}
