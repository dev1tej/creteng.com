<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
})->name('home');

/*Route::get('/advertisement', function () {
    return view('front.registration.advertisement');
});*/

//auth routes
Route::get('admin/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('admin/login', 'Auth\LoginController@login');
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.update');
Route::get('password/reset/temp', function () {
	return view('admin.auth.passwords.reset');
})->name('password.reset.temp');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

Route::prefix('advertiser')->group(function(){
	// front add advertisement routes
	Route::prefix('register')->group(function(){
		Route::get('/personal-detail', 'Auth\AdvertiserRegisterController@showRegisterPersonalDetailForm')->name('showRegisterPersonalDetailForm');
		Route::post('/personal-detail', 'Auth\AdvertiserRegisterController@postRegisterPersonalDetail')->name('postRegisterPersonalDetail');

		Route::get('/select-plan/{advertiser_id}', 'Auth\AdvertiserRegisterController@showRegisterSelectedPlan')->name('showRegisterSelectedPlan');
		Route::post('/select-plan', 'Auth\AdvertiserRegisterController@postRegisterSelectedPlan')->name('postRegisterSelectedPlan');
		Route::post('/country/state/list', 'Auth\AdvertiserRegisterController@getStates')->name("countryStateList");
		
		Route::get('/fill-ad/{advertiser_id}/{plan_id}/{sub_plan_id}/{opt_service_ids}/{no_of_publication_location}', 'Auth\AdvertiserRegisterController@showRegisterAd')->name('showRegisterAd');
		Route::post('/pay', 'Auth\AdvertiserRegisterController@postRegisterAd')->name('postRegisterAd');
		
		Route::post('/do-payment', 'Auth\AdvertiserRegisterController@postRegisterPayment')->name('postRegisterPayment');
	});

	//Route::post('register/payment', 'Auth\AdvertiserRegisterController@registerPayment')->name('registerPayment');
	Route::post('/thanks', 'Auth\AdvertiserRegisterController@postRegisterPayment')->name('postRegisterPayment');
    Route::get('/login', 'Auth\AdvertiserLoginController@showLoginForm')->name('advertiser.login');
    Route::post('/login', 'Auth\AdvertiserLoginController@login')->name('advertiser.login.submit');

	Route::prefix('password')->group(function(){
		Route::get('reset', 'Auth\AdvertiserForgotPasswordController@showLinkRequestForm')->name('advertiser.password.request');
		Route::post('email', 'Auth\AdvertiserForgotPasswordController@sendResetLinkEmail')->name('advertiser.password.email');
		Route::get('reset/{token}', 'Auth\AdvertiserResetPasswordController@showResetForm')->name('advertiser.password.reset');
		Route::post('reset', 'Auth\AdvertiserResetPasswordController@reset')->name('advertiser.password.update');
		Route::get('reset/temp', function () {
			return view('auth.passwords.reset');
		})->name('advertiser.password.reset.temp');
	});
});
Route::group(['prefix' => 'advertiser', 'middleware' => ['auth:adv_user', 'advertiser_auth']], function() {
	Route::get('/', 'front\AdvertiserController@index')->name('advertiser.dashboard');
	Route::post('/logout', 'Auth\AdvertiserLoginController@logout')->name('advertiser.logout');

	//change password advertiser
	Route::get('password/edit/{advertiser}', 'front\AdvertiserController@advertiserPasswordEdit')->name('advertiserPasswordEdit');//
	Route::post('password/update/{advertiser}', 'front\AdvertiserController@advertiserPasswordUpdate')->name('advertiserPasswordUpdate');//

	Route::get('/{advertisement}/show', 'front\AdvertiserController@showAdvertisement')->name('advertiser.showAdvertisement');
	Route::post('/advertisement/comment/delete', 'front\AdvertiserController@deleteAdvertisementComment')->name('advertiser.deleteAdvertisementComment');
	Route::get('/send/ad/publish/request/{advertisement}', 'front\AdvertiserController@sendAdPublishRequest')->name('sendAdPublishRequest');
	Route::get('/{advertisement}/delete', 'front\AdvertiserController@deleteAdvertiserAdvertisement')->name('deleteAdvertiserAdvertisement');

	//Update advertiser
	Route::get('/edit/{advertiser}', 'front\AdvertiserController@advertiserEdit')->name('advertiserEdit');
	Route::patch('/update/{advertiser}', 'front\AdvertiserController@advertiserUpdate')->name('advertiserUpdate');

	//view my plan
	Route::get('/{advertiser}/my-plan', 'front\AdvertiserController@showAdvertiserPlan')->name('showAdvertiserPlan');
	//change plan
	/*
	Route::prefix('change-plan')->group(function(){
		Route::get('info', 'front\AdvertiserController@changePlanInfo')->name('changePlanInfo');
		Route::get('/select-plan/{advertiser_id}', 'front\AdvertiserController@changePlanPlan')->name('changePlanPlan');
		Route::post('/select-plan', 'front\AdvertiserController@postChangePlanPlan')->name('postChangePlanPlan');
		Route::post('/payment', 'front\AdvertiserController@changePlanPayment')->name('changePlanPayment');
		Route::post('/thanks', 'front\AdvertiserController@changePlanThanks')->name('changePlanThanks');
	});
	*/
	Route::prefix('renew-plan')->group(function(){
		//Renew Plan
		Route::get('info-1', 'front\AdvertiserController@renewPlanInfo1')->name('renewPlanInfo1');
		Route::get('info-2', 'front\AdvertiserController@renewPlanInfo2')->name('renewPlanInfo2');
		Route::get('/select-plan', 'front\AdvertiserController@renewPlanSelectPlan')->name('renewPlanSelectPlan');
		Route::get('/get-publicable-location', 'front\AdvertiserController@getPublicableLocation')->name('getPublicableLocation');
		Route::post('/country/state/list', 'front\AdvertiserController@renewPlanCountryStateList')->name("renewPlanCountryStateList");
		Route::post('/select-plan', 'front\AdvertiserController@postRenewPlanSelectPlan')->name('postRenewPlanSelectPlan');
		//Route::get('/payment', 'front\AdvertiserController@renewPlanPayment')->name('renewPlanPayment');
		Route::post('/payment', 'front\AdvertiserController@renewPlanPaymentPost')->name('renewPlanPaymentPost');
	});
	//create new add
	Route::get('/{advertiser}/create/new-ad', 'front\AdvertiserController@createNewAd')->name('createNewAd');
	Route::post('/create/new-ad', 'front\AdvertiserController@postCreateNewAd')->name('postCreateNewAd');
});


Route::get('/privacyPolicy', function () {
	$page = \App\Page::where('page_title', 'Privacy')->first();
	return view('policies.privacyPolicy', compact('page'));
})->name('privacyPolicy');

Route::get('/termsNCondition', function () {
	$page = \App\Page::where('page_title', 'Terms')->first();
	return view('policies.termsNCondition', compact('page'));
})->name('termsNCondition');

Route::get('/sendVerificationEmail/{user}', 'UserController@sendVerificationEmail')->name('sendVerificationEmail');

//stripe payment gateway integration
Route::get('/advertisement/{advertisement}/{mode}/pay/{pay}USD', 'AdvertisementController@stripe')->name('stripe');
Route::post('stripe', 'AdvertisementController@stripePost')->name('stripePost');
Route::group(['prefix' => 'admin', 'middleware' => ['auth','is_admin']], function() {
	
	Route::get('/dashboard', 'HomeController@adminHome')->name('dashboard');
	//Manage Roles
    Route::resource('/roles', 'RoleController');
	//Update admin
	Route::get('/users/edit/{users}', 'UserController@editUser')->name('userEdit');
	Route::post('/users/update/{users}', 'UserController@updateUser')->name('userFormUpdate');
	//change password admin
	Route::get('/password/edit/{user}', 'UserController@adminPasswordEdit')->name('adminPasswordEdit');//
	Route::post('/password/update/{user}', 'UserController@adminPasswordUpdate')->name('adminPasswordUpdate');//

	//Manage Managers
	Route::prefix('manager')->group(function(){
		Route::get('/', 'UserController@showAllManagers')->name('showAllManagers');
		Route::post('/', 'UserController@addManager')->name('addManager');
		Route::get('/create', 'UserController@createManager')->name('createManager');
		Route::post('/status', 'UserController@changeStatusManager')->name('changeStatusManager');
		Route::get('/{manager}/show', 'UserController@showManager')->name('showManager');
		Route::patch('/{manager}', 'UserController@updateManager')->name('updateManager');
		Route::get('/{manager}', 'UserController@deleteManager')->name('deleteManager');
		Route::get('/{manager}/edit', 'UserController@editManager')->name('editManager');

		Route::get('/last/week', 'UserController@managersLastWeek')->name('managersLastWeek');
	});
	
	//Manage Customers
	Route::prefix('customer')->group(function(){
		Route::get('/', 'UserController@showAllCustomer')->name('showAllCustomer');
		Route::post('/', 'UserController@addCustomer')->name('addCustomer');
		Route::get('/create', 'UserController@createCustomer')->name('createCustomer');
		Route::get('/{customer}/edit', 'UserController@editCustomer')->name('editCustomer');
		Route::get('/{customer}/challenges', 'UserController@viewCustomerChallenges')->name('viewCustomerChallenges');
		Route::get('/{customer}/challenger/{challenger}', 'UserController@showChallenger')->name('showChallenger');
		Route::patch('/{customer}', 'UserController@updateCustomer')->name('updateCustomer');
		Route::get('/{customer}', 'UserController@deleteCustomer')->name('deleteCustomer');
		Route::get('/{customer}/show', 'UserController@showCustomer')->name('showCustomer');
		Route::post('/status', 'UserController@customerChangeStatus')->name('customerChangeStatus');
		Route::get('/{customer}/friends', 'UserController@viewCustomerFriends')->name('viewCustomerFriends');
		Route::get('/{customer}/friends/{friends}', 'UserController@showCustomerFriend')->name('showCustomerFriend');

		Route::get('/last/week', 'UserController@customersLastWeek')->name('customersLastWeek');
	});	

	//Manage Challenges
	Route::prefix('challenges')->group(function(){
		Route::get('/', 'ChallengeController@showAllChallenges')->name('showAllChallenges');
		Route::get('/{challenge}/view', 'ChallengeController@viewChallenges')->name('viewChallenges');
		Route::get('/{challenge}', 'ChallengeController@deleteChallenge')->name('deleteChallenge');
		Route::get('/{challenge}/{customer}', 'ChallengeController@showCustomer')->name('showChallengedSpecificCustomer');
		Route::post('/comment/delete', 'ChallengeController@deleteChallengeComment')->name('deleteChallengeComment');
		Route::post('/comment-reply/delete', 'ChallengeController@deleteChallengeCommentReply')->name('deleteChallengeCommentReply');
		Route::post('/challenge-reply/comment/delete', 'ChallengeController@deleteChallengeReplyComment')->name('deleteChallengeReplyComment');
		Route::post('/challenge-reply/comment-reply/delete', 'ChallengeController@deleteChallengeReplyCommentReply')->name('deleteChallengeReplyCommentReply');
	});	

	//Manage Posts
	Route::prefix('post')->group(function(){
		Route::get('/', 'PostController@showAllPosts')->name('showAllPosts');
		Route::get('/{post}/view', 'PostController@viewPost')->name('viewPost');
		Route::get('/{post}', 'PostController@deletePost')->name('deletePost');
		Route::get('/{post}/{customer}', 'PostController@showCustomer')->name('showSpecificCustomer');
		Route::post('/comment/delete', 'PostController@deletePostComment')->name('deletePostComment');
		Route::post('/comment-reply/delete', 'PostController@deletePostCommentReply')->name('deletePostCommentReply');
	});	

	//Manage Advertiser
	Route::prefix('advertiser')->group(function(){
		Route::get('/', 'AdvertisementController@showAllAdvertiser')->name('showAllAdvertiser');
		Route::get('/create', 'AdvertisementController@createAdvertiser')->name('createAdvertiser');
		Route::post('/', 'AdvertisementController@addAdvertiser')->name('addAdvertiser');
		Route::get('/{advertiser}/show', 'AdvertisementController@showAdvertiser')->name('showAdvertiser');
		Route::get('/{advertiser}/edit', 'AdvertisementController@editAdvertiser')->name('editAdvertiser');
		Route::patch('/{advertiser}', 'AdvertisementController@updateAdvertiser')->name('updateAdvertiser');
		Route::get('/{advertiser}', 'AdvertisementController@deleteAdvertiser')->name('deleteAdvertiser');

		Route::get('/{advertiser}/paymentHistory', 'AdvertisementController@adPaymentHistory')->name('adPaymentHistory');
		Route::post('/sendPaymentLink', 'AdvertisementController@sendPaymentLink')->name('sendPaymentLink');
	});

	//Manage Advertisement Plans
	Route::prefix('plan')->group(function(){
		Route::get('/', 'AdvertisementPlanController@showAllAdPlan')->name('showAllAdPlan');
		Route::get('/{plan}/show', 'AdvertisementPlanController@showAdPlan')->name('showAdPlan');
		Route::get('/{plan}/delete', 'AdvertisementPlanController@deleteAdPlan')->name('deleteAdPlan');

		Route::get('/create', 'AdvertisementPlanController@createAdPlan')->name('createAdPlan');
		Route::post('/add/plan', 'AdvertisementPlanController@addAdPlan')->name('addAdPlan');
		Route::post('/sub-plan/add', 'AdvertisementPlanController@addAdSubPlan')->name('addAdSubPlan');

		Route::get('/{plan}/edit', 'AdvertisementPlanController@editAdPlan')->name('editAdPlan');
		Route::patch('/{plan}/update', 'AdvertisementPlanController@updateAdPlan')->name('updateAdPlan');
		Route::patch('/{plan}/sub_plan/update', 'AdvertisementPlanController@updateAdSubPlan')->name('updateAdSubPlan');

		Route::get('/opt_service/create', 'AdvertisementPlanController@createAdPlanOptService')->name('createAdPlanOptService');
		Route::post('/opt_service/add', 'AdvertisementPlanController@addAdPlanOptService')->name('addAdPlanOptService');
		Route::get('/opt_service/view', 'AdvertisementPlanController@viewAdPlanOptService')->name('viewAdPlanOptService');
		Route::get('/opt_service/{opt_service}', 'AdvertisementPlanController@deleteAdPlanOptService')->name('deleteAdPlanOptService');
	});
	
	//Manage Advertisement
	Route::prefix('advertisement')->group(function(){
		Route::get('/', 'AdvertisementController@showAllAdvertisement')->name('showAllAdvertisement');
		Route::get('/archived', 'AdvertisementController@showAllArchivedAdvertisement')->name('showAllArchivedAdvertisement');
		Route::get('/create', 'AdvertisementController@createAdvertisement')->name('createAdvertisement');
		Route::post('/', 'AdvertisementController@addAdvertisement')->name('addAdvertisement');
		//Route::get('/admin/advertisement/advertiser/{advertiser}/show', 'AdvertisementController@showAdvertisementAdvertiser')->name('showAdvertisementAdvertiser');
		Route::get('/{advertisement}/edit', 'AdvertisementController@editAdvertisement')->name('editAdvertisement');
		Route::patch('/{advertisement}', 'AdvertisementController@updateAdvertisement')->name('updateAdvertisement');
		Route::get('/{advertisement}/delete', 'AdvertisementController@deleteAdvertisement')->name('deleteAdvertisement');
		Route::get('/{advertisement}', 'AdvertisementController@archiveAdvertisement')->name('archiveAdvertisement');
		Route::get('/archived/{advertisement}', 'AdvertisementController@unarchiveAdvertisement')->name('unarchiveAdvertisement');
		Route::post('/status', 'AdvertisementController@advertisementChangeStatus')->name('advertisementChangeStatus');
		Route::get('/{advertisement}/show', 'AdvertisementController@showAdvertisement')->name('showAdvertisement');

		Route::post('/advertisement/comment/delete', 'AdvertisementController@deleteAdvertisementComment')->name('deleteAdvertisementComment');
		Route::post('/update/ad/description', 'AdvertisementController@updateAdDescription')->name('updateAdDescription');
		Route::post('/update/ad/url', 'AdvertisementController@updateAdURL')->name('updateAdURL');
	});
	//Manage New Advertisement
	Route::get('/new/advertisement/{advertisement}/approve', 'AdvertisementController@approveNewAdvertisement')->name('approveNewAdvertisement');
	Route::get('/new/advertisement/{advertisement}/disapprove', 'AdvertisementController@disapproveNewAdvertisement')->name('disapproveNewAdvertisement');

	//Manage Report
	Route::prefix('report')->group(function(){
		//customers Report 
		Route::get('/customer', 'UserController@manageCustomerReport')->name('manageCustomerReport');
		Route::post('/customer/customerGenerateCsvReport', 'UserController@customerGenerateCsvReport')->name('customerGenerateCsvReport');
		//posts Report
		Route::get('/post', 'PostController@managePostReport')->name('managePostReport');
		Route::post('/post/postGenerateCsvReport', 'PostController@postGenerateCsvReport')->name('postGenerateCsvReport');
		//challenges Report
		Route::get('/challenge', 'ChallengeController@manageChallengeReport')->name('manageChallengeReport');
		Route::post('/challenge/challengeGenerateCsvReport', 'ChallengeController@challengeGenerateCsvReport')->name('challengeGenerateCsvReport');
	});

	//Manage Onboarding Image 
	Route::prefix('onboarding/image')->group(function(){
		Route::get('/', 'OnBoardingController@showAllOnboardingImage')->name('showAllOnboardingImage');
		Route::get('/create', 'OnBoardingController@createOnboardingImage')->name('createOnboardingImage');
		Route::post('/', 'OnBoardingController@addOnboardingImage')->name('addOnboardingImage');
		Route::get('/{image}', 'OnBoardingController@deleteOnboardingImage')->name('deleteOnboardingImage');
	});

	//Manage Pages
	Route::prefix('pages')->group(function(){
		Route::get('/', 'PageController@showAllPages')->name('showAllPages');
		Route::get('/edit/{page}', 'PageController@editPage')->name('editPage');
		Route::patch('/update/{page}', 'PageController@updatePage')->name('updatePage');
	});
});
		

