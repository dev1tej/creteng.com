<?php

namespace App\Http\Controllers\front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Arr;
use Owenoj\LaravelGetId3\GetId3;
use Illuminate\Support\Facades\Session;
use Stripe\Stripe;
use Stripe\Customer;
use Stripe\Charge;
use App\Advertisement;
use App\AdsPayment;
use App\AdvLike;
use App\AdvView;
use App\AdvComment;
use App\AdvUser;
use App\User;

use App\AdvPlan;
use App\AdvPlDuration;
use App\AdvPlFixedService;
use App\AdvPlOptService;
use App\AdvAvailableService;
use App\AdvPublicationLocation;
use App\Countries;
use App\States;

class AdvertiserController extends Controller
{
    public function usAndEuCountries()
    {
        $usAndEuCountries = [
            'UNITED STATES',
            'FRANCE',
            'SPAIN',
            'SWEDEN',
            'GERMANY',
            'FINLAND',
            'POLAND',
            'ITALY',
            //'ROMANIA',
            'GREECE',
            'BULGARIA',
            'HUNGARY',
            'PORTUGAL',
            'AUSTRIA',
            //'CZECH REPUBLIC',
            'IRELAND',
            'LITHUANIA',
            'LATVIA',
            'CROATIA',
            //'SLOVAKIA',
            'ESTONIA',
            'DENMARK',
            'NETHERLANDS',
            'BELGIUM',
            'SLOVENIA',
            'CYPRUS',
            'LUXEMBOURG',
            'MALTA',
        ];

        return $usAndEuCountries;
    }

    public function index()
    {
        $data = Advertisement::where([
            ['advertiser_id', Auth::user()->id],
        ])->with('services')->orderBy('created_at', 'desc')->get();
        return view('front.advertiser.index', compact('data'));
    }

    /**
     * To show an adv
     * 
     * @param int $id
     * @return \Illuminate\View\View
     */
    public function showAdvertisement($id)
    {
        $data = Advertisement::where('id', $id)->with('advertiser')->first();

        $likes = AdvLike::where([
            ['adv_id', $data->id],
        ])->with('user')->get();
        $data['likes'] = $likes;
        $data['likes']['likes_count'] = $likes->count();

        $views = AdvView::where([
            ['adv_id', $data->id],
        ])->with('user')->get();
        $data['views'] = $views;
        $data['views']['views_count'] = $views->count();

        $comments = AdvComment::where('adv_id', $data->id)
            ->whereNull('comment_id')
            ->with('user')
            ->get();

        $comment_details = array();
        $index = 0;
        foreach ($comments as $comment) {
            $comment_details[$index]['comment'] = $comment;
            $comment_details[$index]['comment']['comment_reply'] = AdvComment::where([
                ['adv_id', $data->id],
                ['comment_id', $comment->id],
            ])->with('user')->get();
            $index++;
        }
        $data['comments'] = $comment_details;

        $ad_services = AdvAvailableService::where('advertiser', $data->advertiser_id)->first();
        $available_service = array();
        $available_service['plan'] = AdvPlan::find($ad_services['plan']);
        $available_service['plan_fixed_service'] = AdvPlFixedService::where('plan_id', $available_service['plan']['id'])->first();
        $available_service['plan_duration'] = AdvPlDuration::find($ad_services['duration']);
        if (!is_null($ad_services['opt_services'])) {
            $available_service['plan_opt_service'] = AdvPlOptService::find($ad_services['opt_services']);
        } else {
            $available_service['plan_opt_service'] = null;
        }
        //echo"<pre>";
        //print_r($available_service);
        //exit();
        return view('front.advertiser.show',compact('data', 'available_service'));
    }

    /**
     * To delete an Advertiser
     * 
     * @param int $id
     * @return View
     */
    public function deleteAdvertiserAdvertisement($id)
    {
        Advertisement::find($id)->delete();
        
        return redirect()->route('advertiser.dashboard')->with('danger','Advertisement deleted successfully');

    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function advertiserEdit($id)
    {
        $user = AdvUser::find($id);

        $usAndEuCountries = $this->usAndEuCountries();
        $countries = Countries::all();
        $countries_with_iso = array();
        foreach($countries as $country) {
            if (in_array($country->name, $usAndEuCountries)) {
                $countries_with_iso[] = $country->name . '-' . $country->iso;
            }
        }

        return view('front.advertiser.user.edit', compact('user', 'countries_with_iso'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function advertiserUpdate(Request $request, $id)
    {
		/* Check Validation */
        $this->validate($request, [
            'name' => 'required|regex:/^[\pL\s\-]+$/u',
            'company_name' => 'string|max:255|nullable',
            'email' => 'required|regex:/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix|unique:adv_users,email,'.$id,
            'contact' => 'required|integer|digits_between: 8,12', 
            'address_line_1' => 'required|string|max:255',
            'address_line_2' => 'string|nullable|max:255',
            'city' => 'required|string|max:100',
            'state' => 'required|string|max:100',
            'postal_code' => 'required|string|max:20',
            'country' => 'required|string|max:100',
        ]);
		
		/* if user dont want to change password or skip password to update */
        $input = $request->all();
		/*Update User */
        $user = AdvUser::find($id);
        $user->update($input);

        return redirect(route('advertiser.dashboard'))->with('flash_message','Profile updated successfully.');
    }

    /**
     * To display edit advertiser password page.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */

    public function advertiserPasswordEdit($id)
    {
        return view('front.advertiser.user.password.edit',compact('id'));
    }
    
    /**
     * To update advertiser password.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function advertiserPasswordUpdate(Request $request, $id)
    {
        $this->validate($request, [
            'password' => 'same:confirm-password',
        ]);
        
        $input = $request->all();
        if(!empty($input['password'])){ 
            $input['password'] = Hash::make($input['password']);
        }
		/*Update User password */
        $user = AdvUser::find($id);
        $user->update($input);
        
        return redirect(route('advertiser.dashboard'))->with('flash_message', 'Password changed successfully.');
    }

    /**
     * Show the advertiser's plan.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function showAdvertiserPlan($user_id)
    {

        $ad_services = AdvAvailableService::where([
            ['advertiser', $user_id],
        ])->orderBy('created_at', 'desc')->first();
        if (!is_null($ad_services)) {

            $my_plan = array();
            //$my_plan['ad'] = $ad;
            $my_plan['plan'] = AdvPlan::find($ad_services['plan']);
            $my_plan['plan_fixed_service'] = AdvPlFixedService::where('plan_id', $my_plan['plan']['id'])->first();
            $my_plan['plan_duration'] = AdvPlDuration::find($ad_services['duration']);
            if (!is_null($ad_services['opt_services'])) {
                $my_plan['plan_opt_service'] = AdvPlOptService::find($ad_services['opt_services']);
            } else {
                $my_plan['plan_opt_service'] = null;
            }
            $my_plan['payment'] = AdsPayment::where([
                ['advertiser_id', $user_id],
                ['status', 'Charged'],
                ['service_id', $ad_services['id']],
            ])->first();

            $my_plan['publishable_locations'] = AdvPublicationLocation::where([
                ['advertiser', $user_id],
                ['service_id', $ad_services['id']],
            ])->get();

            $total_opt_services_cost = 0;
            $multiplying_factor = '';
            if (!is_null($my_plan['plan_opt_service'])) {
                if ($my_plan['plan_duration']['plan_duration'] == 'Weekly') {
                    $multiplying_factor = '*1';
                    $total_opt_services_cost += $my_plan['plan_opt_service']['aditional_price'] * 1;
                } 
                if ($my_plan['plan_duration']['plan_duration'] == 'Monthly') {
                    $multiplying_factor = '*4';
                    $total_opt_services_cost += $my_plan['plan_opt_service']['aditional_price'] * 4;
                } 
                if ($my_plan['plan_duration']['plan_duration'] == 'Quarterly') {
                    $multiplying_factor = '*13';
                    $total_opt_services_cost += $my_plan['plan_opt_service']['aditional_price'] * 13;
                } 
                if ($my_plan['plan_duration']['plan_duration'] == 'Half Yearly') {
                    $multiplying_factor = '*26';
                    $total_opt_services_cost += $my_plan['plan_opt_service']['aditional_price'] * 26;
                } 
                if ($my_plan['plan_duration']['plan_duration'] == 'Yearly') {
                    $multiplying_factor = '*52';
                    $total_opt_services_cost += $my_plan['plan_opt_service']['aditional_price'] * 52;
                }
            }
        } else {
            $my_plan = '';
            $multiplying_factor = '';
            $total_opt_services_cost = '';
        }

        return view('front.advertiser.user.show_plan', compact('my_plan', 'multiplying_factor', 'total_opt_services_cost', 'ad_services'));
    }

    public function changePlanInfo()
    {
        return view('front.advertiser.user.change_plan.info');
    }

    public function changePlanPlan($advertiser_id) 
    {
        $ad_plans = AdvPlan::with('fixServices', 'planDuration')->get();
        $plan_opt_services = AdvPlOptService::all();
        return view('front.advertiser.user.change_plan.plan', compact('ad_plans', 'plan_opt_services', 'advertiser_id'));
    }

    /**
     * Creating new Plan
     * 
     * @param \Illuminate\Http\Request  $request
     * @return \Illuminate\View\View
     */
    public function postChangePlanPlan(Request $request)
    {
        /* Check Validation */
        $this->validate($request, [
            'advertiser_id' => 'required',
            'plan_id' => 'required',
            'sub_plan_id' => 'required',
            'opt_service_ids' => 'array',
        ]);

        $plan = AdvPlan::find($request->plan_id);
        $sub_plan = AdvPlDuration::find($request->sub_plan_id);

        $opt_services = array();
        $total_opt_services_cost = 0;
        $multiplying_factor = '';
        if (isset($request->opt_service_ids) && (!is_null($request['opt_service_ids'][0]))) {
            foreach($request->opt_service_ids as $opt_service_id) {
                $opt_service = AdvPlOptService::find($opt_service_id);

                if ($sub_plan->plan_duration == 'Weekly') {
                    $multiplying_factor = '*1';
                    $total_opt_services_cost += $opt_service->aditional_price * 1;
                } 
                if ($sub_plan->plan_duration == 'Monthly') {
                    $multiplying_factor = '*4';
                    $total_opt_services_cost += $opt_service->aditional_price * 4;
                } 
                if ($sub_plan->plan_duration == 'Quarterly') {
                    $multiplying_factor = '*13';
                    $total_opt_services_cost += $opt_service->aditional_price * 13;
                } 
                if ($sub_plan->plan_duration == 'Half Yearly') {
                    $multiplying_factor = '*26';
                    $total_opt_services_cost += $opt_service->aditional_price * 26;
                } 
                if ($sub_plan->plan_duration == 'Yearly') {
                    $multiplying_factor = '*52';
                    $total_opt_services_cost += $opt_service->aditional_price * 52;
                }
                $opt_services[] = $opt_service;
            }
        } 
        $advertiser_id = $request->advertiser_id;
        $total_price = $sub_plan->price + $total_opt_services_cost;

        return view('front.advertiser.user.change_plan.stripe', compact(
            'advertiser_id', 
            'plan',
            'sub_plan',
            'opt_services',
            'multiplying_factor',
            'total_opt_services_cost',
            'total_price'
        ));
    }

    /**
     * Creating Adv payment at hold position
     * 
     * @param \Illuminate\Http\Request  $request
     * @return \Illuminate\View\View
     */
    public function changePlanPayment(Request $request) 
    {
        $advertisement = Advertisement::find($request->advertisement_id);
        $advertiser    = AdvUser::find($request->advertiser_id);

        Stripe::setApiKey(env('STRIPE_SECRET'));
        $customer = Customer::create(array(
            'name' => $advertiser->name,
            "source" => $request->stripeToken,      
            'email' => $advertiser->email,
            "address" => [
                'city'        => $advertiser->city, 
                'country'     => $advertiser->country, 
                'line1'       => $advertiser->address_line_1, 
                'line2'       => $advertiser->address_line_2, 
                'postal_code' => $advertiser->postal_code, 
                'state'       => $advertiser->state
            ], 
        ));
        $charge = \Stripe\Charge::create([
            'customer' => $customer->id,
            'amount' => 100 * $request->amount,
            'currency' => 'usd',
            'description' => 'Payment request for Ad Publication in Creteng',
            'capture' => false,
        ]);
        $ads_payment  = AdsPayment::create([
            'advertiser_id'    => $advertiser->id,
            'transaction_id'   => $charge->id,
            'payment_mode'     => $request->payment_mode,
            'amount'           => $request->amount,
        ]);
        

        $details = array();
        $details['advertisement']['id'] = $advertisement->id;
        $details['advertisement']['advertisement'] = $advertisement->advertisement;
        $details['advertiser']['id'] = $advertiser->id;
        $details['advertiser']['name'] = $advertiser->name;
        $details['advertiser']['email'] = $advertiser->email;
        $details['advertiser']['contact'] = $advertiser->contact;
        $details['advertisement']['transaction_id'] = $ads_payment->transaction_id;
        $details['advertisement']['payment_mode'] = $ads_payment->payment_mode;
        $details['advertisement']['amount'] = $ads_payment->amount;

        \Mail::to($advertiser->email)->send(new \App\Mail\front\PublishableAdNotificationToAdvertiser($details));
        \Mail::to(env('APP_SUPERADMIN_EMAIL_ID'))->send(new \App\Mail\front\PublishableAdNotificationToSuperAdmin($details));

        
        return view('front.registration.thanks');
    }

    public function changePlanThanks()
    {
        return view('front.advertiser.user.change_plan.thanks');
    }

    public function createNewAd($user_id) 
    {
        /*$ad = Advertisement::where([
            ['advertiser_id', $id],
            ['status', 'Active'],
        ])->first();*/
        $ad_services = AdvAvailableService::where([
            ['advertiser', $user_id],
        ])->orderBy('created_at', 'desc')->first();

        $ad_payment = AdsPayment::where([ 
            ['advertiser_id', $user_id],
            ['service_id', $ad_services['id']],
        ])->orderBy('created_at', 'desc')->first();
        if ($ad_services->status == false) {
            if ($ad_payment->status == 'Charged') {
                return redirect()->back()->with('flash_message','Can\'t create new ad. Your Subscription has expired.');
            } else {
                return redirect()->back()->with('flash_message','Can\'t create new ad. Your subscription is under evaluation');
            }
        }

        $plan = AdvPlan::find($ad_services['plan']);
        $plan_fixed_services = AdvPlFixedService::where('plan_id', $plan['id'])->first();

        if (preg_match("/image/i", $plan_fixed_services->fixed_services)) {
            $ad_type = 'image';
        } elseif (preg_match("/video/i", $plan_fixed_services->fixed_services)) {
            $ad_type = 'video';
        }
        $advertiser_id = $user_id;
        return view('front.advertiser.new_ad', compact(
            'advertiser_id',
            'plan_fixed_services',
            'ad_type'
        ));
    }

    public function postCreateNewAd(Request $request) {

        /* Check Validation */
        $this->validate($request, [
            'advertiser_id' => 'required',
            'ad_type'  => 'required',
            'advertisement'  => 'required|file',
            'ad_description' => 'string|nullable',
            'website_url' => 'string|nullable',
            'google_store_url' => 'string|nullable',
            'apple_store_url' => 'string|nullable',
        ]);

        //getting existing plan details for active ad
        $ad_services = AdvAvailableService::where([
            ['advertiser', $request->advertiser_id],
            ['status', true],
        ])->first();

        $plan = AdvPlan::find($ad_services['plan']);
        $plan_fixed_services = AdvPlFixedService::where('plan_id', $plan['id'])->first();
        $plan_duration = AdvPlDuration::find($ad_services['duration']);
        if (!is_null($ad_services['opt_services'])) {
            $plan_opt_service = AdvPlOptService::find($ad_services['opt_services']);
        } else {
            $plan_opt_service = null;
        }
        
        //validating file upload
        $track = GetId3::fromUploadedFile($request->file('advertisement'));
        $file_info = $track->extractInfo();
        if (($request->ad_type == 'Video') && (preg_match("/Video/i", $file_info['mime_type']))) {
            $play_time = $track->getPlaytime();

            if (preg_match("!\d+!", $plan_fixed_services->fixed_services, $allow_play_time)) {
                if ($allow_play_time[0] < $play_time) {
                    $msg = 'Video length can not be greater than '.$allow_play_time[0].' minutes';
                    return back()->with('danger', $msg);
                }
            } 
        } elseif (($request->ad_type == 'Video') && (preg_match("/image/i", $file_info['mime_type'])) || ($request->ad_type == 'image') && (preg_match("/video/i", $file_info['mime_type']))) {
            return back()->with('danger', 'Uploaded file is not in accordance with the chosen plan');
        }

        //storing file to amazon
        $path = $request->file('advertisement')->store('uservideos/advertisement', 's3');
        $aws_file_path = env('AWS_URL') . $path; 

        //to get dates
        $payment = AdsPayment::where([
            ['advertiser_id', $request->advertiser_id],
            ['status', 'Charged'],
            ['service_id', $ad_services['id']],
        ])->first();

        //storing new ad in unpublished state
        if (isset($request->is_app) && ($request->is_app == 'on')) {
            $is_app = true;
        } else {
            $is_app = false;
        }
        $advertisement = Advertisement::create([
            'advertiser_id'        => $request->advertiser_id,
            'service_id'           => $ad_services['id'],
            'advertisement'        => $aws_file_path,
            'description'          => $request->ad_description,
            'website_url'          => $request->website_url,
            'is_app'               => $is_app,
            'google_store_url'     => $request->google_store_url,
            'apple_store_url'      => $request->apple_store_url,
            'ad_type'              => $request->ad_type,
            'status'               => 'Active',
            'paid'                 => true,
            'publish_date'         => $payment->publish_date,
            'expiry_date'          => $payment->expiry_date,
        ]);

        return redirect(route('advertiser.dashboard'))->with('flash_message','New Ad created successfully. Please send publication request to make it publish in the App.');
    }

    public function sendAdPublishRequest($ad_id) {
        $advertisement = Advertisement::where([
            ['id', $ad_id],
            ['status', 'Active'],
        ])->with('advertiser')->first();

        $advertisement->update(['requested_for_publish' => true]);
        
        $details = array();
        $details['advertiser']['id'] = $advertisement['advertiser']['id'];
        $details['advertiser']['name'] = $advertisement['advertiser']['name'];
        $details['advertiser']['email'] = $advertisement['advertiser']['email'];
        $details['advertiser']['contact'] = $advertisement['advertiser']['contact'];
        $details['advertisement']['id'] = $advertisement['id'];
        $details['advertisement']['ad_type'] = $advertisement['ad_type'];
        $details['advertisement']['advertisement'] = $advertisement['advertisement'];
        $details['advertisement']['status'] = $advertisement['status'];
        $details['advertisement']['paid'] = $advertisement['paid'];
        $details['advertisement']['published'] = $advertisement['published'];
        $details['advertisement']['archive'] = $advertisement['archive'];
        $details['advertisement']['publish_date'] = $advertisement['publish_date'];
        $details['advertisement']['expiry_date'] = $advertisement['expiry_date'];

        \Mail::to(env('APP_SUPERADMIN_EMAIL_ID'))->send(new \App\Mail\front\AdPublishRequestNotificationToSuperAdmin($details));

        return redirect(route('advertiser.dashboard'))->with('flash_message','Request send successfully.');
    }

    public function deleteAdvertisementComment(Request $request)
    {
        $comment_id = $request->comment_id;
        //$comment_id = json_encode($comment_id);
        $comment = AdvComment::find($comment_id);
        $comment->delete();
        return 'ok';
    }

    public function renewPlanInfo1()
    {
        $date_now = date('Y-m-d');
        $plan_expired = false;
        //$date_7days = \Carbon\Carbon::today()->addDays(7)->format('Y-m-d');
        $ads_services = AdvAvailableService::where([
          ['advertiser', Auth::user()->id],
        ])->orderBy('created_at', 'desc')->first();
        $ads_payment = AdsPayment::where([
          ['advertiser_id', Auth::user()->id],
          ['service_id', $ads_services['id']],
        ])->first();
        if ($ads_payment->expiry_date != null) {
            if ($ads_payment->expiry_date < $date_now) {
                $info = 'Your Subscription has expired';
                $plan_expired = true;
            } else {
                $expiry_date = date("d M, Y", strtotime($ads_payment->expiry_date ));
                $info = 'Your Subscription expiry date is '. $expiry_date . '. You can renew plan after this date.';
            }
        } else if ($ads_payment->expiry_date == null) {
            $info = 'Your Subscription is Under Evaluation';
        }
        return view('front.advertiser.user.renew_plan.info_1', compact('info', 'plan_expired'));
    }

    public function renewPlanInfo2() 
    {
        return view('front.advertiser.user.renew_plan.info_2');
    }

    public function getPublicableLocation()
    {
        $usAndEuCountries = $this->usAndEuCountries();
        $countries_details = Countries::all();
        $countries = array();
        foreach($countries_details as $country) {
            if (in_array($country->name, $usAndEuCountries)) {
                $countries[] = $country->name;
            }
        }
        
        $united_states = Countries::where('name', 'UNITED STATES')->first();
        $states_details = States::where('country_id', $united_states->id)->get();
        $states = array();
        foreach ($states_details as $state) {
            $states[] = $state->name;
        }
        rsort($states);
        $data = array();
        $data['countries'] = $countries;
        $data['states'] = $states;
        $data = json_encode($data);

        return $data;
    }

    public function renewPlanCountryStateList(Request $request) {
        $country = Countries::where('name', $request->country)->first();
        if ($country->name == 'UNITED STATES') {
            $states_details = States::where('country_id', $country->id)->get();
            $states = array();
            foreach ($states_details as $state) {
                $states[] = $state->name;
            }
            rsort($states);
        } else {
            $states = array();
            $states[] = $country->name;
        }
        return $states;
    }

    public function renewPlanSelectPlan() 
    {
        if (session()->exists('countries')) {
            Session::forget('countries');
        }
        if (session()->exists('states')) {
            Session::forget('states');
        }
        /*echo"<pre>";
        print_r(Session::get('countries'));
        print_r(Session::get('states'));
        exit();*/
        $advertiser_id = Auth::user()->id;
        $ad_plans = AdvPlan::with('fixServices', 'planDuration')->get();
        $plan_opt_services = AdvPlOptService::all();
        return view('front.advertiser.user.renew_plan.plan', compact('ad_plans', 'plan_opt_services', 'advertiser_id'));
    }

    /**
     * Creating Renew Plan
     * 
     * @param \Illuminate\Http\Request  $request
     * @return \Illuminate\View\View
     */
    public function postRenewPlanSelectPlan(Request $request)
    {
        /* Check Validation */
        $this->validate($request, [
            'plan_id' => 'required',
            'sub_plan_id' => 'required',
            'opt_service_ids' => 'array',
            'no_of_publication_location' => 'required',
            'countries' => 'array',
            'states' => 'array',
        ]);

        if (session()->exists('countries')) {
            Session::forget('countries');
        }
        if (session()->exists('states')) {
            Session::forget('states');
        }
        Session::put('countries', $request->countries);
        Session::put('states', $request->states);

        $plan = AdvPlan::find($request->plan_id);
        $sub_plan = AdvPlDuration::find($request->sub_plan_id);

        $opt_services = array();
        $total_opt_services_cost = 0;
        $multiplying_factor = '';
        if (isset($request->opt_service_ids) && (!is_null($request['opt_service_ids'][0]))) {
            foreach($request->opt_service_ids as $opt_service_id) {
                $opt_service = AdvPlOptService::find($opt_service_id);

                if ($sub_plan->plan_duration == 'Weekly') {
                    $multiplying_factor = '*1';
                    $total_opt_services_cost += $opt_service->aditional_price * 1;
                } 
                if ($sub_plan->plan_duration == 'Monthly') {
                    $multiplying_factor = '*4';
                    $total_opt_services_cost += $opt_service->aditional_price * 4;
                } 
                if ($sub_plan->plan_duration == 'Quarterly') {
                    $multiplying_factor = '*13';
                    $total_opt_services_cost += $opt_service->aditional_price * 13;
                } 
                if ($sub_plan->plan_duration == 'Half Yearly') {
                    $multiplying_factor = '*26';
                    $total_opt_services_cost += $opt_service->aditional_price * 26;
                } 
                if ($sub_plan->plan_duration == 'Yearly') {
                    $multiplying_factor = '*52';
                    $total_opt_services_cost += $opt_service->aditional_price * 52;
                }
                $opt_services[] = $opt_service;
            }
        } 
        $no_of_publication_location = $request->no_of_publication_location;
        $total_price = ($sub_plan->price + $total_opt_services_cost) * $no_of_publication_location;

        return view('front.advertiser.user.renew_plan.stripe', compact(
            'plan',
            'sub_plan',
            'opt_services',
            'multiplying_factor',
            'total_opt_services_cost',
            'no_of_publication_location',
            'total_price'
        ));
    }

    /**
     * Charging payment for renew plan
     * 
     * @param \Illuminate\Http\Request  $request
     * @return \Illuminate\View\View
     */
    public function renewPlanPaymentPost(Request $request) 
    {
        //Validating ad geolocations
        $countries = Session::get('countries');
        $states = Session::get('states');
        $pc_geolocation = array();
        $index = 0;
        foreach ($countries as $country_key=>$country) {
            foreach ($states as $state_key=>$state) {
                if ($country_key == $state_key) {
                    
                    if ($country != $state) {
                        $url = "https://maps.googleapis.com/maps/api/geocode/json?address=".urlencode($state)."&key=".env("GOOGLE_API_KEY");
                        $result_string = file_get_contents($url);
                        $result = json_decode($result_string, true);

                        if ($result['status'] == 'OK') {
                            $pc_geolocation[$index]['lat'] = $result['results'][0]['geometry']['location']['lat'];
                            $pc_geolocation[$index]['lng'] = $result['results'][0]['geometry']['location']['lng'];
                            $pc_geolocation[$index]['country'] = $country;
                            $pc_geolocation[$index]['state'] = $state;
                            $index++;
                        } elseif ($result['status'] == 'ZERO_RESULTS') {
                            return redirect()->route('renewPlanSelectPlan')->with('flash_message', 'Invalid State of a country');
                        }
                    } else {
                        $url = "https://maps.googleapis.com/maps/api/geocode/json?address=".urlencode($country)."&key=".env("GOOGLE_API_KEY");
                        $result_string = file_get_contents($url);
                        $result = json_decode($result_string, true);

                        if ($result['status'] == 'OK') {
                            $pc_geolocation[$index]['lat'] = $result['results'][0]['geometry']['location']['lat'];
                            $pc_geolocation[$index]['lng'] = $result['results'][0]['geometry']['location']['lng'];
                            $pc_geolocation[$index]['country'] = $country;
                            $pc_geolocation[$index]['state'] = $country;
                            $index++;
                        } elseif ($result['status'] == 'ZERO_RESULTS') {
                            return redirect()->back()->with('flash_message', 'Invalid country');
                        }
                        //echo"<pre>";
                        //print_r($result);
                        //exit();
                    }
                } 
            }
        }

        $advertiser_id = Auth::user()->id;
        $advertiser    = AdvUser::find($advertiser_id);
        $country_explode = explode('-', $advertiser['country']);
        $iso_code = $country_explode[1];
        //capturing payment
        Stripe::setApiKey(env('STRIPE_SECRET'));
        $customer = Customer::create(array(
            'name' => $advertiser['name'],
            "source" => $request->stripeToken,      
            'email' => $advertiser['email'],
            "address" => [
                'city'        => $advertiser['city'], 
                'country'     => $iso_code, 
                'line1'       => $advertiser['address_line_1'], 
                'line2'       => $advertiser['address_line_2'], 
                'postal_code' => $advertiser['postal_code'], 
                'state'       => $advertiser['state']
            ], 
        ));
        $charge = \Stripe\Charge::create([
            'customer' => $customer->id,
            'amount' => 100 * $request->amount,
            'currency' => 'usd',
            'description' => 'Payment request for Ad Publication in Creteng',
            //'capture' => false,
        ]);

        $old_service = AdvAvailableService::where([
            ['advertiser', $advertiser_id],
        ])->orderBy('created_at', 'desc')->first();

        //canceling old service if still active
        if ($old_service->status == true) {
            $old_service->update(['status' => false]);
        }

        //saving new service
        if (!is_null($request->opt_services_id)) {
            $opt_services_id = $request->opt_services_id;
            foreach($opt_services_id as $opt_service_id) {
                $new_service = AdvAvailableService::create([
                    'advertiser'   => $advertiser_id,
                    'plan'         => $request->plan_id,
                    'duration'     => $request->sub_plan_id,
                    'opt_services' => $opt_service_id,
                    'status'       => true,
                ]);
            }
        } else {
            $new_service = AdvAvailableService::create([
                'advertiser'   => $advertiser_id,
                'plan'         => $request->plan_id,
                'duration'     => $request->sub_plan_id,
                'opt_services' => null,
                'status'       => true,
            ]);
        }
        

        $adv_pl_duration = AdvPlDuration::find($request->sub_plan_id);

        $publish_date = date("Y-m-d");
        if ($adv_pl_duration['plan_duration'] == 'Weekly') {
            $expiry_date = date("Y-m-d", strtotime('-0 day', strtotime('+1 week', strtotime($publish_date))));
        }
        if ($adv_pl_duration['plan_duration'] == 'Monthly') {
            $expiry_date = date("Y-m-d", strtotime('-1 day', strtotime('+1 month', strtotime($publish_date))));
        }
        if ($adv_pl_duration['plan_duration'] == 'Quarterly') {
            $expiry_date = date("Y-m-d", strtotime('-1 day', strtotime('+3 month', strtotime($publish_date))));
        }
        if ($adv_pl_duration['plan_duration'] == 'Half Yearly') {
            $expiry_date = date("Y-m-d", strtotime('-1 day', strtotime('+6 month', strtotime($publish_date))));
        }
        if ($adv_pl_duration['plan_duration'] == 'Yearly') {
            $expiry_date = date("Y-m-d", strtotime('-1 day', strtotime('+12 month', strtotime($publish_date))));
        }
        //updating old ads
        $advertisements = Advertisement::where([
            ['advertiser_id', $advertiser_id],
            ['service_id', $old_service->id],
        ])->get();

        $msg = '';
        if ($old_service->plan == $request->plan_id) {
            foreach ($advertisements as $ad) {
                if ($ad->published == true) {
                    $ad->update([
                        'service_id'   => $new_service->id,
                        'status'       => 'Active',
                        'paid'         => true,
                        'published'    => true,
                        'publish_date' => $publish_date,
                        'expiry_date'  => $expiry_date,
                    ]);
                } else {
                    $ad->update([
                        'service_id'   => $new_service->id,
                        'status'       => 'Active',
                        'paid'         => true,
                        'published'    => false,
                        'publish_date' => $publish_date,
                        'expiry_date'  => $expiry_date,
                    ]);
                }
            }
            $msg = 'Subscription renewed successfully. All previous advertisements will continue to remain in the same state as they were originally uploaded. Please complete all required fields to upload a new advertisement or go back to your dashboard and send a publish request for an existing ad.';
        } else {
            foreach ($advertisements as $ad) {
                $ad->update([
                    'status'     => 'Inactive',
                    'paid'       => false,
                    'published'  => false,
                ]);
            }
            $msg = 'Subscription renewed successfully. Please upload your advertisement for new subscription.';
        }

        //saving location
        foreach ($pc_geolocation as $geolocation) {
            AdvPublicationLocation::create([
                'advertiser'  => $advertiser_id,
                'service_id'  => $new_service['id'],
                'country'     => $geolocation['country'],
                'state'       => $geolocation['state'],
                //'postal_code' => null,
                'lat'         => $geolocation['lat'],
                'long'        => $geolocation['lng'],
            ]);
        }

        //saving payment
        $ads_payment  = AdsPayment::create([
            'advertiser_id'      => $advertiser_id,
            'service_id'         => $new_service->id,
            'stripe_customer_id' => $customer->id,
            'transaction_id'     => $charge->id,
            'status'             => 'Charged',
            'payment_mode'       => $adv_pl_duration['plan_duration'],
            'amount'             => $request->amount,
            'publish_date'       => $publish_date,
            'expiry_date'        => $expiry_date,
        ]);

        Session::forget('countries');
        Session::forget('states');

        $details = array();
        $details['advertiser']['id'] = $advertiser['id'];
        $details['advertiser']['name'] = $advertiser['name'];
        $details['advertiser']['email'] = $advertiser['email'];
        $details['advertiser']['contact'] = $advertiser['contact'];
        $details['advertisement']['transaction_id'] = $ads_payment->transaction_id;
        $details['advertisement']['payment_mode'] = $ads_payment->payment_mode;
        $details['advertisement']['amount'] = $ads_payment->amount;
        $details['advertisement']['plan'] = AdvPlan::find($new_service['plan']);
        $details['advertisement']['plan_fixed_service'] = AdvPlFixedService::where('plan_id', $details['advertisement']['plan']['id'])->first();
        $details['advertisement']['plan_duration'] = AdvPlDuration::find($new_service['duration']);
        if (!is_null($new_service['opt_services'])) {
            $details['advertisement']['plan_opt_service'] = AdvPlOptService::find($new_service['opt_services']);
        } else {
            $details['advertisement']['plan_opt_service'] = null;
        }
        $details['advertisement']['publishable_locations'] = AdvPublicationLocation::where([
            ['advertiser', $advertiser_id],
            ['service_id', $new_service->id],
        ])->get();

        \Mail::to($advertiser['email'])->send(new \App\Mail\front\RenewPlanNotificationToAdvertiser($details));
        \Mail::to(env('APP_SUPERADMIN_EMAIL_ID'))->send(new \App\Mail\front\RenewPlanNotificationToSuperAdmin($details));

        return redirect(route('createNewAd', $advertiser['id']))->with('flash_success_message', $msg);
    }
}
