<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ config('app.name', 'Creteng') }}</title>
  </head>
  <body>
    <div>
      <p><b><h5>Hi Sir</h5></b></p>
      <p>Challenge Reply has been reported as detailed below.</p>
    </div>
    <div>
      <p><b>Report Detail:</b><br/>
        <b>Report:</b> {{  $details['report']['report_text'] }} <br/>
        <b>Reported Date:</b> {{ date('M-d-Y', strtotime($details['report']['updated_at'])) }} <br/>
        <b>Reported By User Name:</b> {{  $details['reported_by']['name'] }} <br/>
        <b>Reported By User Email:</b> {{  $details['reported_by']['email'] }} <br/>
     </p>
     <p><b>Challenge Reply Detail:</b><br/>
      <b>Challenge Reply Text:</b> {{  $details['report']['challenge_reply']['reply_text'] }} <br/>
      <b>Challenge Reply Link:</b> <a href="{{  $details['report']['challenge_reply']['reply_binary'] }}">{{ $details['report']['challenge_reply']['reply_binary'] }}</a> <br/>
     </p>
     <p><b>Challenge Detail:</b><br/>
      @foreach ($details['report']['challenge_reply']['post'] as $post)
      <b>Challenge Text:</b> {{  $post['post_text'] }} <br/>
      <b>Challenge Link:</b> <a href="{{  $post['post_binary'] }}">{{  $post['post_binary'] }}</a> <br/>
      <b>Challenged By Name:</b> {{  $post['posted_by']['name'] }} <br/>
      <b>Challenged By Email:</b> {{  $post['posted_by']['email'] }} <br/>
      @endforeach
     </p>
    </div>
  </body>
</html>