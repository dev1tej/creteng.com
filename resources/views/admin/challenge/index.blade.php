@extends('admin.layouts.master')
@section('content')
<div class="container">
  <div class="row">
    <div class="col-lg-12 margin-tb">
      <div class="pull-left">
        <h2>Challenges Management</h2>
      </div>
    </div>
  </div>
  @if ($message = Session::get('success'))
  <div class="alert alert-success">
    <p>{{ $message }}</p>
  </div>
  @endif
  @if ($message = Session::get('danger'))
  <div class="alert alert-danger">
    <p>{{ $message }}</p>
  </div>
  @endif
  @if (isset($success))
  <div class="alert alert-success">
    <p>{{ $success }}</p>
  </div>
  @endif
  <section class="content">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-body">
            <table id="challengesData" class="table table-bordered table-striped table-hover">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Challenge Type</th>
                  <th>Challenge Text</th>
                  <th>File Type</th>
                  <th>File Upload</th>
                  <th width="280px">Action</th>
                </tr>
              </thead>
              <tbody>
                @php $srno = '1'; @endphp
                @forelse ($data as $key => $post)
                <tr>
                  <td>@php echo $srno; @endphp</td>
                  <td>
                    @if($post->post_type == 'public')
                    <label class="badge badge-primary">{{ $post->post_type }}</label>
                    @endif
                    @if($post->post_type == 'private')
                    <label class="badge badge-success">{{ $post->post_type }}</label>
                    @endif
                  </td>
                  <td>{{ $post->post_text }}</td>
                  <td>{{ $post->post_binary_mime_type }}</td>
                  <td>
                    @if($post->post_binary_mime_type == 'image')
                    <a href="{{ $post->post_binary }}">
                    <img src="{{ $post->post_binary }}" alt="{{ $post->post_binary }}" width="100" height="120">
                    </a>
                    @endif
                    @if($post->post_binary_mime_type == 'video')
                    <video width="120" height="100" controls>
                      <source src="{{ $post->post_binary }}"  alt="{{ $post->post_binary }}">
                    </video>
                    @endif
                  </td>
                  <td>
                    <a class="btn btn-primary" href="{{ route('viewChallenges',$post->id) }}">View</a>
                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#exampleModal{{ $post->id }}" data-id="{{ $post->id }}" >Delete</button>
                    @if ($post->reported == true) 
                    <mark><i><b>Challenge Reported</b></i></mark><br/>
                    @endif
                    @if ($post->challenge_reply_reported == true) 
                    <mark><i><b>Challenge Reply Reported</b></i></mark>
                    @endif
                  </td>
                </tr>
                <!-- Delete Manager Modal Starts -->
                <div class="modal fade" id="exampleModal{{ $post->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                  <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Warning</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                        If you want to delete this challenge , all information will be lost of this challenge
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                        <a href="{{ route('deleteChallenge', $post->id )}}"><button type="button" class="btn btn-danger">Yes, go forward</button></a>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- Delete Manager Modal Ends -->
                @php $srno++; @endphp
                @empty
                No Challenge Found
                @endforelse
              </tbody>
              <tfoot>
                <tr>
                  <th>No</th>
                  <th>Challenge Type</th>
                  <th>Challenge Text</th>
                  <th>File Type</th>
                  <th>File Upload</th>
                  <th>Action</th>
                </tr>
              </tfoot>
            </table>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

<script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script>
  $(function () {
    $('#challengesData').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    }); 
  });
</script>

@endsection