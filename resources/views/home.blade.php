@extends('layouts.app')
@section('content')

     <!-- ======= Hero Section ======= -->
  <section id="hero">

    <div class="container">
      <div class="row">
        <div class="col-lg-7 pt-5 pt-lg-0 order-2 order-lg-1 d-flex align-items-center">
          <div data-aos="zoom-out">
            <h1>Creteng</h1>
            <h2>Be Creative and Challenge the World!</h2>
            <div class="text-center text-lg-left" style="margin-bottom:50px;">
              <a href="#"><img src="{{ asset('assets/img/app-store.png') }}" alt="App Store" class="img-fluid"></a>
              <a href="#"><img src="{{ asset('assets/img/google-play.png') }}" alt="Google Play" class="img-fluid"></a>
            </div>
			<h3>Advertise with Us!</h3>
			<p style="margin-bottom:120px;"><a href="/advertiser/register/personal-detail" class="header-btn">Click here</a></p>
          </div>
        </div>
        <div class="col-lg-5 order-1 order-lg-2 hero-img" data-aos="zoom-out" data-aos-delay="300">
          <img src="{{ asset('assets/img/hero-img.png') }}" class="img-fluid animated" alt="">
        </div>
      </div>
    </div>
  </section><!-- End Hero -->
    <!-- ======= About Section ======= -->
    <section id="about" class="about">
      <div class="container">
        <div class="row">
          <div class="col-xl-12 col-lg-12 text-center" data-aos="fade-left">
            <h2>About Us</h2>
            <p>Have fun with Creteng! Share your moments and challenge your friends!</p><br><br>
            <div class="row">
            <div class="col-xl-4 col-lg-4" data-aos="zoom-in" data-aos-delay="100">
              <div class="boxbg">
                 <p><img src="{{ asset('assets/img/registration-icon.png') }}" class="img-fluid animated" alt=""></p>
                 <h4 class="title">Easy Registration</h4>
                 <!-- <p class="description">A mobile app is a computer program designed to run on a mobile device such as a phone/tablet or watch.</p> -->
              </div>
            </div>

            <div class="col-xl-4 col-lg-4" data-aos="zoom-in" data-aos-delay="200">              
              <div class="boxbg">
              <p><img src="{{ asset('assets/img/login-icon.png') }}" class="img-fluid animated" alt=""></p>
              <h4 class="title">Login</h4>
              <!-- <p class="description">A mobile app is a computer program designed to run on a mobile device such as a phone/tablet or watch.</p> -->
            </div>
            </div>

            <div class="col-xl-4 col-lg-4" data-aos="zoom-in" data-aos-delay="300">              
              <div class="boxbglast">
              <p><img src="{{ asset('assets/img/challenge-icon.png') }}" class="img-fluid animated" alt=""></p>
              <h4 class="title">Challenge the World</h4>
              <!-- <p class="description">A mobile app is a computer program designed to run on a mobile device such as a phone/tablet or watch.</p> -->
            </div>
            </div>
          </div>
          </div>
        </div>

      </div>
    </section><!-- End About Section -->

    <!-- ======= Details Section ======= -->
    <section id="details" class="details section-bg">
      <div class="container">

        <div class="row content">
          <div class="col-md-4" data-aos="fade-right">
			  <div class="mediouter">
				<img src="{{ asset('assets/img/details-1.png') }}" class="img-fluid" alt="">
				<!-- Media Player-->
				<div class="mediPlayer">
					<audio class="listen" preload="none" data-size="80" src="{{ asset('assets/img/CRETENG BEST TOP (1).mp3') }}"></audio>
				</div>
			  </div>
          </div>
          <div class="col-md-8 pt-4" data-aos="fade-up">
            <h3>Features</h3>            
                        <ul>
              <li><img src="{{ asset('assets/img/featured-icon1.png') }}" class="img-fluid" alt="">User Friendly</li>
              <li><img src="{{ asset('assets/img/featured-icon2.png') }}" class="img-fluid" alt="">Unlimited Features</li>
              <li><img src="{{ asset('assets/img/featured-icon3.png') }}" class="img-fluid" alt="">Smooth Typography</li>
              <li><img src="{{ asset('assets/img/featured-icon4.png') }}" class="img-fluid" alt="">Easy Installation</li>
              <li><img src="{{ asset('assets/img/featured-icon5.png') }}" class="img-fluid" alt="">Super Fast</li>
              <li><img src="{{ asset('assets/img/featured-icon6.png') }}" class="img-fluid" alt="">Well Documented</li>
              <li><img src="{{ asset('assets/img/featured-icon7.png') }}" class="img-fluid" alt="">Creative designed</li>
              <li><img src="{{ asset('assets/img/featured-icon8.png') }}" class="img-fluid" alt="">Extra Booster</li>
            </ul>
          </div>
        </div>
      </div>
    </section><!-- End Details Section -->




<style>

.mediouter {
    position: relative;
}
.mediPlayer {
    float: left;
    margin: 20px;
    position: absolute;
    z-index: 9999;
    width: 80px;
    height: 80px;
	left: 15%;
    top: 25%;
	cursor: pointer;
}

.mediPlayer .control {
    opacity        : 0; /* transition: opacity .2s linear; */
    pointer-events : none;
    cursor         : pointer;
}

.mediPlayer .not-started .play, .mediPlayer .paused .play {
    opacity : 1;

}

.mediPlayer .playing .pause {
    opacity : 1;

}

.mediPlayer .playing .play {
    opacity : 0;
}

.mediPlayer .ended .stop {
    opacity        : 1;
    pointer-events : none;
}

.mediPlayer .precache-bar .done {
    opacity : 0;
}

.mediPlayer .not-started .progress-bar, .mediPlayer .ended .progress-bar {
    display : none;
}

.mediPlayer .ended .progress-track {
    stroke-opacity : 1;
}

.mediPlayer .progress-bar,
.mediPlayer .precache-bar {
    transition        : stroke-dashoffset 500ms;

    stroke-dasharray  : 298.1371428256714;
    stroke-dashoffset : 298.1371428256714;
}
</style>
  
@endsection