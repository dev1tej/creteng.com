<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostsTagged extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 
        'post_id', 'tagged_by', 'tagged_user', 'tagged_type', 'challenge_accepted'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;
    
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        
    ];

    /**
     * Get the post that owns the comment.
     */
    public function post()
    {
        return $this->belongsTo('App\Post', 'post_id', 'id');
    }

    /**
     * Get the user that owns the post.
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id ', 'id');
    }

    /**
     * Get the users that are tagged.
     */
    public function taggedUser()
    {
        return $this->belongsTo('App\User', 'user_id ', 'id');
    }
}
