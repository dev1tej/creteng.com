@extends('admin.layouts.master')
@section('content')
<div class="container">
  <div class="row">
    <div class="col-lg-12 margin-tb">
      <div class="pull-left">
        <h2>Advertisements Management</h2>
      </div>
    </div>
  </div>
  @if ($message = Session::get('success'))
  <div class="alert alert-success">
    <p>{{ $message }}</p>
  </div>
  @endif
  @if ($message = Session::get('danger'))
  <div class="alert alert-danger">
    <p>{{ $message }}</p>
  </div>
  @endif
  @if (isset($success))
  <div class="alert alert-success">
    <p>{{ $success }}</p>
  </div>
  @endif
  <section class="content">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <div class="row">
              <div class="pull-right col-lg-7">
                <a class="btn btn-success" href="{{ route('showAllAdvertisement') }}"> Back </a>
              </div>
            </div>
          </div>
          <div class="card-body">
            <table id="advData" class="table table-bordered table-striped table-hover">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Advertisements</th>
                  <th>Status</th>
                  <th width="280px">Action</th>
                </tr>
              </thead>
              <tbody>
                @php $srno = '1'; @endphp
                @forelse ($data as $key => $advertisement)
                <tr>
                  <td>@php echo $srno; @endphp</td>
                  <td>
                    @if($advertisement->ad_type == 'image')
                    <img src="{{ $advertisement->advertisement }}" alt="{{ $advertisement->advertisement }}" width="100" height="120">
                    @endif
                    @if($advertisement->ad_type == 'video')
                    <video width="120" height="100" controls>
                      <source src="{{ $advertisement->advertisement }}"  alt="{{ $advertisement->advertisement }}">
                    </video>
                    @endif
                  </td>
                  <td>{{ $advertisement->status }}</td>
                  <td>
                    {{--<a class="btn btn-dark" href="{{ route('adPaymentHistory', $advertisement->id) }}">Payment History</a>--}}
                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#exampleModal{{ $advertisement->id }}" data-id="{{ $advertisement->id }}" >Unarchive</button>
                  </td>
                </tr>
                <!-- Archive Advertisement Modal Starts -->
                <div class="modal fade" id="exampleModal{{ $advertisement->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                  <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Warning</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                        If you want to unarchive this Advertisement then active state of advertisement will restored.
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                        <a href="{{ route('unarchiveAdvertisement', $advertisement->id ) }}"><button type="button" class="btn btn-danger">Yes, go forward</button></a>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- Archive Advertisement Modal Ends -->
                @php $srno++; @endphp
                @empty
                No Advertisement Found
                @endforelse
              </tbody>
              <tfoot>
                <th>No</th>
                  <th>Advertisements</th>
                  <th>Status</th>
                  <th>Action</th>
              </tfoot>
            </table>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

<script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script>
  $(function () {
    $('#advData').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    }); 
  });
</script>

@endsection
