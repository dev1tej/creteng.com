<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ config('app.name', 'Creteng') }}</title>
  </head>
  <body>
    <p>Hello <b>{{  $details['tagged_user']['name'] }}</b></p>
    <p>Your friend <b>{{  $details['auth_user']['name'] }}</b> has challenged you in a post.</p>
  </body>
</html>