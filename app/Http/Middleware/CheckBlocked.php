<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Auth;

class CheckBlocked
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
		if(Auth::check()){
			if(Auth::user()->blocked == 1){
				$user = auth()->user();
				auth()->logout();
				$message = 'Your account has been blocked. Please contact administrator.';
				return redirect()->route('login')->withMessage($message);
			}
        }
        return $next($request);
    }
}
