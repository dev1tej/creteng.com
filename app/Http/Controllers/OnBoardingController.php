<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\OnBoardingImage;

class OnBoardingController extends Controller
{
    /**
     * Display a listing of the Onboarding Images.
     *
     * @return \Illuminate\View\View
     */
    public function showAllOnboardingImage()
    {
        $data = OnBoardingImage::orderBy('id','DESC')->get();
        return view('admin.onboarding.index', compact('data'));
    }

    /**
     * Sending to create view.
     *
     * @return \Illuminate\View\View
     */
    public function createOnboardingImage()
    {
        return view('admin.onboarding.create');
    }

    /**
     * Creating new Onboarding Images
     * 
     * @param \Illuminate\Http\Request  $request
     * @return view
     */
    public function addOnboardingImage(Request $request) 
    {
        /* Check Validation */
        $this->validate($request, [
            'onboarding_image' => 'required|image|max:10240',
        ]);
		$path = $request->file('onboarding_image')->store('uservideos/onboarding_image', 's3');
        $aws_file_path = env('AWS_URL') . $path; 
		/* Create OnBoarding Image */
        OnBoardingImage::create([
            'image' => $aws_file_path,
        ]);
        return redirect()->route('showAllOnboardingImage')->with('success','Onboarding Image added successfully');
    }

    /**
     * To delete an Onboarding Images
     * 
     * @param int $image
     * @return view
     */
    public function deleteOnboardingImage($image)
    {
        OnBoardingImage::find($image)->delete();
        
        return redirect()->route('showAllOnboardingImage')->with('danger','Onboarding Image deleted successfully');

    }
}
