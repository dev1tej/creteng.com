<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ config('app.name', 'Creteng') }}</title>
  </head>
  <body>
    <div>
      <p><b><h5>Hi Sir</h5></b></p>
      <p>Post/Challenge has been reported as detailed below.</p>
    </div>
    <div>
      <p><b>Report Detail:</b><br/>
        <b>Report:</b> {{  $details['report']['report_text'] }} <br/>
        <b>Reported Date:</b> {{ date('M-d-Y', strtotime($details['report']['updated_at'])) }} <br/>
        <b>Reported By User Name:</b> {{  $details['reported_by']['name'] }} <br/>
        <b>Reported By User Email:</b> {{  $details['reported_by']['email'] }} <br/>
     </p>
     <p><b>Post/Challenge Detail:</b><br/>
      @foreach ($details['report']['post'] as $post)
      <b>Post Type:</b> @if($post['include_challenges']  == true) Challenge @else Post @endif <br/>
      <b>Post Text:</b> {{  $post['post_text'] }} <br/>
      <b>Post Link:</b> <a href="{{  $post['post_binary'] }}">{{  $post['post_binary'] }}</a> <br/>
      <b>Posted By Name:</b> {{  $post['posted_by']['name'] }} <br/>
      <b>Posted By Email:</b> {{  $post['posted_by']['email'] }} <br/>
      @endforeach
     </p>
    </div>
  </body>
</html>