<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdvPlOptServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adv_pl_opt_services', function (Blueprint $table) {
            $table->id();
            $table->string('optional_services', 255);
            $table->decimal('aditional_price', 6, 2)->comment('in USD');
            $table->string('aditional_price_duration', 255);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('adv_pl_opt_services');
    }
}
