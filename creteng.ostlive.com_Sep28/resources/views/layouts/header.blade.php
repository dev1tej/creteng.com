
<!-- Body main wrapper start -->
    <div class="wrapper">
        <!-- Start Header Style -->
        <div id="header" class="hs-header bg-dark">
            <div id="sticky-header-with-topbar" class="header-wrap hidden-xs">
                <div class="container">
                    <div class="row">
                        <div class="col-md-2 col-lg-2 col-sm-2">
                            <div class="hs-logo">
                                <a href="{{ url('/') }}"><img src="" alt="{{ config('app.name', 'Laravel') }}"></a>
                            </div>
                        </div>
                        <div class="col-md-10 col-lg-10 col-sm-10">
                            <!-- Start Header Top -->
                            <div class="hs-header-top">
                                <div class="header-top-left">
                                    <!-- <p>Opening Hours: <span>Mon-Fri:     8.00 to 18.00</span></p> -->
                                </div>
								<ul class="hs-user-link">
									@guest
										<li>
											<a href="{{ route('login') }}">{{ __('Login') }}</a>
										</li>
										@if (Route::has('register'))
											<li>
												<a href="">{{ __('Register') }}</a>
											</li>
										@endif
										@else
										@php 
											$manager=env("APP_MANAGER"); 
											$admin=env("APP_ADMIN");
											$superadmin=env("APP_SUPERADMIN");
											$customer=env("APP_CUSTOMER"); 
										@endphp
											
											@if(Auth::user()->hasRole($admin) || Auth::user()->hasRole($superadmin) || Auth::user()->hasRole($manager))
												<li><a href="{{ route('dashboard') }}">Dashboard</a></li>
											@endif
											@if(Auth::user()->hasRole($customer))
											<li class="nav-item dropdown">
												<a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>My Accounts <span class="caret"></span></a>
												<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown2">
													<a class="dropdown-item" href="{{ route('manageAccount', Auth::user()->id)}}">Manage Profile</a>
													<a class="dropdown-item" href="{{ route('favourites')}}">Manage Favourite List</a>
													<a class="dropdown-item" href="{{ route('myreviews')}}">My Reviews</a>
													<a class="dropdown-item" href="{{ route('userPasswordEdit', Auth::user()->id ) }}">Change Password</a>
												</div>	
											</li>
											@endif
											<li class="nav-item dropdown">
												<a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
													{{ Auth::user()->name }} <span class="caret"></span>
												</a>

												<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
													<a class="dropdown-item" href="{{ route('logout') }}"
														onclick="event.preventDefault();
																		document.getElementById('logout-form').submit();">
														{{ __('Logout') }}
													</a>

													<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
														@csrf
													</form>
												</div>
											</li>
								    @endguest
                                </ul>
                            </div>
                            <!-- End Header Top -->
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
		<!-- End Header Style -->
		

        
