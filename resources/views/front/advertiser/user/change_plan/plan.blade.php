@extends('front.layouts.master')
@section('content')
<style type="text/css">
.fixed-top {
  position: relative;
}
#footer .footer-top {
 padding: 50px 0 0 0;
 top: 0; 
}
#header.header-scrolled{
  position: fixed;
  top: 0;
  right: 0;
  left: 0;
  z-index: 1030;
}
</style>
<section class="inner-page">
<div class="container">
  <div class="row">
    <div class="col-lg-12 margin-tb">
      <div class="pull-left">
        <h4 id="error" class="text-danger font-weight-bold"></h4>
        <span class="text-danger font-weight-bold">All fields marked with * are required</span>
      </div>
    </div>
  </div>
  @if (count($errors) > 0)
  <div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
      @foreach ($errors->all() as $error)
      <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
  @endif
  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
      <div class="card">
        <div class="col-xs-12 col-sm-12 col-md-12">
          <h4><span class="text-danger font-weight-bold">*</span>Select One Plan</h4>
          @foreach($ad_plans as $ad_plan)
            <strong>{{ $ad_plan->plan_name }}:</strong>
            <input type="radio" id="{{ $ad_plan->id }}" name="radio_plan_name"/>
            <br/><br/>
          @endforeach
        </div>
      </div>
      <br/>
    </div>
  </div>
  <form action="{{ route('postChangePlanPlan') }}" method="post">
    @csrf
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12">
    
        @foreach($ad_plans as $ad_plan)
        <div name="radio_pl_name" id="{{ $ad_plan->id }}" class="unique">
          <br/>
          <div class="card">
            <div class="col-xs-12 col-sm-12 col-md-12">
              <br/>
              <div class="row">
                <div class="col-xs-12 col-sm-4 col-md-4">
                  <strong><span class="text-danger font-weight-bold">*</span>Plan Name:</strong>
                </div>
                <div class="col-xs-12 col-sm-8 col-md-8">
                  <input type="text" name="advertiser_id" class="form-control" value="{{ $advertiser_id }}" hidden/>
                  <p>
                    {{ $ad_plan->plan_name }}
                    <input type="radio" name="plan_id" value="{{ $ad_plan->id }}"/>
                  </p>
                </div>
              </div>
              <br/>

              <div class="row">
                <div class="col-xs-12 col-sm-4 col-md-4">
                  <strong></span>Available Services:</strong>
                </div>
                <div class="col-xs-12 col-sm-8 col-md-8">
                  @foreach($ad_plan->fixServices as $fixService)
                  <p>*{{ $fixService->fixed_services }}</p>
                  @endforeach
                </div>
              </div>
              <br/>

              <strong><span class="text-danger font-weight-bold">*</span>Select a Plan Duration:</strong><br/><br/>
              <table class="table table-bordered table-striped table-hover">
                <thead>
                  <tr>
                    <th>Plan Duration</th>
                    <th>Price (in USD)</th>
                    <th><span class="text-danger font-weight-bold">*</span>Select</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($ad_plan->planDuration as $planDuration)
                  <tr>
                    <td>{{ $planDuration->plan_duration }}</td>
                    <td>{{ $planDuration->price }}</td>
                    <td><input type="radio" id="{{ $planDuration->id }}" name="sub_plan_id" value="{{ $planDuration->id }}"/></td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
              <br/>
            </div>
          </div>
          <br/>
        </div>
        @endforeach
        <div>
          <strong>Select Optional Services:</strong><br/><br/>
          <table class="table table-bordered table-striped table-hover">
            <thead>
              <tr>
                <th>Optional Service</th>
                <th>Service Cost</th>
                <th>Cost Period</th>
                <th>Select</th>
              </tr>
            </thead>
            <tbody>
              @foreach($plan_opt_services as $plan_opt_service)
              <tr>
                <td>{{ $plan_opt_service->optional_services }}</td>
                <td>{{ $plan_opt_service->aditional_price }}</td>
                <td>{{ $plan_opt_service->aditional_price_duration }}</td>
                <td><input type="checkbox" id="{{ $plan_opt_service->id }}" name="opt_service_ids[]" value="{{ $plan_opt_service->id }}"/></td>
              </tr>
              @endforeach
            </tbody>
            <tfoot></tfoot>
          </table>
          <br/>
        </div>

        <div>
          <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <button type="submit" id="submit" class="btn btn-primary">Next</button>
          </div>
        </div>
      </div>
    </div>
  </form>
</div>
@endsection
<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script>
  $(document).ready(function() {
      $('input[name="radio_plan_name"]').click(function(){
          var choosen_plan_id = $(this).attr('id');
          $('[name="radio_pl_name"]').each(function() {
            var plan_id = $(this).attr('id');
            if (choosen_plan_id == plan_id) {
              $(this).show();
              var a = $(this).find('input[name="plan_id"]').attr('checked', 'true');
            } else {
              $(this).hide();
            }
          });
      });
      $('#submit').click(function(event){
        var  plan_parent_id = $('[name="plan_id"]:checked').parents('.unique').attr('id');
        var sub_plan_parent_id = $('[name="sub_plan_id"]:checked').parents('.unique').attr('id');
        if (plan_parent_id != sub_plan_parent_id) {
          document.getElementById('error').innerHTML = "** Error: Plan and Sub plan must belong to same plan group.";
          event.preventDefault();
        }
      });
  });
</script>