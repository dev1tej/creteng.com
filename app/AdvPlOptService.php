<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdvPlOptService extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 
        'optional_services', 'aditional_price', 'aditional_price_duration'
    ];
}
