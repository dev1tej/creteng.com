<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\User;
use App\Profile;
use App\Subscription;
use App\Payments;
use App\Coupan;
use App\Plan;

class SubscriptionUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'subscription:update';
    
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'To get data From STRIPE Now and Saved in our database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
		$this->info('subscription:update Command Run successfully!');
		
		$sk=env("STRIPE_SECRET"); 
		$stripe = new \Stripe\StripeClient($sk);
		
        
        //$subs=Subscription::where('stripe_status','active')->get();
        $subs=Subscription::all();
        
        foreach($subs as $sub){
			$user_id=$sub->user_id;
			$sub_id=$sub->stripe_id;
			
			$sub_status=$sub->stripe_status;
			$sub_ends_at=$sub->ends_at;
			
			$user=User::find($user_id);
			$cus_id=$user->stripe_id;
			
			
			$sub_response = $stripe->subscriptions->retrieve($sub_id,[]);
			//echo "<pre>"; print_r($sub_response); echo "</pre>";  die;
			
			if($sub_id){
				//$stripe = new \Stripe\StripeClient(env('STRIPE_SECRET'));
				//$cus_response=$stripe->subscriptions->retrieve($sub_id,[]);
				
				if(empty($sub_response->discount)){
					$coupon='';
				}else{
					$coupon= $sub_response->discount->coupon->name;
				}
				
				$record_exist=Payments::where('customer_id', $cus_id)->where('subscription_id', $sub_id)->where('plan_id', $sub_response->plan->id)->where('invoice_id',$sub_response->latest_invoice)->exists();
				
				if(empty($record_exist)){
					$payment = Payments::create(
						[
							'user_id' => $user_id,
							'customer_id' => $cus_id,
							'subscription_id' => $sub_id,
							'plan_id' => $sub_response->plan->id,
							'coupan' => $coupon,
							'interval' => $sub_response->items->data[0]->price->recurring->interval,
							'tax' => $sub_response->tax_percent,
							'price' => $sub_response->plan->amount/100,
							'status' => $sub_response->status,
							'invoice_id' => $sub_response->latest_invoice,
							'start_at' => date('Y-m-d H:i:s', $sub_response->current_period_start),
							'end_at' => date('Y-m-d H:i:s', $sub_response->current_period_end),
						]
					);	
				}
				
			}
		
			if($sub_status !== $sub_response->status || $sub_ends_at !== null){
				/* Update in Database */     
				Subscription::where('user_id',$user_id)->where('stripe_id',$sub_id)->update(
						[
							'stripe_status' => $sub_response->status,
							'ends_at' => date('Y-m-d H:i:s',$sub_response->current_period_end),
						]
					);				
			}
		
		
		}
		
		
		/* Get Coupans form stripe */
		$coupons_data= $stripe->coupons->all();
		$coupans = array();
		//echo"<pre>"; print_r($coupons_data->data);
		Coupan::truncate();
		foreach($coupons_data->data as $coupan){

			$crecord_exist=Coupan::where('coupan_code', $coupan->name)->where('coupan_id', $coupan->id)->where('coupan_redeemby', $coupan->redeem_by)->exists();
			
			if(empty($crecord_exist)){
				if(empty($coupan->percent_off) || !empty($coupan->amount_off)){
					$coupan_type="amount";
					$coupan_value=$coupan->amount_off;
				}elseif(!empty($coupan->percent_off) || empty($coupan->amount_off)){
					$coupan_type="percent";
					$coupan_value=$coupan->percent_off.'%';
				}
				$coupan = Coupan::create(
					[
						'coupan_title' => $coupan->name,
						'coupan_type' => $coupan_type,
						'coupan_value' => $coupan_value,
						'coupan_code' => $coupan->name,
						'coupan_id' =>  $coupan->id,
						'coupan_redeemby' => $coupan->redeem_by,
					]
				);
			}
		}
		
		/* Get Subscriptions Plans from Stripe */
		$subplans= $stripe->plans->all();
		foreach($subplans->data as $plans){

			$precord_exist=Plan::where('product_id', $plans->product)->where('price_id', $plans->id)->exists();
			
			if(empty($precord_exist)){
				$plan = Plan::create(
					[
						'plan_title' => $plans->interval.' '.$plans->object,
						'plan_amount' => $plans->amount_decimal/100,
						'plan_currency' => $plans->currency,
						'plan_interval' => $plans->interval,
						'product_id' => $plans->product,
						'price_id' => $plans->id,
					]
				);
			}
		}
			
        \Log::info("subscription:update is working fine!");
    }
}
