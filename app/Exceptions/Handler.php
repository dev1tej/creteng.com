<?php

namespace App\Exceptions;

use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Throwable  $exception
     * @return void
     *
     * @throws \Exception
     */
    public function report(Throwable $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Throwable
     */
    public function render($request, Throwable $exception)
    {
        if ($exception instanceof \Illuminate\Routing\Exceptions\InvalidSignatureException) {
            if (request()->ajax() || request()->wantsJson()) {
                return response()->json(['Error' => ['message' => 'Link expired. Please contact '.env('APP_SUPERADMIN_EMAIL_ID'). ' for new verification link or create a new account']], 500);
            } 
    
            return response()->json(['Error' => ['message' => 'Link expired. Please contact '.env('APP_SUPERADMIN_EMAIL_ID'). ' for new verification link or create a new account']], 500);
        }
        
        return parent::render($request, $exception);
    }

    protected function unauthenticated($request, AuthenticationException $exception)
    {

        if ($request->expectsJson()) {
            return response()->json(['error' => 'Unauthenticated.'], 401);
        }

        if ($request->is('advertiser') || $request->is('advertiser/*')) {
            return redirect()->guest(route('advertiser.login'));
        }

        return redirect()->guest(route('login'));
    }
}
