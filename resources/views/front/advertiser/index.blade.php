@extends('front.layouts.master')
@section('content')
<div class="container">

  @if(Session::has('flash_message'))
    <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {!! session('flash_message') !!}</em></div>
  @endif
  @if ($message = Session::get('success'))
  <div class="alert alert-success">
    <p>{{ $message }}</p>
  </div>
  @endif
  @if ($message = Session::get('danger'))
  <div class="alert alert-danger">
    <p>{{ $message }}</p>
  </div>
  @endif
  @if (isset($success))
  <div class="alert alert-success">
    <p>{{ $success }}</p>
  </div>
  @endif
 
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title"></h3>
            <div class="pull-right">
              <a class="btn btn-success" href="{{ route('createNewAd', Auth::user()->id) }}"> Create New Ad</a>
            </div>
          </div>
          <div class="card-header">
            <div class="row">
              <div class="pull-right col-lg-7">
                <h4 class="nav-link">Advertisements</h4>
              </div>
            </div>
          </div>
          <div class="card-body">            
            <table id="advData" class="table table-bordered table-striped table-hover">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Advertisements</th>
                  <th>Status</th>
                  <th>Publish</th>
                  <th width="280px">Action</th>
                </tr>
              </thead>
              <tbody>
                @php $srno = '1'; @endphp
                @forelse ($data as $key => $advertisement)
                <tr>
                  <td>@php echo $srno; @endphp</td>
                  <td>
                    @if(($advertisement['ad_type'] == 'image') || ($advertisement->ad_type == 'Image'))
                    <img src="{{ $advertisement['advertisement'] }}" alt="{{ $advertisement['advertisement'] }}" width="100" height="120">
                    @endif
                    @if(($advertisement['ad_type'] == 'video') || ($advertisement->ad_type == 'Video'))
                    <video width="120" height="100" controls>
                      <source src="{{ $advertisement['advertisement'] }}"  alt="{{ $advertisement['advertisement'] }}">
                    </video>
                    @endif
                  </td>
                  <td>{{ $advertisement['status'] }}</td>
                  <td>
                    @if($advertisement['published'] == true)
                        Published
                    @elseif($advertisement['published'] == false)
                        Unpublished
                    @endif
                  </td>
                  <td>
                    <a class="btn btn-info btn-sm" href="{{ route('advertiser.showAdvertisement', $advertisement->id) }}">View</a>
                    @if(($advertisement['published'] == false) && ($advertisement['status'] == 'Active') && ($advertisement['services']['status'] == true))
                    <button type="button" class="btn btn-success btn-sm" title="Send request to publish this ad" data-toggle="modal" data-target="#exampleModalPublish{{ $advertisement->id }}" data-id="{{ $advertisement->id }}">Send Publish Request</button>
                    @endif
                    <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#exampleModalDelete{{ $advertisement->id }}" data-id="{{ $advertisement->id }}">Delete</button>
                  </td>
                </tr>
                {{--@if(($advertisement['published'] == false) && ($advertisement['status'] == 'Active'))--}}
                <!-- Delete Advertisement Modal Starts -->
                <div class="modal fade" id="exampleModalDelete{{ $advertisement->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                  <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Warning</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                        If you want to delete this Advertisement , all information will be lost of this Advertisement
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                        <a href="{{ route('deleteAdvertiserAdvertisement', $advertisement->id )}}"><button type="button" class="btn btn-danger">Yes, go forward</button></a>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- Delete Advertisement Modal Ends -->

                <!-- Publish Advertisement Modal Starts -->
                <div class="modal fade" id="exampleModalPublish{{ $advertisement->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                  <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Info</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                        Your current advertisement will be unpublished.
                        Please Confirm to send publishing request for this ad.
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                        <a href="{{ route('sendAdPublishRequest', $advertisement->id )}}"><button type="button" class="btn btn-success">Yes, go forward</button></a>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- Publish Advertisement Modal Ends -->
               {{-- @endif --}}
                @php $srno++; @endphp
                @empty
                No Advertisement Found
                @endforelse
              </tbody>
            </table>            
          </div>
        </div>
      </div>
    </div>
  
</div>

<script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script>
  $(function () {
    $('#advData').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
  });
</script>

@endsection
