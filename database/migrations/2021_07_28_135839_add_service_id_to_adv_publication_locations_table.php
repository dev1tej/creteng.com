<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddServiceIdToAdvPublicationLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('adv_publication_locations', function (Blueprint $table) {
            $table->foreignId('service_id')->after('advertiser')->nullable();

            $table->foreign('service_id')
                ->references('id')
                ->on('adv_available_services')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('adv_publication_locations', function (Blueprint $table) {
            $table->dropColumn('service_id');
        });
    }
}
