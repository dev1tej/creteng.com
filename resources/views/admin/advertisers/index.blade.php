@extends('admin.layouts.master')
@section('content')
<div class="container">
  <div class="row">
    <div class="col-lg-12 margin-tb">
      <div class="pull-left">
        <h2>Advertiser Management</h2>
      </div>
    </div>
  </div>
  @if ($message = Session::get('success'))
  <div class="alert alert-success">
    <p>{{ $message }}</p>
  </div>
  @endif
  @if ($message = Session::get('danger'))
  <div class="alert alert-danger">
    <p>{{ $message }}</p>
  </div>
  @endif
  @if (isset($success))
  <div class="alert alert-success">
    <p>{{ $success }}</p>
  </div>
  @endif
  <section class="content">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title"></h3>
            <div class="pull-right">
              <!--a class="btn btn-success" href="{{-- route('createAdvertiser') --}}"> Create New Advertiser</a-->
            </div>
          </div>
          <div class="card-body">
            <table id="advertisersData" class="table table-bordered table-striped table-hover">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Id</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Contact No.</th>
                  <th width="280px">Action</th>
                </tr>
              </thead>
              <tbody>
                @php $srno = '1'; @endphp
                @forelse ($data as $key => $advertiser)
                <tr>
                  <td>@php echo $srno; @endphp</td>
                  <td>{{ $advertiser->id }}</td>
                  <td><a href="{{ route('showAdvertiser', $advertiser->id) }}">{{ $advertiser->name }}</a></td>
                  <td>{{ $advertiser->email }}</td>
                  <td>{{ $advertiser->contact }}</td>
                  <td>
                    <a class="btn btn-primary" href="{{ route('editAdvertiser', $advertiser->id) }}">Edit</a>
                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#exampleModal{{ $advertiser->id }}" data-id="{{ $advertiser->id }}" >Delete</button>	
                    <a class="btn btn-dark" href="{{ route('adPaymentHistory', $advertiser->id) }}">Payment History</a>
                    {{--@if((isset($advertiser->advertisement)) && ($advertiser->advertisement->status == 'Active') && ($advertiser->ad_expire == true))
                    <button type="button" class="btn btn-success" data-toggle="modal" 
                        data-target="#modalPaymentForm{{ $advertiser->id }}"
                        data-id="{{ $advertiser->id }}"
                      >Send Payment Link</button>
                    @endif--}}
                  </td>
                </tr>
                <!-- Delete Customer Modal Starts -->
                <div class="modal fade" id="exampleModal{{ $advertiser->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                  <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Warning</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                        If you want to delete this Advertiser , all information will be lost of this Advertiser
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                        <a href="{{ route('deleteAdvertiser', $advertiser->id ) }}"><button type="button" class="btn btn-danger">Yes, go forward</button></a>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- Delete Costomer Modal Ends -->

                <!-- Send Payment Mail Model Starts -->
                {{--@if($advertiser->ad_expire == true)
                <div class="modal fade" id="modalPaymentForm{{ $advertiser->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                  aria-hidden="true">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header text-center">
                        <h4 class="modal-title w-100 font-weight-bold">Send Payment Link</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <form id="PaymentForm" method="POST", action="{{ route('sendPaymentLink') }}">
                        @csrf
                        <input type="hidden" name="advertisement_id" value="{{ $advertiser->advertisement->id }}" class="form-control validate" id="advertisement">
                        <div class="modal-body mx-3">
                          <div class="md-form mb-5">
                            <label data-error="wrong" data-success="right" >Amount (in USD)</label>
                            <input type="number" step="0.01" name="amount" class="form-control validate" id="Amount" required>
                          </div>
                          <div class="md-form mb-4">
                            <label data-error="wrong" data-success="right">Payment Mode</label> 
                            <select name="payment_mode" class="form-control validate" id="PaymentMode" required>
                              <option value="Weekly">Weekly</option>
                              <option value="Monthly">Monthly</option>
                              <option value="Quarterly">Quarterly</option>
                              <option value="Half Yearly">Half Yearly</option>
                              <option value="Yearly">Yearly</option>
                            </select>
                          </div>
                        </div>
                        <div class="modal-footer d-flex justify-content-center">
                          <button type="submit" id="submit" class="btn btn-primary">Send</button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
                @endif--}}
                <!-- Send Payment Mail Model Ends -->
                @php $srno++; @endphp
                @empty
                No Advertiser Found
                @endforelse
              </tbody>
              <tfoot>
                <tr>
                  <th>No</th>
                  <th>Id</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Contact No.</th>
                  <th>Action</th>
                </tr>
              </tfoot>
            </table>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

<script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script>
  $(function () {
    $('#advertisersData').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>

@endsection