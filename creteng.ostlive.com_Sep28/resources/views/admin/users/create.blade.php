@extends('admin.layouts.master')

@section('content')
<div class="container">
	
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
			<a href="{{ route('allUsers') }}"><button type="button" class="btn btn-secondary">Go back to Users</button></a>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>	
    			
	@if(Session::has('success'))
		<div class="alert alert-success">
			{{ Session::get('success') }}
            @php
                Session::forget('success');
            @endphp
        </div>
    @endif
	
	<div class="card">
		<div class="card-body">
			<form method="POST" id="fieldgroup-form" action="{{ route('userForm') }}">
			{{ csrf_field() }} 
			  <div class="form-group">
				<label for="name">Name</label>
				<input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
					@if ($errors->has('name'))
						<span class="text-danger">{{ $errors->first('name') }}</span>
					@endif
			  </div>
			  <div class="form-group">
				<label for="name">Email</label>
				<input type="email" class="form-control" id="email" name="email" value="{{ old('email') }}" required autocomplete="email">
					@if ($errors->has('email'))
						<span class="text-danger">{{ $errors->first('email') }}</span>
					@endif
			  </div>
			<div class="form-group">
				<label for="country_code">{{ __('Contact No') }}</label>
				<div class="row">
				<div class="col-md-2">
					<input id="country_code" type="text" class="form-control @error('country_code') is-invalid @enderror" name="country_code" value="{{ old('country_code') }}" required autocomplete="country_code">
					<small class="form-text text-muted">Country Code</small>
					@error('country_code')
						<span class="invalid-feedback" role="alert">
							<strong>{{ $message }}</strong>
						</span>
					@enderror
				</div>
				<div class="col-md-4">
					<input id="contact" type="text" class="form-control @error('contact') is-invalid @enderror" name="contact" value="{{ old('contact') }}" required autocomplete="contact">
					@error('contact')
						<span class="invalid-feedback" role="alert">
							<strong>{{ $message }}</strong>
						</span>
					@enderror
				</div>
				</div>
			</div>
			  <div class="form-group">
				<label for="password">Password</label>
				<input type="password" class="form-control" id="password" name="password" required autocomplete="new-password">
					@if ($errors->has('password'))
						<span class="text-danger">{{ $errors->first('password') }}</span>
					@endif
			  </div>
			  <div class="form-group">
				<label for="password-confirm"> Confirm Password</label>
				<input type="password" class="form-control" id="password-confirm" name="confirm-password" required autocomplete="new-password">
			  </div>
			  <div class="form-group">
				<label for="roles">Roles</label>
					{!! Form::select('roles[]', $roles,[], array('class' => 'form-control')) !!}			  
				</div>
			  <button type="submit" class="btn btn-primary">Submit</button>
			</form>
		</div>
	</div>
	
</div>
@endsection
