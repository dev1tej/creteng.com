<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdsPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ads_payments', function (Blueprint $table) {
            $table->id();
            $table->foreignId('advertiser_id');
            $table->string('transaction_id', 255);
            $table->string('payment_mode', 255);
            $table->decimal('amount', 8, 2)->comment('amount (in USD)');
            $table->timestamps();
        
            $table->foreign('advertiser_id')
                ->references('id')
                ->on('adv_users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ads_payments');
    }
}
