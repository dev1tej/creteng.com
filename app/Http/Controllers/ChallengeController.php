<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Post;
use App\PostLike;
use App\PostComment;
use App\PostCommentReply;
use App\SavePost;
use App\PostsTagged;
use App\PostVideoViewed;
use App\PostsTaggedReply;
use App\ChallengeCommentReply;
use App\ChallengeCommentCommentReply;
use App\ReportPost;
use App\ReportChallengeReply;

class ChallengeController extends Controller
{
    /**
     * Display all the challenges.
     *
     * @return \Illuminate\View\View
     */
    public function showAllChallenges()
    {
        $data = Post::where([
            ['include_challenges', true],
        ])->orderBy('id','DESC')->get();
        foreach ($data as $post) {
            $post->reported = false;
            $is_reported = ReportPost::where('post_id', $post['id'])->first();
            if (!is_null($is_reported)) {
                $post->reported = true;
            } 

            $post->challenge_reply_reported = false;
            $challenge_replies = PostsTaggedReply::where('post_id', $post['id'])->get();
            foreach ($challenge_replies as $challenge_reply) {
                $is_challenge_reply_reported = ReportChallengeReply::where('challenge_reply_id', $challenge_reply['id'])->first();
                if (!is_null($is_challenge_reply_reported)) {
                    $post->challenge_reply_reported = true;
                } 
            }
        }
        return view('admin.challenge.index',compact('data'));
    }

    /**
     * Remove the specified Challenge from storage.
     *
     * @param  int  $id
     * @return View
     */

    public function deleteChallenge($id)
    {
        Post::find($id)->delete();
        
        return redirect()->route('showAllChallenges')->with('danger','Challenge deleted successfully');

    }

    /**
     * Display the specified Customer.
     *
     * @param  int  $post_id, $user_id 
     * @return \Illuminate\View\View
     */

    public function showCustomer($post_id, $user_id)
    {
        $post = Post::find($post_id);
        $user = User::find($user_id);
        return view('admin.challenge.show',compact('user', 'post'));
    }

    /**
     * View the specified challenges Details.
     *
     * @param  int  $id
     * @return \Illuminate\View\View
     */
    public function viewChallenges($id)
    {
        $posts = Post::where([
            ['id', $id],
        ])->get();
        $get_resource_detail_controller = new Api\GetResourceDetailController;
        $posts = $get_resource_detail_controller->getPostDetail($posts);

        foreach ($posts as $post) {
            $challenge = $post;
        }
        $challenge['reports'] = ReportPost::where('post_id', $challenge['id'])->with('user')->get();

        foreach ($challenge['post_challenge_reply']['challenge_reply'] as $challenge_reply) {
            $challenge_reply['reports'] = ReportChallengeReply::where('challenge_reply_id', $challenge_reply['id'])->with('user')->get();
        }
        
        return view('admin.challenge.view', compact('challenge'));
    }

    /**
     * View the manage challenges report page.
     *
     * @return \Illuminate\View\View
     */
    public function manageChallengeReport()
    {
        $data = Post::where([
            ['include_challenges', true],
        ])->orderBy('id','DESC')->get();
        return view('admin.reports.challenges.index',compact('data'));
    }

    /**
     * To generate report in csv format for the challenges.
     *
     * @param \Illuminate\Http\Request  $request
     * @return csv file
     */
    public function challengeGenerateCsvReport(Request $request)
    {
        $from_date = $request->post('from_date');
        $to_date = $request->post('to_date');
        $posts = Post::where([
            ['created_at', '>',  $from_date],
            ['created_at', '<=',  $to_date],
        ])->with('postUser')->get();
        
        $posts_array = array();
        $i = 1;
        foreach ($posts as $post) {
            $post_array = array();
            $post_array['serial_no'] = $i;
            $post_array['user_id'] = $post->postUser->name;
            $post_array['challenge_type'] = $post->post_type;
            $post_array['challenge_text'] = $post->post_text;
            $post_array['challenge_binary'] = $post->post_binary;
            $post_array['challenge_binary_mime_type'] = $post->post_binary_mime_type;
            $post_array['created_at'] = date("m-d-Y", strtotime($post->created_at));

            $post_array['total_challenges'] = PostsTagged::where([
                ['post_id', $post->id],
            ])->get()->count();
            $post_array['total_replies'] = PostsTaggedReply::where([
                ['post_id', $post->id],
            ])->get()->count();
            $post_array['total_likes'] = PostLike::where([
                ['post_id', $post->id],
            ])->get()->count();
            $post_array['total_saves'] = SavePost::where([
                ['post_id', $post->id],
            ])->get()->count();

            if ($post->post_binary_mime_type == 'video') {
                $post_array['total_video_views'] = PostVideoViewed::where([
                    ['post_id', $post->id],
                ])->get()->count();
            } else {
                $post_array['total_video_views'] = 'NA';
            }
            $post_array['total_comments'] = PostComment::where([
                ['post_id', $post->id],
            ])->get()->count();

            $posts_array[] = $post_array;  
            $i++; 
        }
        $headers = [
            'Sr. No',
            'Challenge Owner',
            'Challenge Type',
            'Challenge',
            'File',
            'File Type',
            'Created_at',
            'Total Challenges',
            'Total Replies',
            'Total Likes',
            'Total Saves',
            'Total Video Views',
            'Total Comments',
        ];
        header('Content-type: text/csv');
        header('Content-Disposition: attachment; filename="challenges.csv"');
        header('Pragma: no-cache');
        header('Expires: 0');

        $file = fopen('php://output', 'w');
        fputcsv($file, $headers);
        foreach ($posts_array as $post_array) {
            fputcsv($file, $post_array);
        }
        fclose($file);
        exit();
    }

    public function deleteChallengeComment(Request $request)
    {
        $comment_id = $request->comment_id;
        $comment = PostComment::find($comment_id);
        $comment->delete();
        return 'ok';
    }

    public function deleteChallengeCommentReply(Request $request)
    {
        $comment_reply_id = $request->comment_reply_id;
        $comment_reply = PostCommentReply::find($comment_reply_id);
        $comment_reply->delete();
        return 'ok';
    }

    public function deleteChallengeReplyComment(Request $request)
    {
        $comment_id = $request->comment_id;
        $comment = ChallengeCommentReply::find($comment_id);
        $comment->delete();
        return 'ok';
    }

    public function deleteChallengeReplyCommentReply(Request $request)
    {
        $comment_reply_id = $request->comment_reply_id;
        $comment_reply = ChallengeCommentCommentReply::find($comment_reply_id);
        $comment_reply->delete();
        return 'ok';
    }
}
