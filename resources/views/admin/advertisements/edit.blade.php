@extends('admin.layouts.master')
@section('content')
<div class="container">
  <div class="row">
    <div class="col-lg-12 margin-tb">
      <div class="pull-left">
        <h2>Edit New Advertisement</h2>
      </div>
      <div class="pull-right">
        <a class="btn btn-primary" href="{{ route('showAllAdvertisement') }}"> Back</a>
      </div>
    </div>
  </div>
  @if (count($errors) > 0)
  <div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
      @foreach ($errors->all() as $error)
      <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
  @endif
  {!! Form::model($advertisement, ['method' => 'PATCH','route' => ['updateAdvertisement', $advertisement->id], 'files' => true]) !!}
  <div class="row">
    <div class="col-xs-12 colshowCustomer-sm-12 col-md-12">
      <div class="form-group">
        <strong>Advertisement:</strong>
        {!! Form::file('advertisement', null, array('placeholder' => $advertisement->advertisement,'class' => 'form-control')) !!}
      </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
      <div class="form-group">
        <strong>Advertiser:</strong>
        {!! Form::text('advertiser_name', $advertiser->name, array('placeholder' => 'Advertiser Name','class' => 'form-control', 'readonly')) !!}
        {!! Form::text('advertiser_id', $advertiser->id, array('class' => 'form-control', 'hidden')) !!}  
      </div>
    </div>
    <div class="col-xs-12 colshowCustomer-sm-12 col-md-12">
      <div class="form-group">
        <strong>Order:</strong>
        {!! Form::number('order', null, array('placeholder' => 'Order','class' => 'form-control')) !!}
      </div>
    </div>
    <div class="col-xs-12 colshowCustomer-sm-12 col-md-12">
      <div class="form-group">
        <strong>Publishable Location:</strong>
        {!! Form::text('publishable_location', null, array('placeholder' => 'publishable location','class' => 'form-control')) !!}
      </div>
    </div>
    <div class="col-xs-12 colshowCustomer-sm-12 col-md-12">
      <div class="form-group">
        <strong>Publish Date:</strong>
        {!! Form::date('publish_date', null, array('placeholder' => 'publish date','class' => 'form-control', 'readonly' )) !!}
      </div>
    </div>
    <div class="col-xs-12 colshowCustomer-sm-12 col-md-12">
      <div class="form-group">
        <strong>Expiry Date:</strong>
        {!! Form::date('expiry_date', null, array('placeholder' => 'expiry date','class' => 'form-control')) !!}
      </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
      <button type="submit" class="btn btn-primary">Submit</button>
    </div>
  </div>
  {!! Form::close() !!}
</div>
@endsection