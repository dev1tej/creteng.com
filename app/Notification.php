<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 
        'applicant_id', 
        'respondent_id',
        'applicant_resource_id', 
        'respondent_resource_id',
        'motive',
        'notification_type',
        'message'
    ];

    /**
     * Get the user that owns the comment.
     */
    public function applicantUser()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    /**
     * Get the user that owns the comment.
     */
    public function respondentUser()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
}

