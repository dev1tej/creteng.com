<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
  //  return $request->user();
//});

//Route::get('login', 'Api\UserController@login');

Route::group(['middleware' => 'json_response'], function () {
    
    // public routes
    Route::post('/login', 'Auth\ApiAuthController@login')->name('login.api');
    Route::post('/register','Auth\ApiAuthController@register')->name('register.api');
    Route::post('/forgetPassword', 'Auth\ApiAuthController@forgetPassword')->name('forgetPassword.api');
    //Route::post('/resetPassword', 'Auth\ApiAuthController@resetPassword')->name('resetPassword.api');
    
    // protected routes
    Route::middleware('auth:api')->group(function () {
      Route::post('/logout', 'Auth\ApiAuthController@logout')->name('logout.api');
      Route::post('/changePassword', 'Api\UserController@changePassword')->name('changePassword.api');
      Route::post('/updateProfile', 'Api\UserController@update')->name('updateProfile.api');
      Route::post('/viewProfile', 'Api\UserController@index')->name('viewProfile.api');

      //posts routes
      Route::post('/addPost', 'Api\PostController@store')->name('addPost.api');
      Route::post('/editPost/{id}', 'Api\PostController@update')->name('editPost.api');
      Route::delete('/deletePost/{id}', 'Api\PostController@destroy')->name('deletePost.api');
    });
    
    Route::get('/searchPost/{id}', 'Api\PostController@show')->name('searchPost.api');
});


