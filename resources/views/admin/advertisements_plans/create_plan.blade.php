@extends('admin.layouts.master')
@section('content')
<div class="container">
  <div class="row">
    <div class="col-lg-12 margin-tb">
      <div class="pull-left">
        <h2>Create New Advertisement Plan</h2>
      </div>
      <div class="pull-right">
        <a class="btn btn-primary" href="{{ route('showAllAdPlan') }}"> Back</a>
      </div>
    </div>
  </div>
  @if (count($errors) > 0)
  <div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
      @foreach ($errors->all() as $error)
      <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
  @endif
  <form method="post" action="{{ route('addAdPlan') }}">
    {{ csrf_field() }}
    <div class="form-group row">
        <label for="plan_name" class="col-sm-3 col-form-label">Plan Name</label>
        <div class="col-sm-9">
            <input name="plan_name" type="text" class="form-control" id="plan_name" placeholder="Pro, Premium, Basic etc.">
        </div>
    </div>
    <div class="form-group row">
        <label for="plan_fix_service" class="col-sm-3 col-form-label">Plan Fix Service</label>
        <div class="col-sm-9">
            <select name="plan_fix_service" type="text" class="form-control" id="plan_fix_service">
              <option value="Can upload single image">Can upload single image</option>
              <option value="Can upload at most 2 minute video">Can upload at most 2 minute video</option>
              <option value="Can upload at most 5 minute video">Can upload at most 5 minute video</option>
            </select>
        </div>
    </div>
    <div class="form-group row">
        <div class="offset-sm-3 col-sm-9">
            <button type="submit" class="btn btn-primary">Next</button>
        </div>
    </div>
  </form>
</div>
@endsection
