@extends('front.layouts.master')
@section('content')
<style type="text/css">
.fixed-top {
  position: relative;
}
#footer .footer-top { 
 padding: 50px 0 0 0;
 top: 0; 
}
#header.header-scrolled{
  position: fixed;
  top: 0;
  right: 0;
  left: 0;
  z-index: 1030;
}
</style>
<section class="inner-page">
<div class="container">
  <div class='alert-info alert text-center'>
    <h2><strong>Renew</strong></h2>
    @if($plan_expired == true)
      <p>{{ $info }}</p>
      <p >Renew Your Subscription to Continue Publication of Your Advertisement(s)</p><br/> 
      {{--<p>The process is irreversible.</p><br/> --}}
      <p><a href="{{ route('renewPlanInfo2') }}"> Renew </a></p>
    @else
      <p>{{ $info }}</p>
    @endif
  </div>
</div>
</section>
@endsection