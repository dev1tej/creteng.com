@extends('admin.layouts.master')
@section('content')
<div class="container">
  <div class="row">
    <div class="col-lg-12 margin-tb">
      <div class="pull-left">
        <h2> Show Advertiser</h2>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="card" style="height: 100%; width: 100%;">
      <div class="card-header">
        <div class="col-lg-12 margin-tb">
          <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('showAllAdvertiser') }}"> Back</a>
          </div>
        </div>
      </div>
      <div class="card-body">
        <h5 class="card-title"><strong>{{ $advertiser->name }}</strong></h5>
        <p class="card-text">
          {{ $advertiser->address_line_1 }}
          {{ $advertiser->address_line_2 }}
          {{ $advertiser->city }}
          {{ $advertiser->state }}
          {{ $advertiser->postal_code }}
          {{ $advertiser->country }}
        </p>
        <p class="card-text">{{ $advertiser->email }}</p>
        <p class="card-text">{{ $advertiser->contact }}</p>
        <p class="card-text">{{ $advertiser->company_name }}</p>
        <div class="card" style="height: 100%; width: 100%;">
          <div class="card-body">
            <h2><strong>Chosen Plan History</strong></h2>
            @foreach($my_plans as $my_plan)
            <div class="card" style="height: 100%; width: 100%;">
              <div class="card-body">
                <p class="card-text">Plan: 
                  <span>
                    {{ $my_plan['plan']['plan_name'] }}
                  </span>
                  @if(($my_plan['status'] == true) && ($my_plan['payment']['expiry_date'] >= date('Y-m-d')))
                    <mark><sup><b><i>Active</i></b></sup></mark>
                  @elseif(($my_plan['status'] == false) && ($my_plan['payment']['expiry_date'] != null))
                  <mark><sup><b><i>Expired</i></b></sup></mark>
                  @else
                  <mark><sup><b><i>Inactive</i></b></sup></mark>
                  @endif
                </p>
                <p class="card-text">Plan Duration: <span>{{ $my_plan['plan_duration']['plan_duration'] }}</span></p>
                <p class="card-text">Plan Duration Cost: <span>${{ $my_plan['plan_duration']['price'] }}</span></p>
                <p class="card-text">Fixed Services: <span>{{ $my_plan['plan_fixed_service']['fixed_services'] }}</span></p>
                @if(!is_null($my_plan['plan_opt_service']))
                <p class="card-text">Optional Services: <span>{{ $my_plan['plan_opt_service']['optional_services'] }}</span></p>
                <p class="card-text">Optional Services Cost: <span>${{ $my_plan['plan_opt_service']['aditional_price'] }} {{ $my_plan['plan_opt_service']['aditional_price_duration'] }}</span></p>
                @endif
                <p class="card-text">Ad Publishable Locations:
                  <span>
                    @php $no_of_publication_location = 0; @endphp
                    @foreach ($my_plan['publishable_locations'] as $publishable_location)
                    @php $no_of_publication_location++; @endphp
                    {{ $no_of_publication_location }}. Country: {{  $publishable_location['country'] }} State: {{  $publishable_location['state'] }},
                    @endforeach
                  </span>
                </p>
                <p class="card-text">Number of Publication Location: <span>{{ $no_of_publication_location }}</span></p>
                @if(!is_null($my_plan['plan_opt_service']))
                <p class="card-text">Total Optional Services Cost: <span>${{ $my_plan['total_opt_services_cost'] }}</span></p>
                <p class="card-text">Total Paid: <span>({{ $my_plan['plan_duration']['price'] }} + {{ $my_plan['plan_opt_service']['aditional_price'] }} {{ $my_plan['plan_opt_service']['aditional_price_duration'] }} {{ $my_plan['multiplying_factor'] }}) * {{ $no_of_publication_location }} = ${{ $my_plan['payment']['amount'] }}</span></p>
                @else
                <p class="card-text">Total Paid: <span>${{ $my_plan['payment']['amount'] }}</span></p>
                @endif
                <h2><strong>Advertisements</strong></h2>
                <hr/>
                {{--@if(count($my_plan['advertisements']) != 0)--}}
                @foreach($my_plan['advertisements'] as $data)
                <div class="card-body">
                  <div class="col-md-12">
                    @if(($data['ad_type'] == 'image') || ($data['ad_type'] == 'Image'))
                    <img src="{{ $data['advertisement'] }}" alt="{{ $data['advertisement'] }}" class="card-img" style="width:50%;height:50%;"/>
                    @elseif(($data['ad_type'] == 'video') || ($data['ad_type'] == 'Video'))
                    <video class="img-fluid" controls>
                      <source src="{{ $data['advertisement'] }}" alt="{{ $data['advertisement'] }}"/>
                    </video>
                    @endif
                    <p></p>
                  </div>
                  <hr/>
                  <div class="card">
                    <div class="card-body">
                      <strong>Status:</strong>
                      <p>{{ $data['status'] }}</p>
                      <hr/>
                      <strong>Paid:</strong>
                      @if($data['paid'] == 1)
                      True
                      @elseif($data['paid'] == 0)
                      False
                      @endif
                      <hr/>
                      <strong>Published:</strong>
                      @if($data['published'] == 1)
                      True
                      @elseif($data['published'] == 0)
                      False
                      @endif
                      <hr/>
                      <strong>Publish Date:</strong>
                      <p>{{ $data['publish_date'] }}</p>
                      <hr/>
                      <strong>Expiry Date:</strong>
                      <p>{{ $data['expiry_date'] }}</p>
                      <hr/>
                      <hr/>
                    </div>
                  </div>
                </div>
                @endforeach 
                {{--@elseif(count($my_plan['advertisements']) == 0)
                  <div class="card-body">
                    <p class="card-text">Advertisements under this plan has shifted to similar opted plan</p>
                  </div>   
                @endif--}}
              </div>
            </div>
            @endforeach
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection