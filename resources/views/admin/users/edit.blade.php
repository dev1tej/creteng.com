@extends('admin.layouts.master')
@section('content')
<div class="container">
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <a href="{{ route('dashboard') }}"><button type="button" class="btn btn-secondary">Go back to Dashboard</button></a>
        </div>
      </div>
    </div>
    <!-- /.container-fluid -->
  </section>
  @if(Session::has('success'))
  <div class="alert alert-success">
    {{ Session::get('success') }}
    @php
    Session::forget('success');
    @endphp
  </div>
  @endif
  <div class="card">
    <div class="card-body">
      <form method="POST" id="user-form" action="{{ route('userFormUpdate', $user->id) }}">
        {{ csrf_field() }} 
        <div class="form-group">
          <label for="name">Name</label>
          <input type="text" class="form-control" id="name" name="name" value="{{ $user->name }}" required autocomplete="name" autofocus>
          @if ($errors->has('name'))
          <span class="text-danger">{{ $errors->first('name') }}</span>
          @endif
        </div>
        <div class="form-group">
          <label for="name">Email</label>
          <input type="email" class="form-control" id="email" name="email" value="{{ $user->email }}" required autocomplete="email">
          @if ($errors->has('email'))
          <span class="text-danger">{{ $errors->first('email') }}</span>
          @endif
        </div>
        <div class="form-group">
          <label for="password">Password</label>
          <input type="password" class="form-control" id="password" name="password" autocomplete="new-password">
          @if ($errors->has('password'))
          <span class="text-danger">{{ $errors->first('password') }}</span>
          @endif
        </div>
        <div class="form-group">
          <label for="password-confirm">Confirm Password</label>
          <input type="password" class="form-control" id="confirm-password" name="confirm-password" autocomplete="new-password">
        </div>
        <button type="submit" class="btn btn-primary">Update</button>
      </form>
    </div>
  </div>
</div>
@endsection