@extends('admin.layouts.master')
	
@section('content')
<div class="container">
	
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
			      <a href="{{ route('showAllPages') }}"><button type="button" class="btn btn-primary">Back</button></a>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>	
    	
	@if(Session::has('success'))
		<div class="alert alert-success">
			{{ Session::get('success') }}
            @php
                Session::forget('success');
            @endphp
        </div>
    @endif
	
	<div class="card">
		<div class="card-body">
			<form method="POST" id="page-form" action="{{ route('updatePage', $page['id']) }}">
        @method('PATCH')
			{{ csrf_field() }} 

			  <div class="form-group">
				<label for="page_title">Page Title</label>
				<input type="text" class="form-control" id="page_title" name="page_title" value="{{ $page['page_title'] }}" readonly>
					@if ($errors->has('page_title'))
						<span class="text-danger">{{ $errors->first('page_title') }}</span>
					@endif
			  </div>
			  <div class="form-group">
				<label for="page_data">Page Body</label>
				<textarea class="form-control" id="page_data" name="page_data">{{ $page['page_data'] }}</textarea>
					@if ($errors->has('page_data'))
						<span class="text-danger">{{ $errors->first('page_data') }}</span>
					@endif
			  </div>
			  <button type="submit" class="btn btn-primary">Update</button>
			</form>
		</div>
	</div>
	
</div>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.js"></script>
<script>
  $('#page_data').summernote({
    placeholder: 'Hello stand alone ui',
    tabsize: 2,
    height: 240,
    toolbar: [
      ['style', ['style']],
      ['font', ['bold', 'underline', 'clear']],
      ['color', ['color']],
      ['para', ['ul', 'ol', 'paragraph']],
      ['table', ['table']],
      ['insert', ['link', 'picture', 'video']],
      ['view', ['fullscreen', 'codeview', 'help']]
    ]
  });
</script>
@endsection
