<header class="loginbg">
	<div class="container">
		<div class="logo">
		<a href="{{ url('/') }}"><img src="{{ asset('assets/img/logo.png') }}" alt="{{ config('app.name', 'Creteng') }}" ></a>
 		 <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}"> <span class="navbar-toggler-icon"></span></button>
 		 </div>
	</div>
</header>