@extends('layouts.app')
@section('content')
<style type="text/css">
.fixed-top {
  position: relative;
}
#footer .footer-top { 
 padding: 50px 0 0 0;
 top: 0; 
}
#header.header-scrolled{
  position: fixed;
  top: 0;
  right: 0;
  left: 0;
  z-index: 1030;
}
</style>
<section class="inner-page">
<div class="container">
  <div class='alert-success alert text-center'>
    <p >Thank you for showing interest in publishing your advertisement at Creteng.</p><br/> 
    <p>All advertisements are subject to be approved.</p><br/> 
    <p>Submitted payment will be deducted after approval.</p><br/> 
    <p><a href="{{ route('advertiser.login') }}"> Login </a></p>
  </div>
</div>
</section>
@endsection