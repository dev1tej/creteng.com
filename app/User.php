<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Support\Facades\Hash;
use Laravel\Cashier\Billable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;
    use HasRoles;
    use Billable;
    use HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'requested_to_delete', 'profile_pic', 'about', 'password','blocked', 'lat', 'long'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Get the posts record associated with the user.
     */
    public function post()
    {
        return $this->hasMany('App\Post', 'user_id ', 'id');
    }

    /**
     * Get the comments record associated with the user.
     */
    public function comments()
    {
        return $this->hasMany('App\PostComment', 'user_id ', 'id');
    }

    /**
     * Get the likes record associated with the user.
     */
    public function likes()
    {
        return $this->hasMany('App\PostLike', 'user_id ', 'id');
    }

    /**
     * Get the saved posts record associated with the user.
     */
    public function savePost()
    {
        return $this->hasMany('App\SavePost', 'user_id ', 'id');
    }

    /**
     * Get the friends record associated with the user.
     */
    public function fiends()
    {
        return $this->hasMany('App\UsersFriend', 'user_id ', 'id');
    }
}
