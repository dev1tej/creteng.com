@extends('admin.layouts.master')

@section('content')
<div class="container">
@if(Session::has('flash_message'))
    <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {!! session('flash_message') !!}</em></div>
@endif
	
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>All Users</h1>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
	
	@if($message = Session::get('success'))
		<div class="alert alert-success">
			<p>{{ $message }}</p>
		</div>
	@endif
	
    <section class="content">
      <div class="row">
        <div class="col-12">
			
			
          <div class="card">
            <div class="card-header">
              <h3 class="card-title"></h3>
				<a class="btn btn-success" href="{{ route('createUser') }}"> Create New User</a>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="userdata" class="table table-bordered table-striped">
                <thead>
                <tr>
				          <th>Id</th>
                  <th>User Name</th>
                  <th>Email</th>
                  <th>Roles</th>
                  <th>Blocked</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
			@php $srno = '1'; @endphp
			@forelse($users as $user)
                <tr>
                  <td>@php echo $srno; @endphp</td>
                  <td>{{ $user->name }}</td>
                  <td>{{ $user->email }}</td>
                  <td>
					  <!-- option 1 to get user role names
					  @if(!empty($user->getRoleNames()))
						@foreach($user->getRoleNames() as $v)
						   <label class="badge badge-info">{{ $v }}</label>
						@endforeach
					  @endif  
					  -->
					  @php
						$admin=env("APP_ADMIN");
						$customers=env("APP_CUSTOMER");
            $managers=env("APP_MANAGER");
					  @endphp 
					  @foreach($user->roles->pluck('name') as $v)
						@if($v == $customers)
						<label class="badge badge-warning">{{ $v }}</label>
						@endif
						@if($v == $admin)
						<label class="badge badge-success">{{ $v }}</label>
						@endif
            @if($v == $managers) 
						<label class="badge badge-success">{{ $v }}</label>
						@endif
					  @endforeach 
					  <!-- or option 2 to get user role names  {{ $user->roles->pluck('name') }}  -->        
                  </td>
				  <td>		
					<center>
						<div class="custom-control custom-switch">
							<input type="checkbox" class="custom-control-input" id="{{ $user->id }}" data-id="{{ $user->blocked }}" {{ $user->blocked ? 'checked' : '' }}>
							<label class="custom-control-label" for="{{ $user->id }}">&nbsp;</label>
						</div>
					</center>
				  </td>
                  <td><center>
					  <!--<a href="{{ route('userShow', $user->id )}}"><button type="button" class="btn btn-primary">View</button></a>-->
					  <a href="{{ route('userEdit', $user->id )}}"><button type="button" class="btn btn-dark">Edit</button></a>
					  <!--<a href="{{ route('userDelete', $user->id )}}"><button type="button" class="btn btn-danger">Delete</button></a>-->
					  <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#exampleModal{{ $user->id }}" data-id="{{ $user->id }}" >Delete</button>
                  </center></td>
                </tr>
                
				<!-- Delete User Modal Starts -->
				<div class="modal fade" id="exampleModal{{ $user->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				  <div class="modal-dialog modal-dialog-centered" role="document">
					<div class="modal-content">
					  <div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Warning</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						  <span aria-hidden="true">&times;</span>
						</button>
					  </div>
					  <div class="modal-body">
						If you want to delete this user , all information will be lost of this user
					  </div>
					  <div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
						<a href="{{ route('userDelete', $user->id )}}"><button type="button" class="btn btn-danger">Yes, go forward</button></a>
					  </div>
					</div>
				  </div>
				</div>
                <!-- Delete User Modal Ends -->
                
            @php $srno++; @endphp
            @empty
				No User Found
			@endforelse
                </tbody>
                <tfoot>
                <tr>
				          <th>Id</th>
                  <th>User Name</th>
                  <th>Email</th>
                  <th>Roles</th>
                  <th>Blocked</th>
                  <th>Action</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->

		</div>
	  </div>
	</section>
			
</div>


<script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script>
  $(function () {
    $('#userdata').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
    
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    
    $('body').on("click.bs.toggle", ".custom-control-input", function(b) {
		var status = $(this).is(":checked") == true ? 1 : 0;
		var cat_id = $(this).attr('id');		
		$.ajax({
            type: "POST",
            url: '{{ route("userChangeStatus") }}',
            data: {'status': status, 'user_id': cat_id},
            success: function(data){
				$('.alert-success').show();
				$('.alert-success').html(data.success);
            }
        });
    }); 
    
  });
</script>

@endsection
