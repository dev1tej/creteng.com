@extends('front.layouts.master')
@section('content')
<div class="container">
  
  @if (count($errors) > 0)
  <div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
      @foreach ($errors->all() as $error)
      <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
  @endif
  <div id="error"></div>
  <section class="content">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-body">
            <div class="col-md-12">
              @if(($data['ad_type'] == 'image') || $data['ad_type'] == 'Image')
              <img src="{{ $data['advertisement'] }}" alt="{{ $data['advertisement'] }}" class="card-img" style="width:50%;height:50%;"/>
              @elseif(($data['ad_type'] == 'video') || ($data['ad_type'] == 'Video'))
              <video class="img-fluid" controls>
                <source src="{{ $data['advertisement'] }}" alt="{{ $data['advertisement'] }}"/>
              </video>
              @endif
              <p class="card-text">{{ $data['description'] }}</p>
              @if(!is_null($available_service['plan_opt_service']))
              <span class="count-icon">
                <i class="fa fa-thumbs-up" title="Total Likes" style="font-size:30px;color:DodgerBlue"></i>
                <span class="count">{{ $data['likes']['likes_count'] }}</span>
              </span>
                @if($data['views']['views_count'] > 0)
                <span class="count-icon" style="margin-left: 20px">
                  <i class="fas fa-eye" title="Total Views" style="font-size:30px;color:DodgerBlue"></i>
                  <span class="count">{{ $data['views']['views_count'] }}</span>
                </span>
                @endif
              @endif
            </div>
            <hr/>
            <div class="card">
              <div class="card-body">
                <strong>Website URL:</strong>
                <p>{{ $data['website_url'] }}</p>
                <hr/>
                <strong>Is App ?:</strong>
                @if($data['is_app'] == 1)
                True
                @elseif($data['is_app'] == 0)
                False
                @endif
                <hr/>
                <strong>Google Store URL:</strong>
                <p>{{ $data['google_store_url'] }}</p>
                <hr/>
                <strong>Apple Store URL:</strong>
                <p>{{ $data['apple_store_url'] }}</p>
                <hr/>
                <strong>Status:</strong>
                <p>{{ $data['status'] }}</p>
                <hr/>
                <strong>Paid:</strong>
                @if($data['paid'] == 1)
                True
                @elseif($data['paid'] == 0)
                False
                @endif
                <hr/>
                <strong>Published:</strong>
                @if($data['published'] == 1)
                True
                @elseif($data['published'] == 0)
                False
                @endif
                <hr/>
                <strong>Publish Date:</strong>
                <p>{{ $data['publish_date'] }}</p>
                <hr/>
                <strong>Expiry Date:</strong>
                <p>{{ $data['expiry_date'] }}</p>
                <hr/>
                
              </div>
            </div>
            @if(!is_null($available_service['plan_opt_service']))
            <div class="card">
              <div class="card-header">
                <strong>Comments:</strong>
              </div>
              <div class="card-body">
                <div class="col-12">
        
                  @foreach($data['comments'] as $comment_detail)
                  <div class="row comments comment_remove">
                    <div class="col-3">
                      <img src="{{ $comment_detail['comment']['user']['profile_pic'] }}" class="img-circle" alt="User Image" style="height:40px;width:40%"><br>
                      <a href="">{{ $comment_detail['comment']['user']['name'] }}</a>
                    </div>
                    <div class="col-9">
                      <p class="card-text">{{ $comment_detail['comment']['comments'] }}
                        <button type="button" class="close" aria-label="Close" id="{{ $comment_detail['comment']['id'] }}" title="Delete Comment">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </p>
                      <p class="card-text"><small class="text-muted">Last Updated {{ $comment_detail['comment']['updated_at'] }}</small></p>
                      <hr/>
                      @if(isset($comment_detail['comment']['comment_reply']))
                      @foreach($comment_detail['comment']['comment_reply'] as $comment_reply)
                      <div class="row comment_remove">
                        <div class="col-3">
                          <img src="{{ $comment_reply['user']['profile_pic'] }}" class="img-circle" alt="User Image" style="height:40px;width:40%"><br>
                          <a href="">{{ $comment_reply['user']['name'] }}</a>
                        </div>
                        <div class="col-9">
                          <p class="card-text">{{ $comment_reply['comments'] }}
                            <button type="button" class="close" aria-label="Close" id="{{ $comment_reply['id'] }}" title="Delete Comment">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </p>
                          <p class="card-text"><small class="text-muted">Last Updated {{ $comment_reply['updated_at'] }}</small></p>
                        </div>
                      </div>
                      <hr/>
                      @endforeach
                      @endif
                    </div>
                  </div>
                  <hr/>
                  @endforeach
                </div>
              </div>
            </div>
            @endif
          </div>
        </div>
      </div>
    </div>        
  </section>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<script>
  $( document ).ready(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    
    $(".comments").on("click", ".close", function(b) {
      var comment_id = $(this).attr('id');		
      $.ajax({
            type: "POST",
            url: '{{ route("advertiser.deleteAdvertisementComment") }}',
            data: {'comment_id': comment_id},
            success: function(data){
              if(data == 'ok') {
                $("#"+comment_id).closest(".comment_remove").remove();
                $("#"+comment_id).closest(".comment_remove").next('hr').remove();
                $('#error').html('<div class="alert alert-danger">Comment deteted Successfully</div>');
              }
            }
          }); 
    });
  });
</script>
@endsection