<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SaveReplyToChallenge extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 
        'user_id', 'reply_to_challenge_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;
    
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        
    ];

    /**
     * Get the user that owns the saved post.
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id ', 'id');
    }

    /**
     * Get the post that owns the saved post.
     */
    public function reply_to_challenge()
    {
        return $this->belongsTo('App\PostsTaggedReply', 'reply_to_challenge_id', 'id');
    }
}
