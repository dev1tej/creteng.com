<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SavePost extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 
        'user_id', 'post_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        
    ];

    /**
     * Get the user record associated with the SavePost.
     */
    public function user()
    {
        return $this->hasOne('App\User');
    }

    /**
     * Get the post record associated with the SavePost.
     */
    public function post()
    {
        return $this->hasOne('App\Post');
    }
}
