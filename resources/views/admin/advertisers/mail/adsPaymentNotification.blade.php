<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ config('app.name', 'Creteng') }}</title>
  </head>
  <body>
    <div>
       <p>Hello <b>{{  $details['advertiser']['name'] }}</b></p>
       <p>Your advertisement is not yet published . Pls proceed for the payment to make it publish.</p>
    </div>
    <div>
       <p><b>Payment Detail:</b><br/>
          <b>Name:</b> {{  $details['advertiser']['name'] }} <br/>
          <b>Ad Id:</b> {{  $details['advertisement']['id'] }} <br/>
          <b>Ad:</b> <a href="{{  $details['advertisement']['advertisement'] }}">{{  $details['advertisement']['advertisement'] }}</a> <br/>
          <b>Amount:</b> {{  $details['amount'] }} USD<br/>
          <b>Payment Mode:</b> {{  $details['payment_mode'] }} <br/>
          <a class="btn btn-primary" href="{{ route('stripe', [$details['advertisement']['id'], $details['payment_mode'], $details['amount']]) }}">Click here to Pay</a>
       </p>
    </div>
  </body>
</html>