<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChallengeReplyLikesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('challenge_reply_likes', function (Blueprint $table) {
            $table->id();
            $table->foreignId('challenge_reply_id');
            $table->foreignId('user_id');
            $table->boolean('status');
            $table->timestamps();

            $table->foreign('challenge_reply_id')
                ->references('id')
                ->on('posts_tagged_replies')
                ->onDelete('cascade');
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('challenge_reply_likes');
    }
}
