  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index.html" class="brand-link">
      @php 
      $superadmin=env("APP_SUPERADMIN"); 
      $managers=env("APP_MANAGER");
      @endphp
      @if(Auth::user()->hasRole($superadmin))
      <span class="brand-text font-weight-light">Administrator</span>
      @endif
      @if(Auth::user()->hasRole($managers))
      <span class="brand-text font-weight-light">Manager</span>
      @endif
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          
          @if(Auth::user()->hasRole($superadmin))
          <li class="nav-item has-treeview menu-close">
            <a href="#" class="nav-link active">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Super Admin
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ route('users.index') }}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Manage Users</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('roles.index') }}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Manage Roles</p>
                </a>
              </li>
            </ul>
          </li>
          @endif
          <li class="nav-item has-treeview menu-open">
            <a href="#" class="nav-link active">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              @if(Auth::user()->hasRole($superadmin))
              <p>
                Admin Dashboard
                <i class="right fas fa-angle-left"></i>
              </p>
              @endif
              @if(Auth::user()->hasRole($managers))
              <p>
                Manager Dashboard
                <i class="right fas fa-angle-left"></i>
              </p>
              @endif
            </a>
            <ul class="nav nav-treeview">
              
              @php $user_id = Auth::user()->id; @endphp
              @if(isset($user_id))
              <li class="nav-item">
                <a href="{{ route('adminPasswordEdit', $user_id) }}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Change Password</p>
                </a>
              </li>
              @endif
            </ul>
          </li>

          
		</ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
