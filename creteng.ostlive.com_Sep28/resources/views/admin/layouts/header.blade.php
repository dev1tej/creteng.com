  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="{{ route('dashboard')}}" class="nav-link">Dashboard</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="{{ url('/') }}" class="nav-link">Frontend</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a class="nav-link" href="{{ route('logout') }}"
			onclick="event.preventDefault();
					 document.getElementById('logout-form').submit();">
			Logout
		</a>

		<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
			{{ csrf_field() }}
		</form>
      </li>

    </ul>

	<ul class="navbar-nav ml-auto">
		<li class="nav-item d-none d-sm-inline-block">
		  <div class="user-panel d-flex">
			<div class="image">
			  <img src="{{ asset('image/user2-160x160.jpg') }}" class="img-circle" alt="User Image">
			</div>
			<div class="info">
        <a href="{{ route('userEdit', Auth::user()->id )}}" class="d-block">{{ Auth::user()->name }}</a>
			</div>
        </li>
	</ul>

  </nav>
