@extends('admin.layouts.master')
@section('content')
<div class="container">
  <div class="row">
    <div class="col-lg-12 margin-tb">
      <div class="pull-left">
        <h2>Create New Advertisement</h2>
      </div>
      <div class="pull-right">
        <a class="btn btn-primary" href="{{ route('showAllAdvertisement') }}"> Back</a>
      </div>
    </div>
  </div>
  @if (count($errors) > 0)
  <div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
      @foreach ($errors->all() as $error)
      <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
  @endif
  {!! Form::open(array('route' => 'addAdvertisement', 'method' => 'POST', 'files' => true)) !!}
  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
      <div class="form-group">
        <strong>Advertisement:</strong>
        {!! Form::file('advertisement') !!}
      </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
      <div class="form-group">
        <strong>Advertiser:</strong>
        <select name="advertiser_name_and_id", placeholder = 'Name(Id)', class = 'form-control'>
          <option value="" disabled selected>Name(Id)</option>
          @foreach ($advertiser_name_and_id as $name_and_id) 
              <option value="{{ $name_and_id }}">{{ $name_and_id }}</option>
          @endforeach
        </select>
      </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
      <div class="form-group">
        <strong>Order:</strong>
        {!! Form::number('order', null, array('placeholder' => 'Advertisement Preference Order','class' => 'form-control')) !!}
      </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
      <div class="form-group">
        <strong>Publishable Location:</strong>
        {!! Form::text('publishable_location', null, array('placeholder' => 'Publishable Location','class' => 'form-control')) !!}
      </div>
    </div>
    
    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
      <button type="submit" class="btn btn-primary">Submit</button>
    </div>
  </div>
  {!! Form::close() !!}
</div>
@endsection