@extends('admin.layouts.master')
@section('content')

<div class="container">
  <div class="row">
    <div class="col-lg-12 margin-tb">
      <div class="pull-left">
        <h2>Edit New Advertiser</h2>
      </div>
      <div class="pull-right">
        <a class="btn btn-primary" href="{{ route('showAllAdvertiser') }}"> Back</a>
      </div>
    </div>
  </div>
  @if (count($errors) > 0)
  <div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
      @foreach ($errors->all() as $error)
      <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
  @endif
  <div class="card">
    <div class="card-body">
      <form method="POST" id="user-form" action="{{ route('updateAdvertiser', $user->id) }}">
        {{ csrf_field() }} 
        {{ method_field('PATCH') }}
        <div class="form-group">
          <label for="name">Contact Name</label>
          <input type="text" class="form-control" id="name" name="name" value="{{ $user->name }}" required autocomplete="name" autofocus>
          @if ($errors->has('name'))
          <span class="text-danger">{{ $errors->first('name') }}</span>
          @endif
        </div>
        <div class="form-group">
          <label for="email">Email</label>
          <input type="email" class="form-control" id="email" name="email" value="{{ $user->email }}" required autocomplete="email">
          @if ($errors->has('email'))
          <span class="text-danger">{{ $errors->first('email') }}</span>
          @endif
        </div>
        <div class="form-group">
          <label for="contact">Contact Phone</label>
          <input type="contact" class="form-control" id="contact" name="contact" value="{{ $user->contact }}" required autocomplete="contact">
          @if ($errors->has('contact'))
          <span class="text-danger">{{ $errors->first('contact') }}</span>
          @endif
        </div>
        <div class="form-group">
          <label for="company_name">Company Name</label>
          <input type="text" class="form-control" id="company_name" name="company_name" value="{{ $user->company_name }}" required autocomplete="company_name" autofocus>
          @if ($errors->has('company_name'))
          <span class="text-danger">{{ $errors->first('company_name') }}</span>
          @endif
        </div>
        <div class="form-group">
          <label for="password">New Password</label>
          <input type="password" class="form-control" id="password" name="password" autocomplete="new-password">
          @if ($errors->has('password'))
          <span class="text-danger">{{ $errors->first('password') }}</span>
          @endif
        </div>
        <div class="form-group">
          <label for="password-confirm">Confirm New Password</label>
          <input type="password" class="form-control" id="confirm-password" name="confirm-password" autocomplete="new-password">
          @if ($errors->has('confirm-password'))
          <span class="text-danger">{{ $errors->first('confirm-password') }}</span>
          @endif
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
          <div class="card">
            <h4><strong>Address:</strong></h4>
            <div class="form-group">
            <div class="col-xs-12 col-sm-12 col-md-12">    
              <label for="Address Line 1">Address Line 1</label>        
              <input type="text" id="address_line_1" name="address_line_1" value="{{ $user->address_line_1 }}" class="form-control" required autocomplete="address_line_1">
              @if ($errors->has('address_line_1'))
              <span class="text-danger">{{ $errors->first('address_line_1') }}</span>
              @endif
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12"> 
              <label for="Address Line 2">Address Line 2</label>             
              <input type="text" name="address_line_2" value="{{ $user->address_line_2 }}" class="form-control" autocomplete="address_line_2">   
              @if ($errors->has('address_line_2'))
              <span class="text-danger">{{ $errors->first('address_line_2') }}</span>
              @endif         
            </div>  
          </div>
            
          <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="row">
              <div class="col-xs-12 col-sm-6 col-md-6">
                <div class="form-group">
                  <label for="city">City:</label>  
                  <input type="text" id="city" name="city" value="{{ $user->city }}" class="form-control" required autocomplete="city">
                  @if ($errors->has('city'))
                  <span class="text-danger">{{ $errors->first('city') }}</span>
                  @endif
                </div>
              </div>
              <div class="col-xs-12 col-sm-6 col-md-6">
                <div class="form-group">
                  <label for="state">State:</label>
                  <input type="text" id="state" name="state" value="{{ $user->state }}" class="form-control" required autocomplete="state">
                  @if ($errors->has('state'))
                  <span class="text-danger">{{ $errors->first('state') }}</span>
                  @endif
                </div>
              </div>
            </div>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="row">
              <div class="col-xs-12 col-sm-6 col-md-6">
                <div class="form-group">
                  <label for="Postal Code">Postal Code:</label>
                  <input type="text" id="postal_code" name="postal_code" value="{{ $user->postal_code }}" class="form-control" required autocomplete="postal_code">
                  @if ($errors->has('postal_code'))
                  <span class="text-danger">{{ $errors->first('postal_code') }}</span>
                  @endif
                </div>
              </div>
              <div class="col-xs-12 col-sm-6 col-md-6">
                <div class="form-group">
                  <label for="country">Country:</label>
                  <select name="country" class="form-control" id="country">
                    @foreach ($countries_with_iso as $country_with_iso)
                    @if($country_with_iso == $user->country)
                      <option selected value="{{ $country_with_iso }}">{{ $country_with_iso }}</option>  
                    @else
                    <option value="{{ $country_with_iso }}">{{ $country_with_iso }}</option>
                    @endif
                    @endforeach
                  </select>
                  @if ($errors->has('country'))
                  <span class="text-danger">{{ $errors->first('country') }}</span>
                  @endif
                </div>
              </div>
            </div>
          </div>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
      </form>
    </div>
  </div>
</div>
@endsection