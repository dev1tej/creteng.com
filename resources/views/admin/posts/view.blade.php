@extends('admin.layouts.master')
@section('content')
<div class="container">
  <div class="row">
    <div class="col-lg-12 margin-tb">
      <div class="pull-left">
        <h2>View Post</h2>
      </div>
    </div>
  </div>
  @if (count($errors) > 0)
  <div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
      @foreach ($errors->all() as $error)
      <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
  @endif
  <div id="error"></div>
  <section class="content">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <div class="row">
              <div class="pull-right col-lg-7">
                <div class="pull-right">
                  <a class="btn btn-primary" href="{{ route('showAllPosts') }}"> Back</a>
                </div>
              </div>
            </div>
          </div>
          <div class="card-body">
            <div class="row" style="max-width: 100%;">
              <div class="col-12">
                <div class="card">
                  <div class="card-header">
                    <img src="{{ $post_detail['posted_by']['profile_pic'] }}" class="img-circle" alt="User Image" style="height:15%;width:8%">
                    <a href="{{ route('showSpecificCustomer', [$post_detail['id'], $post_detail['posted_by']['id']]) }}">{{ $post_detail['posted_by']['name'] }}</a>
                  </div>
                  <div class="card-body">
                    <div class="col-md-12">
                      <p class="card-text"><strong><h4>{{ ucfirst($post_detail['post_type']) }} Post</h4></strong></p>
                      @if($post_detail['post_binary_mime_type'] == 'image')
                      <img src="{{ $post_detail['post_binary'] }}" alt="{{ $post_detail['post_binary'] }}" class="card-img" style="width:50%;height:50%;"/>
                      @elseif($post_detail['post_binary_mime_type'] == 'video')
                      <video class="img-fluid" controls>
                        <source src="{{ $post_detail['post_binary'] }}" alt="{{ $post_detail['post_binary'] }}"/>
                      </video>
                      @endif
                      <p class="card-text">{{ $post_detail['post_text'] }}</p>
                      <span class="count-icon">
                        <i class="fa fa-thumbs-up" title="Total Likes" style="font-size:30px;color:DodgerBlue"></i>
                        <span class="count">{{ $post_detail['post_likes']['total_likes'] }}</span>
                      </span>
                      <span class="count-icon" style="margin-left: 20px">
                        <i class="fa fa-1x fa-comment" title="Total Comments" style="font-size:30px;color:DodgerBlue"></i>
                        <span class="count">{{ $post_detail['post_comments']['total_comments'] }}</span>
                      </span>
                      <span class="count-icon" style="margin-left: 20px">
                        <i class="fa fa-save" title="Total Saved" style="font-size:30px;color:DodgerBlue"></i>
                        <span class="count">{{ $post_detail['post_saved']['total_saved'] }}</span>
                      </span>
                      <span class="count-icon" style="margin-left: 20px">
                        <i class="fas fa-user-tag" title="Total Tagged Users" style="font-size:30px;color:DodgerBlue"></i>
                        <span class="count">{{ $post_detail['tagged_users']['tagged_users_count'] }}</span>
                      </span>
                      @if($post_detail['post_binary_mime_type'] == 'video')
                      <span class="count-icon" style="margin-left: 20px">
                        <i class="fas fa-eye" title="Total Video Views" style="font-size:30px;color:DodgerBlue"></i>
                        <span class="count">{{ $post_detail['post_saved']['total_saved'] }}</span>
                      </span>
                      @endif
                      <p class="card-text"><small class="text-muted">Last updated {{ $post_detail['updated_at'] }}</small></p>
                    </div>
                    <hr/>
                    <div class="card">
                      <div class="card-body">
                        <strong>Tagged User:</strong>
                        @foreach($post_detail['tagged_users']['users'] as $tagged_users)
                        <a href="{{ route('showSpecificCustomer', [$post_detail['id'], $tagged_users['id']]) }}">{{ $tagged_users['name'] }}</a>
                        @if($post_detail['tagged_users']['tagged_users_count'] > 1)
                        , 
                        @endif
                        @endforeach
                        <hr/>
                        <strong>Liked By:</strong>
                        @foreach($post_detail['post_likes']['liked_by'] as $liked_by)
                        <a href="{{ route('showSpecificCustomer', [$post_detail['id'], $liked_by['id']]) }}">{{ $liked_by['name'] }}</a>
                        @if($post_detail['post_likes']['total_likes'] > 1)
                        , 
                        @endif
                        @endforeach
                        <hr/>
                        <strong>Saved By:</strong>
                        @foreach($post_detail['post_saved']['saved_by'] as $saved_by)
                        <a href="{{ route('showSpecificCustomer', [$post_detail['id'], $saved_by['id']]) }}">{{ $saved_by['name'] }}</a>
                        @if($post_detail['post_saved']['total_saved'] > 1)
                        , 
                        @endif
                        @endforeach
                        <hr/>
                        
                        @if($post_detail['post_binary_mime_type'] == 'video')
                        <strong>Video View By:</strong>
                        @foreach($post_detail['post_video_views']['viewed_by'] as $view_by)
                        <a href="{{ route('showSpecificCustomer', [$post_detail['id'], $view_by['id']]) }}">{{ $view_by['name'] }}</a>
                        @if($post_detail['post_video_views']['total_video_views'] > 1)
                        , 
                        @endif
                        @endforeach
                        <hr/>
                        @endif
                      </div>
                    </div>
                    <div class="card">
                      <div class="card-header">
                        <strong>Reports:</strong>
                      </div>
                      <div class="card-body">
                        <div class="col-12">
                          @if(isset($post_detail['reports']))
                          @foreach($post_detail['reports'] as $report)
                          <div class="row">
                            <div class="col-3">
                              <img src="{{ $report['user']['profile_pic'] }}" class="img-circle" alt="User Image" style="height:40px;width:40%"><br>
                              <a href="{{ route('showSpecificCustomer', [$post_detail['id'], $report['user']['id']]) }}">{{ $report['user']['name'] }}</a>
                            </div>
                            <div class="col-9">
                              <p class="card-text">
                                {{ $report['report_text'] }}
                              </p>
                              <p class="card-text"><small class="text-muted">Last Updated {{ $report['updated_at'] }}</small></p>
                              <hr/>
                            </div>
                          </div>
                          <hr/>
                          @endforeach
                          @endif
                        </div>
                      </div>
                    </div>
                    <div class="card">
                      <div class="card-header">
                        <strong>Comments:</strong>
                      </div>
                      <div class="card-body">
                        <div class="col-12">
                          @if(isset($post_detail['post_comments']['comment_detail']))
                          @foreach($post_detail['post_comments']['comment_detail'] as $comment_detail)
                          <div class="row comment comment-remove">
                            <div class="col-3">
                              <img src="{{ $comment_detail['comment_by']['profile_pic'] }}" class="img-circle" alt="User Image" style="height:40px;width:40%"><br>
                              <a href="{{ route('showSpecificCustomer', [$post_detail['id'], $comment_detail['comment_by']['id']]) }}">{{ $comment_detail['comment_by']['name'] }}</a>
                            </div>
                            <div class="col-9">
                              <p class="card-text">
                                {{ $comment_detail['comment']['comments'] }}
                                <button type="button" class="close close-comment" aria-label="Close" id="{{ $comment_detail['comment']['id'] }}" title="Delete Comment">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                              </p>
                              <p class="card-text"><small class="text-muted">Last Updated {{ $comment_detail['comment']['updated_at'] }}</small></p>
                              <hr/>
                              @if(isset($comment_detail['comment_reply']))
                              @foreach($comment_detail['comment_reply'] as $comment_reply)
                              <div class="row comment-reply-remove">
                                <div class="col-3">
                                  <img src="{{ $comment_reply['comment_reply_by']['profile_pic'] }}" class="img-circle" alt="User Image" style="height:40px;width:40%"><br>
                                  <a href="{{ route('showSpecificCustomer', [$post_detail['id'], $comment_reply['comment_reply_by']['id']]) }}">{{ $comment_reply['comment_reply_by']['name'] }}</a>
                                </div>
                                <div class="col-9">
                                  <p class="card-text">
                                    {{ $comment_reply['comment_reply']['comment_reply'] }}
                                    <button type="button" class=" close close-reply" aria-label="Close" id="{{ $comment_reply['comment_reply']['id'] }}" title="Delete Comment Reply">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                  </p>
                                  <p class="card-text"><small class="text-muted">Last Updated {{ $comment_reply['comment_reply']['updated_at'] }}</small></p>
                                </div>
                              </div>
                              <hr/>
                              @endforeach
                              @endif
                            </div>
                          </div>
                          <hr/>
                          @endforeach
                          @endif
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<script>
  $( document ).ready(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    
    $(".comment").on("click", ".close-comment", function(b) {
      var comment_id = $(this).attr('id');		
      $.ajax({
        type: "POST",
        url: '{{ route("deletePostComment") }}',
        data: {'comment_id': comment_id},
        success: function(data){
          if(data == 'ok') {
            $("#"+comment_id).closest(".comment-remove").next('hr').remove();
            $("#"+comment_id).closest(".comment-remove").remove();
            $('#error').html('<div class="alert alert-danger">Comment deteted Successfully</div>');
          }
        }
      }); 
    });

    $(".comment").on("click", ".close-reply", function(b) {
      var comment_reply_id = $(this).attr('id');	
      
      $.ajax({
        type: "POST",
        url: '{{ route("deletePostCommentReply") }}',
        data: {'comment_reply_id': comment_reply_id},
        success: function(data){
          if(data == 'ok') {
            $("#"+comment_reply_id).closest(".comment-reply-remove").next('hr').remove();
            $("#"+comment_reply_id).closest(".comment-reply-remove").remove();
            $('#error').html('<div class="alert alert-danger">Reply on Comment deteted Successfully</div>');
          }
        }
      });
    });
  });
</script>
@endsection