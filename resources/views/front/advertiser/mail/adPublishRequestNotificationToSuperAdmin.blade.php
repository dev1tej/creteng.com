<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ config('app.name', 'Creteng') }}</title>
  </head>
  <body>
    <div>
      <p><b><h5>Hi Sir</h5></b></p>
      <p><b>{{  $details['advertiser']['name'] }}</b> has requested to publish his/her another ad at Creteng.</p>
    </div>
    <div>
      <p><b>Advertisement Detail:</b><br/>
        <b>Advertiser Id:</b> {{  $details['advertiser']['id'] }} <br/>
        <b>Advertiser Name:</b> {{  $details['advertiser']['name'] }} <br/>
        <b>Advertiser Email:</b> {{  $details['advertiser']['email'] }} <br/>
        <b>Advertiser Contact:</b> {{  $details['advertiser']['contact'] }} <br/>
        <b>Ad Id:</b> {{  $details['advertisement']['id'] }} <br/>
        <b>Ad Type:</b> {{  $details['advertisement']['ad_type'] }} <br/>
        <b>Ad:</b> <a href="{{  $details['advertisement']['advertisement'] }}">{{  $details['advertisement']['advertisement'] }}</a> <br/>
        <b>Ad Status:</b> {{  $details['advertisement']['status'] }}<br/>
        <b>Ad Paid Status:</b>
        @if($details['advertisement']['paid'] == true)
        Paid
        @elseif($details['advertisement']['paid'] == false)
        Unpaid
        @endif
        <br/>
        <b>Ad Published Status:</b>
        @if($details['advertisement']['published'] == true)
        Publish
        @elseif($details['advertisement']['published'] == false)
        Unpublish
        @endif 
        <br/>
        <b>Ad Archive Status:</b>
        @if($details['advertisement']['archive'] == true)
        Archive
        @elseif($details['advertisement']['archive'] == false)
        Unarchive
        @endif 
        <br/>
        <b>Ad Publish Date:</b> {{  $details['advertisement']['publish_date'] }}<br/>
        <b>Ad Expiry Date:</b> {{  $details['advertisement']['expiry_date'] }}<br/>
     </p>
    </div>
  </body>
</html>