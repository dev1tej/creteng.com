@extends('admin.layouts.master')
@section('content')
<div class="container">
  <div class="row">
    <div class="col-lg-12 margin-tb">
      <div class="pull-left">
        <h2>Payment History</h2>
      </div>
      <div class="pull-right">
        <a class="btn btn-primary" href="{{ route('showAllAdvertiser') }}"> Back</a>
      </div>
    </div>
  </div>
  @if ($message = Session::get('success'))
  <div class="alert alert-success">
    <p>{{ $message }}</p>
  </div>
  @endif
  @if ($message = Session::get('danger'))
  <div class="alert alert-danger">
    <p>{{ $message }}</p>
  </div>
  @endif
  @if (isset($success))
  <div class="alert alert-success">
    <p>{{ $success }}</p>
  </div>
  @endif
  <table class="table table-bordered table-striped table-hover">
    <tr>
      <th>Advertiser</th>
      <th>Transaction ID</th>
      <th>Payment Mode</th>
      <th>Amount (in USD)</th>
      <th>Payment Creation Date</th>
      <th>Plan Publishing Date</th>
      <th>Plan Expiry Date</th>
    </tr>
    @forelse ($data as $key => $payment)
    <tr>
      <td>{{ $payment->advertiser->name }}</td>
      <td>{{ $payment->transaction_id }}</td>
      <td>{{ $payment->payment_mode }}</td>
      <td>{{ $payment->amount }}</td>
      @php
        $created_at = date('Y-m-d', strtotime($payment->created_at));
      @endphp
      <td>{{ $created_at }}</td>
      <td>{{ $payment->publish_date }}</td>
      <td>{{ $payment->expiry_date }}</td>
    </tr>
    @empty
    No Payment History Found
    @endforelse
  </table>
</div>
@endsection
