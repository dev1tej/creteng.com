<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdvPlDuration extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 
        'plan_id', 'plan_duration', 'price'
    ];

    /**
     * Get the plan.
     */
    public function plan()
    {
        return $this->belongsTo('App\AdvPlan', 'plan_id', 'id');
    }
}
