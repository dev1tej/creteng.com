@extends('admin.layouts.master')
@section('content')
<div class="container">
  <div class="row">
    <div class="col-lg-12 margin-tb">
      <div class="pull-left">
        <h2>Customers Report</h2>
      </div>
    </div>
  </div>
  @if ($message = Session::get('success'))
  <div class="alert alert-success">
    <p>{{ $message }}</p>
  </div>
  @endif
  @if ($message = Session::get('danger'))
  <div class="alert alert-danger">
    <p>{{ $message }}</p>
  </div>
  @endif
  @if (isset($success))
  <div class="alert alert-success">
    <p>{{ $success }}</p>
  </div>
  @endif
  <section class="content">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title"></h3>
            <div class = "container">
              <form action="{{ route('customerGenerateCsvReport') }}" method="post"  name = "csvGenerator" onsubmit = "return(validate());">
                @csrf
                From Date : <input type = "date" name = "from_date" id="from_date" />
                To Date   : <input type = "date" name = "to_date" id="to_date" />
                <button class="btn btn-info" type="submit" >Generate CSV</button>
              </form>
            </div>
          </div>
          <div class="card-body">
            <table id="customersReportData" class="table table-bordered table-striped table-hover">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Blocked</th>
                  <th>Account Creation Date</th>
                </tr>
              </thead>
              <tbody>
                @php $srno = '1'; @endphp
                @forelse ($data as $key => $user)
                <tr>
                  <td>@php echo $srno; @endphp</td>
                  <td>{{ $user->name }}</td>
                  <td>{{ $user->email }}</td>
                  <td>
                    @if($user->blocked == 1)
                    True
                    @elseif($user->blocked == 0)
                    False
                    @endif	
                  </td>
                  <td>
                    @php
                    $created_date = date("m-d-Y", strtotime($user->created_at));
                    @endphp
                    {{ $created_date }}
                  </td>
                </tr>
                @php $srno++; @endphp
                @empty
                No Customer Found
                @endforelse
              </tbody>
              <tfoot>
                <tr>
                  <th>No</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Blocked</th>
                  <th>Account Creation Date</th>
                </tr>
              </tfoot>
            </table>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
<script type = "text/javascript">
	function validate() {
	  if( document.csvGenerator.from_date.value == "" ) {
		 alert( "Please provide From Date!" );
		 document.csvGenerator.from_date.focus() ;
		 return false;
	  }
	  if( document.csvGenerator.to_date.value == "" ) {
		 alert( "Please provide To Date!" );
		 document.csvGenerator.to_date.focus() ;
		 return false;
	  }
	  return( true );
   }
</script>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script>
  $(function () {
    $('#customersReportData').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>
@endsection
