<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\UsersFriend;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //   
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showUserProfile($id)
    {
        $user = User::find($id);
        $customer=env("APP_CUSTOMER");
        if($user->hasRole($customer) && !is_null($user)) {
            $response = [
                'status' => 'success',
                'message' => 'User searched successfully',
                'User' => $user,
            ];
            return response($response, 200);
       } else {
            $response = [
                'status' => 'failure',
                'message' => 'There is no such user',
            ];
            return response($response, 422);
       }
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function showMyProfile()
    {
        $user = Auth::user();
        if(!is_null($user)) {
            $response = [
                'status' => 'success',
                'message' => 'My profile fetched successfully',
                'My profile' => $user,
            ];
            return response($response, 200);
       } else {
            $response = [
                'status' => 'failure',
                'message' => 'There is no such profile',
            ];
            return response($response, 422);
       }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $user = Auth::user();

        if ($user) {
            $validator = Validator::make($request->all(), [
                'name' => 'required|max:255|regex:/^[\pL\s\-]+$/u',
                //'email' => 'required|string|email|max:255',
                'profile_pic' => 'string|max:255',
                'about' => 'string|max:255',
            ]);

            if ($validator->fails()) {
                return response([
                    'status' => 'failure',
                    'message'=>$validator->errors()->all()
                ], 422);
            }

            if (empty($request['profile_pic'])) { 
                $request['profile_pic'] = null;
            }

            if (empty($request['about'])) { 
                $request['about'] = null;
            }
            
            $user->update($request->toArray());
            
            $response = [
                'status' => 'success',
                'message' => 'You Profile is updated  successfully',
                'user' => $user->only(
                    'id', 'name', 'profile_pic', 'about', 'email', 'updated_at', 'created_at' 
                ),
            ];
            return response($response, 200);
        } else {
            $response = [
                'status' => 'false',
                'message' => 'User does not exists',
            ];
            return response($response, 422);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Change Password of the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function changePassword(Request $request) {
        $validator = Validator::make($request->all(), [
            'old_password' => 'required',
            'new_password' => 'required|min:8',
            'confirm_password' => 'required|same:new_password',
        ]);
        $userid = Auth::guard('api')->user()->id;
        
        if ($validator->fails()) {
            return response([
                'status' => 'failure',
                'message' => $validator->errors()->all()
            ], 422);
        } else {
            
            if ((Hash::check(request('old_password'), Auth::user()->password)) == false) {
                return response([
                    'status' => 'failure',
                    'message' => 'Check your old password'
                ], 422);
            } else {
                User::where('id', $userid)->update(['password' => Hash::make($request['new_password'])]);
                return response([
                    'status' => 'success',
                    'message' => 'Password updated successfully.'
                ], 200);
            }
            
        }
    }

    public function searchUser(Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255|regex:/^[\pL\s\-]+$/u',
        ]);

        if ($validator->fails()) {
            return response([
                'status' => 'failure',
                'message'=>$validator->errors()->all()
            ], 422);
        }
        //$users = User::where('name', $request->name)->get();
        $customer=env("APP_CUSTOMER");
        $users = User::all();
        
        if ($users) {
            $search_user_list = array();
            $i = 0;
            foreach ($users as $user) {
                if ($user->hasRole($customer) && (preg_match("/{$request->name}/i", $user->name))) {
                    $search_user_list[$i]['id'] = $user->id;
                    $search_user_list[$i]['name'] = $user->name;
                    $search_user_list[$i]['email'] = $user->email;
                    $search_user_list[$i]['profile_pic'] = $user->profile_pic;
                    $search_user_list[$i]['about'] = $user->about;
                    $i++;
                }
            }
            if ($search_user_list) {
                $response = [
                    'status' => 'success',
                    'message' => 'Profiles fetched successfully',
                    'searched user list' => $search_user_list,
                ];
                return response($response, 200);
            } else {
                $response = [
                    'status' => 'success',
                    'message' => 'Search name does not exists',
                ];
                return response($response, 200);
            }
        }  else {
            $response = [
                'status' => 'success',
                'message' => 'Search name does not exists',
            ];
            return response($response, 200);
        }
    }

    public function addFriend($id) 
    {
        $input['user_id'] = Auth::user()->id;
        $input['user_friend_id'] = $id;
        $friend_already_exist = UsersFriend::where([
            ['user_id', $input['user_id']], 
            ['user_friend_id', $input['user_friend_id']]
        ])->get();

        if ($friend_already_exist->isEmpty()) {
            $add_friend = UsersFriend::create($input);
            if ($add_friend) {
                $response = [
                    'status' => 'success',
                    'message' => 'Friend added successfully',
                ];
                return response($response, 200);
            }
        } else {
            $response = [
                'status' => 'failure',
                'message' => 'This User is already exists in your friend list',
            ];
            return response($response, 422);
        }
    }

    public function removeFriend($id) 
    {
        $user_id = Auth::user()->id;
        $user_friend_id = $id;

        $removed_friend = UsersFriend::where([
            ['user_id', $user_id], 
            ['user_friend_id', $user_friend_id]
        ])->delete();

        if ($removed_friend) {
            $response = [
                'status' => 'success',
                'message' => 'Friend removed successfully',
            ];
            return response($response, 200);   
        } else {
            $response = [
                'status' => 'failure',
                'message' => 'Please provide a valid friend id',
            ];
            return response($response, 422);
        }
    }

    public function viewFriendsList() 
    {
        $user_id = Auth::user()->id;  
        $friend_list = UsersFriend::where([
            ['user_id', $user_id], 
        ])->get(); 
        if (!($friend_list->isEmpty())) {

            $friend_list_profile = array();
            foreach ($friend_list as $friend) {
                $friend_id = $friend->user_friend_id;
                $friend_list_profile[] = User::where([
                    ['id', $friend_id], 
                ])->get(); 
            }
            $response = [
                'status' => 'success',
                'message' => 'Friend listed successfully',
                'friend list' => $friend_list_profile,
            ];
            return response($response, 200);
        } else {
            $response = [
                'status' => 'success',
                'message' => 'Your Friend list is empty',
            ];
            return response($response, 200);
        } 
    }

}
