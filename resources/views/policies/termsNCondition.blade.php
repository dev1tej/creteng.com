@extends('layouts.app')
@section('content')
<section class="content">
<div class="container">
<div class="row">    
<div class="col-md-12"> 
  @php
    echo $page->page_data;
  @endphp
</section>
<style type="text/css">
h1{margin: 0 0 20px; } 
h2{margin: 0 0 20px; } 
h3{margin: 0 0 20px; } 

#footer {
  padding: 10px;
}
#footer .footer-top {
  padding: 0;
  top: 0;
}
</style>
@endsection
  