<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Advertisement;
use App\Advertiser;
use App\AdsPayment;
use App\AdvLike;
use App\AdvView;
use App\AdvComment;

use App\AdvPlan;
use App\AdvPlDuration;
use App\AdvPlFixedService;
use App\AdvPlOptService;
use App\AdvAvailableService;

class AdvertisementPlanController extends Controller
{
    /**
     * Display a listing of the Advertisement Plans.
     *
     * @return \Illuminate\View\View
     */
    public function showAllAdPlan()
    {
        $data = AdvPlan::orderBy('id','DESC')->get();
        return view('admin.advertisements_plans.index', compact('data'));
    }

    /**
     * Sending to create view.
     *
     * @return \Illuminate\View\View
     */

    public function createAdPlan()
    {
        return view('admin.advertisements_plans.create_plan');
    }

    /**
     * Creating new advertisement plan
     * 
     * @param \Illuminate\Http\Request  $request
     * @return \Illuminate\View\View
     */
    public function addAdPlan(Request $request)
    {
        /* Check Validation */
        $this->validate($request, [
            'plan_name' => 'required|string|max:255',
            'plan_fix_service' => 'required|string|max:255',
        ]);
        
        /* Create Advertisement Plan */
        $ad_plan = AdvPlan::create([
            'plan_name'        => $request->plan_name,
        ]);
        /* Create Advertisement Plan fixed services*/
        $ad_plan_fixed_service = AdvPlFixedService::create([
            'plan_id'          => $ad_plan->id,
            'fixed_services'   => $request->plan_fix_service,
        ]);
        return view('admin.advertisements_plans.create_sub_plan', compact('ad_plan'))->with('success', 'Plan created successfully. Pls create Plan Durations');
    }

    public function addAdSubPlan(Request $request) {
        if(isset($request['duration_name'])) {
            for ($i = 0; $i < count($request['duration_name']) ; $i++) {
                for ($j = 0; $j < count($request['duration_price']) ; $j++) {
                    if ($i == $j) {
                        AdvPlDuration::create([
                            'plan_id'         => $request['plan_id'],
                            'plan_duration'   => $request['duration_name'][$i],
                            'price'           => $request['duration_price'][$j],
                        ]);  
                    } 
                }
            }
            return 'ok';
        } 
    }

    /**
     * sending to edit adv plan view with data
     * 
     * @param int $id
     * @return \Illuminate\View\View
     */
    public function editAdPlan($id)
    {
        $adv_plan = AdvPlan::where([
            ['id', $id],
        ])->with('fixServices', 'planDuration')->first();

        return view('admin.advertisements_plans.edit_plan',compact('adv_plan'));
    }

    /**
     * To update an adv plan
     * 
     * @param \Illuminate\Http\Request  $request
     * @param int $id
     * @return View
     */
    public function updateAdPlan(Request $request, $id)
    {
        /* Check Validation */
        $this->validate($request, [
            'plan_name' => 'required|string|max:255',
            'plan_fix_services' => 'required|array',
        ]);
        
        $ad_old_plan = AdvPlan::find($id);
        $ad_old_plan->update([
            'plan_name' => $request->plan_name,
        ]);
        
        $plan_old_fix_services = AdvPlFixedService::where([
            ['plan_id', $id],
        ])->get();
        foreach ($plan_old_fix_services as $plan_old_fix_service) {
            foreach ($request->plan_fix_services as $plan_fix_service) {
                $plan_old_fix_service->update([
                    'fixed_services'   => $plan_fix_service,
                ]);
            }
        }
        $ad_plan = AdvPlan::where([
            ['id', $id],
        ])->with('planDuration')->first();
        
        return view('admin.advertisements_plans.edit_sub_plan', compact('ad_plan'))->with('success', 'Plan updated successfully. Continue to update Plan Durations'); 
    }

    /**
     * To update an adv plan
     * 
     * @param \Illuminate\Http\Request  $request
     * @param int $id
     * @return View
     */
    public function updateAdSubPlan(Request $request, $id)
    {
        if(isset($request['duration_name'])) {
            for ($i = 0; $i < count($request['duration_id']) ; $i++) {
                for ($j = 0; $j < count($request['duration_name']) ; $j++) {
                    for ($k = 0; $k < count($request['duration_price']) ; $k++) {
                        if (($i == $j) && ($j == $k) && ($k == $i)) {
                            $plan_duration = AdvPlDuration::where([
                                ['id', $request['duration_id'][$i]],
                                ['plan_id', $id],
                            ])->first();
        
                            $plan_duration->update([
                                'plan_duration' => $request['duration_name'][$j],
                                'price'         => $request['duration_price'][$k],
                            ]);  
                        } 
                    }
                }
            }
            return 'ok';
        }  
    }

    /**
     * To show an adv plan
     * 
     * @param int $id
     * @return \Illuminate\View\View
     */
    public function showAdPlan($id)
    {
        $ad_plan = AdvPlan::where([
            ['id', $id],
        ])->with('fixServices', 'planDuration')->first();

        return view('admin.advertisements_plans.view_plan',compact('ad_plan'));
    }

    /**
     * To delete an Ad Plan
     * 
     * @param int $id
     * @return View
     */
    public function deleteAdPlan($id)
    {
        AdvPlan::find($id)->delete();
        
        return redirect()->route('showAllAdPlan')->with('danger','Ad Plan deleted successfully');

    }

    /**
     * Sending to create view.
     *
     * @return \Illuminate\View\View
     */

    public function createAdPlanOptService()
    {
        return view('admin.advertisements_plans.edit_plan_opt_service');
    }

    /**
     * Creating new advertisement plan optional services
     * 
     * @param \Illuminate\Http\Request  $request
     * @return \Illuminate\View\View
     */
    public function addAdPlanOptService(Request $request)
    {
        /* Check Validation */
        $this->validate($request, [
            'service' => 'required|string|max:255',
            'service_price' => 'required',
            'service_period' => 'required|string|max:255',
        ]);
        
        /* Create Advertisement Plan opt service */
        $ad_plan = AdvPlOptService::create([
            'optional_services'         => $request->service,
            'aditional_price'           => $request->service_price,
            'aditional_price_duration'  => $request->service_period,
        ]);
        
        return redirect()->route('showAllAdPlan')->with('success', 'Plan Optional Service created successfully.');
    }

    /**
     * Display a listing of the Advertisement Plans.
     *
     * @return \Illuminate\View\View
     */
    public function viewAdPlanOptService()
    {
        $data = AdvPlOptService::orderBy('id','DESC')->get();
        return view('admin.advertisements_plans.view_opt_service', compact('data'));
    }

    /**
     * To delete an Ad Plan Optional Service
     * 
     * @param int $id
     * @return View
     */
    public function deleteAdPlanOptService($id)
    {
        AdvPlOptService::find($id)->delete();
        
        return redirect()->route('viewAdPlanOptService')->with('danger','Ad Plan Optional Service deleted successfully');

    }
}
