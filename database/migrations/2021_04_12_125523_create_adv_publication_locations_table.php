<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdvPublicationLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adv_publication_locations', function (Blueprint $table) {
            $table->id();
            $table->foreignId('advertiser');
            $table->string('country', 255);
            $table->string('postal_code', 255)->nullable();
            $table->decimal('lat', 12, 8);
            $table->decimal('long', 12, 8);
            $table->timestamps();

            $table->foreign('advertiser')
                ->references('id')
                ->on('adv_users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('adv_publication_locations');
    }
}
