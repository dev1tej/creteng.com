<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\UsersFriend;
use App\Post;
use App\Notification;
use App\PostsTagged;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Api\CustomPaginationController;
use App\Http\Controllers\Api\GetResourceDetailController;

class UserController extends Controller
{
    /**
     * Display the specified user's profile.
     *
     * @param  int  $id
     * @return Restfull Api Response
     */
    public function viewUserProfile($id)
    {
        try {
            $user = User::find($id);
            $customer=env("APP_CUSTOMER");  
            if(!is_null($user) && $user->hasRole($customer)) {
                $get_resource_detail = new GetResourceDetailController;
                $user = $get_resource_detail->getUserDetail($user);
                $response = [
                    'status' => 'success',
                    'message' => 'User searched successfully',
                    'User' => $user,
                ];
                return response($response, 200);
            } else {
                $response = [
                    'status' => 'failure',
                    'message' => 'There is no such user',
                ];
                return response($response, 422);
            }
        } catch (\Exception $ex) {
            $response = [
                'status' => 'failure',
                'message' => 'Can not view user\'s profile . Unidentified error, please contact at ' . env("APP_SUPERADMIN_EMAIL_ID"),
            ];
            return response($response, 500);
        }
    }

    /**
     * View self profile by logged user.
     *
     * @return Restfull Api Response
     */
    public function viewMyProfile()
    {
        try {
            $user = Auth::user();
            if(!is_null($user)) {
                $get_resource_detail = new GetResourceDetailController;
                $user = $get_resource_detail->getUserDetail($user);
                $response = [
                    'status' => 'success',
                    'message' => 'My profile fetched successfully',
                    'My profile' => $user,
                ];
                return response($response, 200);
            } else {
                    $response = [
                        'status' => 'failure',
                        'message' => 'There is no such profile',
                    ];
                    return response($response, 422);
            }
        } catch (\Exception $ex) {
            $response = [
                'status' => 'failure',
                'message' => 'Can not view my profile. Unidentified error, please contact at ' . env("APP_SUPERADMIN_EMAIL_ID"),
            ];
            return response($response, 500);
        }
    }

    /**
     * Updating self profile by logged user.
     *
     * @param  object  $request
     * @return Restfull Api Response
     */
    public function updateMyProfile(Request $request)
    {
        try {
            $user = Auth::user();
            if ($user) {
                $validator = Validator::make($request->all(), [
                    'name' => 'required|max:255|regex:/^[\pL\s\-]+$/u',
                    'profile_pic' => 'string|max:255',
                    'about' => 'string|nullable|max:255',
                ]);

                if ($validator->fails()) {
                    return response([
                        'status' => 'failure',
                        'message'=>$validator->errors()->all()
                    ], 422);
                }
                $user->update($request->toArray());
                if ($user['profile_pic'] == null) {
                    $user['profile_pic'] = asset('image/c.svg') ;
                }
                $get_resource_detail = new GetResourceDetailController;
                $user = $get_resource_detail->getUserDetail($user);
                $response = [
                    'status' => 'success',
                    'message' => 'You Profile is updated  successfully',
                    'user' => $user,
                ];
                return response($response, 200);
            } else {
                $response = [
                    'status' => 'false',
                    'message' => 'User does not exists',
                ];
                return response($response, 422);
            }
        } catch (\Exception $ex) {
            $response = [
                'status' => 'failure',
                'message' => 'Can not update my profile. Unidentified error, please contact at ' . env("APP_SUPERADMIN_EMAIL_ID"),
            ];
            return response($response, 500);
        }
    } 

    /**
     * To search user's by name caption.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return Restfull Api Response
     */
    public function searchUser(Request $request) 
    {
        try {
            $validator = Validator::make($request->all(), [
                'name' => 'required|max:255|regex:/^[\pL\s\-]+$/u',
            ]);
            if ($validator->fails()) {
                return response([
                    'status' => 'failure',
                    'message'=>$validator->errors()->all()
                ], 422);
            }
            
            $customer=env("APP_CUSTOMER");
            $users = User::all();
            
            if ($users) {
                $search_user_list = array();
                $i = 0;
                foreach ($users as $user) {
                    if ($user->hasRole($customer) && (preg_match("/{$request->name}/i", $user->name))) {
                        $get_resource_detail = new GetResourceDetailController;
                        $search_user_list[$i] = $get_resource_detail->getUserDetail($user);
                        $i++;
                    }
                }
                if ($search_user_list) {
                    $custom_pagination = new CustomPaginationController;
                    $search_user_list = collect($search_user_list);
                    $search_user_list = $custom_pagination->paginate($search_user_list);
                    $response = [
                        'status' => 'success',
                        'message' => 'Profiles fetched successfully',
                        'searched_user_list' => $search_user_list,
                    ];
                    return response($response, 200);
                } else {
                    $response = [
                        'status' => 'success',
                        'message' => 'Search name does not exists',
                    ];
                    return response($response, 200);
                }
            }  else {
                $response = [
                    'status' => 'success',
                    'message' => 'Search name does not exists',
                ];
                return response($response, 200);
            }
        } catch (\Exception $ex) {
            $response = [
                'status' => 'failure',
                'message' => 'Can not search user. Unidentified error, please contact at ' . env("APP_SUPERADMIN_EMAIL_ID"),
            ];
            return response($response, 500);
        }
    }

    /**
     * To send a friend request to a user.
     *
     * @param int $id
     * @return Restfull Api Response
     */
    public function sendFriendRequest($id) 
    {
        try {
            $user = User::find($id);
            $customer=env("APP_CUSTOMER");  
            if(!$user->hasRole($customer)) {
                $response = [
                    'status' => 'failure',
                    'message' => 'Cann\'t send friend request to this user',
                ];
                return response($response, 422);
            }

            $input['user_id'] = Auth::user()->id;
            $input['user_friend_id'] = $id;
            $friend_request_already_sent = UsersFriend::where([
                ['user_id', $input['user_id']], 
                ['user_friend_id', $input['user_friend_id']],
            ])->first();

            $friend_request_already_received = UsersFriend::where([
                ['user_id', $input['user_friend_id']],
                ['user_friend_id', $input['user_id']],
            ])->first();
            if (is_null($friend_request_already_sent) && is_null($friend_request_already_received)) {
                $send_request = UsersFriend::create($input);
                if ($send_request) {

                    //notification
                    $applicant = User::find($input['user_id']);
                    $msg = $applicant->name . ' has sent you a friend request';
                    Notification::create([
                        'applicant_id'     => $input['user_id'],
                        'respondent_id'   => $input['user_friend_id'],
                        'applicant_resource_id' => null,
                        'respondent_resource_id' => null,
                        'motive' => 'friend_request',
                        'notification_type' => 'New',
                        'message' => $msg,
                    ]);
                    $response = [
                        'status' => 'success',
                        'message' => 'Friend request sent successfully',
                    ];
                    return response($response, 200);
                }
            } else {
                if (!is_null($friend_request_already_sent)) {
                    if ($friend_request_already_sent->friend_accepted == 1) {
                        $response = [
                            'status' => 'failure',
                            'message' => 'This User is already exists in your friend list',
                        ];
                        return response($response, 422);
                    }
                    if ($friend_request_already_sent->friend_accepted == 0) {
                        $response = [
                            'status' => 'failure',
                            'message' => 'Friend request have already been sent to this user',
                        ];
                        return response($response, 422);
                    }
                }
                if (!is_null($friend_request_already_received)) {
                    if ($friend_request_already_received->friend_accepted == 1) {
                        $response = [
                            'status' => 'failure',
                            'message' => 'This User is already exists in your friend list',
                        ];
                        return response($response, 422);
                    }
                    
                    if ($friend_request_already_received->friend_accepted == 0) {
                        $response = [
                            'status' => 'failure',
                            'message' => 'Friend request have already been received by this user',
                        ];
                        return response($response, 422);
                    }
                }  
            }
        } catch (\Exception $ex) {
            $response = [
                'status' => 'failure',
                'message' => 'Can not send friend request. Unidentified error, please contact at ' . env("APP_SUPERADMIN_EMAIL_ID"),
            ];
            return response($response, 500);
        }
    }

    /**
     * Add a friend from user's received friend request list.
     *
     * @param int $id
     * @return Restfull Api Response
     */
    public function addFriendRequest($id) 
    {
        try {
            $input['user_id'] = Auth::user()->id;
            $input['user_friend_id'] = $id;
            $friend_request = UsersFriend::where([
                ['user_id', $input['user_friend_id']], 
                ['user_friend_id', $input['user_id']]
            ])->first();

            if (!is_null($friend_request)) {
                if ($friend_request->friend_accepted == 0) {
                    $friend_request->update(['friend_accepted' => 1]);
                    $response = [
                        'status' => 'success',
                        'message' => 'Friend request accepted successfully',
                    ];
                    return response($response, 200);  
                }
                if ($friend_request->friend_accepted == 1) {
                    $response = [
                        'status' => 'failure',
                        'message' => 'User already exists in your friend list',
                    ];
                    return response($response, 422);  
                }  
            } else {
                $response = [
                    'status' => 'failure',
                    'message' => 'No such user exists in your friend request list',
                ];
                return response($response, 422); 
            }
        } catch (\Exception $ex) {
            $response = [
                'status' => 'failure',
                'message' => 'Can not add friend. Unidentified error, please contact at ' . env("APP_SUPERADMIN_EMAIL_ID"),
            ];
            return response($response, 500);
        }
    }

    /**
     * Deny a friend request.
     *
     * @param int $user_id
     * @return Restfull Api Response
     */
    public function denyFriendRequest($user_id) 
    {
        try {
            $auth_id = Auth::user()->id;
            $friend_request_id = $user_id;

            $removed_friend_request = UsersFriend::where([
                ['user_id', $friend_request_id], 
                ['user_friend_id', $auth_id],
                ['friend_accepted', 0]
            ])->delete();

            if ($removed_friend_request) {
                $response = [
                    'status' => 'success',
                    'message' => 'Friend request has denied successfully',
                ];
                return response($response, 200);   
            } else {
                $response = [
                    'status' => 'failure',
                    'message' => 'Please provide a valid friend requested user id',
                ];
                return response($response, 422);
            }
        } catch (\Exception $ex) {
            $response = [
                'status' => 'failure',
                'message' => 'Can not deny a friend request. Unidentified error, please contact at ' . env("APP_SUPERADMIN_EMAIL_ID"),
            ];
            return response($response, 500);
        }
    }

    /**
     * Remove a friend from user's friend list.
     *
     * @param int $id
     * @return Restfull Api Response
     */
    public function removeFriend($id) 
    {
        try {
            $user_id = Auth::user()->id;
            $user_friend_id = $id;

            $removed_friend = UsersFriend::where([
                ['user_id', $user_id], 
                ['user_friend_id', $user_friend_id],
                ['friend_accepted', 1]
            ])->orWhere([
                ['user_id', $user_friend_id],
                ['user_friend_id', $user_id],
                ['friend_accepted', 1]
            ])->delete();

            if ($removed_friend) {
                $response = [
                    'status' => 'success',
                    'message' => 'Friend removed successfully',
                ];
                return response($response, 200);   
            } else {
                $response = [
                    'status' => 'failure',
                    'message' => 'Please provide a valid friend id',
                ];
                return response($response, 422);
            }
        } catch (\Exception $ex) {
            $response = [
                'status' => 'failure',
                'message' => 'Can not remove friend. Unidentified error, please contact at ' . env("APP_SUPERADMIN_EMAIL_ID"),
            ];
            return response($response, 500);
        }
    }

    /**
     * View friend list by user.
     *
     * @return Restfull Api Response
     */
    public function viewFriendsList() 
    {
        try {
            $user_id = Auth::user()->id;
            $get_resource_detail = new GetResourceDetailController;  
            $friend_list_by_sending = UsersFriend::where([
                ['user_id', $user_id],
                ['friend_accepted', 1],
            ])->get(); 
            $friend_list_by_receiving = UsersFriend::where([
                ['user_friend_id', $user_id],
                ['friend_accepted', 1]
            ])->get(); 
            if ((!($friend_list_by_sending->isEmpty())) || (!($friend_list_by_receiving->isEmpty()))) {
                
                $friend_list_profile = array();
                if (!($friend_list_by_sending->isEmpty())) {
                    foreach ($friend_list_by_sending as $friend) {
                        $friend_id = $friend->user_friend_id;
                        $friend_profile = User::where([
                            ['id', $friend_id], 
                        ])->first(); 
                        $friend_list_profile[] = $get_resource_detail->getUserDetail($friend_profile);
                    }
                }
                if (!($friend_list_by_receiving->isEmpty())) {
                    foreach ($friend_list_by_receiving as $friend) {
                        $friend_id = $friend->user_id;
                        $friend_profile = User::where([
                            ['id', $friend_id], 
                        ])->first(); 
                        $friend_list_profile[] = $get_resource_detail->getUserDetail($friend_profile);
                    } 
                }
                
                $custom_pagination = new CustomPaginationController;
                $friend_list_profile = collect($friend_list_profile);
                $friend_list_profile = $custom_pagination->paginate($friend_list_profile, 10);
                $response = [
                    'status' => 'success',
                    'message' => 'Friend listed successfully',
                    'friend_list' => $friend_list_profile,
                ];
                return response($response, 200);
            } else {
                $response = [
                    'status' => 'success',
                    'message' => 'Your Friend list is empty',
                ];
                return response($response, 200);
            } 
        } catch (\Exception $ex) {
            $response = [
                'status' => 'failure',
                'message' => 'Can not show your friend list. Unidentified error, please contact at ' . env("APP_SUPERADMIN_EMAIL_ID"),
            ];
            return response($response, 500);
        }
    }

    /**
     * Display the auth user's received friend request list.
     *
     * @return Restfull Api Response
     */
    public function viewReceivedFriendRequestList() 
    {
        try {
            $auth_id = Auth::user()->id;  
            $received_friend_request_list = UsersFriend::where([
                ['user_friend_id', $auth_id],
                ['friend_accepted', 0],
            ])->orderBy('id', 'DESC')->get(); 
            if (!($received_friend_request_list->isEmpty())) {

                $received_friend_request_list_profile = array();
                foreach ($received_friend_request_list as $received_friend_request) {
                    $friend_request_profile = User::where([
                        ['id', $received_friend_request->user_id], 
                    ])->first(); 
                    $get_resource_detail = new GetResourceDetailController;
                    $received_friend_request_list_profile[] = $get_resource_detail->getUserDetail($friend_request_profile);
                }
                $custom_pagination = new CustomPaginationController;
                $received_friend_request_list_profile = collect($received_friend_request_list_profile);
                $received_friend_request_list_profile = $custom_pagination->paginate($received_friend_request_list_profile, 10);
                $response = [
                    'status' => 'success',
                    'message' => 'Received Friend Requests listed successfully',
                    'received_friend_request_list' => $received_friend_request_list_profile,
                ];
                return response($response, 200);
            } else {
                $response = [
                    'status' => 'success',
                    'message' => 'Your Received Friend Requests list is empty',
                ];
                return response($response, 200);
            } 
        } catch (\Exception $ex) {
            $response = [
                'status' => 'failure',
                'message' => 'Can not show your received friend requests list. Unidentified error, please contact at ' . env("APP_SUPERADMIN_EMAIL_ID"),
            ];
            return response($response, 500);
        }
    }

    /**
     * Display the auth user's send friend request list.
     *
     * @return Restfull Api Response
     */
    public function viewSendFriendRequestList() 
    {
        try {
            $auth_id = Auth::user()->id;  
            $send_friend_request_list = UsersFriend::where([
                ['user_id', $auth_id],
                ['friend_accepted', 0],
            ])->orderBy('id', 'DESC')->get(); 
            if (!($send_friend_request_list->isEmpty())) {

                $send_friend_request_list_profile = array();
                foreach ($send_friend_request_list as $send_friend_request) {
                    $friend_request_profile = User::where([
                        ['id', $send_friend_request->user_friend_id], 
                    ])->first(); 
                    $get_resource_detail = new GetResourceDetailController;
                    $send_friend_request_list_profile[] = $get_resource_detail->getUserDetail($friend_request_profile);
                }
                $custom_pagination = new CustomPaginationController;
                $send_friend_request_list_profile = collect($send_friend_request_list_profile);
                $send_friend_request_list_profile = $custom_pagination->paginate($send_friend_request_list_profile, 10);
                $response = [
                    'status' => 'success',
                    'message' => 'Send Friend Requests listed successfully',
                    'send_friend_request_list' => $send_friend_request_list_profile,
                ];
                return response($response, 200);
            } else {
                $response = [
                    'status' => 'success',
                    'message' => 'Your Send Friend Requests list is empty',
                ];
                return response($response, 200);
            } 
        } catch (\Exception $ex) {
            $response = [
                'status' => 'failure',
                'message' => 'Can not show your send friend requests list. Unidentified error, please contact at ' . env("APP_SUPERADMIN_EMAIL_ID"),
            ];
            return response($response, 500);
        }
    }

    /**
     * Display the specified user's posts.
     *
     * @param  int  $id
     * @return Restfull Api Response
     */
    public function viewUserPost($id)
    {
        try {
            $auth_id = Auth::user()->id;
            $user_id = $id;  
            $user = User::find($user_id);
            if (is_null($user)) {
                $response = [
                    'status' => 'failure',
                    'message' => 'Provide a valid user id',
                ];
                return response($response, 422);
            }
            $posts = array();
            $user_public_posts = Post::where([
                ['user_id', $user_id],
                ['post_type', 'public'], 
            ])->get();
            foreach ($user_public_posts as $user_public_post) {
                $posts[] = $user_public_post;
            }

            //getting user's private post where auth user is tagged
            $user_tagged_posts = PostsTagged::where([
                ['tagged_by', $user_id],
                ['tagged_user', $auth_id],
            ])->get();
            
            if(!is_null($user_tagged_posts)) {
                foreach ($user_tagged_posts as $user_tagged_post) {
                    $tagged_post = Post::where([
                        ['id', $user_tagged_post->post_id],
                        ['post_type', 'private'],
                    ])->first();
                    if (!is_null($tagged_post)) {
                        $posts[] = $tagged_post;
                    }
                }
            }
            //getting other details about posts
            $get_resource_detail = new GetResourceDetailController;
            $posts = $get_resource_detail->getPostDetail($posts);
            //custom pagination
            $custom_pagination = new CustomPaginationController;
            $posts = collect($posts);
            $posts = $custom_pagination->paginate($posts);
        
            $response = [
                'status' => 'success',
                'message' => 'Users Posts found Successfully',
                'users_post' => $posts,
            ];
            return response($response, 200);
            
            /*
            $friend_exists = UsersFriend::where([
                ['user_id', $auth_id], 
                ['user_friend_id', $user_id],
                ['friend_accepted', 1],
            ])->orWhere([
                ['user_friend_id', $auth_id], 
                ['user_id', $user_id],
                ['friend_accepted', 1],
            ])->first();
            
            if (is_null($friend_exists)) {
                $posts = Post::where([
                    ['user_id', $id], 
                    ['post_type', 'public'],
                ])->get();
                //getting other details about posts
                $get_resource_detail = new GetResourceDetailController;
                $posts = $get_resource_detail->getPostDetail($posts);
                //custom pagination
                $custom_pagination = new CustomPaginationController;
                $posts = collect($posts);
                $posts = $custom_pagination->paginate($posts);
            
                $response = [
                    'status' => 'success',
                    'message' => 'Users Posts found Successfully',
                    'users_post' => $posts,
                ];
                return response($response, 200);
            } else {
                $posts = Post::where([
                    ['user_id', $id], 
                ])->get();
                //getting other details about posts
                $get_resource_detail = new GetResourceDetailController;
                $posts = $get_resource_detail->getPostDetail($posts);
                //custom pagination
                $custom_pagination = new CustomPaginationController;
                $posts = collect($posts);
                $posts = $custom_pagination->paginate($posts);
                
                $response = [
                    'status' => 'success',
                    'message' => 'Users Posts found Successfully',
                    'users_post' => $posts,
                ];
                return response($response, 200);
            }
            */
        } catch (\Exception $ex) {
            $response = [
                'status' => 'failure',
                'message' => 'Can not show posts for this user. Unidentified error, please contact at ' . env("APP_SUPERADMIN_EMAIL_ID"),
            ];
            return response($response, 500);
        }
    }

    /**
     * To delete log in user account.
     *
     * @return Restfull Api Response
     */
    public function deleteAccount()
    {
        try {
            $user = Auth::user();
            $user->update(['requested_to_delete' => true]);

            $details = array();
            $details['user'] = $user;
            \Mail::to($user->email)->send(new \App\Mail\DeleteAccountRequestToCustomer($details));
            \Mail::to(env("APP_SUPERADMIN_EMAIL_ID"))->send(new \App\Mail\DeleteAccountRequestToSuperAdmin($details));
            
            $response = [
                'status' => 'success',
                'message' => 'Account deleted request submitted Successfully',
            ];
            return response($response, 200);
        } catch (\Exception $ex) {
            $response = [
                'status' => 'failure',
                'message' => 'Can not send delete account request. Unidentified error, please contact at ' . env("APP_SUPERADMIN_EMAIL_ID"),
            ];
            return response($response, 500);
        }
    }
}
