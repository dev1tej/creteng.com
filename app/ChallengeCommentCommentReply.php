<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChallengeCommentCommentReply extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 
        'user_id', 
        'r_comment_id', 
        'comment_challenge_reply_comment'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        
    ];

    /**
     * Get the user that owns the comment.
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    /**
     * Get the post that owns the comment.
     */
    public function challengeReplyComment()
    {
        return $this->belongsTo('App\ChallengeCommentReply', 'r_comment_id', 'id');
    }
}
