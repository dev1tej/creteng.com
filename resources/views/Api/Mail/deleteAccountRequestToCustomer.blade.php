<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ config('app.name', 'Creteng') }}</title>
  </head>
  <body>
    <p>Hello <b>{{  $details['user']['name'] }}</b></p>
    <p>You have submitted your account deletion request successfully.</p>
  </body>
</html>