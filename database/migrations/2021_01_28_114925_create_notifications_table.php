<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifications', function (Blueprint $table) {
            $table->id();

            $table->foreignId('applicant_id')->comment('sender');
            $table->foreignId('respondent_id')->comment('receiver');
            $table->bigInteger('applicant_resource_id')->nullable();
            $table->bigInteger('respondent_resource_id')->nullable();
            $table->string('motive', 255)->nullable();
            $table->string('message', 255)->nullable();
            $table->timestamps();

            $table->foreign('applicant_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
            $table->foreign('respondent_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notifications');
    }
}
