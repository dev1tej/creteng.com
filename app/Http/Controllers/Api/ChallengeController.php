<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Post;
use App\PostLike;
use App\PostComment;
use App\PostCommentReply;
use App\SavePost;
use App\User;
use App\UsersFriend;
use App\PostsTagged;
use App\PostsTaggedReply;
use App\PostVideoViewed;
use App\ChallengeCommentReply;
use App\ChallengeReplyLike;
use App\SaveReplyToChallenge;
use App\ReportChallengeReply;
use App\ChallengeCommentCommentReply;
use App\ChallengeReplyVideoViewed;
use App\Notification;

class ChallengeController extends Controller
{
    /**
     * To comment on a challenge reply.
     *
     * @param object $request
     * @return Restfull Api Response
     */
    public function commentToChallengeReply(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'challenge_reply_id' => 'required|regex:/^[0-9]{1,11}$/u',
                'challenge_reply_comment' => 'required|string|max:255',
            ]);   

            if ($validator->fails()) {
                return response([
                    'status' => 'failure',
                    'message'=>$validator->errors()->all()
                ], 422);
            }

            $input['user_id'] = Auth::user()->id;
            $input['challenge_comment_id'] = $request->challenge_reply_id;
            $input['challenge_comment_reply'] = $request->challenge_reply_comment;
            $challenge_comment = PostsTaggedReply::where([
                ['id', $request->challenge_reply_id],
            ])->first();
        
            if (is_null($challenge_comment)) {
                $response = [
                    'status' => 'failure',
                    'message' => 'Provid a valid challenge reply id',
                ];
                return response($response, 422);
            } 

            $tagged_users = PostsTagged::where([
                ['post_id', $challenge_comment->post_id],
            ])->get();
            $post = Post::where([
                ['id', $challenge_comment->post_id],
            ])->first();
            if ($post->post_type == 'private') {
                $is_tagged = false;
                foreach ($tagged_users as $tagged_user) {
                    if ($tagged_user->tagged_user == $input['user_id']) {
                        $is_tagged = true;
                        if ($tagged_user->challenge_accepted == 0) {
                            $tagged_user->update([
                                'challenge_accepted' => 1,
                            ]);
                        }
                    }
                }

                if (($is_tagged == false) && ($input['user_id'] != $post->user_id) && ($input['user_id'] != $challenge_comment->reply_by_id)) {
                    $response = [
                        'status' => 'failure',
                        'message' => 'This is a private challenge. Only tagged user, reply owner or challenge owner can reply at this comment.',
                    ];
                    return response($response, 422);
                }
            }
            $challenge_reply_comment = ChallengeCommentReply::create($input); 
            
            //notification
            $applicant = User::find($input['user_id']);
            if ($applicant->id != $challenge_comment->reply_by_id) {
                Notification::create([
                    'applicant_id'     => $applicant->id,
                    'respondent_id'   => $challenge_comment->reply_by_id, 
                    'applicant_resource_id' => $challenge_reply_comment->id, 
                    'respondent_resource_id' => $challenge_comment->id,
                    'motive' => 'comment_to_challenge_reply',
                    'notification_type' => 'New',
                    'message' => $applicant->name . ' has commented to your challenge reply',
                ]);
            }

            

            $response = [
                'status' => 'success',
                'message' => 'The reply on challenge commented successfully',
                'reply' => $challenge_reply_comment,
            ];
            return response($response, 200);
        } catch (\Exception $ex) {
            $response = [
                'status' => 'failure',
                'message' => 'Can not comment on reply at a challenge. Unidentified error, please contact at ' . env("APP_SUPERADMIN_EMAIL_ID"),
            ];
            return response($response, 500);
        }
    }

    /**
     * To delete a challenge reply.
     *
     * @param int $reply_id
     * @return Restfull Api Response
     */
    public function deleteChallengeReply($reply_id)
    {   try {
            $auth_user = Auth::user()->id;
            $reply = PostsTaggedReply::find($reply_id);
            if (is_null($reply)) {
                $response = [
                    'status' => 'failure',
                    'message' => 'Provid a valid reply id',
                ];
                return response($response, 422);
            } 

            $post = Post::find($reply->post_id);
            if (($reply->reply_by_id != $auth_user) && ($post->user_id != $auth_user)) {
                $response = [
                    'status' => 'failure',
                    'message' => 'Only reply owner or challenge owner can delete',
                ];
                return response($response, 422);
            }
            $reply->delete();
            $response = [
                'status' => 'success',
                'message' => 'The reply on a challenge deleted successfully',
            ];
            return response($response, 200);
        } catch (\Exception $ex) {
            $response = [
                'status' => 'failure',
                'message' => 'Can not delete reply on a challenge. Unidentified error, please contact at ' . env("APP_SUPERADMIN_EMAIL_ID"),
            ];
            return response($response, 500);
        }
    }

    /**
     * To reply a challenge.
     *
     * @param object $request
     * @return Restfull Api Response
     */
    public function replyToChallenge(Request $request)
    {
        try {

            $validator = Validator::make($request->all(), [
                'post_id'           => 'required|regex:/^[0-9]{1,11}$/u',
                'reply_type'        => 'required|string|max:255',
                'reply_text'        => 'nullable|string|max:255',
                'reply_binary'      => 'nullable|string|max:255',
                'reply_binary_mime_type' => 'required_with:reply_binary|string|max:255',
                'location'     => 'nullable|string|max:255',
            ]);   
            if ($validator->fails()) {
                return response([
                    'status' => 'failure',
                    'message'=>$validator->errors()->all()
                ], 422);
            }

            $reply_by_id = Auth::id();
            $challenge = PostsTagged::where([
                ['post_id', $request->post_id],
                ['tagged_user', $reply_by_id],
            ])->first();
            if (is_null($challenge)) {
                return response([
                    'status' => 'failure',
                    'message'=>'Can\'t reply. You have not been tagged at this post' 
                ], 422);
            }
            if ($challenge->tagged_type == 'Normal') {
                return response([
                    'status' => 'failure',
                    'message'=>'Can\'t reply. This is not a challenge post' 
                ], 422);
            }
            if ($challenge->challenge_accepted == 0) {
                //updating PostsTagged table 
                $challenge->update([
                    'challenge_accepted' => 1,
                ]);
            }
            $challenge_by_id = $challenge->tagged_by;

            if (empty($request->reply_text) && empty($request->reply_binary)) {
                return response([
                    'status' => 'failure',
                    'message'=>'Pls write or post something to reply'
                ], 422);
            }
        
            $reply_type_pattern = '/^(public)|(private)$/u';
            if (!preg_match($reply_type_pattern, $request->reply_type)) {
                return response([
                    'status' => 'failure',
                    'message'=>'The reply type must be \'public\' or \'private\'.'
                ], 422);
            }
            if (isset($request->reply_binary_mime_type)) {
                $reply_binary_mime_type_pattern = '/^(image)|(video)$/u';
                if (!preg_match($reply_binary_mime_type_pattern, $request->reply_binary_mime_type)) {
                    return response([
                        'status' => 'failure',
                        'message'=>'The reply binary mime type must be \'image\' or \'video\'.'
                    ], 422);
                }
            }

            
            $reply_to_challenge = PostsTaggedReply::create([
                'post_id'                => $request->post_id,
                'challenge_by_id'        => $challenge_by_id,
                'reply_by_id'            => $reply_by_id,
                'reply_type'             => $request->reply_type,
                'reply_text'             => $request->reply_text,
                'reply_binary'           => $request->reply_binary,
                'reply_binary_mime_type' => $request->reply_binary_mime_type,
                'location'               => $request->location,
            ]);

            //notification
            $applicant = User::find($reply_by_id);
            if ($applicant->id != $challenge_by_id) {
                Notification::create([
                    'applicant_id'     => $applicant->id,
                    'respondent_id'   => $challenge_by_id,
                    'applicant_resource_id' => $reply_to_challenge->id,
                    'respondent_resource_id' => $request->post_id,
                    'motive' => 'reply_to_challenge_post',
                    'notification_type' => 'New',
                    'message' => $applicant->name . ' has replied to the challenge',
                ]);
            }

            $reply_to_challenge->challenge_by = User::where([
                ['id', $challenge_by_id],
            ])->first();
            //getting other details about challenge by user
            $get_resource_detail = new GetResourceDetailController;
            $reply_to_challenge->challenge_by = $get_resource_detail->getUserDetail($reply_to_challenge->challenge_by);

            $reply_to_challenge->post = Post::where([
                ['id', $request->post_id],
            ])->get();
            //getting other details about post
            $posts = $get_resource_detail->getPostDetail($reply_to_challenge->post);
            foreach ($posts as $post) {
                $reply_to_challenge->post = $post;
            }
            $response = [
                'status' => 'success',
                'message' => 'You replied to a challenge successfully',
                'reply_to_challenge' => $reply_to_challenge,
            ];
            return response($response, 200);
        } catch (\Exception $ex) {
            $response = [
                'status' => 'failure',
                'message' => 'Can not create reply on a post. Unidentified error, please contact at ' . env("APP_SUPERADMIN_EMAIL_ID") ,
            ];
            return response($response, 500);
        }
    }

    /**
     * To edit reply to a challenge.
     *
     * @param object $request
     * @return Restfull Api Response
     */
    public function editChallengeReply(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'reply_id'               => 'required|regex:/^[0-9]{1,11}$/u',
                'reply_type'             => 'required|string|max:255',
                'reply_text'             => 'string|nullable|max:255',
                'reply_binary'           => 'string|nullable|max:255',
                'reply_binary_mime_type' => 'required_with:reply_binary|string|max:255',
                'location'               => 'nullable|string|max:255',
            ]);  

            if ($validator->fails()) {
                return response([
                    'status' => 'failure',
                    'message'=>$validator->errors()->all()
                ], 422);
            }

            $auth_user = Auth::user()->id;
            $reply = PostsTaggedReply::find($request->reply_id);
            if (is_null($reply)) {
                return response([
                    'status' => 'failure',
                    'message'=>'Invalid reply Id' 
                ], 422);
            }
            if ($reply->reply_by_id != $auth_user) {
                return response([
                    'status' => 'failure',
                    'message'=>'Can\'t edit. Only a reply owner can edit' 
                ], 422);
            }

            if (empty($request->reply_text) && empty($request->reply_binary)) {
                return response([
                    'status' => 'failure',
                    'message'=>'Pls write or post something to edit a reply'
                ], 422);
            }
        
            $reply_type_pattern = '/^(public)|(private)$/u';
            if (!preg_match($reply_type_pattern, $request->reply_type)) {
                return response([
                    'status' => 'failure',
                    'message'=>'The reply type must be \'public\' or \'private\'.'
                ], 422);
            }
            if (isset($request->reply_binary_mime_type)) {
                $reply_binary_mime_type_pattern = '/^(image)|(video)$/u';
                if (!preg_match($reply_binary_mime_type_pattern, $request->reply_binary_mime_type)) {
                    return response([
                        'status' => 'failure',
                        'message'=>'The reply binary mime type must be \'image\' or \'video\'.'
                    ], 422);
                }
            }

            $edited_reply = $reply->update([
                'reply_type'             => $request->reply_type,
                'reply_text'             => $request->reply_text,
                'reply_binary'           => $request->reply_binary,
                'reply_binary_mime_type' => $request->reply_binary_mime_type,
                'location'               => $request->location,
            ]);
            $response = [
                'status' => 'success',
                'message' => 'Your reply has been edited successfully',
                'edited_reply' => $edited_reply,
            ];
            return response($response, 200);
        } catch (\Exception $ex) {
            $response = [
                'status' => 'failure',
                'message' => 'Can not edit reply to challenge. Unidentified error, please contact at ' . env("APP_SUPERADMIN_EMAIL_ID"),
            ];
            return response($response, 500);
        }
    }

    /**
     * To like reply to a challenge.
     *
     * @param object $request
     * @return Restfull Api Response
     */
    public function likeReplyToChallenge(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'challenge_reply_id' => 'required|regex:/^[0-9]{1,11}$/u',
                'status' => 'required|regex:/^[1,0]$/u',
            ]);   
            if ($validator->fails()) {
                return response([
                    'status' => 'failure',
                    'errors'=>$validator->errors()->all()
                ], 422);
            }

            $auth_user = Auth::user()->id;
            $challenge_reply = PostsTaggedReply::where([
                ['id', $request->challenge_reply_id],
            ])->first();
            if (is_null($challenge_reply)) {
                $response = [
                    'status' => 'failure',
                    'message' => 'Provid a valid challenge reply id',
                ];
                return response($response, 422);
            } 

            $post = Post::find($challenge_reply->post_id);

            if ($post->post_type == 'private') {
                $is_tagged = PostsTagged::where([
                    ['tagged_user', $auth_user],
                    ['post_id', $post->id],
                ])->first();
                if (is_null($is_tagged) && ($post->user_id != $auth_user)) {
                    $response = [
                        'status' => 'failure',
                        'message' => 'This is a private challenge. Only tagged user or challenge owner can like or unlike.',
                    ];
                    return response($response, 422);
                }
            }
            if ($request->status == 1) {
                ChallengeReplyLike::create([
                    'challenge_reply_id' => $request->challenge_reply_id,
                    'user_id'            => $auth_user,
                    'status'             => $request->status,
                ]);
                $response = [
                    'status' => 'success',
                    'message' => 'You have liked the challenge reply successfully',
                ];
                return response($response, 200);
            }
            if ($request->status == 0) {
                ChallengeReplyLike::where([
                    ['challenge_reply_id', $request->challenge_reply_id],
                    ['user_id', $auth_user],
                ])->delete();
                $response = [
                    'status' => 'success',
                    'message' => 'You have unliked the challenge reply successfully',
                ];
                return response($response, 200);
            }
        } catch (\Exception $ex) {
            $response = [
                'status' => 'failure',
                'message' => 'Can not like Challenge reply. Unidentified error, please contact at ' . env("APP_SUPERADMIN_EMAIL_ID"),
            ];
            return response($response, 500);
        }
    }

    /**
     * To save reply to a challenge.
     *
     * @param int $reply_id
     * @return Restfull Api Response
     */
    public function saveReplyToChallenge($reply_id)
    {
        try {
            $auth_user = Auth::user()->id;
            $challenge_reply = PostsTaggedReply::find($reply_id);
            if (is_null($challenge_reply)) {
                $response = [
                    'status' => 'failure',
                    'message' => 'Provid a valid challenge reply id',
                ];
                return response($response, 422);
            } 

            if ($challenge_reply->reply_type == 'private') {
                $is_friend = UsersFriend::where([
                    ['user_id', $challenge_reply->reply_by_id],
                    ['user_friend_id', $auth_user],
                    ['friend_accepted', 1],
                ])->orWhere([
                    ['user_id', $auth_user],
                    ['user_friend_id', $challenge_reply->reply_by_id],
                    ['friend_accepted', 1],
                ])->first();
                if (is_null($is_friend) && ($challenge_reply->reply_by_id != $auth_user)) {
                    $response = [
                        'status' => 'failure',
                        'message' => 'This is a private reply to challenge. Only a friend or reply owner can save.',
                    ];
                    return response($response, 422);
                }
            }
            $saved = SaveReplyToChallenge::create([
                'user_id'               => $auth_user,
                'reply_to_challenge_id' => $reply_id,
            ]);
            $response = [
                'status'       => 'success',
                'message'      => 'You have saved the challenge reply successfully',
                'save_details' => $saved,
            ];
            return response($response, 200);
        } catch (\Exception $ex) {
            $response = [
                'status' => 'failure',
                'message' => 'Can not save challenge reply. Unidentified error, please contact at ' . env("APP_SUPERADMIN_EMAIL_ID"),
            ];
            return response($response, 500);
        }
    }

    /**
     * To unsave reply to a challenge.
     *
     * @param int $reply_id
     * @return Restfull Api Response
     */
    public function unsaveReplyToChallenge($reply_id) {
        try {
            $auth_user = Auth::user()->id;
            $reply = PostsTaggedReply::find($reply_id);

            if (is_null($reply)) {
                $response = [
                    'status' => 'failure',
                    'message' => 'Pls provide a valid reply Id',
                ];
                return response($response, 422);
            }
            $saved = SaveReplyToChallenge::where([
                ['user_id', $auth_user],
                ['reply_to_challenge_id', $reply->id],
            ])->first();
            
            if (is_null($saved)) {
                $response = [
                    'status' => 'failure',
                    'message' => 'You have not saved it to unsave it',
                ];
                return response($response, 422);
            }
            

            $deleted = $saved->delete();
            $response = [
                'status'   => 'success',
                'message'  => 'You have unsaved the challenge reply successfully',
                'deleted' => $deleted,
            ];
            return response($response, 200);
        } catch (\Exception $ex) {
            $response = [
                'status' => 'failure',
                'message' => 'Can not unsave challenge reply. Unidentified error, please contact at ' . env("APP_SUPERADMIN_EMAIL_ID"),
            ];
            return response($response, 500);
        }
    }

    /**
     * To report reply to a challenge.
     *
     * @param object $request
     * @return Restfull Api Response
     */
    public function reportChallenegReply(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'challenge_reply_id'     => 'required|regex:/^[0-9]{1,11}$/u',
                'report_text' => 'required|string|max:255',
            ]);   

            if ($validator->fails()) {
                return response([
                    'status' => 'failure',
                    'message'=>$validator->errors()->all()
                ], 422);
            }
            $challenge_reply = PostsTaggedReply::find($request->challenge_reply_id);
            if (is_null($challenge_reply)) {
                $response = [
                    'status' => 'failure',
                    'message' => 'Please provide a valid challenge reply id',
                ];
                return response($response, 422);
            }
            $auth_user = Auth::user()->id;
            $already_reported = ReportChallengeReply::where([
                ['user_id', $auth_user],
                ['challenge_reply_id', $request->challenge_reply_id],
            ])->first();

            if ($already_reported) {
                $response = [
                    'status' => 'failure',
                    'message' => 'Can not be report more then once',
                ];
                return response($response, 422);
            }
            $report = ReportChallengeReply::create([
                'user_id'     => $auth_user,
                'challenge_reply_id'   => $request->challenge_reply_id,
                'report_text' => $request->report_text,
            ]);
            
            $report->challenge_reply = $challenge_reply;
            //getting other details about post 
            $get_resource_detail = new GetResourceDetailController;
            $post = Post::where([
                ['id', $challenge_reply->post_id],
            ])->get();
            //getting other details about post
            $report->challenge_reply->post = $get_resource_detail->getPostDetail($post);

            $details = array();
            $details['report'] = $report;
            $details['reported_by'] = User::find($auth_user);
            \Mail::to(env("APP_SUPERADMIN_EMAIL_ID"))->send(new \App\Mail\ChallengeReplyReportedNotificationToSuperAdmin($details));
            $response = [
                'status' => 'success',
                'message' => 'The challenge reply reported successfully',
                'report' => $report,
            ];
            return response($response, 200);
        } catch (\Exception $ex) {
            $response = [
                'status' => 'failure',
                'message' => 'Can not report challenge reply. Unidentified error, please contact at ' . env("APP_SUPERADMIN_EMAIL_ID"),
            ];
            return response($response, 500);
        }
    }

    /**
     * To get user challenge list.
     *
     * @param int $user_id
     * @return Restfull Api Response
     */
    public function userChallengeLists($user_id)
    {
        try {
            $user = User::find($user_id);
            $auth_id = Auth::id();
            if (is_null($user)) {
                $response = [
                    'status' => 'failure',
                    'message' => 'Please provide a valid user id',
                ];
                return response($response, 422);
            }
            $challenges = PostsTagged::where([
                ['tagged_user', $user_id],
                ['tagged_type', 'Challenge'],
            ])->get();
            $challenge_detail = array();
            $get_resource_detail = new GetResourceDetailController;
            foreach($challenges as $challenge) {
                $posts = Post::where([
                    ['id', $challenge->post_id],
                ])->get();
                foreach ($posts as $post) {
                    if ($post->post_type == 'public') {
                        $challenge_detail[] = $get_resource_detail->getPostDetail($posts);
                    } elseif ($post->post_type == 'private') {
                        //getting only user's private challenge where auth user is tagged
                        $user_tagged_post = PostsTagged::where([
                            ['post_id', $post->id],
                            ['tagged_by', $user_id],
                            ['tagged_user', $auth_id],
                        ])->first();

                        if(!is_null($user_tagged_post)) {
                            $challenge_detail[] = $get_resource_detail->getPostDetail($posts);
                        }
                    }
                }
            }
            $c_details = array();
            foreach ($challenge_detail as $challenges) {
                foreach ($challenges as $challenge) {
                    $c_details[] = $challenge;
                }
            }
            //custom pagination
            $custom_pagination = new CustomPaginationController;
            $c_details = collect($c_details);
            $c_details = $c_details->sortByDesc('updated_at');
            $c_details = $custom_pagination->paginate($c_details);

            $response = [
                'status' => 'success',
                'message' => 'The challenges found successfully',
                'challenges' => $c_details,
            ];
            return response($response, 200);
        } catch (\Exception $ex) {
            $response = [
                'status' => 'failure',
                'message' => 'Can not find challenge list of user. Unidentified error, please contact at ' . env("APP_SUPERADMIN_EMAIL_ID"),
            ];
            return response($response, 500);
        }
    }

    /**
     * To reply a comment under challenge reply.
     *
     * @param object $request
     * @return Restfull Api Response
     */
    public function replyToChallengeReplyComment(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'challenge_reply_comment_id' => 'required|regex:/^[0-9]{1,11}$/u',
                'comment_to_challenge_reply_comment' => 'required|string|max:255',
            ]);   

            if ($validator->fails()) {
                return response([
                    'status' => 'failure',
                    'message'=>$validator->errors()->all()
                ], 422);
            }

            $input['user_id'] = Auth::user()->id;
            $input['r_comment_id'] = $request->challenge_reply_comment_id;
            $input['comment_challenge_reply_comment'] = $request->comment_to_challenge_reply_comment;

            $challenge_reply_comment = ChallengeCommentReply::find($request->challenge_reply_comment_id);
            if (is_null($challenge_reply_comment)) {
                $response = [
                    'status' => 'failure',
                    'message' => 'Provid a valid challenge reply comment id',
                ];
                return response($response, 422);
            } 
            $reply = PostsTaggedReply::find($challenge_reply_comment->challenge_comment_id);
            $tagged_users = PostsTagged::where([
                ['post_id', $reply->post_id],
            ])->get();
            $post = Post::where([
                ['id', $reply->post_id],
            ])->first();
            if ($post->post_type == 'private') {
                $is_tagged = false;
                foreach ($tagged_users as $tagged_user) {
                    if ($tagged_user->tagged_user == $input['user_id']) {
                        $is_tagged = true;
                        if ($tagged_user->challenge_accepted == 0) {
                            $tagged_user->update([
                                'challenge_accepted' => 1,
                            ]);
                        }
                    }
                }

                if (($is_tagged == false) && ($input['user_id'] != $post->user_id) && ($input['user_id'] != $reply->reply_by_id)) {
                    $response = [
                        'status' => 'failure',
                        'message' => 'This is a private challenge. Only tagged user, comment owner or challenge owner can reply at this comment.',
                    ];
                    return response($response, 422);
                }
            }
            $reply_to_challenge_reply_comment = ChallengeCommentCommentReply::create($input); 

            $response = [
                'status' => 'success',
                'message' => 'The comment on challenge replied successfully',
                'reply_to_challenge_reply_comment' => $reply_to_challenge_reply_comment,
            ];
            return response($response, 200);
        } catch (\Exception $ex) {
            $response = [
                'status' => 'failure',
                'message' => 'Can not reply on comment in challenge reply. Unidentified error, please contact at ' . env("APP_SUPERADMIN_EMAIL_ID"),
            ];
            return response($response, 500);
        }
    }

    /**
     * To delete a comment under challenge reply.
     *
     * @param int $comment_id
     * @return Restfull Api Response
     */
    public function deleteCommentToChallengeReply($comment_id)
    {   try {
            $auth_user = Auth::user()->id;
            $comment = ChallengeCommentReply::find($comment_id);
            if (is_null($comment)) {
                $response = [
                    'status' => 'failure',
                    'message' => 'Provid a valid comment id',
                ];
                return response($response, 422);
            } 
            
            $reply = PostsTaggedReply::find($comment->challenge_comment_id);
            $post = Post::find($reply->post_id);
            if (($comment->user_id != $auth_user) && ($reply->reply_by_id != $auth_user) && ($post->user_id != $auth_user)) {
                $response = [
                    'status' => 'failure',
                    'message' => 'Only comment owner, reply owner or challenge owner can delete',
                ];
                return response($response, 422);
            }
            $comment->delete();
            $response = [
                'status' => 'success',
                'message' => 'The comment on reply of a challenge deleted successfully',
            ];
            return response($response, 200);
        } catch (\Exception $ex) {
            $response = [
                'status' => 'failure',
                'message' => 'Can not delete comment on reply of a challenge. Unidentified error, please contact at ' . env("APP_SUPERADMIN_EMAIL_ID"),
            ];
            return response($response, 500);
        }
    }

    /**
     * To delete reply to a comment under challenge reply.
     *
     * @param int $reply_id
     * @return Restfull Api Response
     */
    public function deleteReplyToCommentToChallengeReply($reply_id)
    {   try {
            $auth_user = Auth::user()->id;
            $reply_to_comment = ChallengeCommentCommentReply::find($reply_id);
            if (is_null($reply_to_comment)) {
                $response = [
                    'status' => 'failure',
                    'message' => 'Provid a valid reply to comment id',
                ];
                return response($response, 422);
            } 
            $comment = ChallengeCommentReply::find($reply_to_comment->r_comment_id);
            $reply = PostsTaggedReply::find($comment->challenge_comment_id);
            $post = Post::find($reply->post_id);

            if (($reply_to_comment->user_id != $auth_user) && ($comment->user_id != $auth_user) && ($reply->reply_by_id != $auth_user) && ($post->user_id != $auth_user)) {
                $response = [
                    'status' => 'failure',
                    'message' => 'Only reply to comment owner, comment owner, reply owner or challenge owner can delete',
                ];
                return response($response, 422);
            }
            $reply_to_comment->delete();
            $response = [
                'status' => 'success',
                'message' => 'The comment on reply of a challenge deleted successfully',
            ];
            return response($response, 200);
        } catch (\Exception $ex) {
            $response = [
                'status' => 'failure',
                'message' => 'Can not delete comment on reply of a challenge. Unidentified error, please contact at ' . env("APP_SUPERADMIN_EMAIL_ID"),
            ];
            return response($response, 500);
        }
    }

    /**
     * To view videos under challenge reply.
     *
     * @param int $reply_id
     * @return Restfull Api Response
     */
    public function viewChallengeReplyVideo($reply_id)
    {
        try {
            $user_id = Auth::id();
            $reply = PostsTaggedReply::where([
                ['id', $reply_id],
            ])->first();

            if (!is_null($reply)) {
                if ($reply->reply_binary_mime_type == 'video') {
                    //storing challenge reply video is viewed
                    ChallengeReplyVideoViewed::create([
                        'reply_id' => $reply_id,
                        'user_id' => $user_id,
                    ]);
                    $response = [
                        'status' => 'Success',
                        'message' => 'Video Viewed Successfully', 
                    ];
                    return response($response, 200);
                } else {
                    $response = [
                        'status' => 'failure',
                        'message' => 'Can not view. This challenge reply does not contain a video',
                    ];
                    return response($response, 422);
                }
            } else {
                $response = [
                    'status' => 'failure',
                    'message' => 'Pls provide a valid challenge reply ID',
                ];
                return response($response, 422);
            }
        } catch (\Exception $ex) {
            $response = [
                'status' => 'failure',
                'message' => 'Can not view the video. Unidentified error, please contact at ' . env("APP_SUPERADMIN_EMAIL_ID") ,
            ];
            return response($response, 500);
        }
    }
}
