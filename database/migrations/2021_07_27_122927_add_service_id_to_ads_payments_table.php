<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddServiceIdToAdsPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ads_payments', function (Blueprint $table) {
            $table->foreignId('service_id')->after('advertiser_id')->nullable();

            $table->foreign('service_id')
                ->references('id')
                ->on('adv_available_services')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ads_payments', function (Blueprint $table) {
            $table->dropColumn('service_id');
        });
    }
}
