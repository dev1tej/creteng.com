<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostsTaggedReply extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 
        'post_id', 
        'challenge_by_id', 
        'reply_by_id', 
        'reply_type',
        'reply_text',
        'reply_binary', 
        'reply_binary_mime_type',
        'location'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        
    ];

    /**
     * Get the post record.
     */
    public function post()
    {
        return $this->belongsTo('App\Post', 'post_id', 'id');
    }

    /**
     * Get the challenge by user record.
     */
    public function challengeBy()
    {
        return $this->belongsTo('App\User', 'challenge_by_id', 'id');
    }

    /**
     * Get the reply by user record.
     */
    public function replyBy()
    {
        return $this->belongsTo('App\User', 'reply_by_id', 'id');
    }
}
