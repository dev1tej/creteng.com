<?php

namespace App\Http\Controllers;
//use App\Profile;
use App\User;
use Illuminate\Http\Request;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }
    
    public function adminHome()
    {
        $manager = env("APP_MANAGER");
        $customer = env("APP_CUSTOMER");
        
        /* Get Number of Managers */
		$manager_count = User::role($manager)->count();
		
		/* Get Number of Customers */
		$customer_count = User::role($customer)->count();
		
		/* Get Number of managers of last 7 days */ 
		$date = \Carbon\Carbon::today()->subDays(7);
		$managers_7days = User::role($manager)->where('created_at','>=',$date)->count();
		
		/* Get Number of Customers of last 7 days */
        $customer_7days = User::role($customer)->where('created_at','>=',$date)->count();
         
        return view('admin.dashboard',compact('manager_count', 'customer_count', 'managers_7days', 'customer_7days'));
    }
    
    public function memberindex(Request $request)
    {
		$user_id = Auth::user()->id;
		$profile_data = Profile::where('id',$user_id)->orderby('id','desc')->first();
		//echo "<pre>"; print_r($profile_data); die;
		$profile_img_path=$profile_data->profile_img_path;
		$request->session()->put('profile_img_path', $profile_img_path);
        return view('members.dashboard',compact('profile_data'));
    }   
}
