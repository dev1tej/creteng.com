<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChallengeReplyLike extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 
        'user_id', 'challenge_reply_id', 'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        
    ];

    /**
     * Get the user that owns the like.
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    /**
     * Get the post that owns the like.
     */
    public function challengeReply()
    {
        return $this->belongsTo('App\PostsTaggedReply', 'challenge_reply_id', 'id');
    }
}
