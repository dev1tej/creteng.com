@extends('admin.layouts.master')
@section('content')
<div class="container">
  <div class="row">
    <div class="col-lg-12 margin-tb">
      <div class="pull-left">
        <h2>Advertisements Management</h2>
      </div>
    </div>
  </div>
  @if ($message = Session::get('success'))
  <div class="alert alert-success">
    <p>{{ $message }}</p>
  </div>
  @endif
  @if ($message = Session::get('danger'))
  <div class="alert alert-danger">
    <p>{{ $message }}</p>
  </div>
  @endif
  @if (isset($success))
  <div class="alert alert-success">
    <p>{{ $success }}</p>
  </div>
  @endif
  <section class="content">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <div class="row">
              <div class="pull-right col-lg-7">
                <!--a class="btn btn-success" href="{{-- route('createAdvertisement') --}}"> Create New Advertisement</a-->
                <a class="btn btn-success" href="{{ route('showAllArchivedAdvertisement') }}"> Show Archived Advertisement</a>
              </div>
            </div>
          </div>
          <div class="card-body">
            <table id="advData" class="table table-bordered table-striped table-hover">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Advertiser(id)</th>
                  <th>Advertisements</th>
                  <th>Status</th>
                  <th>Paid Status</th>
                  <th title="Blue button indicate Advertisement is published" >Publish</th>
                  <th width="280px">Action</th>
                </tr>
              </thead>
              <tbody>
                @php $srno = '1'; @endphp
                @forelse ($data as $key => $advertisement)
                <tr>
                  <td>@php echo $srno; @endphp</td>
                  <th>{{ $advertisement->advertiser->name }}({{ $advertisement->advertiser->id }})</th>
                  <td>
                    @if(($advertisement->ad_type == 'image') || ($advertisement->ad_type == 'Image'))
                    <img src="{{ $advertisement->advertisement }}" alt="{{ $advertisement->advertisement }}" width="100" height="120">
                    @endif
                    @if(($advertisement->ad_type == 'video') || ($advertisement->ad_type == 'Video'))
                    <video width="120" height="100" controls>
                      <source src="{{ $advertisement->advertisement }}"  alt="{{ $advertisement->advertisement }}">
                    </video>
                    @endif
                  </td>
                  <td>{{ $advertisement->status }}</td>
                  <td>
                    @if($advertisement->paid == true)
                      Paid
                    @elseif($advertisement->paid == false)
                      Unpaid
                    @endif
                  </td>
                  <td>
                    <center>
                      <div class="custom-control custom-switch">
                        <input type="checkbox" class="custom-control-input" id="{{ $advertisement->id }}" data-id="{{ $advertisement->published }}" {{ $advertisement->published ? 'checked' : '' }} >		
                        <label class="custom-control-label" for="{{ $advertisement->id }}" >&nbsp;</label>
                        @if ($advertisement->requested_for_publish == true) 
                        <mark><i><b>Requested for Publication</b></i></mark>
                        @endif
                      </div>
                    </center>
                  </td>
                  <td>
                    <a class="btn btn-info btn-sm" href="{{ route('showAdvertisement', $advertisement->id) }}">View</a>
                    <!--a class="btn btn-primary" href="{{-- route('editAdvertisement', $advertisement->id) --}}">Edit</a-->
                    
                    <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#exampleModalDelete{{ $advertisement->id }}" data-id="{{ $advertisement->id }}" >Delete</button>
                    
                    @if(($advertisement->status == 'Inactive') && (is_null($advertisement->ads_payment)))
                      <p class="text-info">Incomplete Registration</p>
                    @elseif(($advertisement->status == 'Inactive') && ($advertisement->ads_payment->status == 'Hold'))
                      <a class="btn btn-success btn-sm" href="{{ route('approveNewAdvertisement', $advertisement->id) }}">Approve Ad</a>
                      <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#disapproveAdModal{{ $advertisement->id }}" data-id="{{ $advertisement->id }}" >Disapprove Ad</button>
                    @endif
                    <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#exampleModal{{ $advertisement->id }}" data-id="{{ $advertisement->id }}" >Archive</button>
                    @if($advertisement->multiple_add_for_single_advertiser == true)
                    <p class="text-info">Multiple Ad for Single Plan</p>
                    @endif
                  </td>
                </tr>
                
                <!-- Delete Advertisement Modal Starts -->
                <div class="modal fade" id="exampleModalDelete{{ $advertisement->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                  <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Warning</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                        If you want to Delete this Advertisement then all the information will be lost for this advertisement.
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                        <a href="{{ route('deleteAdvertisement', $advertisement->id ) }}"><button type="button" class="btn btn-danger">Yes, go forward</button></a>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- Delete Advertisement Modal Ends -->

                <!-- Archive Advertisement Modal Starts -->
                <div class="modal fade" id="exampleModal{{ $advertisement->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                  <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Warning</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                        If you want to archive this Advertisement then advertisement will become in archive state.
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                        <a href="{{ route('archiveAdvertisement', $advertisement->id ) }}"><button type="button" class="btn btn-danger">Yes, go forward</button></a>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- Archive Advertisement Modal Ends -->

                <!-- Disapprove Advertiser Modal Starts -->
                <div class="modal fade" id="disapproveAdModal{{ $advertisement->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                  <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Warning</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                        If you disapprove this Advertisement than payment request will be cancel.
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                        <a href="{{ route('disapproveNewAdvertisement', $advertisement->id ) }}"><button type="button" class="btn btn-danger">Yes, go forward</button></a>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- Delete Disapprove Advertiser Modal Ends -->
                @php $srno++; @endphp
                @empty
                No Advertisement Found
                @endforelse
              </tbody>
              <tfoot>
                <th>No</th>
                <th>Advertiser(id)</th>
                <th>Advertisements</th>
                <th>Status</th>
                <th>Paid Status</th>
                <th title="Blue button indicate Advertisement is published" >Publish</th>
                <th>Action</th>
              </tfoot>
            </table>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

<script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script>
  $(function () {
    $('#advData').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    
    $('body').on("click.bs.toggle", ".custom-control-input", function(b) {
      var status = $(this).is(":checked") == true ? 1 : 0;
      var cat_id = $(this).attr('id');		
      var mark_length = $(this).siblings('mark').length;
      if (mark_length != 0) {
        var publish_request = $(this).siblings('mark');
      }
      $.ajax({
              type: "POST",
              url: '{{ route("advertisementChangeStatus") }}',
              data: {'status': status, 'advertisement_id': cat_id},
              success: function(data){
                //console.log(data);
                  //$('.alert-success').show();
                  //$('.alert-success').html(data.success);
                  if (mark_length != 0) {
                    publish_request.remove();
                  }
              }
          });
      }); 
  });
</script>

@endsection
