<header class="loginbg">
  <div class="container">
    <div class="logo">
       <a href="{{ url('/') }}"><img src="{{ asset('assets/img/logo.png') }}" alt="{{ config('app.name', 'Creteng') }}" ></a>     
    </div>
  </div>
  </header>
  <nav class="menubg navbar navbar-expand navbar-white navbar-light">
    <div class="container">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <!--a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a-->
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="{{ route('advertiser.dashboard') }}" class="nav-link">Dashboard</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="{{ route('showAdvertiserPlan', Auth::user()->id)}}" class="nav-link">My Plan</a>
      </li>
      <!--li class="nav-item d-none d-sm-inline-block">
        <a href="{{-- route('changePlanInfo') --}}" class="nav-link">Change Plan</a>
      </li-->
      <li class="nav-item d-none d-sm-inline-block">
        <a href="{{ route('renewPlanInfo1') }}" class="nav-link">Renew Plan</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="{{ route('advertiserEdit', Auth::user()->id)}}" class="nav-link">Update Profile</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="{{ route('advertiserPasswordEdit', Auth::user()->id) }}" class="nav-link">Change Password</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a class="nav-link" href="{{ route('advertiser.logout') }}"
        onclick="event.preventDefault();
            document.getElementById('logout-form').submit();">
        Logout
        </a>

        <form id="logout-form" action="{{ route('advertiser.logout') }}" method="POST" style="display: none;">
          {{ csrf_field() }}
        </form>
      </li>

    </ul>

  <ul class="navbar-nav ml-auto">
    <li class="nav-item d-none d-sm-inline-block">
      <div class="user-panel d-flex">
      <div class="image">
        <img src="{{ asset('image/user2-160x160.jpg') }}" class="img-circle" alt="User Image">
      </div>
      <div class="info">
        <a class="d-block">{{ Auth::user()->name }}</a>
      </div>
        </li>
  </ul>
</div>
  </nav>

  
