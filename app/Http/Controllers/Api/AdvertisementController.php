<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Advertisement;
use App\Advertiser;
use App\AdvLike;
use App\AdvView;
use App\AdvComment;
use App\AdvPublicationLocation;
use App\AdvAvailableService;
use App\User;

class AdvertisementController extends Controller
{
    /**
     * Getting all advertisements.
     *
     * @return Restfull Api Response
     */
    public function showAllAdvertisement(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'latitude' => ['required','regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/'],
                'longitude' => ['required','regex:/^[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/'],
            ]);   

            if ($validator->fails()) {
                return response([
                    'status' => 'failure',
                    'message'=>$validator->errors()->all()
                ], 422);
            }
            $lat_lng = trim($request->latitude).','.trim($request->longitude);
            $url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=".$lat_lng."&sensor=false"."&key=".env("GOOGLE_API_KEY");
            $result_string = file_get_contents($url);
            $result = json_decode($result_string, true);


            $flag = 0;
            if ($result['status'] == 'OK') {
                foreach ($result['results'][0]['address_components'] as $address_components) {
                    $long_name = $address_components['long_name'];
                    $short_name = $address_components['short_name'];
                    $types = $address_components['types'];
                    if ($types[0] == 'administrative_area_level_1') {
                        $state = $long_name;
                    }
                    if ($types[0] == 'country') {
                        $country = $long_name;
                    }
                }
                $flag = 1;
            } elseif ($result['status'] == 'failure') {
                $response = [
                    'status' => $result['status'],
                    'message' => $result['message'],
                ];
                return response($response, 422);
            }

            if ($flag == 0) {
                $response = [
                    'status' => 'success',
                    'message' => 'Advertisement found Successfully',
                    'advertisement' => [],
                ];
                return response($response, 200);
            }

            $publishable_ads = AdvPublicationLocation::where([
                ['country', $country],
                ['state', $state],
            ])->orWhere([
                ['country', $country],
                ['state', $country],
            ])->get();

            $advs = array();
            foreach ($publishable_ads as $ads) {
                $adv = Advertisement::with('advertiser')->where([
                    ['service_id', $ads->service_id],
                    ['status', 'Active'],
                    ['published', 1],
                    ['archive', false],
                ])->get();
                if ($adv != null) {
                    foreach ($adv as $ad) {
                        $advs[] = $ad;
                    }
                }
            }
            $advs = collect($advs);
            $advs = $advs->sortByDesc('updated_at');

            $auth_user = Auth::user()->id;
            
            $advertisement = array();
            foreach ($advs as $adv) {
                $adv_available_service = AdvAvailableService::where([
                    ['id', $adv->service_id],
                ])->first();

                if (!is_null($adv_available_service['opt_services'])) {
                    $adv['is_opt_service_available'] = true;
                } else {
                    $adv['is_opt_service_available'] = false;
                }

                $likes = AdvLike::where([
                    ['adv_id', $adv->id],
                ])->with('user')->get();

                $adv_likes = array();
                $liked_by_logged_user = false;
                $adv_likes['likes_count'] = $likes->count();
                foreach ($likes as $like) {
                    $adv_likes['likes'][] = $like;
                    
                    if ($like->user_id == $auth_user) {
                        $liked_by_logged_user = true;
                    }
                }
                $adv_likes['liked_by_logged_user'] = $liked_by_logged_user;
                $adv['likes'] = $adv_likes;
                
        
                $views = AdvView::where([
                    ['adv_id', $adv->id],
                ])->with('user')->get();
                $adv_views = array();
                $adv_views['views_count'] = $views->count();
                $adv_views['views'] = $views;

                $viewed_by_logged_user = false;
                foreach ($views as $view) {
                    if ($view->user_id == $auth_user) {
                        $viewed_by_logged_user = true;
                    }
                }
                $adv_views['viewed_by_logged_user'] = $viewed_by_logged_user;

                $adv['views'] = $adv_views;
        
                $comments = AdvComment::where('adv_id', $adv->id)
                    ->whereNull('comment_id')
                    ->get();
        
                $comment_details = array();
                $index = 0;
                foreach ($comments as $comment) {
                    $comment_details[$index] = $comment;

                    $can_delete_by_logged_user = false;
                    $can_edit_by_logged_user = false;
                    if ($comment->user_id == $auth_user) {
                        $can_delete_by_logged_user = true;
                        $can_edit_by_logged_user = true;
                    }
                    $comment_details[$index]['can_delete_by_logged_user'] = $can_delete_by_logged_user;
                    $comment_details[$index]['can_edit_by_logged_user'] = $can_edit_by_logged_user;
                    $comment_details[$index]['comment_by'] = User::find($comment['user_id']);

                    $comment_replies = AdvComment::where([
                        ['adv_id', $adv->id],
                        ['comment_id', $comment->id],
                    ])->get();

                    $c_reply = array();
                    $i = 0;
                    foreach ($comment_replies as $comment_reply) {
                        $can_delete_by_logged_user = false;
                        $can_edit_by_logged_user = false;
                        if ($comment_reply->user_id == $auth_user) {
                            $can_delete_by_logged_user = true;
                            $can_edit_by_logged_user = true;
                        }
                        $c_reply['replies'][$i]['id'] = $comment_reply['id'];
                        $c_reply['replies'][$i]['adv_id'] = $comment_reply['adv_id'];
                        $c_reply['replies'][$i]['user_id'] = $comment_reply['user_id'];
                        $c_reply['replies'][$i]['comment_id'] = $comment_reply['comment_id'];
                        $c_reply['replies'][$i]['comment_reply'] = $comment_reply['comments'];
                        $c_reply['replies'][$i]['created_at'] = $comment_reply['created_at'];
                        $c_reply['replies'][$i]['updated_at'] = $comment_reply['updated_at'];
                        $c_reply['replies'][$i]['can_delete_by_logged_user'] = $can_delete_by_logged_user;
                        $c_reply['replies'][$i]['can_edit_by_logged_user'] = $can_edit_by_logged_user;
                        $c_reply['replies'][$i]['reply_by'] = User::find($comment_reply['user_id']);
                        $i++;
                    }
                    if (isset($c_reply['replies'])) {
                        $replies = $c_reply['replies'];
                        $replies = collect($replies);
                        $replies = $replies->sortByDesc('updated_at');
                        unset($c_reply['replies']);
                        foreach ($replies as $reply) {
                            $c_reply['replies'][] = $reply;
                        }
                    }
                    $c_reply['replies_count'] = $comment_replies->count();
                    $comment_details[$index]['comment_replies'] = $c_reply;

                    $index++;
                }
                $c_detail = array();
                $c_detail['total_comments'] = $comments->count();
                $comment_details = collect($comment_details);
                $comment_details = $comment_details->sortByDesc('updated_at');
                foreach ($comment_details as $comment_detail) {
                    $c_detail['comment_detail'][] = $comment_detail;
                }
                $adv['ad_comments'] = $c_detail;

                $advertisement[] = $adv;
            }
            $response = [
                'status' => 'success',
                'message' => 'Advertisement found Successfully',
                'advertisement' => $advertisement,
            ];
            return response($response, 200);
            
        } catch (\Exception $ex) {
            $response = [
                'status' => 'failure',
                'message' => 'Can not show advertisements. Unidentified error, please contact at ' . env("APP_SUPERADMIN_EMAIL_ID"),
            ];
            return response($response, 500);
        }
    }

    /**
     * Like or Unlike an advertisement by logged user.
     *
     * @param  object  $request
     * @return Restfull Api Response
     */
    public function likeAdv(Request $request) 
    {
        try {
            $validator = Validator::make($request->all(), [
                'adv_id' => 'required|regex:/^[0-9]{1,11}$/u',
                'status' => 'required|regex:/^[1,0]$/u',
            ]);   
            if ($validator->fails()) {
                return response([
                    'status' => 'failure',
                    'errors'=>$validator->errors()->all()
                ], 422);
            }

            $input['user_id'] = Auth::user()->id;
            $input['adv_id'] = $request->adv_id;
            $input['status'] = $request->status;
            $adv = Advertisement::where([
                ['id', $input['adv_id']],
            ])->first();
            if (is_null($adv)) {
                $response = [
                    'status' => 'failure',
                    'message' => 'Provid a valid advertisement id',
                ];
                return response($response, 422);
            } 

            if ($input['status'] == 1) {
                //checking if already liked
                $adv_already_liked = AdvLike::where([
                    ['user_id', $input['user_id']], 
                    ['adv_id', $input['adv_id']],
                    ['status', 1],
                ])->get();
                if (!($adv_already_liked->isEmpty())) {
                    $response = [
                        'status' => 'failure',
                        'message' => 'You have already liked this advertisement',
                    ];
                    return response($response, 422);
                } else {
                    AdvLike::create($input);
                    $response = [
                        'status' => 'success',
                        'message' => 'You have liked the advertisement successfully',
                    ];
                    return response($response, 200);
                }
            }

            //to remove like advertisement (Unlike Adv)
            if ($input['status'] == 0) {
                $adv_like = AdvLike::where([
                    ['user_id', $input['user_id']], 
                    ['adv_id', $input['adv_id']],
                ])->delete();

                if (!$adv_like) {
                    $response = [
                        'status' => 'failure',
                        'message' => 'Advertisement not found',
                    ];
                    return response($response, 422);
                } else { 
                    $response = [
                        'status' => 'success',
                        'message' => 'Post Unlike successfully',
                    ];
                    return response($response, 200);
                }
            }
        } catch (\Exception $ex) {
            $response = [
                'status' => 'failure',
                'message' => 'Can not like advertisement. Unidentified error, please contact at ' . env("APP_SUPERADMIN_EMAIL_ID"),
            ];
            return response($response, 500);
        }
    }

    /**
     * To view an advertisement by logged user.
     *
     * @param  int  $post_id
     * @return Restfull Api Response
     */
    public function viewAdv($adv_id)
    {
        try {
            $user_id = Auth::id();
            $adv = Advertisement::where([
                ['id', $adv_id],
            ])->first();

            if (!is_null($adv)) {
                AdvView::create([
                    'adv_id' => $adv_id,
                    'user_id' => $user_id,
                ]);
                $response = [
                    'status' => 'Success',
                    'message' => 'Advertisement Viewed Successfully', 
                ];
                return response($response, 200);
            } else {
                $response = [
                    'status' => 'failure',
                    'message' => 'Pls provide a valid advertisement ID',
                ];
                return response($response, 422);
            }
        } catch (\Exception $ex) {
            $response = [
                'status' => 'failure',
                'message' => 'Can not view advertisement. Unidentified error, please contact at ' . env("APP_SUPERADMIN_EMAIL_ID") ,
            ];
            return response($response, 500);
        }
    }

    /**
     * Comment a existing advertisement by logged user.
     *
     * @param  object  $request
     * @return Restfull Api Response
     */
    public function commentAdv(Request $request) 
    {
        try {
            $validator = Validator::make($request->all(), [
                'adv_id' => 'required|regex:/^[0-9]{1,11}$/u',
                'comment_id' => 'nullable|regex:/^[0-9]{1,11}$/u',
                'is_reply_on_comment' => 'required|boolean',
                'comment' => 'required|string|max:255',
            ]);   

            if ($validator->fails()) {
                return response([
                    'status' => 'failure',
                    'message'=>$validator->errors()->all()
                ], 422);
            }

            $input['user_id'] = Auth::user()->id;
            $input['adv_id'] = $request->adv_id;
            $input['comments'] = $request->comment;

            $adv = Advertisement::where([
                ['id', $input['adv_id']],
            ])->first();
            if (is_null($adv)) {
                $response = [
                    'status' => 'failure',
                    'message' => 'Provid a valid advertisement id',
                ];
                return response($response, 422);
            }

            if ($request->is_reply_on_comment == false) {
                //$input['comment_id'] = null;
                $adv_comment = AdvComment::create($input);
                $response = [
                    'status' => 'Success',
                    'message' => 'Advertisement commented Successfully', 
                    'comment' => $adv_comment,
                ];
                return response($response, 200);
            } elseif ($request->is_reply_on_comment == true) {
                if (is_null($request->comment_id)) {
                    $response = [
                        'status' => 'failure',
                        'message' => 'To reply on a comment, comment ID can not be null',
                    ];
                    return response($response, 422);
                } else {
                    $input['comment_id'] = $request->comment_id;
                    
                    $comment = AdvComment::where([
                        ['id', $input['comment_id']],
                    ])->first();
                    if (is_null($comment)) {
                        $response = [
                            'status' => 'failure',
                            'message' => 'Provid a valid comment id',
                        ];
                        return response($response, 422);
                    }

                    $adv_comment = AdvComment::create($input);
                    $response = [
                        'status' => 'Success',
                        'message' => 'Comment on an advertisement is replied Successfully', 
                        'comment' => $adv_comment,
                    ];
                    return response($response, 200);
                }
            }  
        } catch (\Exception $ex) {
            $response = [
                'status' => 'failure',
                'message' => 'Can not comment. Unidentified error, please contact at ' . env("APP_SUPERADMIN_EMAIL_ID"),
            ];
            return response($response, 500);
        }
    }

    /**
     * Comment a existing advertisement by logged user.
     *
     * @param  object  $request
     * @return Restfull Api Response
     */
    public function deleteCommentAdv(Request $request) 
    {
        try {
            $validator = Validator::make($request->all(), [
                'is_reply_on_comment' => 'required|boolean',
                'comment_id' => 'nullable|regex:/^[0-9]{1,11}$/u',
                'reply_id' => 'nullable|regex:/^[0-9]{1,11}$/u',
            ]);   

            if ($validator->fails()) {
                return response([
                    'status' => 'failure',
                    'message'=>$validator->errors()->all()
                ], 422);
            }

            $auth_id = Auth::user()->id;
            if ($request->is_reply_on_comment == false) {

                if (is_null($request->comment_id)) {
                    $response = [
                        'status' => 'failure',
                        'message' => 'Comment id is require to delete comment.',
                    ];
                    return response($response, 422);
                }
                $adv_comment = AdvComment::where([
                    ['id', $request->comment_id],
                ])->first();
                if (is_null($adv_comment)) {
                    $response = [
                        'status' => 'failure',
                        'message' => 'Provid a valid comment id',
                    ];
                    return response($response, 422);
                }
                if ($adv_comment['user_id'] != $auth_id) {
                    $response = [
                        'status' => 'failure',
                        'message' => 'Only comment owner can delete it',
                    ];
                    return response($response, 422);
                }
                $adv_comment_replies = AdvComment::where([
                    ['comment_id', $adv_comment->id],
                ])->get();
                foreach ($adv_comment_replies as $adv_comment_reply) {
                    $adv_comment_reply->delete();
                }
                $adv_comment->delete();
                $response = [
                    'status' => 'Success',
                    'message' => 'Comment and replies on comment deleted Successfully', 
                ];
                return response($response, 200);
            } elseif ($request->is_reply_on_comment == true) {

                if (is_null($request->reply_id)) {
                    $response = [
                        'status' => 'failure',
                        'message' => 'To delete reply on comment reply id is require',
                    ];
                    return response($response, 422);
                }
                $adv_comment_reply = AdvComment::where([
                    ['id', $request->reply_id],
                ])->first();
                if (is_null($adv_comment_reply)) {
                    $response = [
                        'status' => 'failure',
                        'message' => 'Provid valid reply id',
                    ];
                    return response($response, 422);
                }

                if ($adv_comment_reply['user_id'] != $auth_id) {
                    $response = [
                        'status' => 'failure',
                        'message' => 'Only reply owner can delete it',
                    ];
                    return response($response, 422);
                }
                $adv_comment_reply->delete();
                $response = [
                    'status' => 'Success',
                    'message' => 'Reply on comment deleted Successfully', 
                ];
                return response($response, 200);
            }  
        } catch (\Exception $ex) {
            $response = [
                'status' => 'failure',
                'message' => 'Can not delete comment. Unidentified error, please contact at ' . env("APP_SUPERADMIN_EMAIL_ID"),
            ];
            return response($response, 500);
        }
    }
}
