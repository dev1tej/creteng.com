@extends('admin.layouts.adminlogin.loginmaster')
@section('content')
<div class="container">
  @if(session()->has('message'))
  <div class="alert alert-success">
    {{ session()->get('message') }}
  </div>
  @endif	
  <div class="alert alert-success" style="display:none;"></div>
  <div class="alert alert-danger" style="display:none;">
    <ul></ul>
  </div>
  <div class="row justify-content-center">
    <div class="col-md-8">
      <div class="card">
        <div class="card-header">{{ __('Choose') }}</div>
        <div class="card-body">
          <div class="form-group row">
            <label for="roles" class="col-md-4 col-form-label text-md-right">{{ __('Role') }}</label>
            <div class="col-md-6">
              {!! Form::select('roles[]', $roles,'Providers', array('id' => 'roles','class' => 'form-control','required' => 'required','placeholder' => 'Please Select')) !!}                            
            </div>
          </div>
        </div>
      </div>
      <div class="card" id="customer_form" style="display:none;">
        <div class="card-header">{{ __('Customer Registeration Form') }}</div>
        <div class="card-body">
          <form method="POST" id="customer-form" action="{{ route('register') }}">
            {{ csrf_field() }}
            <div class="form-group row">
              <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>
              <div class="col-md-6">
                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                @error('name')
                <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
                </span>
                @enderror
              </div>
            </div>
            <div class="form-group row">
              <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>
              <div class="col-md-6">
                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
                @error('email')
                <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
                </span>
                @enderror
              </div>
            </div>
            <div class="form-group row">
              <label for="country_code" class="col-md-4 col-form-label text-md-right">{{ __('Contact') }}</label>
              <div class="col-md-2">
                <input id="country_code" type="text" class="form-control @error('country_code') is-invalid @enderror" name="country_code" value="{{ old('country_code') }}" required autocomplete="country_code">
                <small class="form-text text-muted">Country Code</small>
                @error('country_code')
                <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
                </span>
                @enderror
              </div>
              <div class="col-md-4">
                <input id="contact" type="text" class="form-control @error('contact') is-invalid @enderror" name="contact" value="{{ old('contact') }}" required >
                @error('contact')
                <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
                </span>
                @enderror
              </div>
            </div>
            <div class="form-group row">
              <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>
              <div class="col-md-6">
                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                @error('password')
                <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
                </span>
                @enderror
              </div>
            </div>
            <div class="form-group row">
              <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>
              <div class="col-md-6">
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
              </div>
            </div>
            <input id="role" type="hidden" class="form-control" name="roles" value="{{ env('APP_CUSTOMER') }}">
            <div class="form-group row mb-0">
              <div class="col-md-6 offset-md-4">
                <button type="button" id="submitcustomer" class="btn btn-primary">
                {{ __('Register') }}
                </button>
              </div>
            </div>
          </form>
        </div>
      </div>
      <div class="card" id="provider_form" style="display:none;">
        <div class="card-header">{{ __('Provider Registeration Form') }}</div>
        <div class="card-body">
          <form method="POST" id="provider-form" action="{{ route('register') }}"  enctype="multipart/form-data" data-stripe-publishable-key="{{ env('STRIPE_KEY') }}">
            {{ csrf_field() }}
            <div class="page-header">
              <h3>Profile Details</h3>
            </div>
            <hr>
            <div class="form-group">
              <label for="name">{{ __('Name') }}</label>
              <input id="provider-name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
              @error('name')
              <span class="invalid-feedback" role="alert">
              <strong>{{ $message }}</strong>
              </span>
              @enderror
            </div>
            <div class="form-group">
              <label for="profile_username">{{ __('Username') }}</label>
              <input id="profile_username" type="text" class="form-control @error('profile_username') is-invalid @enderror inputreq" name="profile_username" value="{{ old('profile_username') }}" required>
              @error('profile_username')
              <span class="invalid-feedback" role="alert">
              <strong>{{ $message }}</strong>
              </span>
              @enderror
            </div>
            <div class="form-group">
              <label for="email">{{ __('E-Mail Address') }}</label>
              <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
              @error('email')
              <span class="invalid-feedback" role="alert">
              <strong>{{ $message }}</strong>
              </span>
              @enderror
            </div>
            <div class="form-group">
              <label for="country_code">{{ __('Contact No') }}</label>
              <div class="row">
                <div class="col-md-2">
                  <input id="country_code" type="text" class="form-control @error('country_code') is-invalid @enderror" name="country_code" value="{{ old('country_code') }}" required autocomplete="country_code">
                  <small class="form-text text-muted">Country Code</small>
                  @error('country_code')
                  <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
                  </span>
                  @enderror
                </div>
                <div class="col-md-4">
                  <input id="contact" type="text" class="form-control @error('contact') is-invalid @enderror" name="contact" value="{{ old('contact') }}" required autocomplete="contact">
                  @error('contact')
                  <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
                  </span>
                  @enderror
                </div>
              </div>
            </div>
            <div class="form-group">
              <label for="password">{{ __('Password') }}</label>
              <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
              @error('password')
              <span class="invalid-feedback" role="alert">
              <strong>{{ $message }}</strong>
              </span>
              @enderror
            </div>
            <div class="form-group">
              <label for="password-confirm">{{ __('Confirm Password') }}</label>
              <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
            </div>
            <div class="form-group">
              <label for="profile_img_path">{{ __('Choose Profile Image') }} <small class="text-muted">(Upload only 200*200 pxl image)</small></label>
              <input id="profile_img_path" type="file" class="form-control inputreq" name="profile_img_path">
              @error('profile_img_path')
              <span class="invalid-feedback" role="alert">
              <strong>{{ $message }}</strong>
              </span>
              @enderror
            </div>
            <div class="form-group">
              <label for="plan">{{ __('Plan') }}</label>
              <select class="form-control @error('plan') is-invalid @enderror inputreq" name="plan" id="plan" required>
                <option>Select Plan</option>
                @foreach($plans as $key => $value)
                <option value="{{ $key }}" {{ old('Plan') == $key ? 'selected' : ''}}>{{ $value }}</option>
                @endforeach
              </select>
              @error('plan')
              <span class="invalid-feedback" role="alert">
              <strong>{{ $message }}</strong>
              </span>
              @enderror
            </div>
            <!-- Coupan code -->
            <div class="form-group">
              <label for="promo_code">Apply Coupan Code:</label>
              <input type="text" class="form-control" id="coupon_valid" placeholder="Apply Coupan code" name="coupon_valid">
              <b><span id="coupanmessage" style="color:green;"></span></b>
            </div>
            <!-- Payments -->	
            <div class="flex flex-wrap mb-6">
              <label for="card-element" class="block text-gray-700 text-sm font-bold mb-2">
              Credit Card Info
              </label>
              <div id="card-element" class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"></div>
              <div id="card-errors" class="text-red-400 text-bold mt-2 text-sm font-medium"></div>
            </div>
            <input id="role" type="hidden" class="form-control" name="roles" value="{{ env('APP_PROVIDER') }}">
            <div class="form-group row mb-0">
              <div class="col-md-6 offset-md-4">
                <button type="submit" id="submitprovider" class="btn btn-primary">
                {{ __('Register') }}
                </button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- JQUERY File -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!-- Stripe JS -->
<script type="text/javascript" src="https://js.stripe.com/v3/"></script>

<script>
	$("#coupon_valid").on("change", function(){
		var cvalue = $(this).val();
		var coupanarray = <?php echo json_encode($coupans); ?>;
		console.log(coupanarray);
		$('#coupanmessage').empty();
		var find = function(input, target){
		  var found;
		  for (var prop in input) {
			if(input[prop] == target){
				found = prop;
			}
		  };
		  return found;
		};

		var found = find(coupanarray, cvalue);

		if(found){
			$('#coupanmessage').append('<span style="color:green;">Coupan Applied</span>');
			var form = document.getElementById('provider-form');
			var hiddenInput = document.createElement('input');
			hiddenInput.setAttribute('type', 'hidden');
			hiddenInput.setAttribute('name', 'coupan_code');
			hiddenInput.setAttribute('value', found);
			form.appendChild(hiddenInput);
		}else{
			$('#coupanmessage').append('<span style="color:red;">Invalid Coupan</span>');
			cvalue="";			
		}
		/*
		if(coupanarray.indexOf(cvalue) >= 0){
			$('#coupanmessage').append('<span style="color:green;">Coupan Applied</span>');
		}else{
			$('#coupanmessage').append('<span style="color:red;">Invalid Coupan</span>');
			cvalue="";
		}*/
	});
</script>
<script>	
	const stripe = Stripe('{{ env("STRIPE_KEY") }}');
	console.log(stripe);
	const elements = stripe.elements();
	const cardElement = elements.create('card');
	cardElement.mount('#card-element');
	const cardHolderName = document.getElementById('provider-name');
	const cardButton = document.getElementById('submitprovider');
	const clientSecret = cardButton.dataset.secret;
	let validCard = false;
	const cardError = document.getElementById('card-errors');
	cardElement.addEventListener('change', function(event) {
		
		if (event.error) {
			validCard = false;
			cardError.textContent = event.error.message;
		} else {
			validCard = true;
			cardError.textContent = '';
		}
	});
	
	var form = document.getElementById('provider-form');
	form.addEventListener('submit', async (e) => {
		event.preventDefault();
		const { paymentMethod, error } = await stripe.createPaymentMethod(
			'card', cardElement, {
				billing_details: { name: cardHolderName.value }
			}
		);
		if (error) {
			// Display "error.message" to the user...
			console.log(error);
		} else {
			// The card has been verified successfully...
			var hiddenInput = document.createElement('input');
			hiddenInput.setAttribute('type', 'hidden');
			hiddenInput.setAttribute('name', 'payment_method');
			hiddenInput.setAttribute('value', paymentMethod.id);
			form.appendChild(hiddenInput);
			// Submit the form
			//form.submit();
			
			
			//validate fields
			var fail = false;
			var fail_log = '';
			var name;
			$('form#provider-form').find('select, textarea, input').each(function(){
				if(!$(this).prop('required')){

				}else{
					if(!$(this).val()){
						fail = true;
						fd_id = $(this).attr('id');

							var $window = $(window),
								$element = $('#'+fd_id),
								elementTop = $element.offset().top,
								elementHeight = $element.height(),
								viewportHeight = $window.height(),
								scrollIt = elementTop - ((viewportHeight - elementHeight) / 2);
							$window.scrollTop(scrollIt);
							
						$('#'+fd_id).focus();	
						fail_log = fd_id + " is required \n";
					}
				}
			});
			
			if(fail_log.length == 0){			
						
						var data=$('#provider-form')[0];  
						var fd = new FormData(data);
									$.ajax({
									   type:'POST',
									   url:"{{ route('storeUser') }}",
									   data:fd,
									   processData: false,  // Important!
									   contentType: false,
									   cache: false,
									   success:function(data){
											window.location = '/';
									   },
									   error: function(jqXHR, textStatus, errorThrown) {
										   //console.log(jqXHR.responseJSON.errors)
											$('.alert-danger').show();
											$('.alert-success').hide();
											$.each(jqXHR.responseJSON.errors, function(key,value) {
												$('html, body').animate({
													scrollTop: $(".alert-danger").offset().top
												}, 2000);
												$(".alert-danger").find("ul").append('<li>'+value+'</li>');
											}); 

										}
									});		
						
			}else{
				//alert(fail_log);
			}
			
			
			
		}
	});
</script>

<script>
var providers = '{{ env('APP_PROVIDER') }}';
var customers = '{{ env('APP_CUSTOMER') }}';

$(document).ready(function() {
	var roles = document.getElementById('roles');
	if(roles.value == providers)
	{
	  $('#provider_form').show();
	  $('#customer_form').hide();
	}
	if(roles.value == customers)
	{
	  $('#customer_form').show();
	  $('#provider_form').hide();
	}
});

var roles = document.getElementById('roles');
roles.addEventListener("change", function() {
  if(this.value == providers)
  {
	  $('#provider_form').show();
	  $('#customer_form').hide();
  }
  if(this.value == customers)
  {
	  $('#customer_form').show();
	  $('#provider_form').hide();
  }
});
</script>
<script type="text/javascript">
	var ctry = document.getElementById('country');
    ctry.addEventListener("change", function(){
    var countryID = $(this).val();    
    if(countryID){
        $.ajax({
           type:"GET",
           url:"{{url('statelist')}}?country_id="+countryID,
           success:function(res){               
            if(res){
                $("#state").empty();
                $("#state").append('<option>Select</option>');
                $.each(res,function(key,value){
                    $("#state").append('<option value="'+key+'">'+value+'</option>');
                });
           
            }else{
               $("#state").empty();
            }
           }
        });
    }else{
        $("#state").empty();
        $("#city").empty();
    }      
   });
   
   var stat = document.getElementById('state');
    stat.addEventListener("change", function(){
    var stateID = $(this).val();    
    if(stateID){
        $.ajax({
           type:"GET",
           url:"{{url('citylist')}}?state_id="+stateID,
           success:function(res){               
            if(res){
                $("#city").empty();
                $.each(res,function(key,value){
                    $("#city").append('<option value="'+key+'">'+value+'</option>');
                });
           
            }else{
               $("#city").empty();
            }
           }
        });
    }else{
        $("#city").empty();
    }
        
   });
</script>
<script type="text/javascript">
	  	    
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
 
   var cusbutton = document.getElementById('submitcustomer');
   cusbutton.addEventListener("click", function(e){
        e.preventDefault();
        var data=$('#customer-form')[0];  
        var fd = new FormData(data); 

        $.ajax({
           type:'POST',
           url:"{{ route('storeUser') }}",
           data:fd,
           processData: false,  // Important!
		   contentType: false,
           cache: false,
           success:function(data){
                  window.location = '/';
           },
           error: function(jqXHR, textStatus, errorThrown) {
			   //console.log(jqXHR.responseJSON.errors)
				$('.alert-danger').show();
				$('.alert-success').hide();
				$.each(jqXHR.responseJSON.errors, function(key,value) {
					//$('.verror2').append('<div class="alert alert-danger">'+value+'</div>');
					$(".alert-danger").find("ul").append('<li>'+value+'</li>');
				}); 

			}
        });
	}); 

</script>
@endsection
