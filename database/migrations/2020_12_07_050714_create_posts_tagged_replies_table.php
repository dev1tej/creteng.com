<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostsTaggedRepliesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts_tagged_replies', function (Blueprint $table) {
            $table->id();
            $table->foreignId('post_id');
            $table->foreignId('challenge_by_id');
            $table->foreignId('reply_by_id');
            $table->string('reply_type', 255)->nullable();
            $table->string('reply_text', 255)->nullable();
            $table->string('reply_binary', 255)->nullable();
            $table->string('reply_binary_mime_type', 255)->nullable();
            $table->timestamps();

            $table->foreign('post_id')
                ->references('id')
                ->on('posts')
                ->onDelete('cascade');
            $table->foreign('challenge_by_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
            $table->foreign('reply_by_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts_tagged_replies');
    }
}
