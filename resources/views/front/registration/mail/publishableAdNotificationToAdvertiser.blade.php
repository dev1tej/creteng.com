<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ config('app.name', 'Creteng') }}</title>
  </head>
  <body>
    <div>
      <p><b><h5>Hi {{  $details['advertiser']['name'] }}<h5></b></p>
      <p><b>Thank you for showing your interest in publishing your ad at Creteng.
        Your ad is currently under evaluation. You will be updated with the status
        within the next 48 hours. Payment will be deducted after ad approval.</b></p>
    </div>
    <div>
      <p><b>Advertisement Detail:</b><br/>
        <b>Advertiser Name:</b> {{  $details['advertiser']['name'] }} <br/>
        <b>Advertiser Email:</b> {{  $details['advertiser']['email'] }} <br/>
        <b>Advertiser Contact:</b> {{  $details['advertiser']['contact'] }} <br/>
        <b>Advertiser Company:</b> {{  $details['advertiser']['company_name'] }} <br/>
        <b>Ad Id:</b> {{  $details['advertisement']['id'] }} <br/>
        <b>Ad:</b> <a href="{{  $details['advertisement']['advertisement'] }}">{{  $details['advertisement']['advertisement'] }}</a> <br/>
        <b>Ad Publishable Locations:</b><br/>
        @php $no_of_publication_location = 0; @endphp
        @foreach ($details['advertisement']['publishable_locations'] as $publishable_location)
        @php $no_of_publication_location++; @endphp
        {{ $no_of_publication_location }}. Country: {{  $publishable_location['country'] }} State: {{  $publishable_location['state'] }},<br/>
        @endforeach
        <br/>
        <b>Plan:</b> {{  $details['advertisement']['plan']['plan_name'] }} <br/>
        <b>Plan Duration:</b> {{  $details['advertisement']['plan_duration']['plan_duration'] }} <br/>
        <b>Plan Duration Cost:</b> {{  $details['advertisement']['plan_duration']['price'] }} <br/>
        <b>Fixed Services:</b> {{  $details['advertisement']['plan_fixed_service']['fixed_services'] }} <br/>
        @if(!is_null($details['advertisement']['plan_opt_service']))
        <b>Optional Services:</b> {{  $details['advertisement']['plan_opt_service']['optional_services'] }} <br/>
        <b>Optional Services Cost:</b> {{  $details['advertisement']['plan_opt_service']['aditional_price'] }} {{  $details['advertisement']['plan_opt_service']['aditional_price_duration'] }}<br/>
        @endif
        <b>Number of Publication Location:</b> {{ $no_of_publication_location }} <br/>
        @if(!is_null($details['advertisement']['plan_opt_service']))
        <b>Total Optional Services Cost:</b> ${{ $details['advertisement']['total_opt_services_cost'] }} <br/>
        <b>Total Payable Cost:</b> ({{ $details['advertisement']['plan_duration']['price'] }} + {{ $details['advertisement']['plan_opt_service']['aditional_price'] }} {{ $details['advertisement']['plan_opt_service']['aditional_price_duration'] }} {{ $details['advertisement']['multiplying_factor'] }})*{{ $no_of_publication_location }} = ${{ $details['advertisement']['amount'] }} <br/>
        @else
        <b>Total Payable Cost:</b> ${{ $details['advertisement']['amount'] }} <br/>
        @endif
        <b>Payment Status:</b> Hold <br/>
        <b>Transaction Id:</b> {{  $details['advertisement']['transaction_id'] }}<br/>
     </p>
    </div>
  </body>
</html>