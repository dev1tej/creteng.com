<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdvComment extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 
        'user_id', 'adv_id', 'comment_id', 'comments'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        
    ];

    /**
     * Get the user that owns the comment.
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    /**
     * Get the post that owns the comment.
     */
    public function advertisement()
    {
        return $this->belongsTo('App\Advertisement', 'adv_id', 'id');
    }
}
