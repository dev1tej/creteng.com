<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ config('app.name', 'Creteng') }}</title>
  </head>
  <body>
    <div>
      <p><b><h5>Hi Sir</h5></b></p>
      <p><b>{{  $details['advertiser']['name'] }}</b> has renewed his/her subscription plan at Creteng.</p>
    </div>
    <div>
      <p><b>Detail:</b><br/>
        <b>Advertiser Name:</b> {{  $details['advertiser']['name'] }} <br/>
        <b>Advertiser Email:</b> {{  $details['advertiser']['email'] }} <br/>
        <b>Advertiser Contact:</b> {{  $details['advertiser']['contact'] }} <br/>
        <b>Ad Publishable Locations:</b><br/>
        @php $no_of_publication_location = 0; @endphp
        @foreach ($details['advertisement']['publishable_locations'] as $publishable_location)
        @php $no_of_publication_location++; @endphp
        {{ $no_of_publication_location }}. Country: {{  $publishable_location['country'] }} State: {{  $publishable_location['state'] }}, <br/>
        @endforeach
        <br/>
        <b>Plan:</b> {{  $details['advertisement']['plan']['plan_name'] }} <br/>
        <b>Plan Duration:</b> {{  $details['advertisement']['plan_duration']['plan_duration'] }} <br/>
        <b>Fixed Services:</b> {{  $details['advertisement']['plan_fixed_service']['fixed_services'] }} <br/>
        @if(!is_null($details['advertisement']['plan_opt_service']))
        <b>Optional Services:</b> {{  $details['advertisement']['plan_opt_service']['optional_services'] }} <br/>
        @endif
        <b>Number of Publication Location:</b> {{ $no_of_publication_location }} <br/>
        <b>Total Paid:</b> ${{ $details['advertisement']['amount'] }} <br/>
        <b>Payment Status:</b> Charged <br/>
        <b>Transaction Id:</b> {{  $details['advertisement']['transaction_id'] }}<br/>
      </p>
    </div>
  </body>
</html>