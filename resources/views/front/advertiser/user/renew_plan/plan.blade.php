@extends('front.layouts.master')
@section('content')
<style type="text/css">
.fixed-top {
  position: relative;
}
#footer .footer-top {
 padding: 50px 0 0 0;
 top: 0; 
}
#header.header-scrolled{
  position: fixed;
  top: 0;
  right: 0;
  left: 0;
  z-index: 1030;
}
</style>
<section class="inner-page">
  @php
    use App\Http\Controllers\front\AdvertiserController;
    use App\Countries;  
    use App\States; 
    $usAndEuCountries = new AdvertiserController;
    $usAndEuCountries = $usAndEuCountries->usAndEuCountries();
    $countries_details = Countries::all();
    $countries = array();
    foreach($countries_details as $country) {
      if (in_array($country->name, $usAndEuCountries)) {
        $countries[] = $country->name;
      }
    }
    
    $united_states = Countries::where('name', 'UNITED STATES')->first();
    $states_details = States::where('country_id', $united_states->id)->get();
    $states = array();
    foreach ($states_details as $state) {
      $states[] = $state->name;
    }
    rsort($states);
  @endphp
@if(Session::has('flash_message'))
  <div class="alert alert-danger"><span class="glyphicon"></span><em> {!! session('flash_message') !!}</em></div>
@endif
<div class="container">
  <div class="row">
    <div class="col-lg-12 margin-tb">
      <div class="pull-left">
        <h2>Select Plan</h2>
        <h4 id="error" class="text-danger font-weight-bold"></h4>
        <span class="text-danger font-weight-bold">All fields marked with * are required</span>
      </div>
    </div>
  </div>
  @if (count($errors) > 0)
  <div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
      @foreach ($errors->all() as $error)
      <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
  @endif
  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
      <div class="card">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12">
            <h4 class="card-header"><span class="text-danger font-weight-bold">*</span>Select One Plan</h4>
            <div class="card-body">
            @foreach($ad_plans as $ad_plan)
              <strong>{{ $ad_plan->plan_name }}:</strong>
              <input type="radio" id="{{ $ad_plan->id }}" name="radio_plan_name"/>
              <br/><br/>
            @endforeach
            </div>
          </div>
        </div>
      </div>
      <br/>
    </div>
  </div>
  <form action="{{ route('postRenewPlanSelectPlan') }}" method="post">
    @csrf
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12">
    
        @foreach($ad_plans as $ad_plan)
        <div name="radio_pl_name" id="{{ $ad_plan->id }}" class="unique">
          <br/>
          <div class="card">
            <div class="row">
              <div class="col-xs-12 col-sm-12 col-md-12">
                <br/>
                <div class="card-header">
                  <div class="row">
                    <div class="col-xs-12 col-sm-4 col-md-4">
                      <strong><span class="text-danger font-weight-bold">*</span>Plan Name:</strong>
                    </div>
                    <div class="col-xs-12 col-sm-8 col-md-8">
                      <p>
                        {{ $ad_plan->plan_name }}
                        <input type="radio" name="plan_id" value="{{ $ad_plan->id }}"/>
                      </p>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-xs-12 col-sm-4 col-md-4">
                      <strong></span>Available Services:</strong>
                    </div>
                    <div class="col-xs-12 col-sm-8 col-md-8">
                      @foreach($ad_plan->fixServices as $fixService)
                      <p>*{{ $fixService->fixed_services }}</p>
                      @endforeach
                    </div>
                  </div>
                </div>

                <div class="card-body">
                  <strong><span class="text-danger font-weight-bold">*</span>Select a Plan Duration:</strong><br/><br/>
                  <table class="table table-bordered table-striped table-hover">
                    <thead>
                      <tr>
                        <th>Plan Duration</th>
                        <th>Price (in USD)</th>
                        <th><span class="text-danger font-weight-bold">*</span>Select</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($ad_plan->planDuration as $planDuration)
                      <tr>
                        <td>{{ $planDuration->plan_duration }}</td>
                        <td>{{ $planDuration->price }}</td>
                        <td><input type="radio" id="{{ $planDuration->id }}" name="sub_plan_id" value="{{ $planDuration->id }}"/></td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
          <br/>
        </div>
        @endforeach
        <div>
          <strong>Select Optional Services:</strong><br/><br/>
          <table class="table table-bordered table-striped table-hover">
            <thead>
              <tr>
                <th>Optional Service</th>
                <th>Service Cost</th>
                <th>Cost Period</th>
                <th>Select</th>
              </tr>
            </thead>
            <tbody>
              @foreach($plan_opt_services as $plan_opt_service)
              <tr>
                <td>{{ $plan_opt_service->optional_services }}</td>
                <td>{{ $plan_opt_service->aditional_price }}</td>
                <td>{{ $plan_opt_service->aditional_price_duration }}</td>
                <td><input type="checkbox" id="{{ $plan_opt_service->id }}" name="opt_service_ids[]" value="{{ $plan_opt_service->id }}"/></td>
              </tr>
              @endforeach
            </tbody>
            <tfoot></tfoot>
          </table>
          <br/>
        </div>
        <div class="card select-location">
          <div class="form-group row">
            <label class="col-sm-6 col-form-label">Select Number of Advertisement Publication State(s) for US or Countries for EU</label>
            <div class="col-sm-6">
              <select name="no_of_publication_location" class="form-control form-control-sm">
                @for($i = 1; $i <= 10; $i++)
                <option value="{{$i}}">{{$i}}</option>
                @endfor
              </select>
              <span class="text-info font-weight-bold">Total Subscription Amount = No. of States * Subscription Price</span>
            </div>
          </div>
        </div>

        <div class="row location">
          <div class="col-xs-12 col-sm-12 col-md-12">
            <table class="table table-bordered table-striped table-hover">
              <thead>
                <tr>
                  <th>Serial No.</th>
                  <th><span class="text-danger font-weight-bold">*</span>Select Ad Publication Country</th>
                  <th><span class="text-danger font-weight-bold">*</span>Select Ad Publication States/Countries</th>
                </tr>
              </thead>
              <tbody>
                {{--@php $no_of_publication_location = 2 @endphp--}}
                @for($i = 1; $i <= 1; $i++)
                <tr>
                  <td>{{ $i }}</td>
                  <td id="country{{ $i }}">
                    <select name="countries[]" class="form-control country">
                      @foreach ($countries as $country)
                      @if($country == 'UNITED STATES')
                      <option selected value="{{ $country }}" {{ old('country') == $country ? 'selected' : '' }}>{{ $country }}</option>
                      @else
                      <option value="{{ $country }}" {{ old('country') == $country ? 'selected' : '' }}>{{ $country }}</option>
                      @endif
                      @endforeach
                    </select>
                  </td>
                  <td id="state{{ $i }}">
                    <select name="states[]" class="form-control state">
                      @foreach ($states as $state)
                      <option selected value="{{ $state }}" {{ old('state') == $state ? 'selected' : '' }}>{{ $state }}</option>
                      @endforeach
                    </select>
                  </td>
                </tr>
                @endfor
              </tbody>
              <tfoot></tfoot>
            </table>
            <span id="location_error" class="text-danger font-weight-bold"></span>
          </div>
        </div>
        <div>
          <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <button type="submit" id="submit" class="btn btn-primary">Next</button>
          </div>
        </div>
      </div>
    </div>
  </form>
</div>
@endsection
{{--<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>--}}
<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/renew-plan-select-plan.js')}}"></script>