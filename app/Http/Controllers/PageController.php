<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Page;

class PageController extends Controller
{
    public function showAllPages()
    {
        $pages = Page::all();
        return view('admin.pages.index',compact('pages'));
    }

    public function editPage($page_id)
    {
        $page = Page::find($page_id);
        return view('admin.pages.edit',compact('page'));
    }

    public function updatePage(Request $request, $page_id)
    {
        //echo"<pre>";
        //print_r($request->all());
        //exit();
		/* Check Validation */
        $request->validate([
            'page_title' => 'required|max:255',
            'page_data' => 'required',
		]);
        $input = array();
        $input['page_title'] = $request->page_title;
        $input['page_data'] = $request->page_data;
        $page = Page::find($page_id)->update($input);
		
		return redirect(route('showAllPages'))->with('success','Page updated succesfully.');
    }
}
