<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTaggedTypeToPostsTaggedsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('posts_taggeds', function (Blueprint $table) {
            $table->string('tagged_type', 10)
                ->after('tagged_user')
                ->comment('normal/challenge');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('posts_taggeds', function (Blueprint $table) {
            $table->dropColumn('tagged_type');
        });
    }
}
