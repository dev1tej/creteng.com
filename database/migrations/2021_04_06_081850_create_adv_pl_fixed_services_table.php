<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdvPlFixedServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adv_pl_fixed_services', function (Blueprint $table) {
            $table->id();
            $table->foreignId('plan_id');
            $table->string('fixed_services', 255)->nullable();
            $table->timestamps();

            $table->foreign('plan_id')
                ->references('id')
                ->on('adv_plans')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('adv_pl_fixed_services');
    }
}
