<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdvPublicationLocation extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 
        'advertiser', 'service_id', 'country', 'state', 'postal_code', 'lat', 'long'
    ];

    /**
     * Get the plan.
     */
    public function advertiser()
    {
        return $this->belongsTo('App\AdvUser', 'advertiser', 'id');
    }

    /**
     * Get the available service for the publication location.
     */
    public function services()
    {
        return $this->belongsTo('App\AdvAvailableService', 'service_id', 'id');
    }
}
