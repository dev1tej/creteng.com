<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdvPlan extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 
        'plan_name'
    ];

    /**
     * Get the posts record associated with the user.
     */
    public function fixServices()
    {
        return $this->hasMany('App\AdvPlFixedService', 'plan_id', 'id');
    }

    /**
     * Get the posts record associated with the user.
     */
    public function planDuration()
    {
        return $this->hasMany('App\AdvPlDuration', 'plan_id', 'id');
    }
}
