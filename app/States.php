<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class States extends Model
{
    protected $fillable = [
        'name',
        'country_id'
    ];

    /**
     * Get the Country for the states.
     */
    public function user()
    {
        return $this->belongsTo('App\Countries', 'country_id', 'id');
    }

}
