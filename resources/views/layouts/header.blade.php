<!-- ======= Header ======= -->
  <header class="headerbg">
    <div class="container text-center">
      <div class="logo">
        <a href="{{ url('/') }}"><img src="{{ asset('assets/img/logo.png') }}" class="img-fluid" alt="{{ config('app.name', 'Creteng') }}"></a>        
      </div>
    </div>
</header>
<!-- End Header -->