@extends('admin.layouts.master')
@section('content')
<div class="container">
  <div class="row">
    <div class="col-lg-12 margin-tb">
      <div class="pull-left">
        <h2>View Challenge</h2>
      </div>
    </div>
  </div>
  @if (count($errors) > 0)
  <div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
      @foreach ($errors->all() as $error)
      <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
  @endif
  <div id="error"></div>
  <section class="content">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <div class="row">
              <div class="pull-right col-lg-7">
                <div class="pull-right">
                  <a class="btn btn-primary" href="{{ route('showAllChallenges') }}"> Back</a>
                </div>
              </div>
            </div>
          </div>
          <div class="card-body">
            <div class="row" style="max-width: 100%;">
              <div class="col-12">
                <div class="card">
                  <div class="card-header">
                    <img src="{{ $challenge['posted_by']['profile_pic'] }}" class="img-circle" alt="User Image" style="height:8%;width:8%">
                    <a href="{{ route('showChallengedSpecificCustomer', [$challenge['id'], $challenge['posted_by']['id']]) }}">{{ $challenge['posted_by']['name'] }}</a>
                  </div>
                  <div class="card-body">
                    <div class="col-md-12">
                      <p class="card-text"><strong><h4>{{ ucfirst($challenge['post_type']) }} Challenge</h4></strong></p>
                      @if($challenge['post_binary_mime_type'] == 'image')
                      <img src="{{ $challenge['post_binary'] }}" alt="{{ $challenge['post_binary'] }}" class="card-img" style="width:50%;height:50%;"/>
                      @elseif($challenge['post_binary_mime_type'] == 'video')
                      <video class="img-fluid" controls>
                        <source src="{{ $challenge['post_binary'] }}" alt="{{ $challenge['post_binary'] }}"/>
                      </video>
                      @endif
                      <p class="card-text">{{ $challenge['post_text'] }}</p>
                      <span class="count-icon">
                        <i class="fa fa-thumbs-up" title="Total Likes" style="font-size:30px;color:DodgerBlue"></i>
                        <span class="count">{{ $challenge['post_likes']['total_likes'] }}</span>
                      </span>
                      <span class="count-icon" style="margin-left: 20px">
                        <i class="fa fa-1x fa-comment" title="Total Comments" style="font-size:30px;color:DodgerBlue"></i>
                        <span class="count">{{ $challenge['post_comments']['total_comments'] }}</span>
                      </span>
                      <span class="count-icon" style="margin-left: 20px">
                        <i class="fa fa-save" title="Total Saved" style="font-size:30px;color:DodgerBlue"></i>
                        <span class="count">{{ $challenge['post_saved']['total_saved'] }}</span>
                      </span>
                      <span class="count-icon" style="margin-left: 20px">
                        <img src="{{ asset('fonts/challenge.svg') }}" title="Total Challenges" style="font-size:20px;color:DodgerBlue">
                        <span class="count">{{ $challenge['tagged_users']['challenges_count'] }}</span>
                      </span>
                      @if($challenge['post_binary_mime_type'] == 'video')
                      <span class="count-icon" style="margin-left: 20px">
                        <i class="fas fa-eye" title="Total Video Views" style="font-size:30px;color:DodgerBlue"></i>
                        <span class="count">{{ $challenge['total_video_views'] }}</span>
                      </span>
                      @endif
                      <p class="card-text"><small class="text-muted">Last Updated {{ $challenge['updated_at'] }}</small></p>
                    </div>
                    <hr/>
                    <div class="card">
                      <div class="card-body">
                        <strong>Challenged Users List:</strong>
                        @foreach($challenge['tagged_users']['users'] as $challenged_user)
                        @if($challenge['tagged_users']['challenges_count'] != 0)
                        <a href="{{ route('showChallengedSpecificCustomer', [$challenge['id'], $challenged_user['id']]) }}">{{ $challenged_user['name'] }}</a>
                        @if($challenge['tagged_users']['challenges_count'] > 1)
                        , 
                        @endif
                        @endif
                        @endforeach
                        <hr/>
                        <strong>Liked By:</strong>
                        @foreach($challenge['post_likes']['liked_by'] as $liked_by)
                        <a href="{{ route('showChallengedSpecificCustomer', [$challenge['id'], $liked_by['id']]) }}">{{ $liked_by['name'] }}</a>
                        @if($challenge['post_likes']['total_likes'] > 1)
                        , 
                        @endif
                        @endforeach
                        <hr/>
                        <strong>Saved By:</strong>
                        @foreach($challenge['post_saved']['saved_by'] as $saved_by)
                        <a href="{{ route('showChallengedSpecificCustomer', [$challenge['id'], $saved_by['id']]) }}">{{ $saved_by['name'] }}</a>
                        @if($challenge['post_saved']['total_saved'] > 1)
                        , 
                        @endif
                        @endforeach
                        <hr/>
                      </div>
                    </div>
                    <div class="card">
                      <div class="card-header">
                        <strong>Reports:</strong>
                      </div>
                      <div class="card-body">
                        <div class="col-12">
                          @if(isset($challenge['reports']))
                          @foreach($challenge['reports'] as $report)
                          <div class="row">
                            <div class="col-3">
                              <img src="{{ $report['user']['profile_pic'] }}" class="img-circle" alt="User Image" style="height:40px;width:40%"><br>
                              <a href="{{ route('showChallengedSpecificCustomer', [$challenge['id'], $report['user']['id']]) }}">{{ $report['user']['name'] }}</a>
                            </div>
                            <div class="col-9">
                              <p class="card-text">
                                {{ $report['report_text'] }}
                              </p>
                              <p class="card-text"><small class="text-muted">Last Updated {{ $report['updated_at'] }}</small></p>
                              <hr/>
                            </div>
                          </div>
                          <hr/>
                          @endforeach
                          @endif
                        </div>
                      </div>
                    </div>
                    <div class="card">
                      <div class="card-header">
                        <strong>Comments:</strong>
                      </div>
                      <div class="card-body">
                        <div class="col-12">
                          @if(isset($challenge['post_comments']['comment_detail']))
                          @foreach($challenge['post_comments']['comment_detail'] as $comment_detail)
                          <div class="row comment comment-remove">
                            <div class="col-2">
                              <img src="{{ $comment_detail['comment_by']['profile_pic'] }}" class="img-circle" alt="User Image" style="height:40px;width:40%"><br>
                              <a href="{{ route('showChallengedSpecificCustomer', [$challenge['id'], $comment_detail['comment_by']['id']]) }}">{{ $comment_detail['comment_by']['name'] }}</a>
                            </div>
                            <div class="col-10">
                              <p class="card-text">
                                {{ $comment_detail['comments'] }}
                                <button type="button" class="close close-comment" aria-label="Close" id="{{ $comment_detail['id'] }}" title="Delete Comment">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                              </p>
                              <p class="card-text"><small class="text-muted">Last Updated {{ $comment_detail['updated_at'] }}</small></p>
                              <hr/>
                              @if(isset($comment_detail['comment_replies']['replies']))
                              @foreach($comment_detail['comment_replies']['replies'] as $comment_reply)
                              <div class="row comment-reply-remove">
                                <div class="col-3">
                                  <img src="{{ $comment_reply['reply_by']['profile_pic'] }}" class="img-circle" alt="User Image" style="height:40px;width:40%"><br>
                                  <a href="{{ route('showChallengedSpecificCustomer', [$challenge['id'], $comment_reply['reply_by']['id']]) }}">{{ $comment_reply['reply_by']['name'] }}</a>
                                </div>
                                <div class="col-9">
                                  <p class="card-text">
                                    {{ $comment_reply['comment_reply'] }}
                                    <button type="button" class=" close close-reply" aria-label="Close" id="{{ $comment_reply['id'] }}" title="Delete Comment Reply">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                  </p>
                                  <p class="card-text"><small class="text-muted">Last Updated {{ $comment_reply['updated_at'] }}</small></p>
                                </div>
                              </div>
                              <hr/>
                              @endforeach
                              @endif
                            </div>
                          </div>
                          <hr/>
                          @endforeach
                          @endif
                        </div>
                      </div>
                    </div>
                    <div class="card">
                      <div class="card-header">
                        <strong>Challenge Replies:</strong>
                      </div>
                      <div class="card-body">
                        <div class="row" style="max-width: 100%;">
                          <div class="col-12">
                            @foreach($challenge['post_challenge_reply']['challenge_reply'] as $challenge_reply)
                            <div class="card">
                              <div class="card-header">
                                <img src="{{ $challenge_reply['challenge_reply_by']['profile_pic'] }}" class="img-circle" alt="User Image" style="height:8%;width:8%">
                                <a href="{{ route('showChallengedSpecificCustomer', [$challenge['id'], $challenge_reply['challenge_reply_by']['id']]) }}">{{ $challenge_reply['challenge_reply_by']['name'] }}</a>
                              </div>
                              <div class="card-body">
                                <div class="row" style="max-width: 100%;">
                                  <div class="col-md-12">
                                    <p class="card-text"><strong><h4>{{ ucfirst($challenge_reply['reply_type']) }} Challenge Reply</h4></strong></p>
                                    @if($challenge_reply['reply_binary_mime_type'] == 'image')
                                    <img src="{{ $challenge_reply['reply_binary'] }}" alt="{{ $challenge_reply['reply_binary'] }}" class="card-img" style="width:50%;height:50%;"/>
                                    @elseif($challenge_reply['reply_binary_mime_type'] == 'video')
                                    <video class="img-fluid" controls>
                                      <source src="{{ $challenge_reply['reply_binary'] }}" alt="{{ $challenge_reply['reply_binary'] }}"/>
                                    </video>
                                    @endif
                                    <p class="card-text">{{ $challenge_reply['reply_text'] }}</p>
                                    <span class="count-icon">
                                      <i class="fa fa-thumbs-up" title="Total Likes" style="font-size:30px;color:DodgerBlue"></i>
                                      <span class="count">{{ $challenge_reply['reply_likes']['total_likes'] }}</span>
                                    </span>
                                    <span class="count-icon" style="margin-left: 20px">
                                      <i class="fa fa-1x fa-comment" title="Total Comments" style="font-size:30px;color:DodgerBlue"></i>
                                      <span class="count">{{ $challenge_reply['reply_comments']['total_comments'] }}</span>
                                    </span>
                                    <span class="count-icon" style="margin-left: 20px">
                                      <i class="fa fa-save" title="Total Saved" style="font-size:30px;color:DodgerBlue"></i>
                                      <span class="count">{{ $challenge_reply['reply_saved']['total_saved'] }}</span>
                                    </span>
                                    @if($challenge_reply['reply_binary_mime_type'] == 'video')
                                    <span class="count-icon" style="margin-left: 20px">
                                      <i class="fas fa-eye" title="Total Video Views" style="font-size:30px;color:DodgerBlue"></i>
                                      <span class="count">{{ $challenge_reply['total_video_views'] }}</span>
                                    </span>
                                    @endif
                                    <p class="card-text"><small class="text-muted">Last Updated {{ $challenge_reply['updated_at'] }}</small></p>
                                  </div>
                                </div>
                                <div class="card">
                                  <div class="card-body">
                                    <strong>Reply Liked By:</strong>
                                    @foreach($challenge_reply['reply_likes']['liked_by'] as $liked_by)
                                    <a href="{{ route('showChallengedSpecificCustomer', [$challenge['id'], $liked_by['id']]) }}">{{ $liked_by['name'] }}</a>
                                    @if($challenge_reply['reply_likes']['total_likes'] > 1)
                                    , 
                                    @endif
                                    @endforeach
                                    <hr/>
                                    <strong>Reply Saved By:</strong>
                                    @foreach($challenge_reply['reply_saved']['saved_by'] as $saved_by)
                                    <a href="{{ route('showChallengedSpecificCustomer', [$challenge['id'], $saved_by['id']]) }}">{{ $saved_by['name'] }}</a>
                                    @if($challenge_reply['reply_saved']['total_saved'] > 1)
                                    , 
                                    @endif
                                    @endforeach
                                    <hr/>
                                  </div>
                                </div>
                                <div class="card">
                                  <div class="card-header">
                                    <strong>Reports:</strong>
                                  </div>
                                  <div class="card-body">
                                    <div class="col-12">
                                      @if(isset($challenge_reply['reports']))
                                      @foreach($challenge_reply['reports'] as $report)
                                      <div class="row">
                                        <div class="col-3">
                                          <img src="{{ $report['user']['profile_pic'] }}" class="img-circle" alt="User Image" style="height:40px;width:40%"><br>
                                          <a href="{{ route('showChallengedSpecificCustomer', [$challenge['id'], $report['user']['id']]) }}">{{ $report['user']['name'] }}</a>
                                        </div>
                                        <div class="col-9">
                                          <p class="card-text">
                                            {{ $report['report_text'] }}
                                          </p>
                                          <p class="card-text"><small class="text-muted">Last Updated {{ $report['updated_at'] }}</small></p>
                                          <hr/>
                                        </div>
                                      </div>
                                      <hr/>
                                      @endforeach
                                      @endif
                                    </div>
                                  </div>
                                </div>
                                <div class="card">
                                  <div class="card-header">
                                    <strong>Reply Comments:</strong>
                                  </div>
                                  <div class="card-body">
                                    <div class="col-12">
                                      @if(isset($challenge_reply['reply_comments']['comment_detail']))
                                      @foreach($challenge_reply['reply_comments']['comment_detail'] as $comment_detail)
                                      <div class="row challenge-reply-comment challenge-reply-comment-remove">
                                        <div class="col-2">
                                          <img src="{{ $comment_detail['comment_by']['profile_pic'] }}" class="img-circle" alt="User Image" style="height:40px;width:40%"><br>
                                          <a href="{{ route('showChallengedSpecificCustomer', [$challenge['id'], $comment_detail['comment_by']['id']]) }}">{{ $comment_detail['comment_by']['name'] }}</a>
                                        </div>
                                        <div class="col-10">
                                          <p class="card-text">
                                            {{ $comment_detail['challenge_comment_reply'] }}
                                            <button type="button" class="close close-challenge-reply-comment" aria-label="Close" id="{{ $comment_detail['id'] }}" title="Delete Comment">
                                              <span aria-hidden="true">&times;</span>
                                            </button>
                                          </p>
                                          <p class="card-text"><small class="text-muted">Last Updated {{ $comment_detail['updated_at'] }}</small></p>
                                          <hr/>
                                          @if(isset($comment_detail['comment_replies']['replies']))
                                          @foreach($comment_detail['comment_replies']['replies'] as $comment_reply)
                                          <div class="row challenge-reply-comment-reply-remove">
                                            <div class="col-3">
                                              <img src="{{ $comment_reply['reply_by']['profile_pic'] }}" class="img-circle" alt="User Image" style="height:40px;width:40%"><br>
                                              <a href="{{ route('showChallengedSpecificCustomer', [$challenge['id'], $comment_reply['reply_by']['id']]) }}">{{ $comment_reply['reply_by']['name'] }}</a>
                                            </div>
                                            <div class="col-9">
                                              <p class="card-text">
                                                {{ $comment_reply['comment_challenge_reply_comment'] }}
                                                <button type="button" class="close close-challenge-reply-comment-reply" aria-label="Close" id="{{ $comment_reply['id'] }}" title="Delete Comment Reply">
                                                  <span aria-hidden="true">&times;</span>
                                                </button>
                                              </p>
                                              <p class="card-text"><small class="text-muted">Last Updated {{ $comment_reply['updated_at'] }}</small></p>
                                            </div>
                                          </div>
                                          <hr/>
                                          @endforeach
                                          @endif
                                        </div>
                                      </div>
                                      <hr/>
                                      @endforeach
                                      @endif
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            @endforeach
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<script>
  $( document ).ready(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    
    $(".comment").on("click", ".close-comment", function(b) {
      var comment_id = $(this).attr('id');

      $.ajax({
        type: "POST",
        url: '{{ route("deleteChallengeComment") }}',
        data: {'comment_id': comment_id},
        success: function(data){
          if(data == 'ok') {
            $("#"+comment_id).closest(".comment-remove").next('hr').remove();
            $("#"+comment_id).closest(".comment-remove").remove();
            $('#error').html('<div class="alert alert-danger">Comment deteted Successfully</div>');
          }
        }
      });
    });

    $(".comment").on("click", ".close-reply", function(b) {
      var comment_reply_id = $(this).attr('id');	
      
      $.ajax({
        type: "POST",
        url: '{{ route("deleteChallengeCommentReply") }}',
        data: {'comment_reply_id': comment_reply_id},
        success: function(data){
          if(data == 'ok') {
            $("#"+comment_reply_id).closest(".comment-reply-remove").next('hr').remove();
            $("#"+comment_reply_id).closest(".comment-reply-remove").remove();
            $('#error').html('<div class="alert alert-danger">Reply on Comment deteted Successfully</div>');
          }
        }
      });
    });

    $(".challenge-reply-comment").on("click", ".close-challenge-reply-comment", function(b) {
      var comment_id = $(this).attr('id');	
      	
      $.ajax({
        type: "POST",
        url: '{{ route("deleteChallengeReplyComment") }}',
        data: {'comment_id': comment_id},
        success: function(data){
          if(data == 'ok') {
            $("#"+comment_id).closest(".challenge-reply-comment-remove").next('hr').remove();
            $("#"+comment_id).closest(".challenge-reply-comment-remove").remove();
            $('#error').html('<div class="alert alert-danger">Comment on challenge reply deteted Successfully</div>');
          }
        }
      });
    });

    $(".challenge-reply-comment").on("click", ".close-challenge-reply-comment-reply", function(b) {
      var comment_reply_id = $(this).attr('id');	
      
      $.ajax({
        type: "POST",
        url: '{{ route("deleteChallengeReplyCommentReply") }}',
        data: {'comment_reply_id': comment_reply_id},
        success: function(data){
          if(data == 'ok') {
            $("#"+comment_reply_id).closest(".challenge-reply-comment-reply-remove").next('hr').remove();
            $("#"+comment_reply_id).closest(".challenge-reply-comment-reply-remove").remove();
            $('#error').html('<div class="alert alert-danger">Reply on Comment at challenge reply deteted Successfully</div>');
          }
        }
      });
    });
  });
</script>
@endsection