<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ config('app.name', 'Creteng') }}</title>
  </head>
  <body>
    <div>
      <p><b><h5>Hello {{  $details['advertiser']['name'] }}<h5></b></p>
      <p><b>Your below Subscription with Creteng is about to expire.</b></p>
    </div>
    <div>
      <p><b>Plan Detail:</b><br/>
        <b>Plan:</b> {{  $details['plan']['plan_name'] }} <br/>
        <b>Plan Duration:</b> {{  $details['plan_duration']['plan_duration'] }} <br/>
        <b>Fixed Services:</b> {{  $details['plan_fixed_service']['fixed_services'] }} <br/>
        @if(!is_null($details['plan_opt_service']))
          <b>Optional Services:</b> {{  $details['plan_opt_service']['optional_services'] }} <br/>
        @endif
        <b>Subscription Date:</b> {{  $details['ad_payment']['publish_date'] }} <br/>
        <b>Expiry Date:</b> {{  $details['ad_payment']['expiry_date'] }}<br/>
     </p>
    </div>
  </body>
</html>