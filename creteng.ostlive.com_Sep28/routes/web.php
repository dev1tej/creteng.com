<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('comingSoon');
})->name('comingSoon');

Auth::routes();

Route::group(['middleware' => ['auth','is_admin']], function() {
	
	Route::get('/admin/dashboard', 'HomeController@adminHome')->name('dashboard');
	
    Route::resource('roles','RoleController');
    Route::resource('users','UserController');
  
	Route::get('/admin/users/edit/{users}','UserController@edituser')->name('userEdit');
	Route::post('/admin/users/update/{users}','UserController@updateuser')->name('userFormUpdate');
    Route::post('/admin/users/status', 'UserController@changeStatus')->name('userChangeStatus');
	Route::get('/admin/password/edit/{user}','UserController@adminpasswordedit')->name('adminPasswordEdit');//
	Route::post('/admin/password/update/{user}','UserController@adminpasswordupdate')->name('adminPasswordUpdate');//
	
});
		

