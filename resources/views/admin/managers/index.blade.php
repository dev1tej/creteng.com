@extends('admin.layouts.master')
@section('content')
<div class="container">
  <div class="row">
    <div class="col-lg-12 margin-tb">
      <div class="pull-left">
        <h2>Managers Management</h2>
      </div>
    </div>
  </div>
  @if($message = Session::get('success'))
  <div class="alert alert-success">
    <p>{{ $message }}</p>
  </div>
  @endif
  @if($message = Session::get('danger'))
  <div class="alert alert-danger">
    <p>{{ $message }}</p>
  </div>
  @endif
  @if(isset($success))
  <div class="alert alert-success">
    <p>{{ $success }}</p>
  </div>
  @endif
  <section class="content">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title"></h3>
            <div class="pull-right">
              <a class="btn btn-success" href="{{ route('createManager') }}"> Create New Manager</a>
            </div>
          </div>
          <div class="card-body">
            <table id="managersData" class="table table-bordered table-striped table-hover">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Roles</th>
                  <th title="Blue button indicate user is block" >Blocked</th>
                  <th width="280px">Action</th>
                </tr>
              </thead>
              <tbody>
                @php $srno = '1'; @endphp
                @forelse ($data as $key => $user)
                <tr>
                  <td>@php echo $srno; @endphp</td>
                  <td><a href="{{ route('showManager',$user->id) }}">{{ $user->name }}</a></td>
                  <td>{{ $user->email }}</td>
                  <td>
                    @php
                    $managers=env("APP_MANAGER");
                    @endphp
                    @if(!empty($user->getRoleNames()))
                    @foreach($user->getRoleNames() as $v)
                    @if($v == $managers)
                    <label class="badge badge-success">{{ $v }}</label>
                    @endif
                    @endforeach
                    @endif
                  </td>
                  <td>
                    <center>
                      <div class="custom-control custom-switch">
                        <input type="checkbox" class="custom-control-input" id="{{ $user->id }}" data-id="{{ $user->blocked }}" {{ $user->blocked ? 'checked' : '' }}>		
                        <label class="custom-control-label" for="{{ $user->id }}" >&nbsp;</label>
                      </div>
                    </center>
                  </td>
                  <td>
                    <a class="btn btn-primary" href="{{ route('editManager',$user->id) }}">Edit</a>
                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#exampleModal{{ $user->id }}" data-id="{{ $user->id }}" >Delete</button>
                    @if($user->email_verified_at == null)
                    <button style="font-size:18px">
                    <a href="{{ route('sendVerificationEmail', $user->id) }}">
                    <i class="fa fa-envelope" title="send verification mail"></i>
                    </a>	
                    </button>
                    @endif
                  </td>
                </tr>
                <!-- Delete Manager Modal Starts -->
                <div class="modal fade" id="exampleModal{{ $user->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                  <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Warning</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                        If you want to delete this manager , all information will be lost of this user
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                        <a href="{{ route('deleteManager', $user->id )}}"><button type="button" class="btn btn-danger">Yes, go forward</button></a>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- Delete Manager Modal Ends -->
                @php $srno++; @endphp
                @empty
                No Manager Found
                @endforelse
              </tbody>
              <tfoot>
                <tr>
                  <th>No</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Roles</th>
                  <th>Blocked</th>
                  <th>Action</th>
                </tr>
              </tfoot>
            </table>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

<script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script>
  $(function () {
    $('#managersData').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
	  
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    
    $('body').on("click.bs.toggle", ".custom-control-input", function(b) {
		var status = $(this).is(":checked") == true ? 1 : 0;
		var cat_id = $(this).attr('id');		
		$.ajax({
            type: "POST",
            url: '{{ route("changeStatusManager") }}',
            data: {'status': status, 'user_id': cat_id},
            success: function(data){
				$('.alert-success').show();
				$('.alert-success').html(data.success);
            }
        });
    }); 
  });
</script>

@endsection
