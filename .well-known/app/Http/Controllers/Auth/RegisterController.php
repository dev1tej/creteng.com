<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
//use App\Profile;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Spatie\Permission\Models\Role;
use Illuminate\Http\Request;
use Auth;
use Stripe;
use Customer;
use Charge;
use Laravel\Cashier\Exceptions\IncompletePayment;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

	public function showRegistrationForm()
	{
		$stripe = new \Stripe\StripeClient(env('STRIPE_SECRET'));
        
		/* Get Subscriptions Plans from Stripe */
        $subplans= $stripe->plans->all();	
		$plans = array();
		foreach($subplans->data as $plan){
			$interval=$plan->interval;
			$plans[$plan->id]=strtoupper($plan->currency).' '.($plan->amount/100).'/'.($interval);
		}
		/* Get Coupans form stripe */
        $coupons_data= $stripe->coupons->all();
		$coupans = array();
		//echo"<pre>"; print_r($coupons_data->data); die;
		foreach($coupons_data->data as $coupan){
			if($coupan->valid == 1){
				$coupans[$coupan->id]= $coupan->name;
			}
        }		
		/* Get Roles by ENV file */
		$superadmin=env("APP_SUPERADMIN");
		$admin=env("APP_ADMIN");
		$provider=env("APP_PROVIDER");
		$customer=env("APP_CUSTOMER");
		$roles = Role::whereIn('name', [$customer, $provider])->pluck('name','name');
		//dd(compact('roles', 'plans','coupans'));
		return view('auth.register', compact('roles','plans','coupans'));
	}

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(Request $request)
    {
		/*
        return Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'country_code' => ['required', 'integer', 'digits_between: 1,6'],
            'contact' => ['required', 'integer', 'digits_between: 10,12', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'roles' => ['required'],
        ]);*/
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function storeuser(Request $request)
    { 
		//echo "<pre>"; print_r($request->all()); die;
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'country_code' => ['required', 'integer', 'digits_between: 1,6'],
            'contact' => ['required', 'integer', 'digits_between: 10,12', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'roles' => ['required'],	
            'plan' => ['required'],		
            'payment_method' => ['required'],		
		]);
     
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'country_code' => $request->country_code,
            'contact' => $request->contact,
            'password' => Hash::make($request->password),
        ]);
        $user_id=$user->id;
        
        /* Assign user a role*/
        $user->assignRole($request->input('roles'));
        
        /* if user is provider then create profile as well */
        $provider=env("APP_PROVIDER");
        if($provider == $request->input('roles')){
			/* Check Validation */
			Validator::make($request->all(), [
				'name' => ['required', 'string', 'max:255'],
				'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
				'country_code' => ['required', 'integer', 'digits_between: 1,6'],
				'contact' => ['required', 'integer', 'digits_between: 10,12', 'unique:users'],
				'password' => ['required', 'string', 'min:8', 'confirmed'],	
				'profile_username' => ['required','regex:/^[\pL\s\-]+$/u','unique:profiles'],	
				'category' => ['required'],	
				'profile_img_path' => ['mimes:jpeg,png,jpg,gif','max:2048'],	
			]);
			
			/* if request has image then store it */
			if($request->has('profile_img_path')) {
				$image = $request->file('profile_img_path');
				$name = $request->name.'_'.time();
				$folder =  public_path('/profile_images/');
				$filePath = $name. '.' . $image->getClientOriginalExtension();
				$image->move($folder, $filePath);
			}else{
				$filePath="default.png";
			}
			
			$profile_username= $request->profile_username;
			$profile_category= $request->category;
			$dynamic_field= $request->dynamic;
			
			/* Create Profile of user 

			$profile = Profile::create(
				[
					'id' => $user_id,
					'profile_username' => $profile_username,
					'profile_img_path' => $filePath,
				]
			);			
            */
			/* Update Dynamic Data in Profile of user */
			if(isset($dynamic_field))	{	
				//$profile->where('id','=',$user_id)->update($dynamic_field);
			}
			
						
			
		
		}
		        
	}

}
