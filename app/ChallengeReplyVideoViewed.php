<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChallengeReplyVideoViewed extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 
        'reply_id', 'user_id'
    ];

    /**
     * Get the user that viewed the video.
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id ', 'id');
    }

    /**
     * Get the viewed post.
     */
    public function reply()
    {
        return $this->belongsTo('App\PostsTaggedReply', 'reply_id', 'id');
    }
}
