<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChallengeReplyVideoViewedsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('challenge_reply_video_vieweds', function (Blueprint $table) {
            $table->id();
            $table->foreignId('reply_id');
            $table->foreignId('user_id');
            $table->timestamps();

            $table->foreign('reply_id')
                ->references('id')
                ->on('posts_tagged_replies')
                ->onDelete('cascade');
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('challenge_reply_video_vieweds');
    }
}
