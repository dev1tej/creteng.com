@extends('admin.layouts.master')


@section('content')
<div class="container">
	<div class="row">
		<div class="col-lg-12 margin-tb">
			<div class="pull-left">
				<h2>Users Management</h2>
			</div>
			<div class="pull-right">
				<a class="btn btn-success" href="{{ route('users.create') }}"> Create New User</a>
			</div>
		</div>
	</div>


	@if ($message = Session::get('success'))
	<div class="alert alert-success">
	  <p>{{ $message }}</p>
	</div>
	@endif

	@if ($message = Session::get('danger'))
	<div class="alert alert-danger">
	  <p>{{ $message }}</p>
	</div>
	@endif


	<table class="table table-bordered">
	 <tr>
	   <th>No</th>
	   <th>Name</th>
	   <th>Email</th>
	   <th>Roles</th>
	   <th>Blocked</th>
	   <th width="280px">Action</th>
	 </tr>

	 @foreach ($data as $key => $user)

		<tr>
			<td>{{ ++$i }}</td>
			<td>{{ $user->name }}</td>
			<td>{{ $user->email }}</td>
			<td>
			@php
			$customers=env("APP_CUSTOMER");
			$managers=env("APP_MANAGER");
			@endphp
			@if(!empty($user->getRoleNames()))
				@foreach($user->getRoleNames() as $v)
				@if($v == $customers)
				<label class="badge badge-warning">{{ $v }}</label>
				@endif
				@if($v == $managers)
				<label class="badge badge-success">{{ $v }}</label>
				@endif
				@endforeach
			@endif
			</td>
			<td>		
				<center>
					<div class="custom-control custom-switch">
						<input type="checkbox" class="custom-control-input" id="{{ $user->id }}" data-id="{{ $user->blocked }}" {{ $user->blocked ? 'checked' : '' }}>
						<label class="custom-control-label" for="{{ $user->id }}">&nbsp;</label>
					</div>
				</center>
			</td>
			<td>
			<a class="btn btn-info" href="{{ route('users.show',$user->id) }}">Show</a>
			<a class="btn btn-primary" href="{{ route('users.edit',$user->id) }}">Edit</a>
			<!--button type="button" class="btn btn-danger" onclick="confirm('are you sure')" >Delete</button-->
				{!! Form::open(['method' => 'DELETE','route' => ['users.destroy', $user->id], 'style'=>'display:inline', 'id' => 'FormDeleteUser']) !!}
					{!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
				{!! Form::close() !!}
			</td>
		</tr>
	 @endforeach
	</table>

	{!! $data->render() !!}
</div>

<script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script>
  $(function () {
	  
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    
    $('body').on("click.bs.toggle", ".custom-control-input", function(b) {
		var status = $(this).is(":checked") == true ? 1 : 0;
		var cat_id = $(this).attr('id');		
		$.ajax({
            type: "POST",
            url: '{{ route("userChangeStatus") }}',
            data: {'status': status, 'user_id': cat_id},
            success: function(data){
				$('.alert-success').show();
				$('.alert-success').html(data.success);
            }
        });
    }); 

	$("#FormDeleteUser").submit(function (event) {
		var x = confirm("If you want to delete this user , all information will be lost of this user");
		if (x) {
			return true;
		}
		else {

			event.preventDefault();
			return false;
		}

	});
    
  });
</script>

@endsection
