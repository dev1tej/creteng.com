<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;

class Post extends Model
{
    use HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 
        'user_id', 
        'post_text', 
        'post_binary', 
        'post_binary_mime_type', 
        'post_type', 
        'include_challenges',
        'location'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        
    ];

    /**
     * Get the user record associated with the post.
     */
    public function postUser()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    /**
     * Get the post likes record associated with the post.
     */
    public function postLikes()
    {
        return $this->hasMany('App\PostLike', 'post_id', 'id');
    }

    /**
     * Get the post comments record associated with the post.
     */
    public function postComment()
    {
        return $this->hasMany('App\PostComment', 'post_id', 'id');
    }

    /**
     * Get the saved post record associated with the post.
     */
    public function postSave()
    {
        return $this->hasMany('App\SavePost', 'post_id', 'id');
    }

    
}
