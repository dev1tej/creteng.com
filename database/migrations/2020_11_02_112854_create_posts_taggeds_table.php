<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostsTaggedsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts_taggeds', function (Blueprint $table) {
            $table->id();
            $table->foreignId('post_id');
            $table->foreignId('tagged_by');
            $table->foreignId('tagged_user');
            $table->foreign('post_id')
                ->references('id')
                ->on('posts')
                ->onDelete('cascade');
            $table->foreign('tagged_by')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
            $table->foreign('tagged_user')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts_taggeds');
    }
}
