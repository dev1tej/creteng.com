<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Auth;

class IsProvider
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
         if(Auth::user()->hasRole('Providers')){
			 return $next($request);
		 }
         if(Auth::user()->hasRole('Admin') || Auth::user()->hasRole('Superadmin') || Auth::user()->hasRole('Manager')){
			 return redirect('/admin/dashboard');
		 }
		 return redirect(RouteServiceProvider::HOME);
    }
}
