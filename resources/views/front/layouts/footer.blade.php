<p class="text-info text-center font-weight-bold">For best results in Mac, Safari browser is recommended.</p>
  <footer class="footerbg">
    <div class="container">
    <div class="row">
      <div class="col-md-9 text-left">
        <p>Copyright &copy; @php echo date("Y") @endphp <a href="#">Creteng</a>.&nbsp; All rights reserved.</p>
      </div>
      <div class="col-md-3 text-right">
        <p>Developed by<a rel="nofollow" href="https://www.ost.agency/"><img src="{{url('assets/img/credit_logo.png')}}" alt="OpenSource Technologies" ></a></p> 
      </div>
    </div>
    </div>
  </footer>
