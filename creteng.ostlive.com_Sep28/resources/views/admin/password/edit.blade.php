@extends('admin.layouts.master')

@section('content')
<div class="container">
	
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <a href="{{ route('dashboard') }}"><button type="button" class="btn btn-secondary">Go back to Dashboard</button></a>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    
    @if(Session::has('success'))
		<div class="alert alert-success">
			{{ Session::get('success') }}
            @php
                Session::forget('success');
            @endphp
        </div>
    @endif
	<div class="card">
		<div class="card-body">
			<form method="POST" id="user-form" action="{{ route('adminPasswordUpdate', $id) }}">
			{{ csrf_field() }} 
			  <div class="form-group">
				<label for="password">New Password</label>
				<input type="password" class="form-control" id="password" name="password" autocomplete="new-password">
					@if ($errors->has('password'))
						<span class="text-danger">{{ $errors->first('password') }}</span>
					@endif
			  </div>
			  <div class="form-group">
				<label for="password-confirm">Confirm New Password</label>
				<input type="password" class="form-control" id="confirm-password" name="confirm-password" autocomplete="new-password">
			  </div>
			  <button type="submit" class="btn btn-primary">Update</button>
			</form>
		</div>
	</div>
@endsection
