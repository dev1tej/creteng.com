<!DOCTYPE html>
<html>
<style>
body, html {
  height: 100%;
  margin: 0;
}

.bgimg {
  background-image: url("{{ asset('image/photo-1507041957456-9c397ce39c97.jpg') }}");
  height: 100%;
  background-position: center;
  background-size: cover;
  position: relative;
  color: white;
  font-family: "Courier New", Courier, monospace;
  font-size: 25px;
}

.top {
  position: absolute;
  top: 10%;
  left: 40%;
  font-size: 1.5em;
  text-align: center;
}

.tagline {
  position: absolute;
  top: 30%;
  left: 50%;
  transform: translate(-50%, -50%);
  font-size: 0.8em;
  text-align: center;
}

.bottomleft {
  position: absolute;
  bottom: 20%;
  left: 30%;
  font-size: 1em;
}

.middle {
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  text-align: center;
  font-size: 1.5em;
}

hr {
  margin: auto;
  width: 40%;
}
</style>
<body>

<div class="bgimg">
  <div class="top">
    <p>Creteng</p>
  </div>
  <div class="tagline">
    <p>Be Creative and Challenge the World</p>
  </div>
  <div class="middle">
    <h1>COMING SOON</h1>
    
  </div>
  <div class="bottomleft">
    <p>By OpenSource Technologies </p>
  </div>
</div>

</body>
</html>
