<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use App\Advertisement;
use App\AdvAvailableService;
use App\AdsPayment;
use App\PublicationExpireCronTest;

class RefreshAdPublication extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'publication:refresh';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'To refresh advertisement publication after valid publication expire.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('publication:refresh Command Run successfully!');
        $ads_payment = AdsPayment::all();
        $date_now = date('Y-m-d');
        foreach ($ads_payment as $ad_payment) {
            if ($ad_payment->expiry_date < $date_now) {

                $adv_available_service = AdvAvailableService::where([
                    ['id', $ad_payment->service_id],
                    ['advertiser', $ad_payment->advertiser_id],
                    ['status', true],
                ])->first();
                if (!is_null($adv_available_service)) {
                    $adv_available_service->update([
                        'status' => false
                    ]);
                    $ads = Advertisement::where([
                        ['advertiser_id', $ad_payment->advertiser_id],
                        ['service_id', $adv_available_service->id],
                    ])->get();
                    foreach ($ads as $ad) {
                        $ad->update([
                            'paid' => false,
                            'published' => false,
                            'publish_date' => null,
                            'expiry_date' => null
                        ]);
                    }
                }
            }
        }
        PublicationExpireCronTest::create(['run_successfully' => true]);
        Log::info("publication:refresh is working fine!");
    }
}
