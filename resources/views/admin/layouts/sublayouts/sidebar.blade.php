<aside class="main-sidebar sidebar-dark-primary elevation-4">
  <!-- Brand Logo -->
  <a href="index.html" class="brand-link">
    <!--img src="{{--url('image/logo.png')--}}"
      alt="Find Beauty Pros"
      class="brand-image elevation-3"
      style="opacity: .8"-->
    @php 
    $superadmin=env("APP_SUPERADMIN"); 
    $managers=env("APP_MANAGER");
    @endphp
    @if(Auth::user()->hasRole($superadmin))
    <span class="brand-text font-weight-light">Administrator</span>
    @endif
    @if(Auth::user()->hasRole($managers))
    <span class="brand-text font-weight-light">Manager</span>
    @endif
  </a>
  <!-- Sidebar -->
  <div class="sidebar">
    <!-- Sidebar user (optional) -->
    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
          with font-awesome or any other icon font library -->
        @if(Auth::user()->hasRole($superadmin))
        <li class="nav-item has-treeview menu-open">
          <a href="#" class="nav-link active">
            <i class="nav-icon fas fa-tachometer-alt"></i>
            <p>
              Super Admin
              <i class="right fas fa-angle-left"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="{{ route('showAllManagers') }}" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Manage Managers</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ route('showAllCustomer') }}" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Manage Customers</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ route('showAllChallenges') }}" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Manage Challenges</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ route('showAllPosts') }}" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Manage Posts</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ route('showAllAdvertiser') }}" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Manage Advertisers</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ route('showAllAdvertisement') }}" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Manage Advertisements</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ route('showAllAdPlan') }}" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Manage Ad Plans</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ route('roles.index') }}" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Manage Roles</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ route('showAllOnboardingImage') }}" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Manage Onboarding Image</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ route('showAllPages') }}" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Manage Pages</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="#" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Manage Report
                  <i class="right fas fa-angle-left"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="{{ route('manageCustomerReport') }}" class="nav-link">
                    <i class="far fa-circle nav-icon" style="font-size:10px"></i>
                    <p>Manage Customer Report</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="{{ route('managePostReport') }}" class="nav-link">
                    <i class="far fa-circle nav-icon" style="font-size:10px"></i>
                    <p>Manage Posts Report</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="{{ route('manageChallengeReport') }}" class="nav-link">
                    <i class="far fa-circle nav-icon" style="font-size:10px"></i>
                    <p>Manage Challenges Report</p>
                  </a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        @endif
        <li class="nav-item has-treeview menu-open">
          <a href="#" class="nav-link active">
            <i class="nav-icon fas fa-tachometer-alt"></i>
            @if(Auth::user()->hasRole($superadmin))
            <p>
              Admin Dashboard
              <i class="right fas fa-angle-left"></i>
            </p>
            @endif
            @if(Auth::user()->hasRole($managers))
            <p>
              Manager Dashboard
              <i class="right fas fa-angle-left"></i>
            </p>
            @endif
          </a>
          <ul class="nav nav-treeview">
            @if(Auth::user()->hasRole($managers))
            <li class="nav-item">
              <a href="{{ route('showAllCustomer') }}" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Manage Customers</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ route('showAllChallenges') }}" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Manage Challenges</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ route('showAllPosts') }}" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Manage Posts</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="#" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Manage Report
                  <i class="right fas fa-angle-left"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="{{ route('manageCustomerReport') }}" class="nav-link">
                    <i class="far fa-circle nav-icon" style="font-size:10px"></i>
                    <p>Manage Customer Report</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="{{ route('managePostReport') }}" class="nav-link">
                    <i class="far fa-circle nav-icon" style="font-size:10px"></i>
                    <p>Manage Posts Report</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="{{ route('manageChallengeReport') }}" class="nav-link">
                    <i class="far fa-circle nav-icon" style="font-size:10px"></i>
                    <p>Manage Challenges Report</p>
                  </a>
                </li>
              </ul>
            </li>
            <li class="nav-item">
              <a href="{{ route('showAllOnboardingImage') }}" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Manage Onboarding Image</p>
              </a>
            </li>
            @endif
            @php $user_id = Auth::user()->id; @endphp
            @if(isset($user_id))
            <li class="nav-item">
              <a href="{{ route('adminPasswordEdit', $user_id) }}" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Change Password</p>
              </a>
            </li>
            @endif
          </ul>
        </li>
      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
</aside>