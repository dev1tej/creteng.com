<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Post;
use App\PostLike;
use App\PostComment;
use App\SavePost;
use App\User;
use App\UsersFriend;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
//use Illuminate\Pagination\Paginator;
//use Illuminate\Support\Collection;
//use Illuminate\Pagination\LengthAwarePaginator;

class PostController extends Controller
{
    
    public function showAllPost()
    {
        $user_id = Auth::id();
        $all_post = array();
        $public_posts = Post::where([
            ['post_type', 'public'], 
        ])->orderBy('id','ASC')->get();

        foreach ($public_posts as $public_post) {
            $all_post[] = $public_post;
        }

        $user_private_posts = Post::where([
            ['user_id', $user_id],
            ['post_type', 'private'], 
        ])->orderBy('id','ASC')->get();

        foreach ($user_private_posts as $user_private_post) {
            $all_post[] = $user_private_post;
        }

        $user_friends = UsersFriend::where([
            ['user_id', $user_id], 
        ])->orderBy('id','ASC')->get();

        foreach ($user_friends as $friend) {
            $friend_id = $friend['user_friend_id'];
            $friend_privat_posts = Post::where([
                ['user_id', $friend_id],
                ['post_type', 'private'], 
            ])->orderBy('id','ASC')->get();

            foreach ($friend_privat_posts as $friend_privat_post) {
                $all_post[] = $friend_privat_post;
            }
        }
        
        //$all_post = [$public_posts, $user_private_posts, $privat_posts];

        if (is_null($all_post)) {
            $response = [
                'status' => 'success',
                'message' => 'There is no post to show',
                'All public private post' => $all_post,
            ];
            return response($response, 200);
        } else {
            
            $response = [
                'status' => 'success',
                'message' => 'Posts found Successfully',
                'All public private post' => $all_post,
            ];

            return response($response, 200);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'post_text'        => 'string|max:255',
            'post_binary'      => 'string|max:255',
            'post_binary_mime_type' => 'required_with:post_binary|string|max:255',
            'tagged_user_list' => 'max:255',
            'post_type'        => 'required|string|max:255',
        ]);   

        if ($validator->fails()) {
            return response([
                'status' => 'failure',
                'message'=>$validator->errors()->all()
            ], 422);
        }

        if (empty($request->post_text) && empty($request->post_binary)) {
            return response([
                'status' => 'failure',
                'message'=>'There is no post to Upload'
            ], 422);
        }
        
        $tagged_user_list_pattern = '/^\s+$|^[0-9]*(, {1,3}[0-9]+)*?$/u';
        if (!preg_match($tagged_user_list_pattern, $request->tagged_user_list)) {
            return response([
                'status' => 'failure',
                'message'=>'The tagged user list format is invalid.'
            ], 422);
        }

        $post_type_pattern = '/^(public)|(private)$/u';
        if (!preg_match($post_type_pattern, $request->post_type)) {
            return response([
                'status' => 'failure',
                'message'=>'The post type must be \'public\' or \'private\'.'
            ], 422);
        }

        $input['user_id'] = Auth::user()->id;
        if ($input['user_id']) {

            if(!empty($request->post_text)) {
                $input['post_text'] = $request->post_text;
            } else {
                $input['post_text'] = null;
            }

            if(!empty($request->post_binary)) {
                $input['post_binary'] = $request->post_binary;
                $input['post_binary_mime_type'] = $request->post_binary_mime_type;
                
            } else {
                $input['post_binary'] = null;
                $input['post_binary_mime_type'] = null;
            }


            if(!empty($request->tagged_user_list)) {
                $input['tagged_user_list'] = json_encode($request->tagged_user_list);
            } else {
                $input['tagged_user_list'] = null;
            }

            $input['post_type'] = $request->post_type;
        }
          
        $post = Post::create($input);
        $response = [
            'status' => 'success',
            'message' => 'Post uploaded successfully',
            'post' => $post,
        ];
        return response($response, 200);
  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::find($id);
        if (!is_null($post)) {
            $response = [
                'status' => 'success',
                'message' => 'Post searched successfully',
                'post' => $post,
            ];
            return response($response, 200);
       } else {
            $response = [
                'status' => 'failure',
                'message' => 'There is no such post',
            ];
            return response($response, 422);
       }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'post_text'        => 'string|max:255',
            'post_binary'      => 'string|max:255',
            'post_binary_mime_type' => 'required_with:post_binary|string|max:255',
            'tagged_user_list' => 'max:255',
            'post_type'        => 'required|string|max:255',
        ]);   

        if ($validator->fails()) {
            return response([
                'status' => 'failure',
                'message'=>$validator->errors()->all()
            ], 422);
        }

        if (empty($request->post_text) && empty($request->post_binary)) {
            return response([
                'status' => 'failure',
                'message'=>'There is nothing to Update'
            ], 422);
        }
        
        $tagged_user_list_pattern = '/^\s+$|^[0-9]*(, {1,3}[0-9]+)*?$/u';
        if (!preg_match($tagged_user_list_pattern, $request->tagged_user_list)) {
            return response([
                'status' => 'failure',
                'message'=>'The tagged user list format is invalid.'
            ], 422);
        }

        $post_type_pattern = '/^(public)|(private)$/u';
        if (!preg_match($post_type_pattern, $request->post_type)) {
            return response([
                'status' => 'failure',
                'message'=>'The post type must be \'public\' or \'private\'.'
            ], 422);
        }

        $user_id = Auth::user()->id;
        $post = Post::find($id);

        if ($post) {
            if ($post->user_id != $user_id) {
                return response([
                    'status' => 'failure',
                    'message'=>'You are not authorize to edit this post'
                ], 422);
            }
            //updating post table
            $post->update($request->toArray());

            $response = [
                'status' => 'success',
                'message' => 'Post edited successfully',
                'post' => $post,
            ];
            return response($response, 200);  
        } else {
            $response = [
                'status' => 'failure',
                'message' => 'There is no such post',
            ];
            return response($response, 422);
        }        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user_id = Auth::user()->id;
        $post = Post::find($id);

        if ($post) {
            if ($post->user_id != $user_id) {
                return response([
                    'status' => 'failure',
                    'message'=>'You are not authorize to delete this post'
                ], 422);
            }  
            //deleting post table
            $post->delete();
            $response = [
                'status' => 'success',
                'message' => 'Post deleted successfully',
            ];
            return response($response, 200);
        } else {
            $response = [
                'status' => 'failure',
                'message' => 'There is no such post',
            ];
            return response($response, 422);
        }  
    }

    public function postLike(Request $request) 
    {
        $validator = Validator::make($request->all(), [
            'post_id' => 'required|regex:/^[0-9]{1,11}$/u',
            'status' => 'required|boolean',
        ]);   

        if ($validator->fails()) {
            return response([
                'status' => 'failure',
                'errors'=>$validator->errors()->all()], 422);
        }

        $input['user_id'] = Auth::user()->id;
        $input['post_id'] = $request->post_id;
        $input['status'] = $request->status;
        $post_like = PostLike::create($input);

        if (($input['status'] == 1) || ($input['status'] == true)) {
            $status = 'liked';
        } else {
            $status = 'unliked';
        }
        $response = [
            'status' => 'success',
            'message' => 'You have ' . $status . ' the post successfully',
            'post' => $post_like,
        ];
        return response($response, 200);

    }

    public function postComment(Request $request) 
    {
        $validator = Validator::make($request->all(), [
            'post_id' => 'required|regex:/^[0-9]{1,11}$/u',
            'comment' => 'required|string|max:255',
        ]);   

        if ($validator->fails()) {
            return response([
                'status' => 'failure',
                'message'=>$validator->errors()->all()
            ], 422);
        }

        $input['user_id'] = Auth::user()->id;
        $input['post_id'] = $request->post_id;
        $input['comments'] = $request->comment;
        $post_comment = PostComment::create($input);

        
        $response = [
            'status' => 'success',
            'message' => ' the post commented successfully',
            'post' => $post_comment,
        ];
        return response($response, 200);
    }

    public function savePostList(Request $request) 
    {
        $validator = Validator::make($request->all(), [
            'post_id_list' => 'required|max:255|regex:/^[0-9]*(, {0,3}[0-9]+)*?$/u',
        ]);   

        if ($validator->fails()) {
            return response([
                'status' => 'failure',
                'message'=>$validator->errors()->all()
            ], 422);
        }

        $post_id_list_array = explode(',', $request->post_id_list);

        $input['user_id'] = Auth::user()->id;

        foreach ($post_id_list_array as $post_id) {
            $input['post_id'] = trim($post_id);
            SavePost::create($input);
        }
        $response = [
            'status' => 'success',
            'message' => 'Listed posts saved successfully',
        ];
        return response($response, 200);
    }

    public function removeSavePostList(Request $request) 
    {
        $validator = Validator::make($request->all(), [
            'post_id_list' => 'required|max:255|regex:/^[0-9]*(, {0,3}[0-9]+)*?$/u',
        ]);   

        if ($validator->fails()) {
            return response([
                'status' => 'failure',
                'message'=>$validator->errors()->all()
            ], 422);
        }

        $post_id_list_array = explode(',', $request->post_id_list);

        $input['user_id'] = Auth::user()->id;

        foreach ($post_id_list_array as $post_id) {
            $input['post_id'] = trim($post_id);
            //SavePost::create($input);
            $removed = SavePost::where([
                ['user_id', $input['user_id']], 
                ['post_id', $input['post_id']]
            ])->delete();
            if (!$removed) {
                return response([
                    'status' => 'failure',
                    'message'=> 'post id ' .$input['post_id']. ' does not exist',
                ], 422);
            }
        }
        $response = [
            'status' => 'success',
            'message' => 'Listed posts removed from save list successfully',
        ];
        return response($response, 200);
    }

}
