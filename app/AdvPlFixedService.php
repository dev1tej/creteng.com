<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdvPlFixedService extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 
        'plan_id', 'fixed_services'
    ];

    /**
     * Get the plan.
     */
    public function plan()
    {
        return $this->belongsTo('App\AdvPlan', 'plan_id', 'id');
    }
}
