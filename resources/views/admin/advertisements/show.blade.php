@extends('admin.layouts.master')
@section('content')
<div class="container">
  <div class="row">
    <div class="col-lg-12 margin-tb">
      <div class="pull-left">
        <h2>Show Advertisement</h2>
      </div>
    </div>
  </div>
  @if (count($errors) > 0)
  <div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
      @foreach ($errors->all() as $error)
      <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
  @endif
  <div id="error"></div>
  <section class="content">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <div class="row">
              <div class="pull-right col-lg-7">
                <div class="pull-right">
                  <a class="btn btn-primary" href="{{ route('showAllAdvertisement') }}"> Back</a>
                </div>
              </div>
            </div>
          </div>
          <div class="card-body">
            <div class="row" style="max-width: 100%;">
              <div class="col-12">
                <div class="card">
                  <div class="card-header">
                    <p><h5><strong>Advertiser</strong></h5></p>
                    <p><strong>{{ $data['advertiser']['name'] }}</strong></p>
                    <p><strong>{{ $data['advertiser']['email'] }}</strong></p>
                    <p><strong>{{ $data['advertiser']['contact'] }}</strong></p>
                    <p><strong>{{ $data['advertiser']['company_name'] }}</strong></p>

                    <p><h5><strong>Subscription</strong></h5></p>
                    <p><strong>Plan: </strong> {{ $data['plan']['plan_name'] }}</p>
                    <p><strong>Sub Plan: </strong> {{ $data['sub_plan']['plan_duration'] }}</p>
                    <p><strong>Fixed Services:</strong>
                    @foreach ($data['plan_fixed_service'] as $plan_fixed_service)
                    {{ $plan_fixed_service['fixed_services'] }}<br/>
                    @endforeach
                    </p>
                    <p><strong>Optional Services:</strong>
                    @foreach ($data['plan_opt_service'] as $plan_opt_service)
                    {{ $plan_opt_service['optional_services'] }}<br/>
                    @endforeach
                    </p>
                    <p><strong>Ad Publishable Locations:</strong>
                      <span>
                        @php $no_of_publication_location = 0; @endphp
                        @foreach ($data['publishable_locations'] as $publishable_location)
                        @php $no_of_publication_location++; @endphp
                        {{ $no_of_publication_location }}. Country: {{  $publishable_location['country'] }} State: {{  $publishable_location['state'] }},
                        @endforeach
                      </span>
                    </p>
                  </div>
                  <div class="card-body">
                    <div class="col-md-12">
                      @if(($data['ad_type'] == 'image') || ($data['ad_type'] == 'Image'))
                      <img src="{{ $data['advertisement'] }}" alt="{{ $data['advertisement'] }}" class="card-img" style="width:50%;height:50%;"/>
                      @elseif(($data['ad_type'] == 'video') || ($data['ad_type'] == 'Video'))
                      <video class="img-fluid" controls>
                        <source src="{{ $data['advertisement'] }}" alt="{{ $data['advertisement'] }}"/>
                      </video>
                      @endif
                      <p>
                        <span class="card-text">{{ $data['description'] }}</span>
                        <span title="edit"><button class="btn btn-info btn-xs edit-description"><i class='far fa-edit'></i></button></span>
                      </p>
                      <span class="count-icon">
                        <i class="fa fa-thumbs-up" title="Total Likes" style="font-size:30px;color:DodgerBlue"></i>
                        <span class="count">{{ $data['likes']['likes_count'] }}</span>
                      </span>
                      @if($data['views']['views_count'] > 0)
                        <span class="count-icon" style="margin-left: 20px">
                          <i class="fas fa-eye" title="Total Views" style="font-size:30px;color:DodgerBlue"></i>
                          <span class="count">{{ $data['views']['views_count'] }}</span>
                        </span>
                      @endif  
                    </div>
                    <hr/>
                    <div class="card">
                      <div class="card-body">
                        <strong>Website URL:</strong>
                        <p>
                          <span class="card-text">{{ $data['website_url'] }}</span>
                          <span title="edit"><button class="btn btn-info btn-xs edit-url"><i class='far fa-edit'></i></button></span>
                        </p>
                        <hr/>
                        <strong>Is App ?:</strong>
                        @if($data['is_app'] == 1)
                        True
                        @elseif($data['is_app'] == 0)
                        False
                        @endif
                        <hr/>
                        <strong>Google Store URL:</strong>
                        <p>{{ $data['google_store_url'] }}</p>
                        <hr/>
                        <strong>Apple Store URL:</strong>
                        <p>{{ $data['apple_store_url'] }}</p>
                        <hr/>
                        <strong>Status:</strong>
                        <p>{{ $data['status'] }}</p>
                        <hr/>
                        <strong>Paid:</strong>
                        @if($data['paid'] == 1)
                        True
                        @elseif($data['paid'] == 0)
                        False
                        @endif
                        <hr/>
                        <strong>Published:</strong>
                        @if($data['published'] == 1)
                        True
                        @elseif($data['published'] == 0)
                        False
                        @endif
                        <hr/>
                        <strong>Publish Date:</strong>
                        <p>{{ $data['publish_date'] }}</p>
                        <hr/>
                        <strong>Expiry Date:</strong>
                        <p>{{ $data['expiry_date'] }}</p>
                        <hr/>
                        <strong>Liked By:</strong>
                        @foreach($data['likes'] as $liked_by)
                        @if(isset($liked_by['user']['name']))
                        <a href="">{{ $liked_by['user']['name'] }}</a>
                        @if($data['likes']['likes_count'] > 1)
                        , 
                        @endif
                        @endif
                        @endforeach
                        <hr/>
                        
                        @if($data['views']['views_count'] > 0)
                          <strong>Viewed By:</strong>
                          @foreach($data['views'] as $view_by)
                          @if(isset($view_by['user']['name']))
                          <a href="">{{ $view_by['user']['name'] }}</a>
                          @if($data['views']['views_count'] > 1)
                          , 
                          @endif
                          @endif
                          @endforeach
                          <hr/>
                        @endif
                        
                      </div>
                    </div>
                    <div class="card">
                      <div class="card-header">
                        <strong>Comments:</strong>
                      </div>
                      <div class="card-body">
                        <div class="col-12">
                          @foreach($data['comments'] as $comment_detail)
                          <div class="row comments comment_remove">
                            <div class="col-3">
                              <img src="{{ $comment_detail['comment']['user']['profile_pic'] }}" class="img-circle" alt="User Image" style="height:40px;width:40%"><br>
                              <a href="">{{ $comment_detail['comment']['user']['name'] }}</a>
                            </div>
                            <div class="col-9">
                              <p class="card-text">
                                {{ $comment_detail['comment']['comments'] }}
                                <button type="button" class="close" aria-label="Close" id="{{ $comment_detail['comment']['id'] }}" title="Delete Comment">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                              </p>
                              <p class="card-text"><small class="text-muted">Last Updated {{ $comment_detail['comment']['updated_at'] }}</small></p>
                              <hr/>
                              @if(isset($comment_detail['comment']['comment_reply']))
                              @foreach($comment_detail['comment']['comment_reply'] as $comment_reply)
                              <div class="row comment_remove">
                                <div class="col-3">
                                  <img src="{{ $comment_reply['user']['profile_pic'] }}" class="img-circle" alt="User Image" style="height:40px;width:40%"><br>
                                  <a href="">{{ $comment_reply['user']['name'] }}</a>
                                </div>
                                <div class="col-9">
                                  <p class="card-text">
                                    {{ $comment_reply['comments'] }}
                                    <button type="button" class="close" aria-label="Close" id="{{ $comment_reply['id'] }}" title="Delete Comment">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                  </p>
                                  <p class="card-text"><small class="text-muted">Last Updated {{ $comment_reply['updated_at'] }}</small></p>
                                </div>
                              </div>
                              <hr/>
                              @endforeach
                              @endif
                            </div>
                          </div>
                          <hr/>
                          @endforeach
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<script>
  $( document ).ready(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    
    $(".comments").on("click", ".close", function(b) {
      var comment_id = $(this).attr('id');		
      $.ajax({
        type: "POST",
        url: '{{ route("deleteAdvertisementComment") }}',
        data: {'comment_id': comment_id},
        success: function(data){
          if(data == 'ok') {
            $("#"+comment_id).closest(".comment_remove").remove();
            $("#"+comment_id).closest(".comment_remove").next('hr').remove();
            $('#error').html('<div class="alert alert-danger">Comment deteted Successfully</div>');
          }
        }
      }); 
    });
    $(document).on("click", ".edit-description", function() {
      //console.log($(this).parents('p').children(':eq(0)').text());
      var description = $(this).parents('p').children(':eq(0)').text();
      html = '';
      html += '<p>';
      html += '<strong><span>Advertisement Description (Maximum 250 characters):</span><span><button class="btn btn-info btn-xs update-description">Update</button></span></strong>';
      html += '<textarea maxlength="250" class="form-control" id="ad_description" name="ad_description" rows="3">'+description+'</textarea>';
      //html += '<span class="text-info font-weight-bold"><span id="rchars">250</span> Character(s) Remaining</span>';
      html += '</p>';

      $(this).parents('p:eq(0)').replaceWith(html);
    });
    /*var maxLength = 250;
    $(document).keyup('textarea', function() {
      var textlen = maxLength - $(this).val().length;
      alert(textlen);
      $('#rchars').text(textlen);
    });*/
    $(document).on("click", ".update-description", function() {
      var ad_id = "{{ $data['id'] }}";
      var editable = $(this).parents('p:eq(0)');
      var description = $(this).parents('p:eq(0)').children('textarea').val()
      //alert(ad_id);
      //alert($(this).parents('p:eq(0)').children('textarea').val());

      $.ajax({
        type: "POST",
        url: '{{ route("updateAdDescription") }}',
        data: {
          'ad_id': ad_id,
          'description': description
        },
        success: function(data){
          //console.log(data);
          var html = '';
          html += '<p>';
          html += '<span class="card-text">'+data+'</span>';
          html += '<span title="edit"><button class="btn btn-info btn-xs edit-description"><i class="far fa-edit"></i></button></span>';
          html += '</p>';
          editable.replaceWith(html);
          $('#error').html('<div class="alert alert-success">Ad description updated Successfully</div>');
        }
      }); 
    });
    $(document).on("click", ".edit-url", function() {
      //console.log($(this).parents('p').children(':eq(0)').text());
      var url = $(this).parents('p').children(':eq(0)').text();
      html = '';
      html += '<p>';
      html += '<span><button class="btn btn-info btn-xs update-url">Update</button></span>';
      html += '<input class="form-control" id="ad_url" name="ad_url" value="'+url+'"/>';
      html += '</p>';

      $(this).parents('p:eq(0)').replaceWith(html);
    });
    $(document).on("click", ".update-url", function() {
      var ad_id = "{{ $data['id'] }}";
      var editable = $(this).parents('p:eq(0)');
      var url = $(this).parents('p:eq(0)').children('input').val()
      //alert(ad_id);
      //alert($(this).parents('p:eq(0)').children('textarea').val());

      $.ajax({
        type: "POST",
        url: '{{ route("updateAdURL") }}',
        data: {
          'ad_id': ad_id,
          'url': url
        },
        success: function(data){
          console.log(data);
          var html = '';
          html += '<p>';
          html += '<span class="card-text">'+data+'</span>';
          html += '<span title="edit"><button class="btn btn-info btn-xs edit-url"><i class="far fa-edit"></i></button></span>';
          html += '</p>';
          editable.replaceWith(html);
          $('#error').html('<div class="alert alert-success">Ad URL updated Successfully</div>');
        }
      }); 
    });
  });
</script>
@endsection