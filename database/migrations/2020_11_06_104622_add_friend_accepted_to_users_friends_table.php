<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFriendAcceptedToUsersFriendsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users_friends', function (Blueprint $table) {
            $table->boolean('friend_accepted')
                ->default(false)
                ->comment('1/0 => accepted/(waiting to accept)');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users_friends', function (Blueprint $table) {
            $table->dropColumn('friend_accepted');
        });
    }
}
