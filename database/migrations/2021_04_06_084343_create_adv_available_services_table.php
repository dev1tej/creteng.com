<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdvAvailableServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adv_available_services', function (Blueprint $table) {
            $table->id();
            $table->foreignId('advertiser');
            $table->foreignId('plan');
            $table->foreignId('duration');
            $table->foreignId('opt_services')->nullable();
            $table->timestamps();

            $table->foreign('advertiser')
                ->references('id')
                ->on('adv_users')
                ->onDelete('cascade');
            $table->foreign('plan')
                ->references('id')
                ->on('adv_plans')
                ->onDelete('cascade');
            $table->foreign('duration')
                ->references('id')
                ->on('adv_pl_durations')
                ->onDelete('cascade');
            $table->foreign('opt_services')
                ->references('id')
                ->on('adv_pl_opt_services')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('adv_available_services');
    }
}
