@extends('layouts.app')
@section('content')
<style type="text/css">
.fixed-top {
  position: relative;
}
#footer .footer-top {
 padding: 50px 0 0 0;
 top: 0; 
}
#header.header-scrolled{
  position: fixed;
  top: 0;
  right: 0;
  left: 0;
  z-index: 1030;
}
</style>
<section class="inner-page">
<div class="container">
  <div class="row">
    <div class="col-lg-12 margin-tb">
      <div class="pull-left">
        <h2>Step 1: Personal Details</h2>
        <span class="text-danger font-weight-bold">All fields marked with * are required</span>
      </div>
    </div>
  </div>
  @if (count($errors) > 0)
  <div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
      @foreach ($errors->all() as $error)
      <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
  @endif
  <form action="{{ route('postRegisterPersonalDetail') }}" method="post" onsubmit = "return(validate());">
    @csrf
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="card">
          <h4 class="card-header"><strong><span class="text-danger font-weight-bold">*</span>Create Account:</strong></h4>
          <div class="row card-body">
            <div class="col-xs-12 col-sm-12 col-md-12">
              <div class="row">
                <div class="col-xs-12 col-sm-4 col-md-4">
                  <div class="form-group">
                    <strong><span class="text-danger font-weight-bold">*</span>Email:</strong>
                    <input type="text" id="email" name="email" placeholder="Email" value="{{ old('email') }}" class="form-control">
                    <span id="email_error" class="text-danger font-weight-bold"></span>
                  </div>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4">
                  <div class="form-group">
                    <strong><span class="text-danger font-weight-bold">*</span>Password:</strong>
                    <input type="password" id="password" name="password" class="form-control">
                    <span id="password_error" class="text-danger font-weight-bold"></span>
                  </div>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4">
                  <div class="form-group">
                    <strong><span class="text-danger font-weight-bold">*</span>Confirm Password:</strong>
                    <input type="password" id="password_confirmation" name="password_confirmation" class="form-control">
                    <span id="password_confirmation_error" class="text-danger font-weight-bold"></span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
          <strong><span class="text-danger font-weight-bold">*</span>Contact Name:</strong>
          <input type="text" id="name" name="name" placeholder="Name" value="{{ old('name') }}" class="form-control">
          <span id="name_error" class="text-danger font-weight-bold"></span>
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
          <strong><span class="text-danger font-weight-bold">*</span>Contact Phone:</strong>
          <input type="text" id="contact" name="contact" placeholder="Contact" value="{{ old('contact') }}" class="form-control">
          <span id="contact_error" class="text-danger font-weight-bold"></span>
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
          <strong>Company Name:</strong>
          <input type="text" id="company_name" name="company_name" placeholder="Company Name" value="{{ old('company_name') }}" class="form-control">
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="card">
          <h4 class="card-header"><strong><span class="text-danger font-weight-bold">*</span>Address:</strong></h4>
          <div class="card-body">
            <div class="form-group">
              <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">            
                  <strong><span class="text-danger font-weight-bold">*</span>Address Line 1:</strong>
                  <input type="text" id="address_line_1" name="address_line_1" value="{{ old('address_line_1') }}" class="form-control">
                  <span id="address_line_1_error" class="text-danger font-weight-bold"></span>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">            
                  <strong>Address Line 2:</strong>
                  <input type="text" name="address_line_2" value="{{ old('address_line_2') }}" class="form-control">            
                </div> 
              </div>
            </div> 
            <div class="row">
              <div class="col-xs-12 col-sm-6 col-md-6">
                <div class="form-group">
                  <strong><span class="text-danger font-weight-bold">*</span>City:</strong>
                  <input type="text" id="city" name="city" value="{{ old('city') }}" class="form-control">
                  <span id="city_error" class="text-danger font-weight-bold"></span>
                </div>
              </div>
              <div class="col-xs-12 col-sm-6 col-md-6">
                <div class="form-group">
                  <strong><span class="text-danger font-weight-bold">*</span>State:</strong>
                  <input type="text" id="state" name="state" value="{{ old('state') }}" class="form-control">
                  <span id="state_error" class="text-danger font-weight-bold"></span>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-xs-12 col-sm-6 col-md-6">
                <div class="form-group">
                  <strong><span class="text-danger font-weight-bold">*</span>Postal Code:</strong>
                  <input type="text" id="postal_code" name="postal_code" value="{{ old('postal_code') }}" class="form-control">
                  <span id="postal_code_error" class="text-danger font-weight-bold"></span>
                </div>
              </div>
              <div class="col-xs-12 col-sm-6 col-md-6">
                <div class="form-group">
                  <strong><span class="text-danger font-weight-bold">*</span>Country:</strong>
                  <select name="country" class="form-control" id="country">
                    @foreach ($countries_with_iso as $country_with_iso)
                    @if($country_with_iso == 'UNITED STATES-US')
                      <option selected value="{{ $country_with_iso }}" {{ old('country') == $country_with_iso ? 'selected' : '' }}>{{ $country_with_iso }}</option>  
                    @else
                    <option value="{{ $country_with_iso }}" {{ old('country') == $country_with_iso ? 'selected' : '' }}>{{ $country_with_iso }}</option>
                    @endif
                    @endforeach
                  </select>
                  <span id="country_error" class="text-danger font-weight-bold"></span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12 text-center">
        <button type="submit" class="btn btn-primary" id="submit">Next</button>
      </div>
    </div>
  </form>
</div>
@endsection
<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script>
  $(document).ready(function() {
    $('#submit').click(function( event ) { 
      var advertiser_id = '';
      @php
      if (session()->exists('advertiser_id')) {
      @endphp
        advertiser_id = {{ Session::get('advertiser_id') }};
      @php
      }
      @endphp
      if (advertiser_id == '') {
        if( ! confirm('This step is irreversible. Press OK to submit.') ){
            event.preventDefault();
        } 
      } else {
        alert('You already have created your account. You can update your profile after login.');
      }
    });
  });
</script>
<script type="text/javascript">
  // Form validation.
  function validate() {
      
      var name = document.getElementById('name').value;
      var email = document.getElementById('email').value;
      var password = document.getElementById('password').value;
      var password_confirmation = document.getElementById('password_confirmation').value;
      var contact = document.getElementById('contact').value;

      var address_line_1 = document.getElementById('address_line_1').value;
      var city = document.getElementById('city').value;
      var state = document.getElementById('state').value;
      var postal_code = document.getElementById('postal_code').value;
      var country = document.getElementById('country').value;

      //email
      if (email == "") {
        document.getElementById('email_error').innerHTML = " ** Email field is require";
        return false;
      } else {
        document.getElementById('email_error').innerHTML = "";
      }
      if (!email.match(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)) {
        document.getElementById('email_error').innerHTML = " ** Please enter a valid email";
        return false;
      } else {
        document.getElementById('email_error').innerHTML = "";
      }

      //password
      if (password == "") {
        document.getElementById('password_error').innerHTML = " ** Password field is require";
        return false;
      } else {
        document.getElementById('password_error').innerHTML = "";
      }
      if ((password.length < 8)) {
        document.getElementById('password_error').innerHTML = " ** Password length must be at least 8 characters";
        return false;
      } else {
        document.getElementById('password_error').innerHTML = "";
      }
      //password confirmation
      if (password_confirmation == "") {
        document.getElementById('password_confirmation_error').innerHTML = " ** Password Confirmation field is require";
        return false;
      } else {
        document.getElementById('password_confirmation_error').innerHTML = "";
      }

      //name
      if (name == "") {
        document.getElementById('name_error').innerHTML = " ** Name field is require";
        return false;
      } else {
        document.getElementById('name_error').innerHTML = "";
      }
      if (!(name.match(/^[A-Za-z\s]+$/)) || (name.length <=2 )) {
        document.getElementById('name_error').innerHTML = " ** Please enter a valid name";
        return false;
      } else {
        document.getElementById('name_error').innerHTML = "";
      }
      //contact
      if (contact == "") {
        document.getElementById('contact_error').innerHTML = " ** Contact field is require";
        return false;
      } else {
        document.getElementById('contact_error').innerHTML = "";
      }
      if (isNaN(contact)) {
        document.getElementById('contact_error').innerHTML = " ** Please enter a valid contact";
        return false;
      } else {
        document.getElementById('contact_error').innerHTML = "";
      }
      if ((contact.length < 8) || (contact.length > 12)) {
        document.getElementById('contact_error').innerHTML = " ** Contact length must be between 8 to 12 digits";
        return false;
      } else {
        document.getElementById('contact_error').innerHTML = "";
      }

      //address_line_1
      if (address_line_1 == "") {
        document.getElementById('address_line_1_error').innerHTML = " ** Address Line 1 field is require";
        return false;
      } else {
        document.getElementById('address_line_1_error').innerHTML = "";
      }
      //city
      if (city == "") {
        document.getElementById('city_error').innerHTML = " ** City field is require";
        return false;
      } else {
        document.getElementById('city_error').innerHTML = "";
      }
      //state
      if (state == "") {
        document.getElementById('state_error').innerHTML = " ** State field is require";
        return false;
      } else {
        document.getElementById('state_error').innerHTML = "";
      }
      //postal_code
      if (postal_code == "") {
        document.getElementById('postal_code_error').innerHTML = " ** Postal Code field is require";
        return false;
      } else {
        document.getElementById('postal_code_error').innerHTML = "";
      }
      //country
      if (country == "") {
        document.getElementById('country_error').innerHTML = " ** Country field is require";
        return false;
      } else {
        document.getElementById('country_error').innerHTML = "";
      }

      return( true );
   }
</script>