<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddStatusToAdsPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ads_payments', function (Blueprint $table) {
            $table->string('stripe_customer_id', 100)->after('advertisement_id');
            $table->string('status', 10)->after('transaction_id')->comment('Hold/Charged');
            $table->date('publish_date')->after('amount')->nullable();
            $table->date('expiry_date')->after('publish_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ads_payments', function (Blueprint $table) {
            $table->dropColumn('stripe_customer_id');
            $table->dropColumn('status');
            $table->dropColumn('publish_date');
            $table->dropColumn('expiry_date');
        });
    }
}
