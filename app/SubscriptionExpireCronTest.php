<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubscriptionExpireCronTest extends Model
{
    protected $fillable = [ 
        'run_successfully'
    ];
}
