<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostVideoViewed extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 
        'post_id', 'user_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        
    ];
    
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        
    ];

    /**
     * Get the user that viewed the video.
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id ', 'id');
    }

    /**
     * Get the viewed post.
     */
    public function post()
    {
        return $this->belongsTo('App\Post', 'post_id', 'id');
    }
}
