@extends('admin.layouts.master')
@section('content')
<div class="container">
  <div class="row">
    <div class="col-lg-12 margin-tb">
      <div class="pull-left">
        <h2>Advertisement Plans Management</h2>
      </div>
    </div>
  </div>
  @if ($message = Session::get('success'))
  <div class="alert alert-success">
    <p>{{ $message }}</p>
  </div>
  @endif
  @if ($message = Session::get('danger'))
  <div class="alert alert-danger">
    <p>{{ $message }}</p>
  </div>
  @endif
  @if (isset($success))
  <div class="alert alert-success">
    <p>{{ $success }}</p>
  </div>
  @endif
  <section class="content">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title"></h3>
            <div class="pull-right">
              <a class="btn btn-success" href="{{ route('createAdPlan') }}"> Create New Plan</a><br/><br/>
              <!--a class="btn btn-success" href="{{-- route('createAdPlanOptService') --}}"> Create New Advertisement Optional Service</a><br/><br/-->
              <!--a class="btn btn-success" href="{{-- route('viewAdPlanOptService') --}}"> View Advertisement Optional Services</a-->
            </div>
          </div>
          <div class="card-body">
            <table id="adPlanData" class="table table-bordered table-striped table-hover">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Plan Name</th>
                  <th width="280px">Action</th>
                </tr>
              </thead>
              <tbody>
                @php $srno = '1'; @endphp
                @forelse ($data as $key => $ad_plan)
                <tr>
                  <td>@php echo $srno; @endphp</td>
                  <td>{{ $ad_plan->plan_name }}</td>
                  <td>
                    <a class="btn btn-success" href="{{ route('showAdPlan', $ad_plan->id) }}">View</a>
                    <a class="btn btn-primary" href="{{ route('editAdPlan', $ad_plan->id) }}">Edit</a>
                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#exampleModal{{ $ad_plan->id }}" data-id="{{ $ad_plan->id }}" >Delete</button>	
                  </td>
                </tr>
                <!-- Delete Ad Plans Modal Starts -->
                <div class="modal fade" id="exampleModal{{ $ad_plan->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                  <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Warning</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                        If you want to delete this Ad Plan , all information will be lost of this Plan
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                        <a href="{{ route('deleteAdPlan', $ad_plan->id ) }}"><button type="button" class="btn btn-danger">Yes, go forward</button></a>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- Delete Ad Plans Modal Ends -->
                @php $srno++; @endphp
                @empty
                No Ad Plans Found
                @endforelse
              </tbody>
              <tfoot>
                <tr>
                  <th>No</th>
                  <th>Name</th>
                  <th>Action</th>
                </tr>
              </tfoot>
            </table>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

<script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script>
  $(function () {
    $('#adPlanData').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>

@endsection