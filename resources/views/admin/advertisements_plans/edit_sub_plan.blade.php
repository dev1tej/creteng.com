@extends('admin.layouts.master')
@section('content')
<div class="container">
  <div class="row">
    <div class="col-lg-12 margin-tb pull-right">
      <a class="btn btn-primary" href="{{ route('showAllAdPlan') }}"> Back To Plan Page</a>
    </div>
    <div class="col-lg-12 margin-tb">
      <div class="pull-left">
        <h2>Edit Advertisement Durations</h2>
      </div>
    </div>
  </div>
  @if (count($errors) > 0)
  <div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
      @foreach ($errors->all() as $error)
      <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
  @endif
  @if (isset($success))
  <div class="alert alert-success">
    <p>{{ $success }}</p>
  </div>
  @endif
  @if (isset($danger))
  <div class="alert alert-danger">
    <p>{{ $danger }}</p>
  </div>
  @endif
  <div class="container">
    <h3><strong>Plan Name:</strong>  {{ $ad_plan->plan_name }}</h3>
    <br />
    <!--h4 align="center">Add Edit or Remove Ad Sub Plans</h4-->
    <br />
    <form method="post" action="{{ route('updateAdSubPlan', $ad_plan->id) }}" id="insert_form">
     {{ csrf_field() }}
     @method('PATCH')
     <div class="table-repsonsive">
      <span id="error"></span>
      <table class="table table-bordered" id="item_table">
       <tr>
        <th>Plan Duration</th>
        <th>Price (in USD)</th>
       </tr>
       @foreach($ad_plan->planDuration as $planDuration)
       <tr>
        <input type="text" name="duration_id[]" class="form-control" value="{{ $planDuration->id }}" hidden/>
        <td><input type="text" name="duration_name[]" class="form-control" value="{{ $planDuration->plan_duration }}" readonly/></td>
        <td><input type="number" step=".01" name="duration_price[]" class="form-control duration_price" value="{{ $planDuration->price }}" /></td>
       </tr>
       @endforeach
      </table>
      <div align="center">
       <input type="submit" name="submit" class="btn btn-primary" value="Submit" />
      </div>
     </div>
    </form>
   </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script>
  $(document).ready(function(){
   
    $('#insert_form').on('submit', function(event){
      event.preventDefault();
      var error = '';
    
      $('.duration_price').each(function(){
        var count = 1;
        if($(this).val() == '') {
          error += "<p>Enter Plan Duration Price at "+count+" Row</p>";
          return false;
        }
        count = count + 1;
      });
    
    
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });

      var form_data = $(this).serialize();
      if(error == '') {
        $.ajax({
          url:"{{ route('updateAdSubPlan', $ad_plan->id) }}",
          method:"PATCH",
          data: form_data,
          cache: false,
          success:function(response)
          {
            if(response == 'ok') {
              $('#item_table').find("tr:gt(0)").remove();
              $('#error').html('<div class="alert alert-success">Plan Durations updated successfully</div>');
            }
          }
        });
      }
      else {
        $('#error').html('<div class="alert alert-danger">'+error+'</div>');
      }
    });
   
  });
  </script>
  @endsection