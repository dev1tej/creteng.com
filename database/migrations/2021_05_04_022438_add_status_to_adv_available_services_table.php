<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddStatusToAdvAvailableServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('adv_available_services', function (Blueprint $table) {
            $table->boolean('status')->after('opt_services')->default(true)->comment('Active/Inactive');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('adv_available_services', function (Blueprint $table) {
            $table->dropColumn('status');
        });
    }
}
