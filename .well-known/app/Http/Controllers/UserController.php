<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
//use App\Profile;
//use App\Profile_categories;
//use App\Favourite;
use Spatie\Permission\Models\Role;
use DB;
use Hash;
use Arr;
use Stripe\Stripe;
use Stripe\Customer;
use Stripe\Charge;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $customers=env("APP_CUSTOMER");
        $manager=env("APP_MANAGER");
        $data = User::role([$customers,$manager])->orderBy('id','DESC')->paginate(10);
        return view('users.index',compact('data'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }
    
    public function alluserindex(Request $request)
    {
		/* Get admin , provider, customer role form env file */
		//$admin=env("APP_ADMIN");
        $customers=env("APP_CUSTOMER");
        $manager=env("APP_MANAGER");
        $users= User::role([$customers,$manager])->get();
        return view('admin.users.index',compact('users'));
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        $roles = Role::pluck('name','name')->all();
        return view('users.create',compact('roles'));
    }
    
    public function createuser()
    {
		/* Get superadmin role form env file */
		$superadmin=env("APP_SUPERADMIN");
        $roles = Role::where('name', '!=', $superadmin)->pluck('name','name');
        return view('admin.users.create',compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
		/* Check Validation */
        $this->validate($request, [
            'name' => 'required|regex:/^[\pL\s\-]+$/u',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|same:confirm-password',
            'roles' => 'required'
        ]);
		
		/* Create USER */
        $input = $request->all();
        $input['password'] = Hash::make($input['password']);
        $user = User::create($input);
        
        /* Assign user a role*/
        $user->assignRole($request->input('roles'));

        return redirect()->route('users.index')->with('success','User created successfully');
    }
    
    public function storeuser(Request $request)
    {
		/* Check Validation */
        $this->validate($request, [
            'name' => 'required|alpha',
            'email' => 'required|email|unique:users,email',
			'country_code' => 'required|digits_between: 1,6',
			'contact' => 'required|digits_between: 10,12|unique:users',
            'password' => 'required|same:confirm-password',
            'roles' => 'required'
        ]);

        $input = $request->all();
        $input['password'] = Hash::make($input['password']);
        /*Create User*/
        $user = User::create($input);

        $user->assignRole($request->input('roles'));
          
        return redirect(route('allUsers'))->with('flash_message','Customer created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show($id)
    {
        $user = User::find($id);
        return view('users.show',compact('user'));
    }
    
    public function showuser($id)
    {
        $user = User::find($id);
        return view('admin.users.show',compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit($id)
    {
        $user = User::find($id);
        $roles = Role::pluck('name','name')->all();
        $userRole = $user->roles->pluck('name','name')->all();

        return view('users.edit',compact('user','roles','userRole'));
    }
    
    public function edituser($id)
    {
        $user = User::find($id);
        
        $roles = Role::pluck('name','name');
        $userRole = $user->roles->pluck('name','name')->all();

        return view('admin.users.edit',compact('user','roles','userRole'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id)
    {
		/* Check Validation */
        $this->validate($request, [
            'name' => 'required|regex:/^[\pL\s\-]+$/u',
            'email' => 'required|email|unique:users,email,'.$id,
            'password' => 'same:confirm-password',
            'roles' => 'required'
        ]);

		/* if user dont want to change password or skip password to update */
        $input = $request->all();
        if(!empty($input['password'])){ 
            $input['password'] = Hash::make($input['password']);
        }else{
            $input = Arr::except($input,array('password'));    
        }

		/*Update User and delete the previous role and assign new role */
        $user = User::find($id);
        $user->update($input);

        DB::table('model_has_roles')->where('model_id',$id)->delete();

        $user->assignRole($request->input('roles'));
        
        return redirect()->route('users.index')->with('success','User updated successfully');
    }
    
    public function updateuser(Request $request, $id)
    {
		/* Check Validation */
        $this->validate($request, [
            'name' => 'required|regex:/^[\pL\s\-]+$/u',
            'email' => 'required|email|unique:users,email,'.$id,
            'password' => 'same:confirm-password',
            
        ]);
		
		/* if user dont want to change password or skip password to update */
        $input = $request->all();
        if(!empty($input['password'])){ 
            $input['password'] = Hash::make($input['password']);
        }else{
            $input = Arr::except($input,array('password'));    
        }

		/*Update User and delete the previous role and assign new role */
        $user = User::find($id);
        $user->update($input);

        return redirect(route('dashboard'))->with('flash_message','User updated successfully.');
    }

   
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy($id)
    {
        User::find($id)->delete();
        
        return redirect()->route('users.index')->with('danger','User deleted successfully');

    }
    public function destroyuser($id)
    {
        /* Delete User from user table */
        User::find($id)->delete();
        
        /* Delete User from role table */
        if(DB::table('model_has_roles')->where('model_id',$id)->exists()) {
			DB::table('model_has_roles')->where('model_id',$id)->delete();
		}
		
		
        return redirect(route('allUsers'))->with('flash_message','User deleted successfully.');
    }
    
    public function changeStatus(Request $request, User $user)
    {
		$user_id = $request->user_id;
		$status = $request->status;

		User::where('id', $user_id)->update(['blocked'=>$status]);
		
		return response()->json(['success'=>'Status changed successfully.']);
	}

    public function adminpasswordedit($id)
    {
        return view('admin.password.edit',compact('id'));
    }
    
    public function adminpasswordupdate(Request $request, $id)
    {
        $this->validate($request, [
            'password' => 'same:confirm-password',
        ]);
        
        $input = $request->all();
        if(!empty($input['password'])){ 
            $input['password'] = Hash::make($input['password']);
        }
		/*Update User password */
        $user = User::find($id);
        $user->update($input);
        
        return back()->with('success', 'Password changed successfully.');
    }
    
    public function passwordedit($id)
    {
        return view('members.password.edit',compact('id'));
    }
    
    public function passwordupdate(Request $request, $id)
    {
        $this->validate($request, [
            'password' => 'same:confirm-password',
        ]);
        
        $input = $request->all();
        if(!empty($input['password'])){ 
            $input['password'] = Hash::make($input['password']);
        }
		/*Update User password  */
        $user = User::find($id);
        $user->update($input);
        
        return back()->with('success', 'Password changed successfully.');
    }
    
    public function userpasswordedit($id)
    {
        return view('frontend.password.edit',compact('id'));
    }
    
    public function userpasswordupdate(Request $request, $id)
    {
        $this->validate($request, [
            'password' => 'same:confirm-password',
        ]);
        
        $input = $request->all();
        if(!empty($input['password'])){ 
            $input['password'] = Hash::make($input['password']);
        }
		/*Update User password  */
        $user = User::find($id);
        $user->update($input);
        
        return back()->with('success', 'Password changed successfully.');
    }
    
    public function useredit(Request $request, User $user)
    {
		$customer_id= $user->id;
		$user_data = User::where('id','=', $customer_id)->first();
		return view('frontend.account.edit', compact('user_data'));
    }
    
    public function userupdate(Request $request, User $user)
    {
		$customer_id=$user->id;
        $request->validate([
            'name' => ['required', 'alpha', 'max:255'],
            //'email' => ['required', 'email', 'max:255', 'unique:users,email,'.$customer_id],
            'country_code' => ['required', 'digits_between: 1,6'],
            'contact' => ['required', 'digits_between: 10,12', 'unique:users,contact,'.$customer_id],
		]);

        $input = $request->all();
        
		/* Update customer */
        $user = User::find($customer_id);
        $user->update($input);
        
        //return redirect(route('customers'))->with('flash_message','Customer updated successfully.');
        return back()->with('success', 'Profile updated successfully.');
		
    }

    /**
     * To send email verification link to unverified users
     */
    public function sendVerificationEmail($id) 
    {
        $user = User::find($id);
        $user->sendEmailVerificationNotification();
        return back()->with('success', 'Email sent successfully.');
    }

}
