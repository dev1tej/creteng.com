<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAdDescriptionToAdvertisementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('advertisements', function (Blueprint $table) {
            $table->string('description', 255)->after('advertisement')->nullable();
            $table->boolean('is_app')->after('description')->default(false);
            $table->string('google_store_url', 255)->after('is_app')->nullable();
            $table->string('apple_store_url', 255)->after('google_store_url')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('advertisements', function (Blueprint $table) {
            $table->dropColumn('description');
            $table->dropColumn('is_app');
            $table->dropColumn('google_store_url');
            $table->dropColumn('apple_store_url');
        });
    }
}
