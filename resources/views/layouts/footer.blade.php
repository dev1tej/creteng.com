<!-- ======= Footer ======= -->
  <footer id="footer">
    <div class="footer-top"> 
    <div class="container">      
      <!--<a href="index.html"><img src="{{ asset('assets/img/white-logo.png') }}" class="img-fluid" alt="{{ config('app.name', 'Creteng') }}"></a>  -->
      <ul class="flink">
        <li><a href="{{ route('showRegisterPersonalDetailForm') }}">Advertiser Registration</a></li>
        <li><a href="{{ route('advertiser.login') }}">Advertiser Login</a></li>
        <li><a href="{{ route('privacyPolicy') }}">Privacy</a></li>
        <li><a href="{{ route('termsNCondition') }}">Terms</a></li>
      </ul>
      <p class="copyright">
        &copy; @php echo date("Y") @endphp All Rights Reserved by Creteng        
      </p>
      <p class="powredby">Powered by <a rel="nofollow" href="https://www.ost.agency/"><img src="{{ asset('assets/img/credit_logo.png') }}" alt="OpenSource Technologies"></a></p>
    </div> 
    </div>    
  </footer><!-- End Footer -->

  <!-- Placed js at the end of the document so the pages load faster -->
  <!-- jquery latest version -->
  <script src="{{ asset('assets/vendor/jquery/jquery.min.js')}}"></script>
  <script src="{{ asset('assets/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
  <script src="{{ asset('assets/vendor/jquery.easing/jquery.easing.min.js')}}"></script>
    <script src="{{ asset('assets/vendor/venobox/venobox.min.js')}}"></script>
  <script src="{{ asset('assets/vendor/waypoints/jquery.waypoints.min.js')}}"></script>
  <script src="{{ asset('assets/vendor/counterup/counterup.min.js')}}"></script>
  <script src="{{ asset('assets/vendor/owl.carousel/owl.carousel.min.js')}}"></script>
  <script src="{{ asset('assets/vendor/aos/aos.js')}}"></script>

  <!-- Template Main JS File -->
  <script src="{{ asset('assets/js/main.js')}}"></script>

