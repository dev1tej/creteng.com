<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

class CustomPaginationController extends Controller
{
    /**
     * To paginate the Api response.
     *
     * @param array $items
     * @param int $per_page
     * @return $data (paginated data)
     */
    public function paginate($items, $per_page = 5, $page = null, $options = [])

    {

        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);

        $items = $items instanceof Collection ? $items : Collection::make($items);

        //This would contain all data to be sent to the view
        $data = array();
        
        //Slice the collection to get the items to display in current page
        $current_page_results = $items->slice(($page-1) * $per_page, $per_page)->all();
        
        //remove indexes from $currentPageResults
        $current_page_results_without_index = array();
        foreach ($current_page_results as $current_page_results) {
            $current_page_results_without_index[] = $current_page_results;
        }

        //Create our paginator and add it to the data array
        $data = new LengthAwarePaginator($current_page_results_without_index, count($items), $per_page);
        
        return $data;
    }
}
