<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

use Illuminate\Support\Facades\Password;

class AuthController extends Controller
{
    public function register(Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255|regex:/^[\pL\s\-]+$/u',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:8|confirmed',
        ]);
        if ($validator->fails())
        {
            return response([
                'status' => 'failure',
                'message'=>$validator->errors()->all()], 422);
        }

        $request['blocked'] = 1;
        $request['password'] = Hash::make($request['password']);
        //$request['remember_token'] = Str::random(10);
        $user = User::create($request->toArray());

        $customers = env('APP_CUSTOMER');
        $user->assignRole($customers);
        $token = $user->createToken('Registration')->accessToken;

        $user->sendEmailVerificationNotification();

        $response = [
            'status' => 'success',
            'message' => 'A Verification email has been sent to your email address. Pls verify email to continue',
            'Verification link expiration time' => '60 minute',
            'user' => $user->only(
                'id', 'name', 'email', 'email_verified_at', 'updated_at', 'created_at' 
            ),
            'token' => $token
        ];
        return response($response, 200);
    }

    

    public function login(Request $request) {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255',
            'password' => 'required|string|min:8',
        ]);
        if ($validator->fails())
        {
            return response([
                'status' => 'failure',
                'message' => $validator->errors()->all()], 422);
        }

        $user = User::where('email', $request->email)->first();

        if ($user) {
            if (Hash::check($request->password, $user->password)) {

                if ($user->email_verified_at == null) {
                    $response = [
                        'status' => 'success',
                        'message' => 'Your email address is not verified yet. Please contact administration at superadmin@gmail.com !',
                    ];
                    return response($response, 422);
                }

                if ($user->blocked == 1) {
                    $response = [
                        'status' => 'failure',
                        'message' => 'Unauthorized access, Pls contact administration at superadmin@gmail.com !',
                    ];
                    return response($response, 422);
                }

                $token = $user->createToken('login')->accessToken;
                $response = [
                    'status' => 'success',
                    'message' => 'You are logged successfully',
                    'user' => $user->only(
                        'id', 'name', 'email', 'about', 'profile_pic'
                    ),
                    'token' => $token
                ];
                return response($response, 200);
                
            } else {
                $response = [
                    'status' => 'failure',
                    'message' => 'Password mismatch'];
                return response($response, 422);
            }
            
        } else {
            $response = [
                'status' => 'failure',
                'message' => 'User does not exist'];
            return response($response, 422);
        }
    }

    public function logout(Request $request) {
        $token = $request->user()->token();
        $token->revoke();
        $response = [
            'status' => 'success',
            'message' => 'You are successfully logged out!'];
        return response($response, 200);
    }

    public function forgetPassword(Request $request) {
        $user = User::where('email', $request->email)->first();

        if ($user) {
            if ($user->email_verified_at == null) {
                $response = [
                    'status' => 'failure',
                    'message' => 'Your email address is not verified yet. Please contact at superadmin@gmail.com for further action.',
                ];
                return response($response, 422);
            }
            
            $credencials = request()->validate(['email' => 'required|email']);

            Password::sendResetLink($credencials);

            $response = [
                'status' => 'success',
                'message' => 'Reset password link has sent to your email Id.'];
            return response($response, 200);   
        } else {
            $response = [
                'status' => 'failure',
                'message' => 'User does not exist',
            ];
            return response($response, 422);
        }                   
    }

    /*
    public function resetPassword(Request $request) {
        $credencials = request()->validate([
            'email' => 'required|email',
            'password' => 'required|string|max:25|confirmes',
            'token' => 'required|string'
            ]
        );
        $email_password_status = Password::reset($credencials, function ($user, $password) {
            $user->password = $password;
            $user->save();
        });

        if ($email_password_status = password::INVALID_TOKEN) {
                return $this->respondBadRequest();//????????????
        }
    }
    */
}
