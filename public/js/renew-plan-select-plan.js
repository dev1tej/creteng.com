$(document).ready(function() {

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('input[name="radio_plan_name"]').click(function(){
        var choosen_plan_id = $(this).attr('id');
        $('[name="radio_pl_name"]').each(function() {
          var plan_id = $(this).attr('id');
          if (choosen_plan_id == plan_id) {
            $(this).show();
            $(this).find('input[name="plan_id"]').attr('checked', 'true');
          } else {
            /*if ($(this).find('input[name="plan_id"]').is(':checked')) {
              $(this).find('input[name="plan_id"]').attr('checked', 'false');
            }*/
            $(this).hide();
          }
        });
    });
    $('#submit').click(function(event){
      var  plan_parent_id = $('[name="plan_id"]:checked').parents('.unique').attr('id');
      var sub_plan_parent_id = $('[name="sub_plan_id"]:checked').parents('.unique').attr('id');
      if (plan_parent_id != sub_plan_parent_id) {
        document.getElementById('error').innerHTML = "** Error: Plan and Sub plan must belong to same plan group.";
        event.preventDefault();
      }
    });

    $("select[name='no_of_publication_location']").on('change', function(){
        var no_of_publication_location = $(this).val();
        //alert(no_of_publication_location);
        $.ajax({
            type: "GET",
            url: "/advertiser/renew-plan/get-publicable-location",
            success: function(data) {
                var data = JSON.parse(data);
                //console.log(data);
                var html = '';
                html += '<div class="row location"><div class="col-xs-12 col-sm-12 col-md-12"><table class="table table-bordered table-striped table-hover">';
                html += '<thead><tr><th>Serial No.</th><th><span class="text-danger font-weight-bold">*</span>Select Ad Publication Country</th><th><span class="text-danger font-weight-bold">*</span>Select Ad Publication States/Countries</th></tr></thead>';
                html += '<tbody>';
                
                for (var i = 1; i <= no_of_publication_location; i++) {
                    html += '<tr>';
                    html += '<td>' +i+ '</td>';
                    html += '<td id="country'+i+'"><select name="countries[]" class="form-control country">';
                    $.each(data['countries'], function(index, country) {
                        if (country == 'UNITED STATES') {
                            html += '<option selected value="'+country+'">'+country+'</option>';
                        } else {
                            html += '<option value="'+country+'">'+country+'</option>';
                        }
                    });
                    html += '</select></td>';
                    html += '<td id="state'+i+'"><select name="states[]" class="form-control state">';
                    $.each(data['states'], function(index, state) {
                        html += '<option selected value="'+state+'">'+state+'</option>';
                    });
                    html += '</select></td></tr>';
                }
                html += '</tbody><tfoot></tfoot>';
                html += '</table><span id="location_error" class="text-danger font-weight-bold"></span></div></div>';

                //$('.select-location').next().append(html);
                //$(html).insertAfter($('.select-location'));
                if ($('.select-location').next('div').hasClass('location')) {
                    $('.select-location').next('div').remove();
                }
                $(html).insertAfter($('.select-location'));
            },
        });
    });

    $(document).on('change', 'select[name="countries[]"]', function(event) {
        var country = this.value;
        var td_id = $(this).closest("td").attr("id");
        
        $.ajax({
          type: "POST",
          url: "/advertiser/renew-plan/country/state/list",
          data: { country : country },
      
          success: function(data) {
            var td = $(document).find("#"+td_id).next("td");
            var select = $(document).find("#"+td_id).next("td").children("select");
            
            var html = '';
            html += '<select name="states[]" class="form-control state">';
            $.each(data, function(index, state) {
              html += '<option selected value="' +state+ '">' +state+ '</option>';
            });
            html += '</select>';
  
            select.remove();
            td.append(html);
          },
          error: function(error) {
              $('#location_error').text(error);
          },
          /*complete: function(){
              $('.ajax-loader').css("visibility", "hidden");
          }*/
        });
    }); 
});