<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
//use App\Category;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
		/* To share category on all views */
		//view()->composer(['layouts.footer'], function ($view) {
		//	$category=Category::where('published',1)->get(); //Change this to the code you would use to get the notifications
		//	$view->with('category', $category);
		//});
		
        Schema::defaultStringLength(191);
    }
}
